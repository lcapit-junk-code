/*
 * fake-serial-device: A fake modem-like device and device driver implementation.
 *
 *  This driver was written just to teach me a little about the Linux's kernel
 * tty layer.
 *
 * FIXME:
 *
 * 1. User-level programs read()s lots of '\0' before the actual response
 * 2. fake_device_handler() doesn't handle the case in which the buffer
 *    become its size +1, but end-of-command wasn't issued yet
 *
 * TODO:
 *
 * 1. Documents the design
 * 2. Documents every function
 * 3. Implements a true MCR and MSR emulation support (the current
 *    implementation wasn't even tested)
 * 4. ioctl() is bogus
 * 5. Change the driver to act as a real modem
 * 6. Moves some configurable values (like debug and number of devices) as
 *    module's parameters
 * 7. Cleanup some uneeded and/or brain-damaged code (like open count at
 *    module's initialization)
 */
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/timer.h>
#include <linux/tty.h>
#include <linux/tty_driver.h>
#include <linux/tty_flip.h>
#include <linux/serial_reg.h>
#include <linux/module.h>
#include <asm/semaphore.h>

#define FAKE_DEVICE_MAJOR         188
#define FAKE_DEVICE_MINOR           0
#define FAKE_DEVICE_NR_DEVS         5
#define FAKE_DEVICE_IBUF_LEN       12
#define FAKE_DEVICE_DELAY     (HZ * 1)
#define FAKE_DEVICE_END_CMD_CHAR   '%'

struct fake_device {
	char *ibuf;
	int idx;
	size_t len;
	int open_count;
	struct tty_struct *tty;
	struct timer_list timer;
	unsigned int msr;
	unsigned int mcr;
};

#define for_each_ibuf_char(p, fake) \
	for (p = fake->ibuf; (p - fake->ibuf) < fake->idx; p++)

static struct fake_device *fake_devices[FAKE_DEVICE_NR_DEVS];
static struct tty_driver *fake_device_tty;
static int fake_device_pr_debug = 1;

#define fake_device_pr_debug(format, a...) \
	do { if (fake_device_pr_debug) \
		printk(KERN_DEBUG "%s (%d): " format, __FUNCTION__, \
				(int) current->pid, ##a); \
	} while (0)

static char *fake_device_commands[] = {
	"AT+A%",
	"AT+AB%",
	"AT+FOO%",
	NULL
};

static char *fake_device_responses[] = {
	"AT+A: aaaaaa%",
	"AT+AB: seu puto%",
	"AT+FOO: canalha!%",
	NULL
};

static char *fake_device_error_resp = "ERROR: Sua mula%";

static DECLARE_MUTEX(fake_device_mutex);

static void fill_tty_buffer(struct tty_struct *tty, const char *buf,
			    int buf_len)
{
	int room;

	room = tty_buffer_request_room(tty, buf_len);
	if (!room) {
		printk(KERN_ALERT "%s: No memory available!\n", __FUNCTION__);
		return;
	}
	tty_insert_flip_string(tty, buf, buf_len);
	tty_flip_buffer_push(tty);
}

static void fake_device_ibuf_dump(struct fake_device *fake)
{
	char *p;

	for_each_ibuf_char(p, fake)
		fake_device_pr_debug("--> (%lu) 0x%X %c\n", p - fake->ibuf,
				     *p, *p);

	fake_device_pr_debug("---> len: %lu | idx: %d\n", fake->len,
			     fake->idx);
}

static void fake_device_handler(unsigned long data)
{
	int i, found = 0;
	char *p, *resp = NULL;
	struct fake_device *fake = (struct fake_device *) data;

	fake_device_pr_debug("%s: Running\n", __FUNCTION__);

	fake_device_ibuf_dump(fake);

	for_each_ibuf_char(p, fake)
		if (*p == FAKE_DEVICE_END_CMD_CHAR) {
			found = 1;
			break;
		}

	if (!found) {
		/* No end of command has been found */
		if (!fake->len)
			goto out_reset;
		return;
	}

	/*
	 * End of command has been found
	 */

	for (i = 0; fake_device_commands[i]; i++) {
		if (!memcmp(fake->ibuf, fake_device_commands[i],
			    (p - fake->ibuf) + 1)) {
			resp = fake_device_responses[i];
			fake_device_pr_debug("--> result: %s\n", resp);
			goto out_reset;
		}
	}

	resp = fake_device_error_resp;
	fake_device_pr_debug("---> result: %s\n", resp);

 out_reset:
	if (resp)
		fill_tty_buffer(fake->tty, resp, strlen(resp));
	fake->idx = 0;
	fake->len = FAKE_DEVICE_IBUF_LEN;
}

static int fake_device_open(struct tty_struct *tty, struct file *filp)
{
	int idx;
	int ret = 0;
	struct fake_device *fake;

	down(&fake_device_mutex);

	idx = tty->index;

	fake = fake_devices[idx];
	if (!fake) {
		fake = kmalloc(sizeof(struct fake_device), GFP_KERNEL);
		if (!fake) {
			printk(KERN_ALERT "Couldn't alloc a fake_device\n");
			ret = -ENOMEM;
			goto out;
		}
		fake->idx = 0;
		fake->len = FAKE_DEVICE_IBUF_LEN;
		fake->ibuf = kmalloc(fake->len, GFP_KERNEL);
		if (!fake->ibuf) {
			printk(KERN_ALERT "Couldn't alloc ibuf\n");
			kfree(fake);
			ret = -ENOMEM;
			goto out;
		}
		fake->open_count = 0;
		init_timer(&fake->timer);
		fake->timer.data = (unsigned long) fake;
		fake->timer.function = fake_device_handler;
		fake->tty = tty;
		fake->msr = fake->mcr = 0;
		fake_devices[idx] = fake;
		tty->driver_data = fake;
	}

	fake->open_count++;
	if (fake->open_count == 1)
		fake_device_pr_debug("First time opening %d\n", idx);

	fake_device_pr_debug("Device %d opened\n", tty->index);
 out:
	up(&fake_device_mutex);
	return ret;
}

static void fake_device_close(struct tty_struct *tty, struct file *filp)
{
	int idx;
	struct fake_device *fake;

	down(&fake_device_mutex);

	idx = tty->index;
	fake = tty->driver_data;

	fake->open_count--;
	BUG_ON(fake->open_count < 0);
	if (!fake->open_count) {
		del_timer(&fake->timer);
		kfree(fake->ibuf);
		kfree(fake);
		fake_devices[idx] = NULL;
		tty->driver_data = NULL;
		fake_device_pr_debug("Device %d closed forever\n", idx);
	} else {
		fake_device_pr_debug("Device %d closed\n", idx);
	}

	up(&fake_device_mutex);
}

static int fake_device_write(struct tty_struct *tty, const unsigned char *buf,
			     int count)
{
	int ret = 0;
	struct fake_device *fake;

	fake = tty->driver_data;
	if (!fake)
		return -ENODEV;

	del_timer_sync(&fake->timer);

	fake_device_pr_debug("%d device %d bytes written\n", tty->index, count);
	memcpy(fake->ibuf + fake->idx, buf, fake->len);
	if (count > fake->len) {
		ret = fake->len;
		fake->idx += fake->len;
		fake->len = 0;
	} else {
		ret = count;
		fake->idx += count;
		fake->len -= count;
	}

	fake->timer.expires = jiffies + FAKE_DEVICE_DELAY;
	add_timer(&fake->timer);

	return ret;
}

static int fake_device_write_room(struct tty_struct *tty)
{
	struct fake_device *fake;

	fake = tty->driver_data;
	if (!fake)
		return -ENODEV;

	return fake->len;
}

static int fake_device_ioctl(struct tty_struct *tty, struct file *file,
			     unsigned int cmd, unsigned long arg)
{
	return -ENOIOCTLCMD;
}

static void fake_device_set_termios(struct tty_struct *tty, struct termios *old)
{
	fake_device_pr_debug("");
	return;
}

static void fake_device_throttle(struct tty_struct *tty)
{
	struct fake_device *fake;

	fake = tty->driver_data;
	if (!fake)
		return;

	printk(KERN_ALERT "%s: called for minor %d\n", __FUNCTION__,
	       tty->index);
	return;
}

static void fake_device_unthrottle(struct tty_struct *tty)
{
	struct fake_device *fake;

	fake = tty->driver_data;
	if (!fake)
		return;

	printk(KERN_ALERT "%s: called for minor %d\n", __FUNCTION__,
	       tty->index);
	return;
}

static void fake_device_break_ctl(struct tty_struct *tty, int break_state)
{
	struct fake_device *fake;

	fake = tty->driver_data;
	if (!fake)
		return;

	printk(KERN_ALERT "%s: sending BREAK to device %d\n", __FUNCTION__,
	       tty->index);
	return;
}

static int fake_device_chars_in_buffer(struct tty_struct *tty)
{
	/*
	 * That's our bogus implementation, we never have queued data
	 */
	return 0;
}

static int fake_device_tiocmget(struct tty_struct *tty, struct file *file)
{
	unsigned int msr, mcr;
	unsigned int result = 0;
	struct fake_device *fake;

	fake = tty->driver_data;
	if (!fake)
		return -ENODEV;

	msr = fake->msr;
	mcr = fake->mcr;

	result = ((mcr & UART_MCR_DTR)  ?  TIOCM_DTR  : 0) | /* DTR is set */
		((mcr & UART_MCR_RTS)  ?  TIOCM_RTS  : 0) | /* RTS is set */
		((mcr & UART_MCR_LOOP) ?  TIOCM_LOOP : 0) | /* LOOP is set */
		((msr & UART_MSR_CTS)  ?  TIOCM_CTS  : 0) | /* CTS is set */
		((msr & UART_MSR_DCD)  ?  TIOCM_CAR  : 0) | /* Carrier detect is set*/
		((msr & UART_MSR_RI)   ?  TIOCM_RI   : 0) | /* Ring Indicator is set */
		((msr & UART_MSR_DSR)  ?  TIOCM_DSR  : 0);  /* DSR is set */

	return result;
}

static int fake_device_tiocmset(struct tty_struct *tty, struct file *file,
				unsigned int set, unsigned int clear)
{
	unsigned int mcr;
	struct fake_device *fake;

	fake = tty->driver_data;
	if (!fake)
		return -ENODEV;

	mcr = fake->mcr;

	if (set & TIOCM_RTS)
		mcr |= UART_MCR_RTS;
	if (set & TIOCM_DTR)
		mcr |= UART_MCR_RTS;

	if (clear & TIOCM_RTS)
		mcr &= ~UART_MCR_RTS;
	if (clear & TIOCM_DTR)
		mcr &= ~UART_MCR_RTS;

	fake->mcr = mcr;

	return 0;
}

static struct tty_operations fake_device_ops = {
	.open            = fake_device_open,
	.close           = fake_device_close,
	.write           = fake_device_write,
	.write_room      = fake_device_write_room,
	.ioctl           = fake_device_ioctl,
	.set_termios     = fake_device_set_termios,
	.throttle        = fake_device_throttle,
	.unthrottle      = fake_device_unthrottle,
	.break_ctl       = fake_device_break_ctl,
	.chars_in_buffer = fake_device_chars_in_buffer,
	.tiocmget        = fake_device_tiocmget,
	.tiocmset        = fake_device_tiocmset,
};

static int __init fake_device_init(void)
{
	int i, err;

	fake_device_pr_debug("Module is loading...\n");

	fake_device_tty = alloc_tty_driver(FAKE_DEVICE_NR_DEVS);
	if (!fake_device_tty)
		return -ENOMEM;

	fake_device_tty->owner = THIS_MODULE;
	fake_device_tty->driver_name = "fake_device";
	fake_device_tty->name = "ttyFAKEDEVICE";
	fake_device_tty->major = FAKE_DEVICE_MAJOR;
	fake_device_tty->minor_start = 0;
	fake_device_tty->type = TTY_DRIVER_TYPE_SERIAL;
	fake_device_tty->subtype = SERIAL_TYPE_NORMAL;
	fake_device_tty->flags = TTY_DRIVER_REAL_RAW | TTY_DRIVER_NO_DEVFS;
	fake_device_tty->init_termios = tty_std_termios;
	fake_device_tty->init_termios.c_cflag = B9600 | CS8 | CREAD | HUPCL | CLOCAL;
	tty_set_operations(fake_device_tty, &fake_device_ops);

	err = tty_register_driver(fake_device_tty);
	if (err) {
		printk(KERN_ALERT "Register failed\n");
		put_tty_driver(fake_device_tty);
		return err;
	}

	for (i = 0; i < FAKE_DEVICE_NR_DEVS; i++) {
		fake_devices[i] = NULL;
		tty_register_device(fake_device_tty, i, NULL);
	}

	fake_device_pr_debug("Loaded!!\n");
	return 0;
}

static void __exit fake_device_exit(void)
{
	int i;

	for (i = 0; i < FAKE_DEVICE_NR_DEVS; i++)
		tty_unregister_device(fake_device_tty, i);
	tty_unregister_driver(fake_device_tty);
	put_tty_driver(fake_device_tty);
	fake_device_pr_debug("Exiting...\n");
}

module_init(fake_device_init);
module_exit(fake_device_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Luiz Capitulino <lcapitulino@gmail.com>");
MODULE_DESCRIPTION("A fake serial device driver implementation");
