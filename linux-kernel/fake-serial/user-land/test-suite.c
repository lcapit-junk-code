#include <stdio.h>
#include <check.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>

#define FAKE_TESTS_BUF_MAX     64
#define FAKE_TESTS_DEF_TIMEOUT 10
#define FAKE_TESTS_DFL_PORT    "/dev/ttyFAKEDEVICE0"

#define NOT_USED(x)  (x = x)

static int global_fd;
static struct termios original;
static long fake_devices_number;

static int read_cmd_response(int fd, char *buf, size_t buf_len)
{
	char *p;
	size_t count;
	ssize_t bytes;

	p = buf;
	count = buf_len;
	memset(buf, 0, count);

	while ((unsigned long) (p - buf) < buf_len && !strchr(buf, '%')) {
		bytes = read(fd, p, count);
		if (bytes < 0)
			return -1;

		p += bytes;
		count -= bytes;
	}

	return 0;
}

START_TEST(test_created_devs)
{
	int fd;
	long i;
	char port[PATH_MAX+1];

	for (i = 0; i < fake_devices_number; i++) {
		memset(port, 0, sizeof(port));
		sprintf(port, "/dev/ttyFAKEDEVICE%lu", i);
		fd = open(port, O_RDWR);
		fail_unless(fd > 0, "open() %lu failed: %s", i,
				strerror(errno));
		close(fd);
	}
}
END_TEST

static void setup(void) {
	int fd, ret;
	struct termios tio;

	fd = open(FAKE_TESTS_DFL_PORT, O_RDWR | O_NOCTTY);
	fail_unless(fd > 0, "open(): %s\n", strerror(errno));

	ret = tcgetattr(fd, &original);
	fail_unless(ret == 0, "tcgetattr(): %s\b", strerror(errno));

	memset(&tio, 0, sizeof(struct termios));
	tio.c_cflag = CS8 | CREAD | CRTSCTS | CLOCAL | HUPCL;
	tio.c_iflag = IGNPAR | IGNBRK;
	tio.c_oflag = 0;
	tio.c_lflag = 0;
	tio.c_cc[VTIME] = 1;
	tio.c_cc[VMIN] = 0;

	/* If it fails, we will use the default settings */
	cfsetispeed(&tio, B115200);
	cfsetospeed(&tio, B115200);

	ret = tcflush(fd, TCIFLUSH);
	fail_unless(ret == 0, "tcflush(): %s\b", strerror(errno));

	ret = tcsetattr(fd, TCSANOW, &tio);
	fail_unless(ret == 0, "tcsetattr(): %s\b", strerror(errno));

	global_fd = fd;
}

static void teardown(void)
{
	tcsetattr(global_fd, TCSANOW, &original);
	close(global_fd);
	global_fd = -1;
}

START_TEST(test_wrong_cmd)
{
	int ret;
	ssize_t bytes;
	char buf[FAKE_TESTS_BUF_MAX];
	const char wcmd[] = "AAAA%";
	const size_t wcmd_len = sizeof(wcmd);
	const char *error_cmd = "ERROR: Sua mula%";

	bytes = write(global_fd, wcmd, wcmd_len);
	fail_unless((unsigned long) bytes == wcmd_len, "Written %d\n", (int) bytes);

	ret = read_cmd_response(global_fd, buf, sizeof(buf));
	fail_unless(ret == 0, "read_cmd_response(): %s\n", strerror(errno));
	fail_unless(!strcmp(buf, error_cmd), "Got: %s\n", buf);
}
END_TEST

START_TEST(test_a_cmd)
{
	int ret;
	ssize_t bytes;
	char buf[FAKE_TESTS_BUF_MAX];
	const char wcmd[] = "AT+A%";
	const size_t wcmd_len = sizeof(wcmd);
	const char *resp = "AT+A: aaaaaa%";

	bytes = write(global_fd, wcmd, wcmd_len);
	fail_unless((unsigned long) bytes == wcmd_len, "Written %d\n", (int) bytes);

	ret = read_cmd_response(global_fd, buf, sizeof(buf));
	fail_unless(ret == 0, "read_cmd_response(): %s\n", strerror(errno));
	fail_unless(!strcmp(buf, resp), "Got: %s\n", buf);
}
END_TEST

START_TEST(test_ab_cmd)
{
	int ret;
	ssize_t bytes;
	char buf[FAKE_TESTS_BUF_MAX];
	const char wcmd[] = "AT+AB%";
	const size_t wcmd_len = sizeof(wcmd);
	const char *resp = "AT+AB: seu puto%";

	bytes = write(global_fd, wcmd, wcmd_len);
	fail_unless((unsigned long) bytes == wcmd_len, "Written %d\n", (int) bytes);

	ret = read_cmd_response(global_fd, buf, sizeof(buf));
	fail_unless(ret == 0, "read_cmd_response(): %s\n", strerror(errno));
	fail_unless(!strcmp(buf, resp), "Got: %s\n", buf);
}
END_TEST

START_TEST(test_foo_cmd)
{
	int ret;
	ssize_t bytes;
	char buf[FAKE_TESTS_BUF_MAX];
	const char wcmd[] = "AT+FOO%";
	const size_t wcmd_len = sizeof(wcmd);
	const char *resp = "AT+FOO: canalha!%";

	bytes = write(global_fd, wcmd, wcmd_len);
	fail_unless((unsigned long) bytes == wcmd_len, "Written %d\n", (int) bytes);

	ret = read_cmd_response(global_fd, buf, sizeof(buf));
	fail_unless(ret == 0, "read_cmd_response(): %s\n", strerror(errno));
	fail_unless(!strcmp(buf, resp), "Got: %s\n", buf);
}
END_TEST

static Suite *fake_device_suite(void)
{
	Suite *s = suite_create("Fake device basic");
	TCase *tc_basic = tcase_create("Basic");
	TCase *tc_func  = tcase_create("Functionality");

	suite_add_tcase(s, tc_basic);
	tcase_add_test(tc_basic, test_created_devs);

	suite_add_tcase(s, tc_func);
	tcase_add_test(tc_func, test_wrong_cmd);
	tcase_add_test(tc_func, test_a_cmd);
	tcase_add_test(tc_func, test_ab_cmd);
	tcase_add_test(tc_func, test_foo_cmd);
	tcase_add_checked_fixture(tc_func, setup, teardown);
	tcase_set_timeout(tc_func, FAKE_TESTS_DEF_TIMEOUT);

	return s;
}

int main(int argc, char *argv[])
{
	int nf;
	Suite *s;
	SRunner *sr;

	NOT_USED(argc);

	if (!argv[1]) {
		fprintf(stderr, "You most specify the number of devices\n");
		exit(1);
	}

	fake_devices_number = strtol(argv[1], NULL, 10);
	if (fake_devices_number == LONG_MIN ||
			fake_devices_number == LONG_MAX) {
		perror("strtol()");
		exit(1);
	}

	s = fake_device_suite();
	sr = srunner_create(s);
	srunner_run_all(sr, CK_NORMAL);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (nf == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
