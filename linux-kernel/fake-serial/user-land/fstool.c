#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define NOT_USED(x)  (x = x)

struct termios original;

/**
 * close_serial_port - Close a serial port
 *
 * @param: the port to be closed
 *
 * Close the specified serial port and sets the
 * its termios structure to the default settings.
 */
static void close_serial_port(int fd)
{
	tcsetattr(fd, TCSANOW, &original);
	close(fd);
}

/**
 * open_serial_port - Open a serial port
 *
 * @param port: the port to be opened
 *
 * @return On success a file descriptor,
 * -1 otherwise
 */
static int open_serial_port(const char *port)
{
	int fd, ret;
	struct termios tio;

	if (!port) {
		errno = EINVAL;
		return -1;
	}

	fd = open(port, O_RDWR | O_NOCTTY);
	if (fd < 0)
		goto out;

	ret = tcgetattr(fd, &original);
	if (ret < 0)
		goto err_out;

	memset(&tio, 0, sizeof(struct termios));
	tio.c_cflag = CS8 | CREAD | CRTSCTS | CLOCAL | HUPCL;
	tio.c_iflag = IGNPAR | IGNBRK;
	tio.c_oflag = 0;
	tio.c_lflag = 0;
	tio.c_cc[VTIME] = 1;
	tio.c_cc[VMIN] = 0;

	/* If it fails, we will use default settings. */
	cfsetispeed(&tio, B115200);
	cfsetospeed(&tio, B115200);

	ret = tcflush(fd, TCIFLUSH);
	if (ret < 0)
		goto err_out;

	ret = tcsetattr(fd, TCSANOW, &tio);
	if (ret)
		goto err_out;

 out:
	return fd;
 err_out:
	close(fd);
	fd = -1;
	goto out;
}

int main(int argc, char *argv[])
{
	int i, fd;
	size_t count;
	ssize_t bytes;
	char *cmd, *p, buf[128];

	NOT_USED(argc);

	cmd = argv[2];
	if (!cmd) {
		fprintf(stderr, "No cmd.\n");
		exit(1);
	}

	fd = open_serial_port(argv[1]);
	if (fd < -1) {
		perror("open()");
		exit(1);
	}

	for (i = 0; i < (int) strlen(cmd); i++) {

		bytes = write(fd, &cmd[i], (size_t) 1);
		if (bytes < 0) {
			perror("write()");
			exit(1);
		} else if (bytes != 1) {
			fprintf(stderr, "Warning wrote %d instead of 1\n", bytes);
		}
	}

	p = buf;
	count = sizeof(buf);
	memset(p, '\0', count);
	bytes = 1;

	while ((p - buf) < (unsigned int) sizeof(buf) && !strchr(buf, '%')) {
		bytes = read(fd, p, count);
		if (bytes < 0) {
			perror("read()");
			exit(1);
		}

		p += bytes;
		count -= bytes;
	}
	close_serial_port(fd);

	printf("%s\n", buf);
	return 0;
}
