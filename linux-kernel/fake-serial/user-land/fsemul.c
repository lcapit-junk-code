#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <readline/readline.h>
#include <readline/history.h>

#define ULCMD_PROMPT "ulcmd>>> "
#define FAKE_DEV_IBUFF_LEN 12
#define END_CMD_CHAR '%'

struct fake_device {
	char *ibuf;
	char *lresult;
	int idx;
	size_t len;
};

static char *responses[] = {
	"AT+A: hehehe\r",
	"AT+AB: muuu\r",
	"AT+FOO: bar\n",
	NULL
};

static char *commands[] = {
	"AT+A%",
	"AT+AB%",
	"AT+FOO%",
	NULL
};

static char *command_error = "ERROR: sua mula\r";

static void fake_device_rst_counters(struct fake_device *fake, int print_msg)
{
	if (print_msg)
		printf("-> Resetting counters\n");
	fake->idx = 0;
	fake->len = FAKE_DEV_IBUFF_LEN;
}

static int fake_device_init(struct fake_device *fake, int print_msg)
{
	fake_device_rst_counters(fake, print_msg);
	fake->lresult = NULL;
	fake->ibuf = malloc(fake->len);
	if (!fake->ibuf)
		return -1;
	return 0;
}

static void fake_device_del(struct fake_device *fake)
{
	free(fake->ibuf);
}

static void fake_device_ibuf_dump(struct fake_device *fake, FILE *stream)
{
	char *p;

	if (!fake->idx) {
		fprintf(stream, "-> ibuf dump: Nothing to dump.\n");
		return;
	}

	fprintf(stream, "-> ibuf dump:\n");

	for (p = fake->ibuf; (p - fake->ibuf) < fake->idx; p++)
		fprintf(stream, "--> (%ld) 0x%x %c\n", p - fake->ibuf, *p, *p);

	fprintf(stream, "--> idx: %d | len: %d\n", fake->idx, (int) fake->len);
}

static int fake_device_handler(struct fake_device *fake)
{
	char *p;
	int i, found = 0;

	for (p = fake->ibuf; (p - fake->ibuf) < fake->idx; p++)
		if (*p == END_CMD_CHAR) {
			found = 1;
			break;
		}

	if (!found) {
		if (!fake->len)
			fake_device_rst_counters(fake, 1);
		return 0;
	}

	/*
	 * End of a command has been found
	 */

	for (i = 0; commands[i]; i++)
		if (!strncmp(fake->ibuf, commands[i],
					(size_t) (p - fake->ibuf) + 1)) {
			fake->lresult = responses[i];
			goto out;
		}

	/* No valid command found */
	fake->lresult = command_error;
out:
	fake_device_rst_counters(fake, 1);
	return 1;
}

int main(void)
{
	int ret;
	char *line;
	size_t line_len;
	struct fake_device fake;

	fake_device_init(&fake, 0);

	for (;;) {
		line = readline(ULCMD_PROMPT);
		if (!line) {
			printf("\nExiting...\n");
			return 0;
		}
		printf("-> Line read is: %s\n", line);

		line_len = strlen(line);
		strncpy(fake.ibuf + fake.idx, line, fake.len);
		if (line_len > fake.len) {
			fake.idx += fake.len;
			fake.len = 0;
		} else {
			fake.idx += line_len;
			fake.len -= line_len;
		}

		fake_device_ibuf_dump(&fake, stdout);
		ret = fake_device_handler(&fake);
		if (ret) {
			assert(fake.lresult != NULL);
			printf("-> Command response: %s\n", fake.lresult);
			fake.lresult = NULL;
		}
	}

	fake_device_del(&fake);
	return 0;
}
