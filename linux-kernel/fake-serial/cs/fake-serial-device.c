/*
 * TODO:
 *       1. Missing methods
 *       2. Cleanups
 *       3. Documentations
 */
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/timer.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>
#include <linux/serial_core.h>

#define FAKE_DEVICE_MAJOR         188
#define FAKE_DEVICE_MINOR           0
#define FAKE_DEVICE_NR_DEVS         5
#define FAKE_DEVICE_IBUF_LEN       12
#define FAKE_DEVICE_DELAY     (HZ * 1)
#define FAKE_DEVICE_END_CMD_CHAR   '%'

struct fake_device {
	struct uart_port port;
	struct timer_list timer;
	char *ibuf;
	int idx;
	size_t len;
};

static struct fake_device fake_devices[FAKE_DEVICE_NR_DEVS];

#define for_each_ibuf_char(p, fake) \
	for (p = fake->ibuf; (p - fake->ibuf) < fake->idx; p++)

static int fake_device_pr_debug = 1;

#define fake_device_pr_debug(format, a...) \
	do { if (fake_device_pr_debug) \
		printk(KERN_DEBUG "%s (%d): " format, __FUNCTION__, \
				(int) current->pid, ##a); \
	} while (0)

static char *fake_device_commands[] = {
	"AT+A%",
	"AT+AB%",
	"AT+FOO%",
	NULL
};

static char *fake_device_responses[] = {
	"AT+A: aaaaaa%",
	"AT+AB: seu puto%",
	"AT+FOO: canalha!%",
	NULL
};

static char *fake_device_error_resp = "ERROR: Sua mula%";

static int fill_tty_buffer(struct tty_struct *tty, const char *buf,
			   int buf_len)
{
	int room;

	room = tty_buffer_request_room(tty, buf_len);
	if (!room) {
		printk(KERN_ALERT "%s: No memory available!\n", __FUNCTION__);
		return 0;
	}

	tty_insert_flip_string(tty, buf, room);
	tty_flip_buffer_push(tty);

	fake_device_pr_debug("Pushed %s using %p\n", buf, tty);
	return room;
}

static void fake_device_ibuf_dump(struct fake_device *fake)
{
	char *p;

	for_each_ibuf_char(p, fake)
		fake_device_pr_debug("--> (%lu) 0x%X %c\n", p - fake->ibuf,
				     *p, *p);

	fake_device_pr_debug("---> len: %lu | idx: %d\n", fake->len,
			     fake->idx);
}

static void fake_device_handler(unsigned long data)
{
	int i, found = 0;
	char *p, *resp = NULL;
	struct fake_device *fake = (struct fake_device *) data;

	fake_device_pr_debug("Handler is running\n");

	fake_device_ibuf_dump(fake);

	for_each_ibuf_char(p, fake)
		if (*p == FAKE_DEVICE_END_CMD_CHAR) {
			found = 1;
			break;
		}

	if (!found) {
		/* No end of command has been found */
		if (!fake->len)
			goto out_reset;
		return;
	}

	/*
	 * End of command has been found
	 */

	for (i = 0; fake_device_commands[i]; i++) {
		if (!memcmp(fake->ibuf, fake_device_commands[i],
			    (p - fake->ibuf) + 1)) {
			resp = fake_device_responses[i];
			fake_device_pr_debug("--> result: %s\n", resp);
			goto out_reset;
		}
	}

	resp = fake_device_error_resp;
	fake_device_pr_debug("---> result: %s\n", resp);

 out_reset:
	if (resp) {
		int bytes;

		bytes = fill_tty_buffer(fake->port.info->tty, resp,
					strlen(resp));
		fake->port.icount.rx += bytes;
	}
	fake->idx = 0;
	fake->len = FAKE_DEVICE_IBUF_LEN;
}

static unsigned int fake_device_tx_empty(struct uart_port *port)
{
	fake_device_pr_debug("%d\n", port->line);
	return TIOCSER_TEMT;
}

static void fake_device_set_mctrl(struct uart_port *port, unsigned int mctrl)
{
	fake_device_pr_debug("%d\n", port->line);
}

static unsigned int fake_device_get_mctrl(struct uart_port *port)
{
	fake_device_pr_debug("%d\n", port->line);

	/*
	 * XXX: If CTS is not set, the serial_core will set
	 * 'tty->hw_stopped' and write()s from used user-space will not
	 * reach our driver.
	 */
	return (0 | TIOCM_CTS);
}

static void fake_device_stop_tx(struct uart_port *port)
{
	fake_device_pr_debug("%d\n", port->line);
}

static void fake_device_start_tx(struct uart_port *port)
{
	struct fake_device *fake = (struct fake_device *) port;
	struct circ_buf *xmit = &port->info->xmit;
	int count;

	fake_device_pr_debug("%d\n", port->line);

	del_timer_sync(&fake->timer);

	if (port->x_char) {
		fake_device_pr_debug("XCHAR: %c\n", port->x_char);
		port->icount.tx++;
		port->x_char = 0;
		return;
	}

	if (!fake->len)
		goto out_sched;

	if (uart_tx_stopped(port)) {
		fake_device_stop_tx(port);
		return;
	}

	if (uart_circ_empty(xmit))
		return;

	count = port->fifosize;
	do {
		fake->ibuf[fake->idx] = xmit->buf[xmit->tail];
		xmit->tail = (xmit->tail + 1) & (UART_XMIT_SIZE - 1);
		fake->idx++;
		fake->len--;
		port->icount.tx++;
		if (uart_circ_empty(xmit))
			break;
	} while (--count > 0 && fake->len > 0);

	if (uart_circ_chars_pending(xmit) < WAKEUP_CHARS)
		uart_write_wakeup(port);

	/* XXX: Why? */
	if (uart_circ_empty(xmit))
		fake_device_stop_tx(port);

 out_sched:
	fake->timer.expires = jiffies + FAKE_DEVICE_DELAY;
	add_timer(&fake->timer);
}

static void fake_device_send_xchar(struct uart_port *port, char ch)
{
	fake_device_pr_debug("%d\n", port->line);
}

static void fake_device_stop_rx(struct uart_port *port)
{
	struct fake_device *fake = (struct fake_device *) port;

	fake_device_pr_debug("%d\n", port->line);

	del_timer_sync(&fake->timer);
}

static void fake_device_enable_ms(struct uart_port *port)
{
	fake_device_pr_debug("%d\n", port->line);
}

static void fake_device_break(struct uart_port *port, int break_state)
{
	fake_device_pr_debug("%d\n", port->line);
}

static int fake_device_startup(struct uart_port *port)
{
	int idx = port->line;
	struct fake_device *fake;

	fake_device_pr_debug("%d\n", idx);

	fake = (struct fake_device *) port;
	fake->idx = 0;
	fake->len = FAKE_DEVICE_IBUF_LEN;
	fake->ibuf = kmalloc(fake->len, GFP_KERNEL);
	if (!fake->ibuf) {
		printk(KERN_ALERT "Couldn't alloc ibuf\n");
		kfree(fake);
		return -ENOMEM;
	}
	init_timer(&fake->timer);
	fake->timer.data = (unsigned long) fake;
	fake->timer.function = fake_device_handler;
	fake_device_pr_debug("-> Initialized fake %d\n", idx);

	return 0;
}

static void fake_device_shutdown(struct uart_port *port)
{
	int idx = port->line;
	struct fake_device *fake;

	fake_device_pr_debug("%d\n", idx);

	fake = (struct fake_device *) port;
	BUG_ON(!fake);
	del_timer(&fake->timer);
	kfree(fake->ibuf);
	fake->ibuf = NULL;
	fake_device_pr_debug("-> Freed fake for %d\n", idx);
}

static void fake_device_set_termios(struct uart_port *port,
				    struct termios *termios,
				    struct termios *old_termios)
{
	fake_device_pr_debug("%d\n", port->line);
}

static const char *fake_device_type(struct uart_port *port)
{
	fake_device_pr_debug("%d\n", port->line);
	return "fake_serial_device";
}

static void fake_device_release_port(struct uart_port *port)
{
	fake_device_pr_debug("%d\n", port->line);
}

static int fake_device_request_port(struct uart_port *port)
{
	fake_device_pr_debug("%d\n", port->line);
	return 0;
}

static void fake_device_config_port(struct uart_port *port, int flags)
{
	fake_device_pr_debug("%d\n", port->line);
}

static struct uart_ops fake_device_ops = {
	.tx_empty     = fake_device_tx_empty,
	.set_mctrl    = fake_device_set_mctrl,
	.get_mctrl    = fake_device_get_mctrl,
	.stop_tx      = fake_device_stop_tx,
	.start_tx     = fake_device_start_tx,
	.send_xchar   = fake_device_send_xchar,
	.stop_rx      = fake_device_stop_rx,
	.enable_ms    = fake_device_enable_ms,
	.break_ctl    = fake_device_break,
	.startup      = fake_device_startup,
	.shutdown     = fake_device_shutdown,
	.set_termios  = fake_device_set_termios,
	.type         = fake_device_type,
	.release_port = fake_device_release_port,
	.request_port = fake_device_request_port,
	.config_port  = fake_device_config_port,
};

static struct uart_driver fake_uart_driver = {
	.owner       = THIS_MODULE,
	.driver_name = "fake_serial_driver",
	.dev_name    = "ttyFAKEDEVICE",
	.major       = FAKE_DEVICE_MAJOR,
	.minor       = FAKE_DEVICE_MINOR,
	.nr          = FAKE_DEVICE_NR_DEVS,
	.cons        = NULL,
};

static int __init fake_device_init(void)
{
	int i, err;

	err = uart_register_driver(&fake_uart_driver);
	if (err)
		return err;

	for (i = 0; i < FAKE_DEVICE_NR_DEVS; i++) {
		fake_devices[i].port.ops      = &fake_device_ops;
		fake_devices[i].port.dev      = NULL;
		fake_devices[i].port.iotype   = UPIO_PORT;
		fake_devices[i].port.membase  = NULL;
		fake_devices[i].port.type     = PORT_16550;
		fake_devices[i].port.fifosize = 16;
		fake_devices[i].port.flags    = UPF_LOW_LATENCY;
		fake_devices[i].port.line     = i;
		err = uart_add_one_port(&fake_uart_driver, &fake_devices[i].port);
		if (err) {
			printk(KERN_ERR "uart_add_one_port() failed!\n");
			goto failed;
		}
	}

	fake_device_pr_debug("Loaded!\n");
	return 0;

 failed:
	/* FIXME: ports must be removed! */
	uart_unregister_driver(&fake_uart_driver);
	return err;
}

static void __exit fake_device_exit(void)
{
	int i;

	for (i = 0; i < FAKE_DEVICE_NR_DEVS; i++)
		uart_remove_one_port(&fake_uart_driver, &fake_devices[i].port);

	uart_unregister_driver(&fake_uart_driver);

	fake_device_pr_debug("Exiting..\n");
}

module_init(fake_device_init);
module_exit(fake_device_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Luiz Capitulino <lcapitulino@gmail.com>");
MODULE_DESCRIPTION("A fake serial device driver implementation");
