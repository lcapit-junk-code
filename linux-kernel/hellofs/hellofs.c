#include <linux/init.h>
#include <linux/fs.h>
#include <linux/vfs.h>
#include <linux/module.h>
#include <linux/binfmts.h>
#include <asm/uaccess.h>

#define HELLOFS_MAGIC	0x64626724

static ssize_t
hellofs_read(struct file *file, char __user *buf, size_t nbytes, loff_t *ppos)
{
	char *s = "Hello, world!\n";
	int len = strlen(s);
	loff_t pos = *ppos;

	if (pos < 0)
		return -EINVAL;
	if (pos >= len)
		return 0;
	if (len < pos + nbytes)
		nbytes = len - pos;
	if (copy_to_user(buf, s + pos, nbytes))
		return -EFAULT;
	*ppos = pos + nbytes;

	printk("%s: File read!\n", __FUNCTION__);
	return nbytes;
}

static const struct file_operations hellofs_operations = {
	.read		= hellofs_read,
};

static int hellofs_fill_super(struct super_block *sb, void *data, int silent)
{
	int err;
	static struct tree_descr hellofs_files[] = {
		[1] = {"hello", &hellofs_operations, S_IWUSR|S_IRUGO},
		{""}
	};

	err = simple_fill_super(sb, HELLOFS_MAGIC, hellofs_files);
	if (err) {
		printk("%s: Can't fill super block\n", __FUNCTION__);
		return err;
	}

	return 0;
}

static int hellofs_get_sb(struct file_system_type *fs_type,
	int flags, const char *dev_name, void *data, struct vfsmount *mnt)
{
	return get_sb_single(fs_type, flags, data, hellofs_fill_super, mnt);
}

static struct file_system_type hellofs_fs_type = {
	.owner		= THIS_MODULE,
	.name		= "hellofs",
	.get_sb		= hellofs_get_sb,
	.kill_sb	= kill_litter_super,
};

static int __init hellofs_init(void)
{
	printk("%s: Loading!\n", __FUNCTION__);
	return register_filesystem(&hellofs_fs_type);
}

static void __exit hellofs_exit(void)
{
	printk("%s: Unloading!\n", __FUNCTION__);
	unregister_filesystem(&hellofs_fs_type);
}

module_init(hellofs_init);
module_exit(hellofs_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Luiz Capitulino <lcapitulino@gmail.com>");
