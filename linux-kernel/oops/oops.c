/*
 * OOPses the kernel.
 *
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <linux/init.h>
#include <linux/module.h>

static int oops_init(void)
{
	printk("Goodbye cruel world!!\n");
	*((int *)0) = 666;

	return 0;
}

module_init(oops_init);

MODULE_LICENSE("GPL");
