/*
 * Linux' Red Black tree usage example.
 * 
 * This code define a struct called my_value, which holds an integer.
 * 
 * At initialization time, integers from 0 to NODE_MAX are inserted in
 * a red black tree and some simple testing is done.
 * 
 * At exit time each node's content is printed and the tree is
 * destroyed.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmal.com>
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/rbtree.h>

struct my_value {
	struct rb_node rb_node;
	int num;
};

#define NODE_MAX 15

/* root of the tree */
static struct rb_root value_root = RB_ROOT;

/* insert data into the tree */
static int __init value_insert(struct rb_root *root, struct my_value *data)
{
	struct rb_node **p = &root->rb_node;
	struct rb_node *parent = NULL;
	struct my_value *__value;

	while (*p) {
		parent = *p;
		__value = rb_entry(parent, struct my_value, rb_node);

		if (data->num < __value->num)
			p = &(*p)->rb_left;
		else if (data->num > __value->num)
			p = &(*p)->rb_right;
		else
			return -1;
	}

	rb_link_node(&data->rb_node, parent, p);
	rb_insert_color(&data->rb_node, root);

	return 0;
}

/* search for num into the tree */
static struct my_value __init *value_find(struct rb_root *root, int num)
{
	struct rb_node *n = root->rb_node;
	struct my_value *value;

	while (n) {
		value = rb_entry(n, struct my_value, rb_node);

		if (num < value->num)
			n = n->rb_left;
		else if (num > value->num)
			n = n->rb_right;
		else
			return value;
	}

	return NULL;
}

/* print all elements of the tree */
static void __init value_show_all(void)
{
	struct rb_node *node;
	struct my_value *value;

	printk("%s: showing elements\n\n\t", __FUNCTION__);

	for (node = rb_first(&value_root); node; node = rb_next(node)) {
		value = rb_entry(node, struct my_value, rb_node);
		printk("%d ", value->num);
	}

	printk("\n\n");
}

/* remove all elements of the tree
 * 
 * XXX: I don't know how to transverse the tree using post-order, that
 * should be faster than this...
 */
static void __init value_remove_all(void)
{
	struct rb_node *node;
	struct my_value *value;

	for (node = rb_first(&value_root); node; node = rb_next(node)) {
		value = rb_entry(node, struct my_value, rb_node);
		rb_erase(&value->rb_node, &value_root);
	}
}

/* run some basic tests */
static void __init do_tests(void)
{
	int failed, i;
	struct my_value *ret;

	ret = value_find(&value_root, NODE_MAX + 15);
	if (!ret)
		printk("%s: no value test PASSED\n", __FUNCTION__);
	else
		printk("%s: no value test FAILED\n", __FUNCTION__);

	printk("%s: find test running...\n", __FUNCTION__);
	failed = 0;

	for (i = 0; i < NODE_MAX; i++) {
		ret = value_find(&value_root, i);
		if (!ret) {
			printk("\tcould not find %d\n", i);
			failed = 1;
			continue;
		}

		if (ret->num != i) {
			printk("\t got %d but expected %d\n", ret->num, i);
			failed = 1;
			continue;
		}
	}

	if (failed)
		printk("%s: find test FAILED\n", __FUNCTION__);
	else
		printk("%s: find test PASSED\n", __FUNCTION__);
}

static int __init rbtree_value_init(void)
{
	int err, i;
	struct my_value *new;

	printk("\n\n%s: Inserting values\n", __FUNCTION__);

	for (i = 0; i < NODE_MAX; i++) {
		new = kzalloc(sizeof(struct my_value), GFP_KERNEL);
		if (!new) {
			printk(KERN_ERR "%s: Can not allocate memory\n",
			       __FUNCTION__);
			return -ENOMEM;
		}

		new->num = i;
		err = value_insert(&value_root, new);
		if (err) {
			printk(KERN_ERR "%s: Could not insert: %d\n",
			       __FUNCTION__, new->num);
			return -EINVAL;
		}
	}

	printk("%s: Done inserting values\n", __FUNCTION__);

	do_tests();
	value_show_all();
	value_remove_all();

	return -EINVAL; /* module is never loaded */
}

module_init(rbtree_value_init);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Luiz Capitulino <lcapitulino@gmail.com>");
