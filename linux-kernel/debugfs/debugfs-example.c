/*
 * Debugfs example module.
 * 
 * Mount debugfs with:
 * 
 * # mount -t debugfs debugfs /proc/sys/debugfs
 * 
 * After loading this module, a directory called 'debugfs-example'
 * will be created under debugfs root dir and it will have two
 * files in it.
 * 
 * One file is called 'stats_file' and it's an example of file
 * handling with debugfs. The other file is called 'statfs_u32'
 * and its an example of integer handling.
 * 
 * Both files shows the contents of the stats_counter variable.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/debugfs.h>

static struct dentry *root_dir, *dentry_stats, *dentry_u32;
static u32 stats_counter;

static int stats_open(struct inode *inode, struct file *file)
{
	++stats_counter;
	return 0;
}

#define TMPBUFSIZE 12 // random value

static ssize_t stats_read(struct file *file, char __user *buf, size_t nbytes,
			  loff_t *ppos)
{
	size_t maxlen;
	char tmpbuf[TMPBUFSIZE];

	maxlen = snprintf(tmpbuf, TMPBUFSIZE, "stats: %u\n", stats_counter);
	if (maxlen > TMPBUFSIZE)
		maxlen = TMPBUFSIZE;

	return simple_read_from_buffer(buf, nbytes, ppos, tmpbuf, maxlen);
}

static const struct file_operations stats_operations = {
	.owner   = THIS_MODULE,
	.open    = stats_open,
	.read    = stats_read,
};

static int __init debugfs_example_init(void)
{
	root_dir = debugfs_create_dir("debugfs-example", NULL);
	if (!root_dir) {
		printk(KERN_ERR "Could not create main directory\n");
		return -ENOMEM;
	}

	dentry_stats = debugfs_create_file("stats_file",
					   S_IFREG|S_IRUGO|S_IWUSR,
					   root_dir, NULL, &stats_operations);
	if (!dentry_stats) {
		printk(KERN_ERR "ERROR: could not create stats_file\n");
		debugfs_remove(root_dir);
		return -ENOMEM;
	}

	dentry_u32 = debugfs_create_u32("stats_u32",
					S_IFREG|S_IRUGO|S_IWUSR,
					root_dir, &stats_counter);
	if (!dentry_u32) {
		printk(KERN_ERR "ERROR: could not create stats_u32");
		debugfs_remove(dentry_stats);
		debugfs_remove(root_dir);
		return -ENOMEM;
	}

	return 0;
}

static void __exit debugfs_example_exit(void)
{
	debugfs_remove(dentry_stats);
	debugfs_remove(dentry_u32);
	debugfs_remove(root_dir);
}

module_init(debugfs_example_init);
module_exit(debugfs_example_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("A debugfs example module");
MODULE_AUTHOR("Luiz Capitulino <lcapitulino@gmail.com>");
