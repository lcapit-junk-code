/*
 * tk-test: send a netlink message to the kernel w/o any payload.
 * 
 * This requires the netlink-tiny-tk kernel module. It will read
 * the netlink message and print this process' ID in the kernel
 * ring buffer.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>

#define NETLINK_TEST 20 // from netlink-tiny-tk.c

int main(void)
{
	ssize_t bytes;
	int err, sock_fd;
	struct iovec iov;
	struct msghdr msg;
	struct nlmsghdr nlhdr;
	struct sockaddr_nl sa, nladdr;

	/*
	 * Create the netlink socket and bind it to a port
	 * (which is our pid).
	 * 
	 * Data structure used: struct sockaddr_nl sa
	 */

	sock_fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_TEST);
	if (sock_fd < 0) {
		perror("socket()");
		exit(1);
	}

	memset(&sa, 0, sizeof(sa));
	sa.nl_family = AF_NETLINK;
	sa.nl_pid = getpid(); // self pid

	err = bind(sock_fd, (struct sockaddr *) &sa, sizeof(sa));
	if (err) {
		perror("bind()");
		exit(1);
	}

	/*
	 * Send a netlink message to the kernel
	 * 
	 * Fill the required structures and send a netlink
	 * message to the kernel using sendmsg().
	 * 
	 * Data structures used:
	 * 
	 *   struct nlmsghdr nlhdr: netlink header
	 *   struct iovec iov: payload (?)
	 *   struct msghdr msg: required by sendmsg()
	 */

	memset(&nlhdr, 0, sizeof(nlhdr));
	nlhdr.nlmsg_len = sizeof(nlhdr);
	nlhdr.nlmsg_pid = getpid();

	iov.iov_base = (void *) &nlhdr;
	iov.iov_len  = nlhdr.nlmsg_len;

	memset(&nladdr, 0, sizeof(nladdr));
	nladdr.nl_family = AF_NETLINK;

	memset(&msg, 0, sizeof(msg));
	msg.msg_name = (void *) &(nladdr);
	msg.msg_namelen = sizeof(nladdr);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	bytes = sendmsg(sock_fd, &msg, 0);
	if (bytes < 0) {
		perror("sendmsg()");
		exit(1);
	}

	close(sock_fd);

	printf("Message sent! My pid: %d\n", getpid());

	return 0;
}
