/*
 * netlink-tiny-tk: Wait for a netlink message from user-space, when it
 * arraives print the process ID of the process which sent the message.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/mutex.h>
#include <linux/net.h>
#include <linux/skbuff.h>
#include <linux/netlink.h>
#include <net/sock.h>

#define NETLINK_TINY_TK 20   // always check whether this is safe
#define LOG_PREFIX "KNETLINK_TINY_TK: "

static struct sock *nl;
static DEFINE_MUTEX(netlink_tiny_tk_mutex);

static void netlink_tiny_tk_input(struct sk_buff *skb)
{
	struct nlmsghdr *nlh;

	mutex_lock(&netlink_tiny_tk_mutex);

	if (skb->len < sizeof(*nlh))
		return;

	nlh = nlmsg_hdr(skb);
	printk(LOG_PREFIX "message from: %d\n", nlh->nlmsg_pid);

	mutex_unlock(&netlink_tiny_tk_mutex);
}

static int __init netlink_tiny_tk_init(void)
{
	nl = netlink_kernel_create(&init_net, NETLINK_TINY_TK, 10,
				   netlink_tiny_tk_input, NULL, THIS_MODULE);
	if (!nl) {
		printk(KERN_ERR LOG_PREFIX "cannot create socket\n");
		return -EINVAL;
	}

	return 0;
}

static void __exit netlink_tiny_tk_exit(void)
{
	sock_release(nl->sk_socket);
}

module_init(netlink_tiny_tk_init);
module_exit(netlink_tiny_tk_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Luiz Capitulino <lcapitulino@gmail.com>");
MODULE_DESCRIPTION("Netlink (to kernel) example module");
