/*
 * netlink-echo: Receives a string from user-space and echoes it back.
 * It's just yet another netlink usage example.
 * 
 * FIXME: I don't whether it's ok to call netlink_unicast() from
 * netlink_echo_input().
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/string.h>
#include <linux/mutex.h>
#include <linux/net.h>
#include <linux/skbuff.h>
#include <linux/netlink.h>
#include <net/sock.h>

#define NETLINK_ECHO 20   // always check whether this is safe

static struct sock *nl;
static DEFINE_MUTEX(netlink_echo_mutex);

static void send_packet(int pid, const char *message)
{
	size_t len;
	struct sk_buff *skb;
	struct nlmsghdr *nlh;

	len = NLMSG_SPACE(strlen(message) + 1);
	skb = alloc_skb(len, GFP_ATOMIC);
	if (!skb) {
		printk(KERN_ERR "[netlink_echo]: cannot allocate memory\n");
		return;
	}

	nlh = NLMSG_PUT(skb, 0, 0, 0, len - sizeof(*nlh));
	strcpy(NLMSG_DATA(nlh), message);

	NETLINK_CB(skb).pid = 0;      /* from kernel */
	NETLINK_CB(skb).dst_group = 0;  /* unicast */
	netlink_unicast(nl, skb, pid, GFP_ATOMIC);

	return;

nlmsg_failure:
	kfree_skb(skb);
}

static void netlink_echo_input(struct sk_buff *skb)
{
	char *msg;
	struct nlmsghdr *nlh;

	mutex_lock(&netlink_echo_mutex);

	if (skb->len < sizeof(*nlh))
		return;

	nlh = nlmsg_hdr(skb);
	msg = NLMSG_DATA(nlh);
	send_packet(nlh->nlmsg_pid, msg);

	mutex_unlock(&netlink_echo_mutex);
}

static int __init netlink_echo_init(void)
{
	nl = netlink_kernel_create(&init_net, NETLINK_ECHO, 0,
				   netlink_echo_input, NULL, THIS_MODULE);
	if (!nl) {
		printk(KERN_ERR "[netlink_echo]: cannot create socket\n");
		return -EINVAL;
	}

	return 0;
}

static void __exit netlink_echo_exit(void)
{
	sock_release(nl->sk_socket);
}

module_init(netlink_echo_init);
module_exit(netlink_echo_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Luiz Capitulino <lcapitulino@gmail.com>");
MODULE_DESCRIPTION("Netlink example module");
