/*
 * echo-test: sends a netlink message to the kernel and reads it back.
 * 
 * This requires the netlink-echo kernel module. It reads a netlink
 * message (a string) from user-space and sends it back.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>

#define NETLINK_ECHO 20 // from netlink-echo.c

static void usage(void)
{
	printf("echo-test < string >\n");
}

/* create_socket(): Create a netlink socket and bind it.
 * Return a file-descriptor on success, -1 otherwise. */
static int create_socket(void)
{
	int err, fd;
	struct sockaddr_nl sa;

	fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_ECHO);
	if (fd < 0)
		return -1;

	memset(&sa, 0, sizeof(sa));
	sa.nl_family = AF_NETLINK;
	sa.nl_pid = getpid(); // self pid

	err = bind(fd, (struct sockaddr *) &sa, sizeof(sa));
	if (err)
		return -1;

	return fd;
}

/* send_message(): Send a netlink message to the kernel.
 * The argument 'message' must be a human readable string.
 * Return the amount of bytes sent on success, -1 on failure. */
static ssize_t send_message(int sock_fd, const char *message)
{
	size_t len;
	ssize_t bytes;
	struct iovec iov;
	struct msghdr msg;
	struct nlmsghdr *nlhdr;
	struct sockaddr_nl nladdr;

	len = sizeof(*nlhdr) + strlen(message) + 1;
	nlhdr = malloc(len);
	if (!nlhdr)
		return -1;

	memset(nlhdr, 0, len);
	nlhdr->nlmsg_len = len;
	nlhdr->nlmsg_pid = getpid();
	strcpy(NLMSG_DATA(nlhdr), message);

	iov.iov_base = (void *) nlhdr;
	iov.iov_len  = nlhdr->nlmsg_len;

	memset(&nladdr, 0, sizeof(nladdr));
	nladdr.nl_family = AF_NETLINK;

	memset(&msg, 0, sizeof(msg));
	msg.msg_name = (void *) &(nladdr);
	msg.msg_namelen = sizeof(nladdr);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	bytes = sendmsg(sock_fd, &msg, 0);

	free(nlhdr);
	return bytes;
}

static struct nlmsghdr *read_message(int sock_fd, size_t len)
{
	ssize_t bytes;
	struct iovec iov;
	struct msghdr msg;
	struct nlmsghdr *nlhdr;
	struct sockaddr_nl nladdr;

	len += sizeof(*nlhdr);
	nlhdr = malloc(len);
	if (!nlhdr)
		return NULL;

	memset(nlhdr, 0, len);
	nlhdr->nlmsg_len = len;
	nlhdr->nlmsg_pid = getpid();
	iov.iov_base = (void *) nlhdr;
	iov.iov_len  = nlhdr->nlmsg_len;

	memset(&nladdr, 0, sizeof(nladdr));
	nladdr.nl_family = AF_NETLINK;

	memset(&msg, 0, sizeof(msg));
	msg.msg_name = (void *) &(nladdr);
	msg.msg_namelen = sizeof(nladdr);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	bytes = recvmsg(sock_fd, &msg, 0);
	if (bytes < 0) {
		free(nlhdr);
		return NULL;
	}

	return nlhdr;
}

int main(int argc, char *argv[])
{
	int sock_fd;
	ssize_t bytes;
	struct nlmsghdr *msg;

	if (argc != 2) {
		usage();
		exit(1);
	}

	sock_fd = create_socket();
	if (sock_fd < 0) {
		perror("create_socket()");
		exit(1);
	}

	bytes = send_message(sock_fd, argv[1]);
	if (bytes < 0) {
		perror("send_message()");
		exit(1);
	}

	msg = read_message(sock_fd, strlen(argv[1]) + 1);
	if (!msg) {
		perror("read_message()");
		exit(1);
	}

	printf("%s\n", (char *) NLMSG_DATA(msg));

	free(msg);
	close(sock_fd);

	return 0;
}
