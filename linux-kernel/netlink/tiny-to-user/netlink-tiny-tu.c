/*
 * netlink-tiny-tu: Send a netlink message to user-space after
 * NETLINK_TINY_DELAY seconds the module has been loaded.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/string.h>
#include <linux/mutex.h>
#include <linux/net.h>
#include <linux/skbuff.h>
#include <linux/netlink.h>
#include <linux/timer.h>
#include <net/sock.h>

#define NETLINK_TINY_TU 20   // always check whether this is safe
#define TINY_TY_GRP      1
#define LOG_PREFIX "KNETLINK_TINY_TU: "
#define NETLINK_TINY_DELAY (HZ * 7)  // seven seconds

#define UNUSED(x)  (x = x)

static struct sock *nl;
static struct timer_list netlink_tiny_timer;

static int send_message(const char *message)
{
	size_t len;
	struct sk_buff *skb;
	struct nlmsghdr *nlh;

	len = NLMSG_SPACE(strlen(message) + 1);
	skb = alloc_skb(len, GFP_ATOMIC);
	if (!skb) {
		printk(KERN_ERR LOG_PREFIX "cannot allocate memory\n");
		return -ENOMEM;
	}

	nlh = NLMSG_PUT(skb, 0, 0, 0, len - sizeof(*nlh));
	strcpy(NLMSG_DATA(nlh), message);

	NETLINK_CB(skb).pid = 0;      /* from kernel */
	NETLINK_CB(skb).dst_group = TINY_TY_GRP;  /* group */

	netlink_broadcast(nl, skb, 0, TINY_TY_GRP, GFP_ATOMIC);

	return 0;

nlmsg_failure:
	kfree_skb(skb);
	return -EINVAL;
}

static void netlink_tiny_timer_handler(unsigned long data)
{
	int err;

	UNUSED(data);

	err = send_message("Hello, world!");
	if (err)
		printk(KERN_ERR LOG_PREFIX "could not send message\n");

	printk(LOG_PREFIX "message sent!");
}

static void __init netlink_tiny_timer_init(void)
{
	init_timer(&netlink_tiny_timer);
	netlink_tiny_timer.function = netlink_tiny_timer_handler;
	netlink_tiny_timer.expires = jiffies + NETLINK_TINY_DELAY;
	add_timer(&netlink_tiny_timer);
}

static int __init netlink_tiny_tu_init(void)
{
	nl = netlink_kernel_create(&init_net, NETLINK_TINY_TU, TINY_TY_GRP,
				   NULL, NULL, THIS_MODULE);
	if (!nl) {
		printk(KERN_ERR LOG_PREFIX "cannot create socket\n");
		return -EINVAL;
	}

	netlink_tiny_timer_init();

	return 0;
}

static void __exit netlink_tiny_tu_exit(void)
{
	sock_release(nl->sk_socket);
}

module_init(netlink_tiny_tu_init);
module_exit(netlink_tiny_tu_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Luiz Capitulino <lcapitulino@gmail.com>");
MODULE_DESCRIPTION("Netlink (to user-space) example module");
