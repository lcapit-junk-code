/*
 * tu-test: Read a netlink message from the kernel.
 * 
 * This program requires the netlink-tiny-tu kernel module. It sends a
 * netlink message to user-space.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>

#define MSG_MAX_SIZE    512
#define NETLINK_TINY_TU 20 // from netlink-tiny-tu.c

/* create_socket(): Create a netlink socket and bind it.
 * Return a file-descriptor on success, -1 otherwise. */
static int create_socket(void)
{
	int err, fd;
	struct sockaddr_nl sa;

	fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_TINY_TU);
	if (fd < 0)
		return -1;

	memset(&sa, 0, sizeof(sa));
	sa.nl_family = AF_NETLINK;
	sa.nl_pid = getpid(); // self pid
	sa.nl_groups = 1;     // group

	err = bind(fd, (struct sockaddr *) &sa, sizeof(sa));
	if (err)
		return -1;

	return fd;
}

/* read_message(): Read a netlink message from 'sock_fd'
 * and return it. */
static struct nlmsghdr *read_message(int sock_fd, size_t len)
{
	ssize_t bytes;
	struct iovec iov;
	struct msghdr msg;
	struct nlmsghdr *nlhdr;
	struct sockaddr_nl nladdr;

	len = NLMSG_SPACE(len);
	nlhdr = malloc(len);
	if (!nlhdr)
		return NULL;

	memset(nlhdr, 0, len);
	iov.iov_base = (void *) nlhdr;
	iov.iov_len  = len;

	memset(&nladdr, 0, sizeof(nladdr));

	memset(&msg, 0, sizeof(msg));
	msg.msg_name = (void *) &(nladdr);
	msg.msg_namelen = sizeof(nladdr);
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;

	bytes = recvmsg(sock_fd, &msg, 0);
	if (bytes < 0) {
		free(nlhdr);
		return NULL;
	}

	return nlhdr;
}

int main(int argc, char *argv[])
{
	int sock_fd;
	struct nlmsghdr *msg;

	sock_fd = create_socket();
	if (sock_fd < 0) {
		perror("create_socket()");
		exit(1);
	}

	msg = read_message(sock_fd, MSG_MAX_SIZE);
	if (!msg) {
		perror("read_message()");
		exit(1);
	}

	printf("%s\n", (char *) NLMSG_DATA(msg));

	free(msg);
	close(sock_fd);

	return 0;
}
