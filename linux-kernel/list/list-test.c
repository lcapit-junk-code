/*
 * Basic Linux list API usage.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/list.h>

struct foobar {
	struct list_head list;
	int num;
};

static int __init list_test_init(void)
{
	int i;
	struct foobar head, *p;
	struct list_head *pos, *q;
	const int max_itens = 10;

	/* initialize the head of the list */
	INIT_LIST_HEAD(&head.list);

	/* populate the list */
	for (i = max_itens; i >= 0; i--) {
		p = kzalloc(sizeof(*p), GFP_KERNEL);
		p->num = i;
		list_add(&p->list, &head.list);
	}

	/* transverse the list */
	printk("\n-> Itens in the list:\n");
	list_for_each(pos, &head.list) {
		p = list_entry(pos, struct foobar, list);
		printk("\t-> %d\n", p->num);
	}

	printk("\n\n");

	/* delete all list itens */
	printk("-> Removing itens:\n");
	list_for_each_safe(pos, q, &head.list) {
		p = list_entry(pos, struct foobar, list);
		printk("\t-> %d\n", p->num);
		list_del(pos);
		kfree(p);
	}

	printk("\n");

	return -EINVAL; /* prevent loading the module */
}

module_init(list_test_init);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Luiz Capitulino <lcapitulino@gmail.com>");
