<html>
 <body>
  <?php
	$nr_max_table = 10;

	function generate_results()
	{
		global $nr_max_table;

		for ($i = 1; $i <= $nr_max_table; $i++) {
			for ($j = 0; $j <= 10; $j++) {
				$res = $i * $j;
				$results[$i] .= "$i * $j = $res<br/>\n";
			}
		}

		return $results;
	}

	echo("<table border=\"1\" cellpadding=\"10\">\n");
	echo("<tr>\n");

	$results = generate_results();
	for ($i = 1; $i <= $nr_max_table; $i++) {
		echo("<td>\n");
		echo($results[$i]);
		echo("</td>\n");

		// Brakes the table in the middle
		if ($i == ($nr_max_table / 2))
			echo("</tr>\n<tr>\n");
	}

	echo("</tr>\n");
	echo("</table>\n");
  ?>
 </body>
</html>
