<html>
<head>
	<link rel="stylesheet" type="text/css" href="fsbrowser.css"/>
</head>
<body>
<?php
	$directory = ".";

	function fsbrowser_init()
	{
		global $directory;

		if (strlen($_GET["dir"]) > 0)
			$directory = $_GET["dir"];
	}

	function remove_file()
	{
		$file = $_GET["remove"];

		if ($file)
			unlink($file);
	}

	function is_image($file)
	{
		$images = array("gif", "png", "jpg", "jpeg", "bmp", "svg");

		foreach($images as $i)
			if (strstr($file, "." . $i))
				return true;
		return false;
	}

	function is_php($fname)
	{
		if (strstr($fname, ".php"))
			return true;
		return false;
	}

	function put_icon($icon)
	{
		printf(" <td><img src=%s /></td>\n", $icon);
	}

	function put_tr()
	{
		static $light = 1;

		if ($light == 1) {
			$name = "light";
			$light = 0;
		} else {
			$name = "dark";
			$light = 1;
		}

		printf("<tr class='%s'>\n", $name);
	}

	function make_link($fname)
	{
		global $directory;

		if ($fname == ".")
			return;

		put_tr();

		$fpath = $directory . "/" . $fname;
		if (is_dir($fpath)) {
			put_icon("folder-icon.gif");
			echo(" <td>");
			echo("<a class='dir' href=fsbrowser.php?dir=" . $fpath . ">");
			echo($fname . "</a></td>\n");
			echo(" <td></td>\n");
		} else {
			if (is_image($fname))
				put_icon("image-icon.gif");
			else if (is_php($fname))
				put_icon("php-icon.gif");
			else
				echo(" <td></td>\n");
			echo(" <td>" . $fname . "</td>\n");
			echo(" <td class='del'>");
			printf("<a class='del' href=fsbrowser.php?dir=%s", $directory);
			printf("&remove=%s>apagar</a>", $fpath);
		}

		echo("</tr>\n");
	}

	function list_dir()
	{
		global $directory;

		printf("<h3>Caminho: %s/</h3>\n", $directory);

		$files = scandir($directory);
		if (!$files) {
			echo("Diretorio vazio\n");
			return;
		}

		echo("<table border='0'>\n");

		foreach($files as $f)
			make_link($f);

		echo("</table>\n");
	}

	fsbrowser_init();
	remove_file();
	list_dir();
?>
</body>
</html>
