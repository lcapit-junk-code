<?php
	class matrix {
		protected $rows;
		protected $cols;
		protected $matrix;

		public function get_rows()
		{
			return $this->rows;
		}

		public function get_cols()
		{
			return $this->cols;
		}

		protected function mk_matrix()
		{
			$mat = array();

			for ($i = 0; $i < $this->rows; $i++)
				for ($j = 0; $j < $this->cols; $j++)
					$mat[$i][$j] = false;

			return $mat;
		}

		public function check_values($mat2, $value)
		{
			if (($this->rows != $mat2->rows) ||
				($this->cols != $mat2->cols))
				throw new Exception("Matrizes sao diferentes");

			for ($i = 0; $i < $this->rows; $i++) {
				for ($j = 0; $j < $this->cols; $j++) {
					if ($this->matrix[$i][$j] == $value &&
						$mat2->matrix[$i][$j] == $value)
						throw new Exception("Matrizes com o mesmo valor");
				}
			}
		}

		public function __construct($r, $c)
		{
			if ($r <= 0 || $c <= 0)
				throw new Exception("Colunas ou Linhas invalidas");

			$this->rows = $r;
			$this->cols = $c;
			$this->matrix = $this->mk_matrix($r, $c);
		}
	}

	class disciplina extends matrix {
		private $nome;

		private function dia_to_row($d)
		{
			$dias = array("seg" => 0, "ter" => 1, "qua" => 2,
				"qui" => 3, "sex" => 4);

			if (!array_key_exists($d, $dias))
				throw new Exception("Dia invalido");

			return $dias[$d];
		}

		public function get_nome()
		{
			return $this->nome;
		}

		public function set_nome($n)
		{
			$this->nome = $n;
		}

		private function change_horario($d, $h, $value)
		{
			$h -= 1;

			if ($h < 0 || $h > $this->cols)
				throw new Exception("Hora invalida");

			if ($value != true && $value != false)
				throw new Exception("value tem que ser booleano");

			$cur = &$this->matrix[$this->dia_to_row($d)][$h];
			if ($cur == $value)
				throw new Exception("Valor ja alterado");

			$cur = $value;
		}

		public function has_horario($dia, $hora)
		{
			$hora -= 1;
			return $this->matrix[$this->dia_to_row($dia)][$hora];
		}

		public function set_horario($d, $h)
		{
			if ($this->has_horario($d, $h))
				throw new Exception("Dia e hora ja existem " .
					$d . " " . $h);

			$this->change_horario($d, $h, true);
		}

		public function unset_horario($d, $h)
		{
			$this->change_horario($d, $h, false);
		}

		public function __construct($n)
		{
			$rows = 5;
			$cols = 6;
			parent::__construct($rows, $cols);
			$this->set_nome($n);
		}
	}
?>
