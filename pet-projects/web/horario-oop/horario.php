<?php
	class horario {
		private $dias;
		private $horario;
		private $diciplinas;

		public function add($item)
		{
			foreach ($this->disciplinas as $dis)
				$item->check_values($dis, true);

			$this->disciplinas[] = $item;
		}

		private function print_dia_cel($h)
		{
			foreach ($this->dias as $dia) {
				$show = "-----";
				foreach ($this->disciplinas as $item)
					if ($item->has_horario($dia, $h))
						$show = $item->get_nome();
				echo("<td> " . $show . "</td>\n");
			}
		}

		private function print_horario($h)
		{
			echo("<td>" . $this->horario[$h - 1] . "</td>\n");
		}

		private function print_table_header()
		{
			echo("<h1>Quadro de horario</h1>");

			echo("<tr>\n");

			echo("<td>hora</td>\n");
			foreach ($this->dias as $dia)
				echo("<td>" . $dia . "</td>\n");

			echo("</tr>\n");
		}

		public function show()
		{
			echo("<table border='1'>\n");

			$this->print_table_header();

			for ($i = 1; $i <= $this->disciplinas[0]->get_rows(); $i++) {
				echo("<tr>\n");
				$this->print_horario($i);
				$this->print_dia_cel($i);
				echo("</tr>\n");
			}

			echo("</table>\n");
		}

		public function __construct()
		{
			$this->horario = array("7:30 - 8:20", "8:20 - 9:10", "9:30 - 10:20",
				"10:20 - 11:10", "11:10 - 12:00", "12:00 - 12:50");
			$this->dias = array("seg", "ter", "qua", "qui", "sex");
			$this->disciplinas = array();
		}
	}
?>
