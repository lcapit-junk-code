<html>
<body>
<?php
	include("horario.php");
	include("disciplina.php");

	function echo_error($e)
	{
		echo("ERRO: " . $e->getMessage());
	}

	$hor = new horario();

	$mat = new disciplina("matematica");
	try {
		$mat->set_horario("seg", 1);
		$mat->set_horario("seg", 2);
		$mat->set_horario("ter", 1);
		$mat->set_horario("sex", 6);
		$hor->add($mat);
	} catch (Exception $e) {
		echo_error($e);
	}

	$port = new disciplina("portuques");
	try {
		$port->set_horario("seg", 3);
		$port->set_horario("qua", 1);
		$port->set_horario("qui", 1);
		$hor->add($port);
	} catch (Exception $e) {
		echo_error($e);
	}

	$qui = new disciplina("quimica");
	try {
		$qui->set_horario("seg", 4);
		$qui->set_horario("seg", 5);
		$qui->set_horario("qua", 2);
		$qui->set_horario("qua", 3);
		$hor->add($qui);
	} catch (Exception $e) {
		echo_error($e);
	}

	$his = new disciplina("historia");
	try {
		$his->set_horario("ter", 2);
		$his->set_horario("ter", 3);
		$his->set_horario("sex", 1);
		$his->set_horario("sex", 2);
		$hor->add($his);
	} catch (Exception $e) {
		echo_error($e);
	}

	$hor->show();
?>
</body>
</html>
