#ifndef _DAS_PASS1_H
#define _DAS_PASS1_H

#include <stdio.h>

int das_pass1(FILE *inputf, FILE *outputf, int debug);

#endif /* _DAS_PASS1_H */
