#ifndef _DAS_SYMBOL_TBL_H
#define _DAS_SYMBOL_TBL_H

/* sizes */
#define LABEL_LEN 12

/* exported functions */
int symbol_tbl_lookup(const char *name);
int symbol_tbl_insert(const char *name, int value, int line_nr);
void symbol_tbl_dump(void);

#endif /* _DAS_SYMBOL_TBL_H */
