#ifndef _DAS_FILE_H
#define _DAS_FILE_H

#include <stdio.h>

/* syntax definitions */

#define ASM_COMMENT ';'
#define ASM_NEWLINE '\n'
#define ASM_SPACE   ' '
#define ASM_TAB     '\t'

#define ASM_OP_IMM  '$'
#define ASM_OP_REG  '%'
#define ASM_OP_SEP  ','

/* helper macros */

#define is_comment(c)  (c == ASM_COMMENT)
#define is_newline(c)  (c == ASM_NEWLINE)

/* exported values */

#define DEF_LINE_LEN 64

/* exported helpers for parsing */
char *skip_spaces(char *line, size_t len);
void remove_comment(char *line, size_t len);
char *get_line_end(char *line, size_t len, int line_nr);

/* exported functions */
int file_read_line(FILE *file, char *line, size_t len);
FILE *file_open(const char *path, const char *mode);
FILE *file_open_tmp(char *template);
void file_write_bin(const void *ptr, size_t size, FILE *stream);

#endif /* _DAS_FILE_H */
