#ifndef _DAS_PASS2_H
#define _DAS_PASS2_H

#include <stdio.h>

int das_pass2(FILE *inputf, FILE *outputf, int debug);

#endif /* _DAS_PASS2_H */
