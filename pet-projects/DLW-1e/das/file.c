#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>

#include "file.h"
#include "error.h"

/* is_blank(): return true if character 'c' is a space or
 * tab, return 0 otherwise */
static int is_blank(int c)
{
	return (c == ASM_SPACE || c == ASM_TAB);
}

/* skipe_spaces(): skip spaces, return the first no space
 * address in 'line' or NULL if 'line' is only spaces */
char *skip_spaces(char *line, size_t len)
{
	size_t i;

	for (i = 0; i < len; i++)
		if (!is_blank(line[i]))
			break;

	return (i == len ? NULL : &line[i]);
}

/* remove_comment(): remove comment from 'line' of
 * length 'len' */
void remove_comment(char *line, size_t len)
{
	size_t i;

	for (i = 0; i < len; i++)
		if (is_comment(line[i]))
			break;

	for (i = i - 1; i != 0; i--)
		if (!isspace(line[i]))
			break;

	if ((len - i) >= 2) {
		line[i + 1] = '\n';
		line[i + 2] = '\0';
	}
}

/* get_line_end(): return a pointer to the end of 'line',
 * where end is either a '\0' or ASM_OP_SEP */
char *get_line_end(char *line, size_t len, int line_nr)
{
	char *p;

	if (line[0] == '\0') {
		/* nothing to do */
		return line;
	}

	p = skip_spaces(line, len);
	if (*p != '\0' && *p != ASM_OP_SEP) {
		error(line_nr, "invalid separator or end of line");
		return NULL;
	}

	return p;
}

/* file_read_line()
 * 
 * Read one line from 'file' in 'line' of length 'len'.
 * 
 * Return 0 on success, 1 if EOF, and aborts program
 * execution on error.
 */
int file_read_line(FILE *file, char *line, size_t len)
{
	char *ret;

	memset(line, 0, len);

	ret = fgets(line, len, file);
	if (!ret) {
		if (feof(file))
			return 1;
		/* error */
		fatal("could not read line from file: %s", strerror(errno));
	}

	return 0;
}

/* file_open(): wrapper for fopen() which abort program execution
 * on error */
FILE *file_open(const char *path, const char *mode)
{
	FILE *file;

	file = fopen(path, mode);
	if (!file)
		fatal("could not open '%s': %s", path, strerror(errno));

	return file;
}

/* file_open_tmp(): open a temporary, abort program execution
 * on error */
FILE *file_open_tmp(char *template)
{
	int fd;
	FILE *file;

	fd = mkstemp(template);
	if (fd < 0)
		fatal("could not create temporary file: %s", strerror(errno));

	file = fdopen(fd, "w");
	if (!file)
		fatal("fdopen() failed: %s", strerror(errno));

	return file;
}

/* file_write_bin(): wrapper for fwrite() which abort program execution
 * on error */
void file_write_bin(const void *ptr, size_t size, FILE *stream)
{
	size_t bytes;

	bytes = fwrite(ptr, size, 1, stream);
	if (bytes != 1)
		fatal("could not write line in binary file: %s",
		      strerror(errno));
}
