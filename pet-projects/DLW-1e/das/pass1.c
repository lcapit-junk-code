#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <errno.h>

#include "pass1.h"
#include "file.h"
#include "symbol_tbl.h"
#include "error.h"

static char *read_label(char *line, int lcount, int line_nr, int *pass1_error)
{
	int err;
	size_t len;
	char *p, label[LABEL_LEN];

	p = strchr(line, ':');
	if (!p)
		return line;

	len = p - line;
	if (len >= (sizeof(label) - 1)) {
		error(line_nr, "label is too big");
		*pass1_error = 1;
	}

	*p++ = '\0';

	if (*pass1_error == 0) {
		memset(label, 0, sizeof(label));
		strncpy(label, line, sizeof(label) - 1);
		err = symbol_tbl_insert(label, lcount, line_nr);
		if (err)
			*pass1_error = 1;
	}

	return skip_spaces(p, strlen(p));
}

/* das_pass1()
 *
 * Perform assembler first pass
 * 
 *     o Populates the symbol table
 *     o Remove prefixed spaces
 *     o Remove labels
 * 
 * asmfp: asm FILE pointer (input)
 * interfp: intermediate FILE pointer (output)
 * debug: debug mode
 */
int das_pass1(FILE *asmfp, FILE *interfp, int debug)
{
	char line[DEF_LINE_LEN], *p;
	int line_nr, lcount, err, ret;

	err = lcount = 0;

	for (line_nr = 1;; line_nr++) {
		ret = file_read_line(asmfp, line, DEF_LINE_LEN);
		if (ret == 1)
			break;

		p = skip_spaces(line, strlen(line));
		if (!p)
			p = line;

		p = read_label(p, lcount, line_nr, &err);

		assert(p != NULL);

		ret = fprintf(interfp, "%s", p);
		if (ret < 0) {
			fatal("pass1 could not write to file: %s",
			      strerror(errno));
		}

		if (isalpha(*p))
			lcount += 2;
	}

	if (debug)
		symbol_tbl_dump();

	return err;
}
