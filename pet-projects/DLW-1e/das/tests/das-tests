#!/bin/bash
#
# Execute DAS' test-suite
#
# Luiz Fernando N. Capitulino
# <lcapitulino@gmail.com>

# load config file
. tests.conf

PNAME=$(basename $0)

usage()
{
	echo "usage: $PNAME [ options ]"
	echo
	echo "options:"
	echo "         -h,--help     this text"
	echo "         -p1,--pass1   run only first pass tests"
	echo "         -p2,--pass2   run only second pass tests"
	echo "         -err,--error  run only error tests"
	echo "         -p,--program  run only programs tests"
	echo
}

compare_files()
{
	local expected=$1
	local output=$2

	cmp -s $expected $output
	ret=$?

	rm -f $output

	return $ret
}

do_err_test()
{
	local file=${1}
	local ext=$2
	local input=${file}.asm
	local output=${file}.out
	local expected=${file}.${ext}

	$DAS -o /dev/null $input &> $output
	if [ $? -ne 1 ]; then
		return 1
	fi

	compare_files $expected $output

	return $?
}

do_test()
{
	local file=${1}
	local ext=$2
	local opt=$3
	local input=${file}.asm
	local output=${file}.out
	local expected=${file}.${ext}

	$DAS $opt -o $output $input
	if [ $? -ne 0 ]; then
		exit 1
	fi

	compare_files $expected $output

	return $?
}

report_results()
{
	local nr_tests=$1
	local nr_passed=$2
	local nr_failed=$3
	local passed_per=0

	passed_per=$(expr 100 \* $nr_passed)
	passed_per=$(expr $passed_per / $nr_tests)

	printf "\n  -> %d%% passed" $passed_per
	printf " [ %d tests %d pass %d error ]\n" $nr_tests \
		$nr_passed $nr_failed
	printf "\n"
}

run_test()
{
	local message="$1"
	local asm_files="$2"
	local ext="$3"
	local testfunc="$4"
	local opt=""
	local nr_tests=0
	local nr_passed=0
	local nr_failed=0
	local file=""

	if [ "$ext" = "p1" ]; then
		# das should only execute
		# first pass
		opt="-1"
	fi

	printf "* %s tests\n\n" "$message"

	for file in $asm_files; do
		((nr_tests++))
		printf "\t%-10s " $file
		$testfunc "$file" "$ext" "$opt"
		ret=$?
		if [ $ret -eq 0 ]; then
			printf "PASSED\n"
			((nr_passed++))
		else
			printf "FAILED [ %d ]\n" $ret
			((nr_failed++))
		fi
	done

	report_results $nr_tests $nr_passed $nr_failed
}

# Collect command-line options

while [ $# -gt 0 ]; do
	case $1 in
		-h|--help)
			usage
			exit 0
			;;
		-p1|--pass1)
			P2_TESTS=0
			ERR_TESTS=0
			PROG_TESTS=0
			;;
		-p2|--pass2)
			P1_TESTS=0
			ERR_TESTS=0
			PROG_TESTS=0
			;;
		-err|--error)
			P1_TESTS=0
			P2_TESTS=0
			PROG_TESTS=0
			;;
		-p|--program)
			P1_TESTS=0
			P2_TESTS=0
			ERR_TESTS=0
			;;
		*)
			echo "ERROR: unknown option '$1'" > /dev/stderr
			exit 1
			;;
	esac
	shift
done

# main program

printf "\n"

if [ $P1_TESTS -eq 1 ]; then
	run_test "$P1_NAME" "$P1_FILES" "$P1_EXT" "$P1_TEST"
fi

if [ $P2_TESTS -eq 1 ]; then
	run_test "$P2_NAME" "$P2_FILES" "$P2_EXT" "$P2_TEST"
fi

if [ $ERR_TESTS -eq 1 ]; then
	run_test "$ERR_NAME" "$ERR_FILES" "$ERR_EXT" "$ERR_TEST"
fi

if [ $PROG_TESTS -eq 1 ]; then
	run_test "$PROG_NAME" "$PROG_FILES" "$PROG_EXT" "$PROG_TEST"
fi
