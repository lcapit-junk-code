#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "opcode.h"

static struct opcode opcode_tbl[] = {
{ "add",   0x0, INST_ARIT, 3, { { OP_RI, 0 }, { OP_RI, 0 }, { OP_REG, 0 } } },
{ "cmp",   0x4, INST_COMP, 2, { { OP_RI, 0 }, { OP_REG,0 }, { OP_INV, 0 } } },
{ "halt",  0x7, INST_NARG, 0, { { OP_INV,0 }, { OP_INV,0 }, { OP_INV, 0 } } },
{ "jump",  0x5, INST_BRAN, 1, { { OP_RI, 0 }, { OP_INV,0 }, { OP_INV, 0 } } },
{ "load",  0x2, INST_MEM,  2, { { OP_RI, 0 }, { OP_REG,0 }, { OP_INV, 0 } } },
{ "store", 0x3, INST_MEM,  2, { { OP_REG,0 }, { OP_RI, 0 }, { OP_INV, 0 } } },
{ "sub",   0x1, INST_ARIT, 3, { { OP_RI, 0 }, { OP_RI, 0 }, { OP_REG, 0 } } },
{ NULL,    0x0,         0, 0, { { 0,     0 }, { 0,     0 }, { 0,      0 } } },
};

struct opcode *opcode_lookup(const char *mnemonic)
{
	struct opcode *p;

	for (p = opcode_tbl; p->mnemonic != NULL; p++)
		if (!strcmp(mnemonic, p->mnemonic))
			return p;

	errno = EINVAL;
	return NULL;
}
