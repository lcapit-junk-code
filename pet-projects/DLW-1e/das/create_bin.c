#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	int i;
	unsigned short inst;

	if (argc == 1) {
		printf("Usage: create_bin <inst1> [inst2 ... instN]\n");
		exit(1);
	}

	for (i = 1; i < argc; i++) {
		inst = strtol(argv[i], NULL, 16);
		write(1, &inst, sizeof(inst));
	}

	return 0;
}
