#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>

#include "error.h"

const char *program_name;

/* report(): Write 'mesg' with specified 'prefix' to stderr */
static void report(const char *prefix, const char *msg, va_list params)
{
	fputs(prefix, stderr);
	vfprintf(stderr, msg, params);
	fputs("\n", stderr);
}

void error(int line_nr, const char *err, ...)
{
	va_list params;

	fprintf(stderr, "%s:%d: error: ", program_name, line_nr);

	va_start(params, err);
	vfprintf(stderr, err, params);
	va_end(params);

	fputs("\n", stderr);
}

void fatal(const char *err, ...)
{
	va_list params;

	va_start(params, err);
	report("fatal: ", err, params);
	va_end(params);

	exit(1);
}

void error_setup(const char *name)
{
	char *p;

	p = strrchr(name, '/');
	program_name = (p == NULL ? name : ++p);
}
