#ifndef _DAS_OPCODE_H
#define _DAS_OPCODE_H

#include <stdint.h>

/* type definitions*/

typedef uint16_t inst_t;  // instruction
typedef uint8_t  oper_t;   // operand

/* operand struct */
struct operand {
	int type;
	oper_t value;
};

/* operand definitions */

#define OP_INV 0x0  /* invalid or not used */
#define OP_REG 0x1  /* register */
#define OP_IMM 0x2  /* immediate */

/* just for your convenience */
#define OP_RI (OP_REG | OP_IMM)

/* maximum number of operands */
#define OP_MAX 3

/* opcode table */
struct opcode {
	const char *mnemonic;
	int opcode;
	int type;
	int nr_operands;
	struct operand operands[OP_MAX];
};

/* instruction types */
#define INST_ARIT 0
#define INST_BRAN 1
#define INST_MEM  2
#define INST_COMP 3
#define INST_NARG 4

/* instruction offsets */
#define OFF_INST 12
#define OFF_SRC1 10
#define OFF_SRC2  8
#define OFF_IMM_DEST OFF_SRC2
#define OFF_DEST  6

#define OFF_MEM_REG_SRC1 10
#define OFF_MEM_REG_DEST  6
#define OFF_MEM_IMM_DEST  8

/* masks */
#define MASK_IMM 0xFF

/* special bits */
#define BIT_IMM 0x8000

/* helper macros */

/* set_reg(): set register 'reg' at offset 'off' in 'instruction' */
static inline void set_reg(inst_t *instruction, oper_t reg, int off)
{
	*instruction |= ((inst_t) (reg << off));
}

/* set_imm(): set immediate 'imm' in 'instruction' */
static inline void set_imm(inst_t *instruction, oper_t imm)
{
	*instruction |= BIT_IMM;
	*instruction |= ((inst_t) (MASK_IMM & imm));
}

/* exported functions */
struct opcode *opcode_lookup(const char *mnemonic);

#endif /* _DAS_OPCODE_H */
