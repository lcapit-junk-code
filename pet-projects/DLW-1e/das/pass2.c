#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>

#include "pass2.h"
#include "file.h"
#include "opcode.h"
#include "symbol_tbl.h"
#include "error.h"

static char *get_opcode(char *line, unsigned long line_nr,
			struct opcode **opcode)
{
	char *endp, *p;

	p = strchr(line, '\n');
	if (p)
		*p = '\0';

	endp = strchr(line, '\0');

	p = strchr(line, ' ');
	if (p)
		*p = '\0';

	*opcode = opcode_lookup(line);
	if (!*opcode) {
		error(line_nr, "instruction '%s' is not known", line);
		return NULL;
	}

	p = strchr(line, '\0');

	if (p == endp && ((*opcode)->nr_operands > 0)) {
		error(line_nr, "instruction requires operands");
		return NULL;
	}

	return ++p;
}

static char *eat_immediate(oper_t *op_value, char *line, int line_nr)
{
	int i;
	long ret;
	char c, *p;

	for (i = 0; isdigit(line[i]); i++)
		/* NOTHING */ ;

	if (i == 0) {
		error(line_nr, "no immediate in line");
		return NULL;
	}

	p = get_line_end(&line[i], strlen(&line[i]), line_nr);

	c = *p;
	*p = '\0';

	ret = strtol(line, NULL, 10);
	if (ret > 255) {
		error(line_nr, "immediate value overflow");
		return NULL;
	}
	if (ret < 0) {
		error(line_nr, "negative immediate value not suported yet");
		return NULL;
	}

	*p = c;

	*op_value = (oper_t) ret;

	return p;
}

static char *eat_register(oper_t *op_value, char *line, int line_nr)
{
	size_t len;

	len = strlen(line);
	if (len < 2)
		goto out_err;

	if (line[0] != 'r')
		goto out_err;

	*op_value = (oper_t) strtol(&line[1], NULL, 10);
	if (*op_value < 1 || *op_value > 4)
		goto out_err;

	*op_value -= 1;
	return get_line_end(&line[2], len, line_nr);

out_err:
	error(line_nr, "invalid register specified");
	return NULL;
}

static char *eat_label(oper_t *op_value, char *line, int line_nr)
{
	char c;
	int ret, i;

	for (i = 0; isalpha(line[i]) || isdigit(line[i]); i++)
		/* NOTHING */ ;

	c = line[i];
	line[i] = '\0';

	ret = symbol_tbl_lookup(line);
	if (ret < 0) {
		error(line_nr, "undefined symbol %s", line);
		return NULL;
	}

	*op_value = (oper_t) ret;
	line[i] = c;

	return get_line_end(&line[i], strlen(&line[i]), line_nr);
}

static char *get_next_operand(char *line, struct operand *operand,
			      int line_nr)
{
	char *p;

	for (p = line; *p != '\0'; p++, line_nr++) {
		switch (*p) {
		case ASM_SPACE:
		case ASM_OP_SEP:
			break;
		case ASM_COMMENT:
			goto out;
		case ASM_OP_IMM:
			operand->type = OP_IMM;
			p = eat_immediate(&operand->value, ++p, line_nr);
			goto out;
		case ASM_OP_REG:
			operand->type = OP_REG;
			p = eat_register(&operand->value, ++p, line_nr);
			goto out;
		default:
			/* label */
			operand->type = OP_IMM;
			p = eat_label(&operand->value, p, line_nr);
			goto out;
		}
	}

out:
	return p;
}

static void encode_arit(const struct operand *operands, inst_t *instruction)
{
	int type;
	oper_t dest;

	dest = operands[2].value;
	set_reg(instruction, dest, OFF_DEST);

	type = operands[0].type | operands[1].type;
	if (type & OP_IMM) {
		oper_t imm, src1;

		if (operands[0].type == OP_IMM) {
			imm = operands[0].value;
			src1 = operands[1].value;
		} else {
			imm = operands[1].value;
			src1 = operands[0].value;
		}

		set_reg(instruction, src1, OFF_SRC1);
		set_reg(instruction, dest, OFF_IMM_DEST);
		set_imm(instruction, imm);
	} else {
		oper_t reg;

		reg = operands[0].value;
		set_reg(instruction, reg, OFF_SRC1);

		reg = operands[1].value;
		set_reg(instruction, reg, OFF_SRC2);
	}
}

static void encode_branch(const struct operand *operands,
			  inst_t *instruction)
{
	oper_t dest;

	dest = operands[0].value;

	if (operands[0].type & OP_IMM)
		set_imm(instruction, dest);
	else
		set_reg(instruction, dest, OFF_MEM_REG_SRC1);
}

static void encode_store_imm(const struct operand *operands,
			     inst_t *instruction)
{
	oper_t imm, src;

	src = operands[0].value;
	set_reg(instruction, src, OFF_SRC1);

	imm = operands[1].value;
	set_imm(instruction, imm);
}

static int inst_is_store(inst_t instruction)
{
	const int store = 0x3000;

	return instruction == store;
}

static int operands_has_imm(const struct operand *operands)
{
	int i;

	for (i = 0; i < OP_MAX; i++)
		if (operands[i].type == OP_IMM)
			return 1;
	return 0;
}

static void encode_mem(const struct operand *operands,
		       inst_t *instruction)
{
	oper_t src1, dest;

	/* Fix this hack... */
	if (inst_is_store(*instruction) &&
	    operands_has_imm(operands)) {
		encode_store_imm(operands, instruction);
		return;
	}

	src1 = operands[0].value;
	dest = operands[1].value;

	if (operands[0].type & OP_IMM) {
		set_imm(instruction, src1);
		set_reg(instruction, dest, OFF_MEM_IMM_DEST);
	} else {
		set_reg(instruction, src1, OFF_MEM_REG_SRC1);
		set_reg(instruction, dest, OFF_MEM_REG_DEST);
	}
}

static void encode_comp(const struct operand *operands,
			inst_t *instruction)
{
	oper_t src1, src2;

	src1 = operands[0].value;

	src2 = operands[1].value;
	set_reg(instruction, src2, OFF_SRC2);

	if (operands[0].type & OP_IMM)
		set_imm(instruction, src1);
	else
		set_reg(instruction, src1, OFF_SRC1);
}

static void encode_inst(const struct opcode *opcode, inst_t *instruction)
{
	*instruction = (inst_t) opcode->opcode << OFF_INST;
}

static int check_operands(const struct opcode *opcode,
			  const struct operand *operands,
			  int operand_nr, int line_nr)
{
	int i;

	if (operand_nr > opcode->nr_operands) {
		error(line_nr, "instruction only supports %d operands",
		      opcode->nr_operands);
		return 1;
	}

	for (i = 0; i < opcode->nr_operands; i++) {
		if (opcode->operands[i].type == OP_INV &&
		    operands[i].type != OP_INV) {
			error(line_nr, "instruction only accepts %d operands",
			      opcode->nr_operands);
			return 1;
		}

		if (operands[i].type == OP_INV &&
		    opcode->operands[i].type != OP_INV) {
			error(line_nr, "operand %d is missing", i + 1);
			return 1;
		}

		if ((opcode->operands[i].type != OP_INV) &&
		    !(opcode->operands[i].type & operands[i].type)) {
			error(line_nr, "wrong type for operand %d", i + 1);
			return 1;
		}
	}

	return 0;
}

static int assembly(const struct opcode *opcode,
		    const struct operand *operands,
		    int operand_nr, inst_t *instruction, int line_nr)
{
	int err;

	err = check_operands(opcode, operands, operand_nr, line_nr);
	if (err)
		return -1;

	encode_inst(opcode, instruction);

	switch (opcode->type) {
	case INST_ARIT:
		encode_arit(operands, instruction);
		break;
	case INST_MEM:
		encode_mem(operands, instruction);
		break;
	case INST_BRAN:
		encode_branch(operands, instruction);
		break;
	case INST_COMP:
		encode_comp(operands, instruction);
		break;
	case INST_NARG:
		/* do nothing */
		break;
	};

	return 0;
}

static int read_operands(char *line, int line_nr, int operand_nr,
			 struct operand *operands)
{
	int i;
	char *p;

	// initialize
	for (i = 0; i < OP_MAX; i++) {
		operands[i].value = 0;
		operands[i].type = OP_INV;
	}

	p = line;

	/* read operands */
	for (i = 0; i < operand_nr; i++) {
		p = get_next_operand(p, &operands[i], line_nr);
		if (!p)
			return -1;

		if (*p == '\0' || *p == ASM_COMMENT)
			break;
	}

	// number of operands doesn't start at 0
	if (i)
		++i;

	return i;
}

/*
 * das_pass2()
 * 
 * Perform assembler second pass
 * 
 *   o assembly instructions
 *   o report errors
 * 
 * interfp: intermediate FILE pointer (input)
 * binfp:   binary FILE pointer (output) 
 * debug:   debug mode
 */
int das_pass2(FILE *interfp, FILE *binfp, int debug)
{
	inst_t instruction;
	int line_nr, err, ret;
	struct opcode *opcode;
	char line[DEF_LINE_LEN], *p;
	struct operand operands[OP_MAX];

	err = 0;

	for (line_nr = 1; ; line_nr++) {
		ret = file_read_line(interfp, line, DEF_LINE_LEN);
		if (ret == 1)
			break;

		if (is_comment(*line) || is_newline(*line))
			continue;

		remove_comment(line, strlen(line));

		p = get_opcode(line, line_nr, &opcode);
		if (!p) {
			err = 1;
			continue;
		}

		ret = read_operands(p, line_nr, opcode->nr_operands, operands);
		if (ret < 0) {
			err = 1;
			continue;
		}

		ret = assembly(opcode, operands, ret, &instruction, line_nr);
		if (ret) {
			err = 1;
			continue;
		}

		file_write_bin(&instruction, sizeof(instruction), binfp);

		if (debug)
			printf("%#x\n", instruction);
	}

	return err;
}
