#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "pass1.h"
#include "pass2.h"
#include "file.h"
#include "error.h"

/* usage(): print das's usage information */
static void usage(void)
{
	printf(
	       "usage: das [ options ] < -o file > < file.asm >\n"
	       "   options:\n"
	       "-h             this text\n"
	       "-1             performs only first pass\n"
	       "-d             debug mode\n"
	       "-D             do not remove first pass temp file\n"
	       "-o < file >    output (object) file\n"
	       );
}

int main(int argc, char *argv[])
{
	char *output, *input;
	FILE *asmfp, *interfp, *binfp;
	char pass1file[] = ".pass1.XXXXXX";
	int err, ret, del_p1file, p1_only, debug;

	err = 0;
	del_p1file = 1;
	p1_only = debug = 0;
	output = input = NULL;

	while ((ret = getopt(argc, argv, "1dDho:")) != -1) {
		switch (ret) {
		case '1':
			p1_only = 1;
			break;
		case 'd':
			debug = 1;
			break;
		case 'D':
			del_p1file = 0;
			break;
		case 'h':
			usage();
			exit(0);
		case 'o':
			output = optarg;
			break;
		default:
			usage();
			exit(1);
		}
	}

	if (optind >= argc)
		fatal("no input file given");

	input = argv[optind];

	if (!output)
		fatal("no output file given");

	error_setup(input);

	/*
	 * First pass
	 */

	asmfp = file_open(input, "r");

	if (p1_only)
		interfp = file_open(output, "w");
	else
		interfp = file_open_tmp(pass1file);

	ret = das_pass1(asmfp, interfp, debug);
	if (ret)
		err = 1;

	fclose(asmfp);

	if (p1_only) {
		/* done */
		fclose(interfp);
		exit(0);
	}

	/*
	 * Second pass
	 */

	binfp = file_open(output, "w");

	/* XXX: Why do I need freopen() to rewind() ? */
	interfp = freopen(pass1file, "r", interfp);
	rewind(interfp);

	ret = das_pass2(interfp, binfp, debug);
	if (ret)
		err = 1;

	fclose(interfp);
	fclose(binfp);

	if (del_p1file)
		remove(pass1file);

	/* we remove the binary file on error because
	 * it's probably botched */
	if (err)
		remove(output);

	return err;
}
