#include <stdio.h>
#include <string.h>

#include "symbol_tbl.h"
#include "error.h"

struct symbol {
	char name[LABEL_LEN];
	int value;
};

#define SYM_TBL_MAX_SIZE 16

static size_t symbol_tbl_size;
static struct symbol symbol_tbl[SYM_TBL_MAX_SIZE];

static struct symbol *table_lookup(const char *name)
{
	size_t i;

	for (i = 0; i < symbol_tbl_size; i++)
		if (!strcmp(name, symbol_tbl[i].name))
			return &symbol_tbl[i];

	return NULL;
}

int symbol_tbl_lookup(const char *name)
{
	struct symbol *symbol;

	symbol = table_lookup(name);
	if (!symbol)
		return -1;

	return symbol->value;
}

static void fill_symbol(struct symbol *new, const char *name, int value)
{
	memset(new, 0, sizeof(struct symbol));
	strncpy(new->name, name, LABEL_LEN - 1);
	new->value = value;
}

int symbol_tbl_insert(const char *name, int value, int line_nr)
{
	struct symbol *sym;

	sym = table_lookup(name);
	if (sym) {
		error(line_nr, "symbol %s redefined", name);
		return -1;
	}

	if (symbol_tbl_size == SYM_TBL_MAX_SIZE)
		fatal("symbol table is full");

	fill_symbol(&symbol_tbl[symbol_tbl_size], name, value);
	symbol_tbl_size++;

	return 0;
}

static void dump_symbol(const struct symbol *sym)
{
	printf("%s   %d\n", sym->name, sym->value);
}

void symbol_tbl_dump(void)
{
	size_t i;

	for (i = 0; i < symbol_tbl_size; i++)
		dump_symbol(&symbol_tbl[i]);
}
