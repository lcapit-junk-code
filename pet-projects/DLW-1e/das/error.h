#ifndef _DAS_ERROR_H
#define _DAS_ERROR_H

void error_setup(const char *name);
void error(int line_nr, const char *err, ...);
void fatal(const char *err, ...);

#endif /* _DAS_ERROR_H */
