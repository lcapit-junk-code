/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdlib.h>
#include <time.h>
#include "mballs.h"
#include "board.h"
#include "computer.h"

static int computer_is_playing;

/* computer_init(): initializes random number generator */
void computer_init(void)
{
	srand(time(NULL));
	computer_is_playing = 1;
}

/* is_computer_turn(): return 1 if this' computer's turn,
 * 0 otherwise */
int is_computer_turn(void)
{
	if (computer_is_playing && current_player == GAME_PLAYER2)
		return 1;
	return 0;
}

/* computer_move(): Handle computer move */
void computer_move(const struct game_board *board,
		   struct user_move *move)
{
	for (;;) {
		move->from_row = rand() % board->row;
		move->from_col = rand() % board->col;
		if (board_src_is_valid(board, move))
			break;
	}

	for (;;) {
		move->to_row = rand() % board->row;
		move->to_col = rand() % board->col;
		if (board_dst_is_valid(board, move))
			break;
	}
}
