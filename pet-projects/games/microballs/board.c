/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <math.h>
#include "mballs.h"
#include "board.h"
#include "misc.h"

/* board_init(): initialize game board */
void board_init(struct game_board *board, int size)
{
	int i, j;

	board->row = board->col = size;

	for (i = 0; i < board->row; i++)
		for (j = 0; j < board->col; j++)
			board->board[i][j] = GAME_FREE_SLOT;

	board->board[0][0] = GAME_PLAYER1;
	board->board[board->row - 1][board->col - 1] = GAME_PLAYER1;

	board->board[0][board->col - 1] = GAME_PLAYER2;
	board->board[board->row - 1][0] = GAME_PLAYER2;

	board->free_positions = board->row * board->col - 4;
	board->balls_play1 = board->balls_play2 = 2;
}

/* board_draw(): put the board on the screen */
void board_draw(const struct game_board *board)
{
	int i, j;
	char ch = 'a';

	clear_screen();

	printf("\n\t   ");
	for (i = 1; i <= board->row; i++)
		printf("%2d ", i);
	printf("\n\n");

	for (i = 0; i < board->row; i++) {
		printf("\t%c  ", ch++);
		for (j = 0; j < board->col; j++) {
			int val = board->board[i][j];

			if (val != GAME_FREE_SLOT)
				printf("%2c ", val);
			else
				printf("%2d ", val);
		}
		printf("\n");
	}
	printf("\n\n");

	printf("\t\t%c: %d   %c: %d\n\n\n",
	       GAME_PLAYER1, board->balls_play1,
	       GAME_PLAYER2, board->balls_play2);

	fflush(stdout);
}

/* row_is_valid(): check if row doesn't cross the board */
static int row_is_valid(const struct game_board *board, int row)
{
	if (row < 0 || row >= board->row)
		return 0;
	return 1;
}

/* col_is_valid(): check if col doesn't cross the board */
static int col_is_valid(const struct game_board *board, int col)
{
	if (col < 0 || col >= board->col)
		return 0;
	return 1;
}

/* range_is_valid(): check if the range in 'move' doesn't cross
 * the board */
static int range_is_valid(const struct game_board *board,
			  const struct user_move *move)
{
	if (row_is_valid(board, move->from_row) &&
	    col_is_valid(board, move->from_col) &&
	    row_is_valid(board, move->to_row)   &&
	    col_is_valid(board, move->to_col))
		return 1;
	return 0;
}

/* distance(): calculates the distance between two points */
static int distance(const struct user_move *move)
{
	int x, y;

	x = pow(move->to_col - move->from_col, 2);
	y = pow(move->to_row - move->from_row, 2);
	return sqrt(x + y);
}

static const int max_move = 2;

/* board_dst_is_valid(): check if destination point is valid */
int board_dst_is_valid(const struct game_board *board,
		       const struct user_move *move)
{
	enum game_piece piece;

	piece = board->board[move->to_row][move->to_col];
	if (piece != GAME_FREE_SLOT)
		return 0;

	if (distance(move) > max_move)
		return 0;

	return 1;
}

/* board_src_is_valid(): check if the source is from current player */
int board_src_is_valid(const struct game_board *board,
		       const struct user_move *move)
{
	enum game_piece piece;

	piece = board->board[move->from_row][move->from_col];
	return piece == current_player;
}

/* score_inc(): increments current's player score */
static void score_inc(struct game_board *board)
{
	if (current_player == GAME_PLAYER1)
		board->balls_play1++;
	else
		board->balls_play2++;
}

/* score_inc(): descrements current's player score */
static void score_dec(struct game_board *board)
{
	if (current_player == GAME_PLAYER1)
		board->balls_play2--;
	else
		board->balls_play1--;
}

/* __board_move(): actually perform current's player move */
static void __board_move(struct game_board *board,
			 const struct user_move *move)
{
	board->board[move->to_row][move->to_col] = current_player;
	if (distance(move) == max_move) {
		board->board[move->from_row][move->from_col] = GAME_FREE_SLOT;
		return;
	}
	score_inc(board);
	board->free_positions--;
}

/* do_conversion(): convert opponent's balls */
static void do_conversion(struct game_board *board, int row, int col)
{
	enum game_piece *p;

	if (!row_is_valid(board, row) || !col_is_valid(board, col))
		return;

	p = &board->board[row][col];
	if (*p != GAME_FREE_SLOT && *p != current_player) {
		*p = current_player;
		score_inc(board);
		score_dec(board);
	}
}

/* convert_lines(): convert oppenent's balls in all the lines */
static void convert_lines(struct game_board *board,
			  const struct user_move *move)
{
	do_conversion(board, move->to_row + 1, move->to_col);
	do_conversion(board, move->to_row - 1, move->to_col);
}

/* convert_cols(): convert oppenent's balls in all the columns */
static void convert_cols(struct game_board *board,
			 const struct user_move *move)
{
	do_conversion(board, move->to_row, move->to_col + 1);
	do_conversion(board, move->to_row, move->to_col - 1);
}

/* convert_diags(): convert oppenent's balls in all the diagonals */
static void convert_diags(struct game_board *board,
			  const struct user_move *move)
{
	do_conversion(board, move->to_row - 1, move->to_col - 1);
	do_conversion(board, move->to_row - 1, move->to_col + 1);
	do_conversion(board, move->to_row + 1, move->to_col + 1);
	do_conversion(board, move->to_row + 1, move->to_col - 1);
}

/* board_convert(): convert all oppenent's balls in the board */
static void board_convert(struct game_board *board,
			  const struct user_move *move)
{
	convert_lines(board, move);
	convert_cols(board, move);
	convert_diags(board, move);
}

/* is_winner(): check if current player is the winner */
static int is_winner(const struct game_board *board)
{
	if (!board->balls_play1) {
		current_player = GAME_PLAYER2;
		return GAME_FINISHED;
	}

	if (!board->balls_play2) {
		current_player = GAME_PLAYER1;
		return GAME_FINISHED;
	}

	if (board->free_positions)
		return GAME_OK;

	if (board->balls_play1 == board->balls_play2) {
		current_player = GAME_FREE_SLOT;
		return GAME_FINISHED;
	}

	current_player = board->balls_play1 > board->balls_play2 ?
		GAME_PLAYER1 : GAME_PLAYER2;
	return GAME_FINISHED;
}

/* board_move(): perform current's player move */
enum game_state board_move(struct game_board *board,
			  const struct user_move *move)
{
	if (!range_is_valid(board, move))
		return GAME_REST_TURN;

	if (!board_dst_is_valid(board, move))
		return GAME_REST_TURN;

	if (!board_src_is_valid(board, move))
		return GAME_REST_TURN;

	__board_move(board, move);
	board_convert(board, move);

	return is_winner(board) ? GAME_FINISHED : GAME_OK;
}
