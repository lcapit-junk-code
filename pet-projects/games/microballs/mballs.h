/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#ifndef MBALLS_H
#define MBALLS_H

enum game_state {
	GAME_OK,         /* no error */
	GAME_FINISHED,   /* game is finished */
	GAME_REST_TURN,  /* restart the current turn */
};

struct user_move {
	int from_row;
	int from_col;
	int to_row;
	int to_col;
};

enum game_piece {
	GAME_FREE_SLOT,
	GAME_PLAYER1 = 'X',
	GAME_PLAYER2 = 'Y',
};

extern enum game_piece current_player;

#endif /* MBALLS_H */
