/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#ifndef COMPUTER_H
#define COMPUTER_H

void computer_init(void);
int is_computer_turn(void);
void computer_move(const struct game_board *board,
		   struct user_move *move);

#endif /* COMPUTER_H */
