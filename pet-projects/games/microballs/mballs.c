/*
 * mballs: An amazing micro-balls game!
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "mballs.h"
#include "board.h"
#include "computer.h"
#include "misc.h"

enum game_piece current_player = GAME_PLAYER1;

/* set_next_player(): select who plays next */
static void set_next_player(void)
{
	if (current_player == GAME_PLAYER1)
		current_player = GAME_PLAYER2;
	else
		current_player = GAME_PLAYER1;
}

/* read_input(): read input from user */
static void read_input(struct user_move *move)
{
	char line[32];

	move->from_row = move->from_col = 0;
	move->to_row = move->to_col = 0;

	memset(line, '\0', sizeof(line));
	fgets(line, sizeof(line), stdin);
	sscanf(line, "%c%d-%c%d\n", (char *) &move->from_row, &move->from_col,
	       (char *) &move->to_row, &move->to_col);
}

/* get_next_move(): read the next move from the user */
static void get_next_move(const struct game_board *board,
			  struct user_move *move)
{
	if (is_computer_turn()) {
		computer_move(board, move);
		return;
	}

	print_message("Player '%c' turn: ", current_player);
	read_input(move);

	move->from_row -= 'a';
	move->from_col--;

	move->to_row -= 'a';
	move->to_col--;
}

static void usage(void)
{
	printf("mballs [ -h ] [ -c ] [ -e<size> ]\n");
}

int main(int argc, char *argv[])
{
	enum game_state st;
	struct user_move move;
	struct game_board board;
	int size = BOARD_DEF_SIZE;

	if (argc > 3) {
		fprintf(stderr, "ERROR: supports only 3 options\n");
		usage();
		exit(1);
	}

	if (argc >= 2) {
		if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
			usage();
			exit(0);
		}

		if (!strcmp(argv[1], "-c"))
			computer_init();

		if (argc == 3 && !memcmp(argv[2], "-e", 2)) {
			size = (int) strtol(argv[2] + 2, NULL, 10);
			if (size <= BOARD_DEF_SIZE || size > BOARD_MAX_SIZE) {
				fprintf(stderr,
					"ERROR: invalid board size: %d\n", size);
				exit(1);
			}
		}
	}

	board_init(&board, size);

	for (;;) {
		board_draw(&board);

		get_next_move(&board, &move);

		st = board_move(&board, &move);
		if (st == GAME_REST_TURN)
			continue;
		else if (st == GAME_FINISHED)
			break;

		set_next_player();
	}

	board_draw(&board);
	print_message("%c won!!\n\n", current_player);
	return 0;
}
