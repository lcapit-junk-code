/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "misc.h"

#ifdef LINUX_CLEAR_SCREEN
void clear_screen(void)
{
	system("clear");
}
#else /* !LINUX_CLEAR_SCREEN */
void clear_screen(void)
{
	system("cls");
}
#endif /* LINUX_CLEAR_SCREE */

/* print_message(): standard way to print a message on
 * the screen */
void print_message(const char *fmt, ...)
{
	va_list ap;

	fprintf(stdout, "  -> ");

	va_start(ap, fmt);
	vfprintf(stdout, fmt, ap);
	va_end(ap);
}
