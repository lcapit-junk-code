/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#ifndef MISC_H
#define MISC_H

void clear_screen(void);
void print_message(const char *fmt, ...);

#endif /* MISC_H */
