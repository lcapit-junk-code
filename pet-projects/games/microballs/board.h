/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#ifndef BOARD_H
#define BOARD_H

#include "mballs.h"

/* board sizes */
#define BOARD_DEF_SIZE 8
#define BOARD_MAX_SIZE 16

struct game_board {
	enum game_piece board[BOARD_MAX_SIZE][BOARD_MAX_SIZE]; /* game board */
	int row;                         /* number of rows in use */
	int col;                         /* number of columns in use */
	int free_positions;              /* free positions in the board */
	int balls_play1;                 /* number of player 1's balls */
	int balls_play2;                 /* number of player 2's balls */
};

void board_init(struct game_board *board, int size);
void board_draw(const struct game_board *board);
enum game_state board_move(struct game_board *board,
			  const struct user_move *move);
int board_dst_is_valid(const struct game_board *board,
		       const struct user_move *move);
int board_src_is_valid(const struct game_board *board,
		       const struct user_move *move);

#endif /* BOARD_H */
