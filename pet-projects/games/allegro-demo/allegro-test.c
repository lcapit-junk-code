#include <allegro.h>

static BITMAP *tux, *gnu, *buffer;

static inline void game_color_depth(void)
{
	int depth;

	depth = desktop_color_depth();
	if (!depth)
		depth = 32;
	set_color_depth(depth);
}

static void game_init(void)
{
	int ret;

	allegro_init();
	game_color_depth();

	ret = set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0);
	if (ret) {
		allegro_message(allegro_error);
		exit(1);
	}

	install_timer();
	install_keyboard();
	install_mouse();

	/* add other initializations here */
	tux = load_bitmap("Tux.bmp", NULL);
	if (!tux) {
		allegro_message(allegro_error);
		exit(1);
	}

	gnu = load_bitmap("gnu.bmp", NULL);
	if (!gnu) {
		allegro_message(allegro_error);
		exit(1);
	}

	buffer = create_bitmap(SCREEN_W, SCREEN_H);
	if (!buffer) {
		allegro_message(allegro_error);
		exit(1);
	}
}

static void game_deinit(void)
{
	clear_keybuf();
	/* add other deinitializations here */
	//FIXME: free tux, gnu and buffer
}

int main(void)
{
	int col = 400;
	int lin = 2;

	game_init();

	while (!key[KEY_ESC]) {

		if (key[KEY_LEFT] && col > 0)
			col--;
		if (key[KEY_RIGHT] && col < (SCREEN_W - tux->w))
			col++;
		if (key[KEY_UP] && lin > 0)
			lin--;
		if (key[KEY_DOWN] && lin < (SCREEN_H - tux->h))
			lin++;

		clear_bitmap(buffer);
		draw_sprite(buffer, tux, col, lin);
		draw_sprite(buffer, gnu, 100, 40);
		draw_sprite(screen, buffer, 0, 0);
	}

	game_deinit();
	return 0;
}
END_OF_MAIN()
