/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>

#include "dict.h"
#include "misc.h"

enum dictionary_menu_opt {
	DICT_PRINT = 1,
	DICT_SEARCH,
	DICT_INSERT,
	DICT_REMOVE,
	DICT_EXIT
};

static const char dict_file_path[] = "dict.txt";

/*
 * Helpers
 */

/* skip_char(): skip a sequence of characters 'c' in 'file' */
static void skip_char(FILE *file, int c)
{
	int rc;

	while ((rc = getc(file)) == c)
		/* NOTHING */ ;
	ungetc(rc, file);
}

/* skip_space(): skip spaces in 'file' */
static void skip_space(FILE *file)
{
	skip_char(file, ' ');
}

/* seek_line_end(): go to the end of a dict line */
static void seek_line_end(FILE *file)
{
	int c;

	for (;;) {
		c = getc(file);
		if (c == EOF || c == ':' || c == '\n')
			break;
	}
}

/* str_kill_nl(): kill a new-line character at end of line */
static void str_kill_nl(char *line)
{
	line[strlen(line) - 1] = '\0';
}

/*
 * List functions
 */

/* list_empty(): Return 1 if 'list' is empty, 0 otherwise */
static int list_empty(const struct dict_list *list)
{
	return list->first == list->last;
}

/* word_compare(): comparison function used by insertion_sort() */
static int word_compare(const void *key1, const void *key2)
{
	const char *str1 = ((const struct dict_line *) key1)->word;
	const char *str2 = ((const struct dict_line *) key2)->word;

	return memcmp(str1, str2, DICT_WORD_MAX);
}

/* insertion_sort(): sort data by using insertion sort algorithm */
static int insertion_sort(void *data, int size, int esize,
			  int (*compare) (const void *key1, const void *key2))
{
	char *a = data;
	void *key;
	int i, j;

	key = (char *) malloc(esize);
	if (!key)
		return -1;

	for (j = 1; j < size; j++) {
		memcpy(key, &a[j * esize], esize);
		i = j - 1;

		while (i >= 0 && compare(&a[i * esize], key) > 0) {
			memcpy(&a[(i + 1) * esize], &a[i * esize], esize);
			i--;
		}

		memcpy(&a[(i + 1) * esize], key, esize);
	}

	free(key);
	return 0;
}

/* list_sort(): sort list */
static int list_sort(struct dict_list *list)
{
	return insertion_sort(list->dict_line, list->last,
			      sizeof(struct dict_line), word_compare);
}

/* list_init(): Initialize 'list' */
static void list_init(struct dict_list *list)
{
	list->first = list->last = 0;
}

/* list_insert(): Insert element 'line' into 'list' */
static int list_insert(struct dict_list *list, struct dict_line *line)
{
	if (list->last == DICT_LIST_MAX) {
		errno = ENOMEM;
		return -1;
	}

	list->dict_line[list->last] = *line;
	list->last++;

	return 0;
}

/* list_remove(): Removes element at position 'pos' from 'list', the
 * elements' address is returned in 'ret' */
static int list_remove(int pos,
		       struct dict_list *list, struct dict_line *ret)
{
	int i;

	if (list_empty(list) || pos >= list->last)
		return -1;

	if (ret)
		*ret = list->dict_line[pos];

	list->last--;
	
	for (i = pos; i < list->last; i++)
		list->dict_line[i] = list->dict_line[i + 1];

	return 0;
}

/* list_write(): Write 'list' to the dict file */
static int list_write(struct dict_list *list)
{
	int i;
	FILE *dict;

	dict = fopen(dict_file_path, "w");
	if (!dict)
		return -1;

	for (i = 0; i < list->last; i++) {
		fprintf(dict, "%s: %s\n",
			list->dict_line[i].word, list->dict_line[i].tip);
	}

	fclose(dict);
	return 0;
}

/* show_entry(): print 'entry' in a standard way */
static void show_entry(int nr, const struct dict_line *entry)
{
	printf("%d. word: %s\n", nr, entry->word);
	printf("    tip: %s\n", entry->tip);
	printf("\n");
}

/* list_print(): Print 'list' to the standard output */
static void list_print(const struct dict_list *list)
{
	int i;

	if (list_empty(list))
		return;

	printf("\n");

	for (i = 0; i < list->last; i++)
		show_entry(i, &list->dict_line[i]);
}

static void handle_pattern(char *pattern,
			   const struct dict_list *list)
{
	int i, showed;

	if (pattern[0] == '\n') {
		list_print(list);
		return;
	}

	showed = 0;
	pattern[strlen(pattern) - 1] = '\0';

	for (i = 0; i < list->last; i++) {
		if (strstr(list->dict_line[i].word, pattern)) {
			show_entry(i, &list->dict_line[i]);
			showed = 1;
		}
	}

	if (!showed)
		printf("\n-> Nao encontrado\n");
}

static void show_prompt(void)
{
	clear_screen();

	printf("Aperte <enter> para fazer a pesquisa e "
	       "Ctrl-D para sair\n\n");
	printf(">>> ");
}

static void list_search(const struct dict_list *list)
{
	char *ret;
	char pattern[DICT_WORD_MAX];

	for (;;) {
		show_prompt();

		ret = fgets(pattern, sizeof(pattern), stdin);
		if (!ret)
			return;

		handle_pattern(pattern, list);
		mprint("");
	}
}

/*
 * Dict high level functions
 */

/* dict_read_line(): read a line from dict file 'file' and return it
 * in 'line' of size 'size' */
static int dict_read_line(FILE *file, char *line, size_t size)
{
	size_t i;
	int end, c;

	end = 0;
	memset(line, 0, size);

	skip_space(file);

	for (i = 0; i < size; i++) {
		c = getc(file);
		if (c == EOF || c == ':' || c == '\n') {
			end = 1;
			break;
		}
		line[i] = c;
	}

	if (!end) {
		seek_line_end(file);
		--i;
	}

	line[i] = '\0';
	return c;
}

/* dict_read_file(): read the entire dict file in the 'dict_line'
 * array, of elements 'size'. Return the number of read elements
 * in 'read' */
static int dict_read_file(struct dict_line *dict_line, size_t size, int *read)
{
	int ret;
	size_t i;
	FILE *dict;

	*read = 0;

	dict = fopen(dict_file_path, "r");
	if (!dict)
		return -1;

	for (i = 0; i < size; i++) {
		ret = dict_read_line(dict, dict_line[i].word, DICT_WORD_MAX);
		if (ret == EOF)
			break;

		ret = dict_read_line(dict, dict_line[i].tip, DICT_TIP_MAX);
		if (ret == EOF)
			break;
	}

	*read = i;

	fclose(dict);
	return 0;
}

/* dict_read_user(): Read a dict line from the user */
static void dict_read_user(struct dict_line *line)
{

	line->word[0] = line->tip[0] = '\0';

	fflush(stdin);
	printf("Entre com a palavra chave: ");
	fgets(line->word, DICT_WORD_MAX, stdin);
	str_kill_nl(line->word);

	fflush(stdin);
	printf("Entre com a dica: ");
	fgets(line->tip, DICT_TIP_MAX, stdin);
	str_kill_nl(line->tip);
}

/*
 * Main functions
 */

void dict_pick_up_word(struct dict_line *line)
{
	int err;
	struct dict_list list;

	list_init(&list);

	err = dict_read_file(list.dict_line, DICT_LIST_MAX, &list.last);
	if (err && errno != ENOENT) {
		eprint("dict_read_file() failed");
		return;
	}

	srand(time(NULL));

	*line = list.dict_line[rand() % list.last];
}

void dictionary_menu(void)
{
	int op, pos, err;
	struct dict_line line;
	struct dict_list list;

	list_init(&list);

	err = dict_read_file(list.dict_line, DICT_LIST_MAX, &list.last);
	if (err && errno != ENOENT) {
		eprint("dict_read_file() failed");
		return;
	}

	for (;;) {
		err = list_sort(&list);
		if (err) {
			eprint("insertion_sort() failed");
			return;
		}

		clear_screen();
		printf("\nManipular dicionario\n\n");
		printf("1. Imprimir conteudo\n"
		       "2. Pesquisa\n"
		       "3. Inserir\n"
		       "4. Remover\n"
		       "5. Sair\n"
		       "\n-> opcao: ");
		scanf("%d", (int *) &op);
		flush_all();

		switch (op) {
		case DICT_PRINT:
			list_print(&list);
			mprint("");
			break;
		case DICT_SEARCH:
			list_search(&list);
			break;
		case DICT_INSERT:
			dict_read_user(&line);
			err = list_insert(&list, &line);
			if (err)
				eprint("Failed\n");
			else
				mprint("Success\n");
			break;
		case DICT_REMOVE:
			list_print(&list);
			printf("Digite o numero para remover: ");
			scanf("%d", &pos);
			flush_all();
			err = list_remove(pos, &list, NULL);
			if (err)
				eprint("Failed\n");
			else
				mprint("Success\n");
			break;
		case DICT_EXIT:
			goto out;
		default:
			errno = EINVAL;
			eprint("Function not implemented");
		}
	}

out:
	list_write(&list);
}
