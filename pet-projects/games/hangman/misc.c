/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>

#include "misc.h"

void eprint(const char *fmt, ...)
{
	va_list ap;

	fprintf(stderr, "\n-> ERRO: ");

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	fprintf(stderr, " (%s)\n", strerror(errno));
	
	fprintf(stderr, "[pressione ENTER para continuar] ");
	getchar();
}

void mprint(const char *fmt, ...)
{
	va_list ap;

	printf("\n-> ");

	va_start(ap, fmt);
	vfprintf(stdout, fmt, ap);
	va_end(ap);

	printf("[pressione ENTER para continuar] ");
	getchar();
}

void flush_all(void)
{
	getchar();
	fflush(stdout);
}

#ifdef LINUX_CLEAR_SCREEN
void clear_screen(void)
{
	system("clear");
}
#else /* !LINUX_CLEAR_SCREEN */
void clear_screen(void)
{
	system("cls");
}
#endif /* LINUX_CLEAR_SCREE */
