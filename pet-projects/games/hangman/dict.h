#ifndef DICT_H
#define DICT_H

#define DICT_WORD_MAX 12
#define DICT_TIP_MAX  16
#define DICT_LIST_MAX 12

struct dict_line {
	char word[DICT_WORD_MAX];
	char tip[DICT_TIP_MAX];
};

struct dict_list {
	struct dict_line dict_line[DICT_LIST_MAX];
	int first;
	int last;
};

void dictionary_menu(void);
void dict_pick_up_word(struct dict_line *line);

#endif /* DICT_H */
