/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "game.h"
#include "misc.h"

#define ALPHABET_SIZE 26

struct player_game {
	const char *orig_word;
	const char *tip;
	char *word;
	size_t len;
	int hits;
	int errors;
	char alphabet[ALPHABET_SIZE];
};

enum game_result {
	WIN = 1,
	LOST,
};

int game_debug;
static int max_times = 6;

static int player_game_init(struct player_game *p,
			    const char *word,
			    const char *tip)
{
	int i;

	p->len = strlen(word);

	if ((int) p->len >= max_times)
		max_times += 2;

	p->word = malloc(p->len);
	if (!p->word)
		return -1;
	memset(p->word, '-', p->len);
	p->word[p->len] = '\0';

        p->orig_word = word;
	p->tip = tip;
	p->hits = p->errors = 0;

	for (i = 'a'; i <= 'z'; i++)
		p->alphabet[i - 'a'] = i;

	return 0;
}

static void player_destroy(const struct player_game *p)
{
	free(p->word);
}

static void player_show_status(const struct player_game *p)
{
	int i;

	clear_screen();

	printf("Palavra: ");
	for (i = 0; i < (int) p->len; i++)
		printf("%c", p->word[i]);

	if (game_debug)
		printf(" (%s)", p->orig_word);

	printf("\n\n");

	printf("Letras disponiveis: ");
	for (i = 0; i < ALPHABET_SIZE; i++)
		printf("%c", p->alphabet[i]);
	printf("\n\n");

	printf("Falhas disponiveis: %d\n\n", max_times - p->errors);

	if (p->errors == max_times - 1)
		printf("Dica: %s\n\n", p->tip);
}

static int count_letter(const char *word, char c)
{
	int count, i;

	count = 0;

	for (i = 0; word[i] != '\0'; i++)
		if (word[i] == c)
			count++;

	return count;
}

static enum game_result end_of_game(const struct player_game *p)
{
	if (p->hits == (int) p->len)
		return WIN;

	if (p->errors == max_times)
		return LOST;

	return 0;
}

static void show_final_stats(const struct player_game *p,
			     enum game_result result)
{
	switch (result) {
	case WIN:
		printf("Parabens! Acertou ");
		break;
	case LOST:
		printf("Azar! Perdeu ");
		break;
	}

	printf("usando %d letras (%d erros)\n", p->hits + p->errors,p->errors);
	if (result == LOST)
		printf("\nA palavra era: %s\n", p->orig_word);
	mprint("");
}

static void player_turn(struct player_game *p)
{
	char c;
	size_t i;

	printf("-> Digite uma letra e pressione enter: ");
	scanf("%c", &c);
	c = tolower(c);
	flush_all();

	if (c < 'a' || c > 'z') {
		mprint("Apenas sao permitidas letras de a-z\n");
		return;
	}

	if (p->alphabet[c - 'a'] == '-') {
		mprint("Letra ja escolhida antes\n");
		return;
	}

	for (i = 0; i < p->len; i++) {
		if (p->orig_word[i] == c && p->word[i] == '-') {
			p->word[i] = c;
			p->hits++;
			break;
		}
	}

	if (count_letter(p->orig_word, c) == count_letter(p->word, c))
		p->alphabet[c - 'a'] = '-';

	if (p->word[i] != c) {
		mprint("Errou!\n");
		p->errors++;
	}
}

void run_game(const char *word, const char *tip)
{
	struct player_game player;

	player_game_init(&player, word, tip);

	for (;;) {
		enum game_result res;

		player_show_status(&player);
		player_turn(&player);
		res = end_of_game(&player);
		if (res) {
			player_show_status(&player);
			show_final_stats(&player, res);
			break;
		}
	}

	player_destroy(&player);
}
