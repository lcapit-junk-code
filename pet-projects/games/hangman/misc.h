#ifndef MISC_H
#define MISC_H

void eprint(const char *fmt, ...);
void mprint(const char *fmt, ...);
void flush_all(void);
void clear_screen(void);

#endif /* MISC_H */
