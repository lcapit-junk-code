/*
 * A very stupid seabattle game written using a very stupid game library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <allegro.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

/*
 * Screen size, note that (x, y) = (w, h)
 */
#define DEF_SCREEN_W 640
#define DEF_SCREEN_H 480

/*
 * Ship related definitions
 */
#define SHIP_FIRST      0
#define SHIP_WATER      0
#define SHIP_WATER_HIT  1
#define SHIP_SUB        2
#define SHIP_SUB_HIT    3
#define SHIP_DEST       4
#define SHIP_DEST_HIT   5
#define SHIP_ARCC       6
#define SHIP_ARCC_HIT   7
#define SHIP_LAST       SHIP_ARCC_HIT
#define SHIP_MAX        (SHIP_LAST + 1)

#define SHIP_W          49
#define SHIP_H          49

#define SHIP_SUB_NR     2
#define SHIP_DEST_NR    4
#define SHIP_ARCC_NR    3
#define SHIP_DEF_NR     (SHIP_SUB_NR + SHIP_DEST_NR + SHIP_ARCC_NR)

/*
 * Low-level arrays sizes
 */
#define MAP_ARRAY_W  (DEF_SCREEN_W / SHIP_W / 2)
#define MAP_ARRAY_H  (DEF_SCREEN_H / SHIP_H)

/*
 * Global variables
 */
static BITMAP *buffer, *ships[SHIP_MAX];

static char *ships_pics[] = {
	        "water.bmp",
		"water_hit.bmp",
		"sub.bmp",
		"sub_hit.bmp",
		"dest.bmp",
		"dest_hit.bmp",
		"arcc.bmp",
		"arcc_hit.bmp",
};

struct player_object {
	volatile int ship_nr;
	int map_array[MAP_ARRAY_H][MAP_ARRAY_W];
};

static struct player_object player_user, player_comp;

/*
 * Misc functions and macros
 */

#define ROUNDDOWN(a, n)                   \
({                                        \
	uint32_t __a = (uint32_t) (a);    \
	(typeof(a)) (__a - __a % (n));    \
})                                        \

/*
 * die(): die with a nice message from the user
 */
static void die(const char *fmt, ...)
{
	va_list va;

	va_start(va, fmt);
	vfprintf(stderr, fmt, va);
	va_end(va);

	exit(1);
}

/*
 * Allegro and generic game related functions
 */

/*
 * game_color_depth(): Set the color depth to be used
 */
static void game_color_depth(void)
{
	int depth;

	depth = desktop_color_depth();
	if (!depth)
		depth = 32;
	set_color_depth(depth);
}

/*
 * game_srand_init(): Initialize the random number
 * generator
 */
static void game_srand_init(void)
{
	int fd;
	ssize_t bytes;
	unsigned int seed;

	fd = open("/dev/urandom", O_RDONLY);
	if (fd < 0) {
		perror("open()");
		exit(1);
	}

	seed = 0;
	bytes = read(fd, &seed, sizeof(unsigned int));
	if (bytes != sizeof(unsigned int))
		die("Could not read enough bytes\n");
	if (!seed)
		die("seed is zero\n");

	close(fd);
	srand(seed);
}

/*
 * game_rand(): Return two random generated numbers, in
 * the range [0, MAP_ARRAY_H). Can only be called after
 * a call to game_srand_init()
 */
static void game_srand(int *x, int *y)
{
	*x = rand() % MAP_ARRAY_W;
	*y = rand() % MAP_ARRAY_H;
}

/*
 * game_print(): Print a single message in the screen
 * buffer
 */
static void game_print(const char *s)
{
	textprintf_centre_ex(screen, font, DEF_SCREEN_W / 2, 120,
			     makecol(0, 255, 0), -1,
			     "%s", s);
}

static void player_init(struct player_object *p);

/*
 * game_init(): Initialize every single thing we need
 * to make this game to work
 */
static void game_init(void)
{
	int i, err;

	allegro_init();
	game_color_depth();

	err = set_gfx_mode(GFX_AUTODETECT_WINDOWED, DEF_SCREEN_W,
			   DEF_SCREEN_H, 0, 0);
	if (err) {
		allegro_message(allegro_error);
		exit(1);
	}

	install_timer();
	install_keyboard();

	buffer = create_bitmap(SCREEN_W, SCREEN_H);
	if (!buffer) {
		allegro_message(allegro_error);
		exit(1);
	}

	// load all ships
	for (i = 0; i < SHIP_MAX; i++) {
		ships[i] = load_bitmap(ships_pics[i], NULL);
		if (!ships[i]) {
			allegro_message(allegro_error);
			exit(1);
		}
	}

	player_init(&player_user);
	player_init(&player_comp);

	game_srand_init();
}

/*
 * game_install_mouse(): install the mouse handling registering
 * a callback
 */
static void game_install_mouse(void (*callback)(int flags))
{
	mouse_callback = callback;
	install_mouse();
}

/*
 * game_remove_mouse(): remove a mouse handler and its
 * registered callback
 */
static void game_remove_mouse(void)
{
	show_mouse(NULL);
	remove_mouse();
	mouse_callback = NULL;
}

/*
 * game_deinit(): free all allocated memory
 */
static void game_deinit(void)
{
	int i;

	clear_keybuf();

	for (i = 0; i < SHIP_MAX; i++)
		destroy_bitmap(ships[i]);

	destroy_bitmap(buffer);
}

/*
 * Ship related functions
 */

/*
 * ship_draw(): Draw a ship in the double buffer
 */
static void ship_draw(int ship, int x, int y)
{
	if (ship < SHIP_FIRST || ship > SHIP_LAST)
		die("%s: unknown ship code: %d\n", __FUNCTION__, ship);

	draw_sprite(buffer, ships[ship], x, y);
}

/*
 * ship_is_hidden(): Return true is ship must not be visible
 * in the screen
 */
static int ship_is_hidden(int ship)
{
	switch (ship) {
	case SHIP_SUB:
	case SHIP_DEST:
	case SHIP_ARCC:
		return 1;
	}

	return 0;
}

/*
 * ship_is_hit(): Return true is ship is of the hit type
 */
static int ship_is_hit(int ship)
{
	switch (ship) {
	case SHIP_WATER_HIT:
	case SHIP_SUB_HIT:
	case SHIP_DEST_HIT:
	case SHIP_ARCC_HIT:
		return 1;
	}

	return 0;
}

/*
 * ship_get_size(): Return the size of a given ship, if the
 * ship doesn't exist return -1
 */
static int ship_get_size(int ship)
{
	switch (ship) {
	case SHIP_SUB:
		return 1;
	case SHIP_DEST:
		return 2;
	case SHIP_ARCC:
		return 3;
	}

	return -1;
}

/*
 * Player object methods
 */

/*
 * player_str(): Return a string which describes the player_object
 * human name
 */
static const char *player_str(const struct player_object *p)
{
	return (p == &player_user ? "user" : "computer");
}

/*
 * player_ship_nr_def(): Set the default number of ships
 */
static void player_ship_nr_def(struct player_object *p)
{
	p->ship_nr = SHIP_DEF_NR;
}

/*
 * player_init(): Initialize a player object
 */
static void player_init(struct player_object *p)
{
	int x, y;

	for (y = 0; y < MAP_ARRAY_H; y++)
		for (x = 0; x < MAP_ARRAY_W; x++)
			p->map_array[y][x] = 0;

	player_ship_nr_def(p);
}

/*
 * player_array_dump(): Dump low-level array contents
 */
static void player_array_dump(struct player_object *p)
{
	int *map, i, j;

	map = &p->map_array[0][0];

	printf("%s map:\n", player_str(p));
	for (i = 0; i < MAP_ARRAY_H; i++) {
		for (j = 0; j < MAP_ARRAY_W; j++, map++)
			printf("%d ", *map);
		putchar('\n');
	}
	putchar('\n');
	fflush(stdout);
}

/*
 * player_ship_nr(): Return the number of ships the
 * object currently has
 */
static int player_ship_nr(const struct player_object *p)
{
	return p->ship_nr;
}

/*
 * player_ship_nr_dec(): Decrement by 1 the number of ships the
 * object currently has
 */
static void player_ship_nr_dec(struct player_object *p)
{
	p->ship_nr--;
}

/*
 * player_array_set(): set 'ship' in the (x, y) positions
 * of the provided player's map array.
 * 
 * If the position was really set (ie, the previous element
 * was SHIP_WATER) we return 1, otherwise return 0.
 */
static int player_array_set(struct player_object *p,
			    int ship, int x, int y)
{
	if (p->map_array[y][x] == SHIP_WATER) {
		p->map_array[y][x] = ship;
		return 1;
	}

	return 0;
}

/*
 * player_ship_set(): set 'ship' in the (x, y) positions
 * of the provided player's map array.
 * 
 * If the position was really set (ie, the previous element
 * stored in there was different) we return 1 and decrement
 * ship_nr, otherwise just return 0
 */
static int player_ship_set(struct player_object *p,
			   int ship, int x, int y)
{
	if (player_array_set(p, ship, x, y)) {
		player_ship_nr_dec(p);
		return 1;
	}

	return 0;
}

/*
 * player_ship_complex_set(): Wrapper for player_ship_set() that
 * supports loading ships with more than one part. Detects
 * collisions and other problems.
 * 
 * Return 1 if the position was set, 0 otherwise
 */
static int player_ship_complex_set(struct player_object *p,
				   int ship, int x, int y)
{
	int parts, i;

	parts = ship_get_size(ship);
	if (parts <= 0)
		die("ship: %d has %d parts\n", ship, parts);

	// Do not allow ships to cross the map
	if ((x + parts) > MAP_ARRAY_W)
		return 0;

	// Check if a collision is possible: left to right
	for (i = 0; i < parts; i++)
		if (p->map_array[y][x + i] != SHIP_WATER)
			return 0;

	// Okay, just set it then
	for (i = 0; i < parts; i++)
		player_ship_set(p, ship, x + i, y);

	return 1;
}


/*
 * player_map_draw(): Draw a player's map in the screen
 * 
 * Note that we're smart, we know how to hide ships
 */
static void player_map_draw(struct player_object *p,
			    int start_x, int start_y, int hide)
{
	int *map, y, x;

	map = &p->map_array[0][0];

	for (y = start_y; y < (MAP_ARRAY_H + start_y); y++)
		for (x = start_x; x < (MAP_ARRAY_W + start_x); x++, map++) {
			if (hide && ship_is_hidden(*map)) {
				ship_draw(SHIP_WATER,x * SHIP_W,y * SHIP_H);
				continue;
			}

			ship_draw(*map, x * SHIP_W, y * SHIP_H);
		}

	show_mouse(buffer);
}

/*
 * player_shoot(): Standard shooting function
 * 
 * Return 1 if the shoot was valid, 0 otherwise
 */
static int player_shoot(struct player_object *p, int x, int y)
{
	int *ship;

	ship = &p->map_array[y][x];
	if (*ship < SHIP_FIRST || *ship > SHIP_LAST) {
		die("%s: unknow ship code from '%s': %d\n", __FUNCTION__,
		    player_str(p));
	}

	if (ship_is_hit(*ship))
		return 0;

	*ship += 1; // SHIP_*_HIT

	if (*ship != SHIP_WATER_HIT)
		player_ship_nr_dec(p);

	return 1;
}

/*
 * Map functions
 */

/*
 * map_to_array(): Transforms (x, y) map positions into
 * (x, y) low-level array positions
 */
static void map_to_array(int mx, int my, int *ax, int *ay)
{
	*ax = ROUNDDOWN(mx, SHIP_W) / SHIP_W;
	*ay = ROUNDDOWN(my, SHIP_H) / SHIP_H;
}

/*
 * map_clear(): Clear the main map
 */
static void map_clear(void)
{
	clear_bitmap(buffer);
}

/*
 * User (player) related functions
 */

/*
 * mouse_set_user_map(): Limit the mouse pointer in the user
 * map range
 */
static void mouse_set_user_map(void)
{
	set_mouse_range(0, 0, MAP_ARRAY_W * SHIP_W - 1,
			SCREEN_H - SHIP_H - 1);
}

/*
 * user_setup_mouse_callback(): Mouse callback used by the
 * user setup code
 */
static void user_setup_mouse_callback(int event)
{
	int ship_nr, x, y;

	if (event != MOUSE_FLAG_LEFT_DOWN)
		return;

	ship_nr = player_ship_nr(&player_user);
	if (!ship_nr)
		return;

	map_to_array(mouse_x, mouse_y, &x, &y);

	if (ship_nr > 7)
		player_ship_set(&player_user, SHIP_SUB, x, y);
	else if (ship_nr > 3)
		player_ship_complex_set(&player_user, SHIP_DEST, x, y);

	player_ship_complex_set(&player_user, SHIP_ARCC, x, y);
}

/*
 * user_setup(): Setup user map and array
 */
static void user_setup(void)
{
	player_array_dump(&player_user);
	mouse_set_user_map();

	game_install_mouse(user_setup_mouse_callback);

	for (;;) {
		if (!player_ship_nr(&player_user))
			break;

		if (player_ship_nr(&player_user) < 0) {
			die("%s: user ships: %d\n", __FUNCTION__,
			    player_ship_nr(&player_user));
		}

		if (key[KEY_ESC])
			die("%s: user abort\n", __FUNCTION__);

		map_clear();
		player_map_draw(&player_user, 0, 0, 0);
		draw_sprite(screen, buffer, 0, 0);
	}

	game_remove_mouse();
	player_array_dump(&player_user);

	// Reset user's ship number, since it was set
	// to zero by the setup code
	player_ship_nr_def(&player_user);
}

/*
 * user_shoot(): fire!!
 */
static int user_shoot(int x, int y)
{
	// XXX: Not sure why is this -1 thing needed...
	x--;
	y--;
	return player_shoot(&player_comp, x, y);
}

static void comp_shoot(void);

/*
 * user_shoot_mouse_callback(): Mouse callback used by the
 * user's shoot code
 */
static void user_shoot_mouse_callback(int event)
{
	int ret, x, y;

	if (event != MOUSE_FLAG_LEFT_DOWN)
		return;

	if (!player_ship_nr(&player_comp))
		return;

	if (!player_ship_nr(&player_user))
		return;

	map_to_array(mouse_x, mouse_y, &x, &y);

	ret = user_shoot(x, y);
	if (!ret) {
		// If the shoot was invalid, we let the user
		// try it again
		return;
	}

	if (!player_ship_nr(&player_comp))
		return;

	/*
	 * Okay, user's shoot was valid. Now it's the
	 * computer's turn.
	 * 
	 * It's a bit kludge to call the computer's
	 * shoot function from the user one, but the computer
	 * can only shoot just after the user.
	 */
	comp_shoot();
}

/*
 * Computer (player) related functions
 */

/*
 * mouse_set_comp_map(): Limit the mouse pointer in the
 * computer map range
 */
static void mouse_set_comp_map(void)
{
	set_mouse_range(MAP_ARRAY_W * SHIP_W + SHIP_W - 1, 0,
			SCREEN_W - 1, SCREEN_H - SHIP_H - 1);

}

/*
 * comp_setup(): Setup computer's array
 */
static void comp_setup(void)
{
	int ship_nr, x, y;

	while (player_ship_nr(&player_comp)) {

		game_srand(&x, &y);

		ship_nr = player_ship_nr(&player_comp);
		if (ship_nr > 7)
			player_ship_set(&player_comp, SHIP_SUB, x, y);
		else if (ship_nr > 3)
			player_ship_complex_set(&player_comp, SHIP_DEST,x,y);
		else
			player_ship_complex_set(&player_comp, SHIP_ARCC,x,y);
	}

	player_array_dump(&player_comp);

	// Reset computer's ship number, since it was set
	// to zero by the setup code
	player_ship_nr_def(&player_comp);
}

/*
 * comp_shoot(): fire!!
 */
static void comp_shoot(void)
{
	int ret, x, y;

	for (;;) {
		game_srand(&x, &y);
		ret = player_shoot(&player_user, x, y);
		if (ret)
			break;
	}
}

int main(void)
{
	game_init();

	user_setup();
	comp_setup();

	game_install_mouse(user_shoot_mouse_callback);
	mouse_set_comp_map();

	while (!key[KEY_ESC] && !key[KEY_ENTER]) {
		map_clear();
		player_map_draw(&player_user, 0, 0, 0);
		player_map_draw(&player_comp, MAP_ARRAY_W + 1, 0, 1);

		draw_sprite(screen, buffer, 0, 0);

		if (!player_ship_nr(&player_comp)) {
			game_remove_mouse();
			game_print("USER'S VICTORY!!");
		}

		if (!player_ship_nr(&player_user)) {
			game_remove_mouse();
			game_print("COMPUTER'S VICTORY!!");
		}
	}

	game_deinit();
	return 0;
}
END_OF_MAIN()
