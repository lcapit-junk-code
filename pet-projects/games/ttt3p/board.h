/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#ifndef BOARD_H
#define BOARD_H

/* Main matrix size */
#define ROW 4
#define COL 4

/* Visible board pieces */
#define GAME_FREE_SLOT (0)
#define GAME_PIECE_P1 ('X')
#define GAME_PIECE_P2 ('Y')
#define GAME_PIECE_P3 ('Z')

extern int game_board[ROW][COL];
extern int game_board_positions;

void board_draw(void);
int board_row_is_filled(int row);
int board_column_is_filled(int col);
int board_diagonal1_is_filled(void);
int board_diagonal0_is_filled(void);
enum game_state board_set_pos(int row, int col);

#endif /* BOARD_H */
