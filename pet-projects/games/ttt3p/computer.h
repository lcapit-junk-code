/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#ifndef COMPUTER_H
#define COMPUTER_H

int is_computer_turn(void);
void computer_move(int *row, int *col);
void computer_init(void);
void computer_wait(void);

extern int computer_players;
extern int computer_watch;

#endif /* COMPUTER_H */
