/*
 * ttt3p: An amazing version of the famous tic tac toe game that
 * supports up to 3 players!
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ttt3p.h"
#include "board.h"
#include "computer.h"
#include "misc.h"

int game_current_player = GAME_PLAYER_1; /* player playing right now */
enum game_mode game_mode = GAME_MODE_NORMAL;

static void read_input(int *row, int *col)
{
	char line[16];

	*row = *col = 0;

	memset(line, '\0', sizeof(line));
	fgets(line, sizeof(line), stdin);
	sscanf(line, "%c%d", (char *) row, col);
	fflush(stdout);
}

/* get_next_move(): read the next move from the user */
static enum game_state get_next_move(int *row, int *col)
{
	if (is_computer_turn()) {
		computer_move(row, col);
		return GAME_OK;
	}

	print_message("Player '%c' turn: ", game_current_player);
	read_input(row, col);

	if (*row < 'a' || *row > 'd')
		return GAME_REST_TURN;
	*row -= 'a';

	/* In the screen, the board column starts at 1,
	 * but in arrays it starts at 0. */
	--*col;
	if (*col < 0 || *col >= COL)
		return GAME_REST_TURN;

	return GAME_OK;
}

static void set_next_player(void)
{
	if (++game_current_player == GAME_NEW_TURN)
		game_current_player = GAME_PLAYER_1;
}

static void usage(void)
{
	printf("usage: ttt3p [ nr-computers <-s> ]\n");
}

int main(int argc, char *argv[])
{
	int row, col;
	enum game_state st;

	if (argc > 3) {
		fprintf(stderr, "ERROR: wrong number of parameters\n");
		usage();
		exit(1);
	} else if (argc >= 2) {
		if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
			usage();
			exit(0);
		}

		computer_players = atoi(argv[1]);
		if (computer_players < 1 || computer_players > 3) {
			fprintf(stderr,
				"ERROR: max number of computer players is 3\n");
			usage();
			exit(1);
		}
		if (argc == 3 && !strcmp(argv[2], "-s"))
			computer_watch = 1;
		computer_init();
	}

	for (;;) {
		board_draw();

		st = get_next_move(&row, &col);
		if (st == GAME_REST_TURN)
			continue;

		st = board_set_pos(row, col);
		if (st == GAME_REST_TURN)
			continue;

		computer_wait();

		if (st == GAME_FINISHED)
			break;
		set_next_player();
	}

	board_draw();
	print_message("%c won!!\n", game_current_player);

	return 0;
}
