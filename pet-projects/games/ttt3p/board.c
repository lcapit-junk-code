/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include "ttt3p.h"
#include "board.h"

int game_board[ROW][COL]; /* main board */
int game_board_positions = ROW * COL; /* positions left */

#ifdef LINUX_CLEAR_SCREEN
static void clear_screen(void)
{
	system("clear");
}
#else /* !LINUX_CLEAR_SCREEN */
static void clear_screen(void)
{
	system("cls");
}
#endif /* LINUX_CLEAR_SCREE */

/* board_draw(): put the board on the screen */
void board_draw(void)
{
	int i, j;
	char ch = 'a';

	clear_screen();

	printf("\n\t   ");
	for (i = 1; i <= ROW; i++)
		printf("%d ", i);
	printf("\n\n");

	for (i = 0; i < ROW; i++) {
		printf("\t%c  ", ch++);
		for (j = 0; j < COL; j++) {
			int val = game_board[i][j];

			if (val != GAME_FREE_SLOT)
				printf("%c ", val);
			else
				printf("%d ", val);
		}
		printf("\n");
	}
	printf("\n");
}

static int current_pos_add(int *val, int row, int col)
{
	int *p;
	int normal = game_mode == GAME_MODE_NORMAL;

	p = &game_board[row][col];
	if (*p == game_current_player) {
		++*val;
		return 0;
	} else if (normal && *p != GAME_FREE_SLOT)
		return 1;

	return 0;
}

/* board_row_is_filled(): return the amount of current player's
 * pieces in row 'row'. Return 0 if no piece is found or if a
 * piece from other player is found. */
int board_row_is_filled(int row)
{
	int i, ret = 0;

	for (i = 0; i < COL; i++)
		if (current_pos_add(&ret, row, i))
			return 0;
	return ret;
}

/* board_column_is_filled(): return the amount of current player's
 * pieces in column 'col'. Return 0 if no piece is found or if a
 * piece from other player is found. */
int board_column_is_filled(int col)
{
	int i, ret = 0;

	for (i = 0; i < ROW; i++)
		if (current_pos_add(&ret, i, col))
			return 0;
	return ret;
}

/* board_diagonal1_is_filled(): return the amount of current
 * player's pieces found in the diagonal which starts at row 3.
 * Return 0 if no piece is found or if a piece from other player
 * is found. */
int board_diagonal1_is_filled(void)
{
	int i, j, ret;

	j = ret = 0;
	for (i = ROW - 1; i >= 0; i--)
		if (current_pos_add(&ret, i, j++))
			return 0;
	return ret;
}

/* board_diagonal0_is_filled(): return the amount of current
 * player's pieces found in the diagonal which starts at row 0.
 * Return 0 if no piece is found or if a piece from other player
 * is found. */
int board_diagonal0_is_filled(void)
{
	int i, ret = 0;

	for (i = 0; i < ROW; i++)
		if (current_pos_add(&ret, i, i))
			return 0;
	return ret;
}

/* is_winner(): return 1 if the last move of the current player
 * filled a row, a column or a diagonal. 0 otherwise */
static int is_winner(int row, int col)
{
	const int won = ROW;

	if (board_row_is_filled(row) == won)
		return 1;

	if (board_column_is_filled(col) == won)
		return 1;

	if (board_diagonal0_is_filled() == won)
		return 1;

	if (board_diagonal1_is_filled() == won)
		return 1;

	return 0;
}

/* board_set_pos(): set the positions 'row' and 'col' in
 * the board in the behalf of the current player */
enum game_state board_set_pos(int row, int col)
{
	int *p;

	p = &game_board[row][col];
	if (*p != GAME_FREE_SLOT && game_mode != GAME_MODE_FULL)
		return GAME_REST_TURN;

	if (game_mode == GAME_MODE_FULL && *p == game_current_player)
		return GAME_REST_TURN;

	*p = game_current_player;
	if (is_winner(row, col))
		return GAME_FINISHED;

	if (game_mode == GAME_MODE_NORMAL && --game_board_positions == 0)
		game_mode = GAME_MODE_FULL;

	return GAME_OK;
}
