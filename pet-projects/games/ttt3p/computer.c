/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "ttt3p.h"
#include "board.h"
#include "computer.h"
#include "misc.h"

int computer_players; /* number of computers players */
int computer_watch; /* watch computers playing */

void computer_init(void)
{
	srand(time(NULL));
}

/* is_computer_turn(): return 1 if this' computer's turn,
 * 0 otherwise */
int is_computer_turn(void)
{
	switch (computer_players) {
	case 0:
		break;
	case 1:
		if (game_current_player == GAME_PLAYER_3)
			return 1;
		break;
	case 2:
		if (game_current_player == GAME_PLAYER_2 ||
		    game_current_player == GAME_PLAYER_3)
			return 1;
		break;
	case 3:
		return 1;
	}

	return 0;
}

/*
 * FIXME: the search_win_* functions duplicates a lot of code
 */

/* search_win_row(): try to find a row which someone may be
 * about to win the game. */
static int search_win_row(int *row, int *col)
{
	int i, j;
	int sym_last, sym, found;

	for (i = 0; i < ROW; i++) {
		int sfree = 0;

		for (j = 0; j < COL; j++) {
			if (game_board[i][j] == GAME_FREE_SLOT) {
				++sfree;
				*row = i;
				*col = j;
			}
		}

		if (sfree != 1)
			continue;

		found = 1;
		sym_last = -1;
		for (j = 0; j < COL; j++) {
			sym = game_board[i][j];
			if (sym == GAME_FREE_SLOT)
				continue;
			else if (sym_last == -1)
				sym_last = sym;
			else if (sym_last != sym) {
				found = 0;
				break;
			}
		}

		if (found)
			return 1;
	}

	return 0;
}

/* search_win_col(): try to find a column which someone may be
 * about to win the game. */
static int search_win_col(int *row, int *col)
{
	int i, j;
	int sym_last, sym, found;

	for (i = 0; i < COL; i++) {
		int sfree = 0;

		for (j = 0; j < ROW; j++) {
			if (game_board[j][i] == GAME_FREE_SLOT) {
				++sfree;
				*row = j;
				*col = i;
			}
		}

		if (sfree != 1)
			continue;

		found = 1;
		sym_last = -1;
		for (j = 0; j < ROW; j++) {
			sym = game_board[j][i];
			if (sym == GAME_FREE_SLOT)
				continue;
			else if (sym_last == -1)
				sym_last = sym;
			else if (sym_last != sym) {
				found = 0;
				break;
			}
		}

		if (found)
			return 1;
	}

	return 0;
}

/* search_win_dig0(): check if someone is about to fill the
 * diagonal which starts at row 0. */
static int search_win_dig0(int *row, int *col)
{
	int i, sfree = 0;
	int found, sym_last, sym;

	for (i = 0; i < ROW; i++) {
		if (game_board[i][i] == GAME_FREE_SLOT) {
			++sfree;
			*row = *col = i;
		}
	}

	if (sfree != 1)
		return 0;

	found = 1;
	sym_last = -1;
	for (i = 0; i < ROW; i++) {
		sym = game_board[i][i];
		if (sym == GAME_FREE_SLOT)
			continue;
		else if (sym_last == -1)
			sym_last = sym;
		else if (sym_last != sym) {
			found = 0;
			break;
		}
	}

	return found;
}

/* search_win_dig1(): check if someone is about to fill the
 * diagonal which starts at row 3. */
static int search_win_dig1(int *row, int *col)
{
	int i, j, sfree;
	int found, sym_last, sym;

	j = sfree = 0;
	for (i = ROW - 1; i >= 0; i--) {
		if (game_board[i][j++] == GAME_FREE_SLOT) {
			++sfree;
			*row = i;
			*col = j - 1;
		}
	}

	if (sfree != 1)
		return 0;

	j = 0;
	found = 1;
	sym_last = -1;
	for (i = ROW - 1; i >= 0; i--) {
		sym = game_board[i][j++];
		if (sym == GAME_FREE_SLOT)
			continue;
		else if (sym_last == -1)
			sym_last = sym;
		else if (sym_last != sym) {
			found = 0;
			break;
		}
	}

	return found;
}

/* computer_move(): Handles computer players moves */
void computer_move(int *row, int *col)
{
	int i, ret, nr_row, nr_col;

	if (game_mode == GAME_MODE_NORMAL) {
		if (search_win_row(row, col))
			return;

		if (search_win_col(row, col))
			return;

		if (search_win_dig0(row, col))
			return;

		if (search_win_dig1(row, col))
			return;
	}

	nr_row = nr_col = 0;
	*row = *col = -1;

	for (i = 0; i < ROW; i++) {
		ret = board_row_is_filled(i);
		if (ret > nr_row) {
			nr_row = ret;
			*row = i;
		}
	}

	for (i = 0; i < COL; i++) {
		ret = board_column_is_filled(i);
		if (ret > nr_col) {
			nr_col = ret;
			*col = i;
		}
	}

	if (nr_col != 0 && nr_row != 0) {
		if (nr_col > nr_row)
			*row = -1;
		else
			*col = -1;
	}

	if (*row == -1)
		*row = rand() % ROW;

	if (*col == -1)
		*col = rand() % COL;
}

/* computer_wait(): computer plays too fast, we can wait if the
 * user asks we to do so */
void computer_wait(void)
{
	if (computer_watch && is_computer_turn()) {
		print_message("'%c' turn [ENTER to continue]\n",
			      game_current_player);
		getchar();
	}
}
