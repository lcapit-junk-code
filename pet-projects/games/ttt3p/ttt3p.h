/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#ifndef TTT3P_H
#define TTT3P_H

#include "board.h"

enum game_state {
	GAME_OK,         /* no error */
	GAME_FINISHED,   /* game is finished */
	GAME_REST_TURN,  /* restarted the current turn */
};

enum game_mode {
	GAME_MODE_NORMAL, /* normal tic tac toe rules */
	GAME_MODE_FULL,   /* board is full */
};

/* Players are represented by pieces */
#define GAME_PLAYER_1 GAME_PIECE_P1
#define GAME_PLAYER_2 GAME_PIECE_P2
#define GAME_PLAYER_3 GAME_PIECE_P3

/* Indicates when a new turn happen */
#define GAME_NEW_TURN (GAME_PLAYER_3 + 1)

extern int game_current_player;
extern enum game_mode game_mode;

#endif /* TTT3P_H */
