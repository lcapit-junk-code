#include <stdio.h>
#include <stdarg.h>
#include "misc.h"

/*
 * This file is licensed under the GPLv2 license
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */

/* print_message(): standard way to print a message on
 * the screen */
void print_message(const char *fmt, ...)
{
	va_list ap;

	printf("  -> ");

	va_start(ap, fmt);
	vfprintf(stdout, fmt, ap);
	va_end(ap);
}
