#!/bin/sh
#
# This script runs the entire emu8086 test-suite.
#
# Luiz Fernando Nogueira Capitulino
# <lcapitulino@gmail.com>

TEST_GROUPS="control data-transfer arithmetic"
EMU_JAR_FILE="emu8086.jar"
GROUP_ORDER="order.txt"
RUN_JAR="java -jar $EMU_JAR_FILE -b"

run_test()
{
	dtest=$1
	script="$dtest/test.e"
	output="$dtest/output.txt"
	expected="$dtest/expected.txt"

	$RUN_JAR $script > $output 2>&1
	ret=$?
	if [ $ret -eq 0 ]; then
		cmp -s $expected $output
		ret=$?
	fi

	return $ret
}

success=0
failed=0
runs=0

run_group()
{
	group=$1
	order="$group/$GROUP_ORDER"

	if [ ! -f $order ]; then
		printf "\nERROR: group \"$group\" doesn't have a order file\n"
		exit 1
	fi

	for file in $(cat $order); do
		dtest="$group/$file"
		if [ -d "$dtest" ]; then
			make -s -C $dtest
			printf " %25s: " $file
			run_test $dtest
			if [ $? -eq 0 ]; then
				printf "PASSED\n"
				((success++))
			else
				printf "FAILED\n"
				((failed++))
			fi
			((runs++))
		fi
	done
}

run_suite()
{
	printf "\nRunning tests:\n"

	for group in $TEST_GROUPS; do
		printf "\n\t$group\n"
		run_group $group
	done

	# Print report
	printf "\n\n"
	printf "Checks: %d, " $runs
	per=$(echo "(100 * $success / $runs)" | bc)
	printf "Passed: %d (%d%%), " $success $per
	per=$(echo "(100 * $failed / $runs)" | bc)
	printf "Failed: %d (%d%%)" $failed $per
	printf "\n\n"
}

##
## Starts here
##

if [ ! -f "$EMU_JAR_FILE" ]; then
	printf "ERROR: hm? Wanna run tests without the program?\n"
	exit 1
fi

if [ -n "$*" ]; then
	# Run the tests specified in the cmd-line
	TEST_GROUPS="$*"
fi

run_suite
