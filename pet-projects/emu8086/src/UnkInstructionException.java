
public class UnkInstructionException extends Exception {
	private String instruction;

	public String getMessage()
	{
		return "unknown instruction " + instruction;
	}

	public UnkInstructionException(short inst)
	{
		instruction = Integer.toHexString((int) inst);
	}
	
	public UnkInstructionException(String inst)
	{
		instruction = inst;
	}
}