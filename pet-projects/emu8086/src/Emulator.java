import java.io.*;

/**
 * High level 8086 emulator.
 * 
 * @author Luiz Fernando N. Capitulino
 */

public class Emulator {
	/**
	 * 8086 Processor.
	 */
	private Processor processor;

	/**
	 * Get processor status (general purpose and flags register).
	 * 
	 * @return processor status
	 */
	public ProcStatus getStatus()
	{
		return processor.getStatus();
	}

	/**
	 * Get processor RAM
	 * 
	 * @return processor RAM
	 */
	public RAMMemory getRAM()
	{
		return processor.getRAM();
	}

	/**
	 * Load the specified 8086 program into RAM.
	 * 
	 * @param prog 8086 program to load
	 * @throws InvalidMemException 
	 */
	public void loadProgram(String prog) throws InvalidMemException
	{
		int b;
		int addr = 0;
		FileInputStream in = null;

		try {
			in = new FileInputStream(prog);

			while ((b = in.read()) != -1)
					processor.writeRAM(addr++, (short) b);
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * Run already loaded program
	 * @throws InvalidMemException 
	 * @throws InvalidRegisterException 
	 */
	public void run() throws UnkInstructionException, InvalidMemException, InvalidRegisterException
	{
		while (processor.halted != true)
			processor.executeNext();
	}

	/**
	 * Execute next instruction
	 * @throws InvalidMemException 
	 * @throws InvalidRegisterException
	 * @return false if the processor is halted, true otherwise 
	 */
	public boolean ExecuteNext() throws UnkInstructionException, InvalidMemException, InvalidRegisterException
	{
		if (processor.halted == true)
			return false;
		
		processor.executeNext();
		return true;
	}

	/**
	 * Save an object at the specified location
	 * 
	 * @param object object to be saved
	 * @param filename name of the object
	 * @throws IOException
	 */
	private void saveObject(Serializable object, String filename) throws IOException
	{
		ObjectOutputStream objstream = new ObjectOutputStream(new FileOutputStream(filename));
		objstream.writeObject(object);
		objstream.close();
	}

	/**
	 * Load an object from the specified location
	 * 
	 * @param filename name of the object
	 * @return the loaded object
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 * @throws ClassNotFoundException
	 */
    private static Object loadObject(String filename) throws FileNotFoundException, IOException, ClassNotFoundException
    {
        ObjectInputStream objstream = new ObjectInputStream(new FileInputStream(filename));
        Object object = objstream.readObject();
        objstream.close();
        return object;
     }

	/**
	 * Save the Virtual Machine state
	 *
	 * @param statedir where states are stored
	 * @param name state name
	 */
	public void saveState(String statedir, String name) throws IOException
	{
		// Create the state storage
		String statepath = statedir + "/" + name + "/";
		(new File(statepath)).mkdir();

		ProcStatus status = processor.getStatus();
		saveObject(status, statepath + "status.obj");
	
		RAMMemory ram = processor.getRAM();
		saveObject(ram, statepath + "ram.obj");
	}

	/**
	 * Load the Virtual Machine state
	 *
	 * @param statedir where states are stored
	 * @param name state name
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void loadState(String statedir, String name) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		ProcStatus status = null;
		RAMMemory ram = null;
		String statepath = statedir + "/" + name + "/";

		status = (ProcStatus) loadObject(statepath + "status.obj");
		processor.setStatus(status);

		ram = (RAMMemory) loadObject(statepath + "ram.obj");
		processor.setRAM(ram);
	}

	/**
	 * Constructor.
	 */
	public Emulator()
	{
		processor = new Processor();
	}
}