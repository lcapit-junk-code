
public class InvalidMemException extends Exception {
	private String address;

	public String getMessage()
	{
		return "invalid memory address " + address;
	}

	public InvalidMemException(int addr)
	{
		address = Integer.toHexString(addr);
	}
}