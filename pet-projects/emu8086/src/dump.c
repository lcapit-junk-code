#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

static void dump(const char *file)
{
	int i, fd;
	unsigned char byte;

	fd = open(file, O_RDONLY);

	for (i = 0; read(fd, &byte, 1) > 0; i++)
		printf("[%d] 0x%x\n", i, byte);

	close(fd);
}

int main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr, "dump < filename >\n");
		exit(1);
	}

	dump(argv[1]);
	return 0;
}
