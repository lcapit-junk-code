/**
 * 8086 General purpose and segment registers.
 * 
 * @author Luiz Fernando Nogueira Capitulino
 */
public enum Reg8086 {
	REG_AX,
	REG_AH,
	REG_AL,
	REG_CX,
	REG_CH,
	REG_CL,
	REG_DX,
	REG_DH,
	REG_DL,
	REG_BX,
	REG_BH,
	REG_BL,
	REG_SI,
	REG_DI,
	REG_ES,
	REG_SP,
	REG_BP,
	REG_SS,
	REG_IP,
	REG_CS,
	REG_DS;

	/**
	 * Convert spec-defined value into Reg8086 enum (w = 1)
	 * 
	 * @param reg spec-defined value to convert
	 * @return Reg8086 converted value
	 * @throws InvalidRegisterException 
	 */
	public static Reg8086 convert_word_set(int reg) throws InvalidRegisterException
	{
		switch (reg)
		{
		case 0x0: // AX
			return REG_AX;
		case 0x1: // CX
			return REG_CX;
		case 0x2: // DX
			return REG_DX;
		case 0x3: // BX
			return REG_BX;
		case 0x4: // SP
			return REG_SP;
		case 0x5: // BP
			return REG_BP;
		case 0x6: // SI
			return REG_SI;
		case 0x7: // DI
			return REG_DI;
		default:
			throw new InvalidRegisterException(reg);
		}
	}

	/**
	 * Convert spec-defined value into Reg8086 enum (w = 0)
	 * 
	 * @param reg spec-defined value to convert
	 * @return Reg8086 converted value
	 * @throws InvalidRegisterException 
	 */
	public static Reg8086 convert_word_unset(int reg) throws InvalidRegisterException
	{
		switch (reg)
		{
		case 0x0: // AL
			return REG_AL;
		case 0x1: // CL
			return REG_CL;
		case 0x2: // DL
			return REG_DL;
		case 0x3: // BL
			return REG_BL;
		case 0x4: // AH
			return REG_AH;
		case 0x5: // CH
			return REG_CH;
		case 0x6: // DH
			return REG_DH;
		case 0x7: // BH
			return REG_BH;
		default:
			throw new InvalidRegisterException(reg);
		}
	}

	/**
	 * Convert spec-defined value into Reg8086 enum
	 * 
	 * @param reg spec-defined value to convert
	 * @param word spec's word bit
	 * @return Reg8086 converted value
	 * @throws InvalidRegisterException 
	 */
	public static Reg8086 convert_reg(int reg, boolean word) throws InvalidRegisterException
	{
		return (word ? convert_word_set(reg) : convert_word_unset(reg));
	}

	/**
	 * Convert integer to Reg8086 value
	 * 
	 * @param value integer to convert
	 * @return Reg8086 converted value
	 * @throws InvalidRegisterException 
	 */
	public static Reg8086 convert(int value) throws InvalidRegisterException
	{
		for (Reg8086 i: values())
			if (i.ordinal() == value)
				return i;
		
		throw new InvalidRegisterException(value);
	}
}
