import java.io.*;

/**
 * Emulates 8086-based computer RAM memory.
 * 
 * @author Luiz Fernando N. Capitulino
 */
public class RAMMemory implements Serializable {
	/**
	 * RAM memory contents.
	 */
	private short[] memory;

	/**
	 * Triggers InvalidMemException if addr is invalid
	 * 
	 * @param addr memory address to check
	 * @throws InvalidMemException 
	 */
	private void invalid_address(int addr) throws InvalidMemException
	{
		if ((addr < 0) || (addr >= memory.length))
			throw new InvalidMemException(addr);
	}

	/**
	 * Write all memory contents to stdout.
	 */
	public void dump()
	{
		for (int i = 0; i < memory.length; i++)
			System.out.printf("%x   0x%x \n", i, memory[i]);
	}

	/**
	 * Print the contents of the specified address.
	 * 
	 * @param addr RAM address to print contents
	 */
	public void show(int addr)
	{
		System.out.printf("[MEM 0x%x] 0x%x\n", addr, memory[addr]);
	}

	/**
	 * Write one byte into the specified RAM address.
	 * 
	 * @param addr RAM address to write to
	 * @param value new contents for the specified address
	 * @throws InvalidMemException 
	 */
	public void write(int addr, short value) throws InvalidMemException
	{
		invalid_address(addr);
		memory[addr] = (short) (value & (short) 255);
	}

	/**
	 * Read one byte from the specified RAM address.
	 * 
	 * @param addr RAM address to read from
	 * @return address contents
	 * @throws InvalidMemException 
	 */
	public short read(int addr) throws InvalidMemException
	{
		invalid_address(addr);
		return ((short) (memory[addr] & 255));
	}

	/**
	 * Constructor.
	 * 
	 * @size RAM size in bytes.
	 */
	public RAMMemory(int size)
	{
		memory = new short[size];
	}
}