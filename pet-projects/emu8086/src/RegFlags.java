
/**
 * 8086 flags register flags.
 * 
 * @author Luiz Fernando N. Capitulino
 */
public enum RegFlags {
	FLAG_A,
	FLAG_C,
	FLAG_D,
	FLAG_I,
	FLAG_O,
	FLAG_P,
	FLAG_S,
	FLAG_Z
}