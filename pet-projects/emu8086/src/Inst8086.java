/**
 * 8086 Instructions.
 * 
 * @author Luiz Fernando N. Capitulino
 */
public class Inst8086 {
	// Processor Control
	public static final short CLC = 0xF8;
	public static final short CMC = 0xF5;
	public static final short STC = 0xF9;
	public static final short CLD = 0xFC;
	public static final short STD = 0xFD;
	public static final short CLI = 0xFA;
	public static final short STI = 0xFB;
	public static final short HLT = 0xF4;

	// Data Transfer
	public static final short MOV_IR = 0xB0;
	public static final short MOV_AC = 0xA2;
	public static final short MOV_CA = 0xA0;
	public static final short MOV_RM_R = 0x88;

	// Arithmetic
	public static final short INC_REG = 0x40;
	public static final short DEC_REG = 0x48;
	public static final short ADD_REG = 0x00;
	public static final short SUB_REG = 0x28;
}