
/**
 * Emulates a 8086 processor.
 * 
 * @author Luiz Fernando N. Capitulino
 */
public class Processor {
	/**
	 * System RAM
	 */
	private RAMMemory ram;

	/**
	 * Processor status (general purpose registers and flags)
	 */
	private ProcStatus registers;

	/**
	 * Halted flag
	 */
	public boolean halted;

	/**
	 * Read RAM location pointed to by IP register and increment it.
	 * 
	 * @return RAM memory contents pointed to by IP
	 * @throws InvalidMemException 
	 */
	private int read_current_ip() throws InvalidMemException
	{
		int ip = registers.getRegister(Reg8086.REG_IP);
		int value = readRAM(ip);
		registers.setRegister(Reg8086.REG_IP, ++ip);

		return value;
	}

	/**
	 * Make a 16-bit value from two 8-bit values.
	 * 
	 * @param low 8-bit low
	 * @param high 8-bit high
	 * @return New 16-bit value
	 */
	private int make_16bit(int low, int high)
	{
		int value = low;
		value |= ((high & 0xFF) << 8);
		return value;
	}

	/*
	 * Processor control instructions
	 */

	/**
	 * Halt
	 */
	private void inst_hlt()
	{
		halted = true;
	}

	/**
	 * CLC
	 */
	private void inst_clc()
	{
		registers.unsetFlags(RegFlags.FLAG_C);
	}

	/**
	 * CMC
	 */
	private void inst_cmc()
	{
		if (registers.getFlag(RegFlags.FLAG_C))
			registers.unsetFlags(RegFlags.FLAG_C);
		else
			registers.setFlags(RegFlags.FLAG_C);
	}

	/**
	 * STC
	 */
	private void inst_stc()
	{
		registers.setFlags(RegFlags.FLAG_C);
	}

	/**
	 * CLD
	 */
	private void inst_cld()
	{
		registers.unsetFlags(RegFlags.FLAG_D);
	}

	/**
	 * STD
	 */
	private void inst_std()
	{
		registers.setFlags(RegFlags.FLAG_D);
	}

	/**
	 * CLI
	 */
	private void inst_cli()
	{
		registers.unsetFlags(RegFlags.FLAG_I);
	}

	/**
	 * STI
	 */
	private void inst_sti()
	{
		registers.setFlags(RegFlags.FLAG_I);
	}

	/**
	 * Processor control instruction dispatcher.
	 * 
	 * @param instruction current instruction
	 * @return true if the instructions was executed, false otherwise
	 */
	private boolean processor_control(short instruction)
	{
		switch (instruction)
		{
		case Inst8086.CLC:
			inst_clc();
			break;
		case Inst8086.CMC:
			inst_cmc();
			break;
		case Inst8086.STC:
			inst_stc();
			break;
		case Inst8086.CLD:
			inst_cld();
			break;
		case Inst8086.STD:
			inst_std();
			break;
		case Inst8086.CLI:
			inst_cli();
			break;
		case Inst8086.STI:
			inst_sti();
			break;
		case Inst8086.HLT:
			inst_hlt();
			break;
		default:
			return false;
		}

		return true;
	}

	/*
	 * Data transfer instructions
	 */

	/**
	 * Move a immediate to the specified register
	 * 
	 * @param xvalue 16-bit value
	 * @param hvalue 8-bit value high
	 * @param lvalue 8-bit value low
	 * @param xreg 16-bit register
	 * @param hreg 8-bit register high
	 * @param lreg 8-bit register low
	 * @param word true if it's a word operation
	 * @throws InvalidRegisterException 
	 */
	private void mov_to_register(int xvalue, int hvalue, int lvalue,
								Reg8086 xreg, Reg8086 hreg, Reg8086 lreg,
								boolean word) throws InvalidRegisterException
	{
		if (word)
			registers.setRegister_X(xreg, xvalue);
		else
			registers.setRegister_L(lreg, lvalue);
	}

	/**
	 * MOV: immediate to register.
	 * 
	 * @param instruction current instruction
	 * @return true if the instructions was executed, false otherwise
	 * @throws InvalidMemException 
	 * @throws InvalidRegisterException 
	 */
	private boolean mov_immediate_to_register(short instruction) throws InvalidMemException, InvalidRegisterException
	{
		short word_mask = 0x8;
		short imm_reg_mask = 0xF0;

		if (!((instruction & imm_reg_mask) == Inst8086.MOV_IR))
			return false;

		// low 8-bit data
		int low = read_current_ip();

		boolean word = false;
		int high = 0;
		int xvalue = 0;
		if ((instruction & word_mask) == word_mask) {
			// high 8-bit data
			word = true;
			high = read_current_ip();
			xvalue = make_16bit(low, high);
		}

		short reg_mask = 0x7;
		short reg = (short) (instruction & reg_mask);
		switch (reg) {
		case 0x0: // AX
			mov_to_register(xvalue, high, low, Reg8086.REG_AX,
							Reg8086.REG_AH, Reg8086.REG_AL, word);
			break;
		case 0x1: // CX
			mov_to_register(xvalue, high, low, Reg8086.REG_CX,
					Reg8086.REG_CH, Reg8086.REG_CL, word);
			break;
		case 0x2:// DX
			mov_to_register(xvalue, high, low, Reg8086.REG_DX,
					Reg8086.REG_DH, Reg8086.REG_DL, word);
			break;
		case 0x3: // BX
			mov_to_register(xvalue, high, low, Reg8086.REG_BX,
					Reg8086.REG_BH, Reg8086.REG_BL, word);
			break;
		case 0x4: // SP if word, AH otherwise
			if (word) {
				registers.setRegister(Reg8086.REG_SP, xvalue);
			} else {
				registers.setRegister(Reg8086.REG_AH, low);
				registers.setRegister(Reg8086.REG_AX, low);
			}
			break;
		case 0x5: // BP if word, CH otherwise
			if (word) {
				registers.setRegister(Reg8086.REG_BP, xvalue);
			} else {
				registers.setRegister(Reg8086.REG_CH, low);
				registers.setRegister(Reg8086.REG_CX, low);
			}
			break;
		case 0x6: // SI if word, DH otherwise
			if (word) {
				registers.setRegister(Reg8086.REG_SI, xvalue);
			} else {
				registers.setRegister(Reg8086.REG_DH, low);
				registers.setRegister(Reg8086.REG_DX, low);
			}
			break;
		case 0x7: // DI if word, BH otherwise
			if (word) {
				registers.setRegister(Reg8086.REG_DI, xvalue);
			} else {
				registers.setRegister(Reg8086.REG_BH, low);
				registers.setRegister(Reg8086.REG_BX, low);
			}
			break;
		default:
			throw new InvalidRegisterException((int) reg);
		}

		return true;
	}

	/**
	 * MOV: accumulator to memory.
	 * 
	 * @param instruction current instruction
	 * @return true if the instructions was executed, false otherwise
	 * @throws InvalidMemException 
	 */
	private boolean mov_accumulator_to_memory(short instruction) throws InvalidMemException
	{
		short acct_mem_mask = 0xFE;

		if (!((instruction & acct_mem_mask) == Inst8086.MOV_AC))
			return false;

		int addr_low = read_current_ip();
		int addr_high = read_current_ip();
		ram.write(addr_low, (short) registers.getRegister(Reg8086.REG_AL));

		short word_mask = 0x1;
		if ((instruction & word_mask) == 1) {
			if (addr_high == 0) {
				// FIXME: shouldn't GAS generate this address?
				addr_high = addr_low + 1;
			}
			ram.write(addr_high, (short) registers.getRegister(Reg8086.REG_AH));
		}

		return true;
	}

	/**
	 * MOV: memory to accumulator.
	 * 
	 * @param instruction current instruction
	 * @return true if the instructions was executed, false otherwise
	 * @throws InvalidMemException 
	 */
	private boolean mov_memory_to_accumulator(short instruction) throws InvalidMemException
	{
		short mem_acct_mask = 0xFE;

		if (!((instruction & mem_acct_mask) == Inst8086.MOV_CA))
			return false;

		int addr_low = read_current_ip();
		short value_low = ram.read(addr_low);
		registers.setRegister(Reg8086.REG_AL, value_low);
		registers.setRegister(Reg8086.REG_AX, value_low);

		int addr_high = read_current_ip();
		short word_mask = 0x1;
		if ((instruction & word_mask) == 1) {
			if (addr_high == 0) {
				// FIXME: shouldn't GAS generate this address?
				addr_high = addr_low + 1;
			}
			short value_high = ram.read(addr_high);
			registers.setRegister(Reg8086.REG_AH, value_high);
			int value = make_16bit(value_low, value_high);
			registers.setRegister(Reg8086.REG_AX, value);
		}

		return true;
	}

	/**
	 * MOV: Memory/Register to/from Register
	 * 
	 * @param instruction current instruction
	 * @return true if the instructions was executed, false otherwise
	 * @throws InvalidMemException 
	 * @throws UnkInstructionException 
	 * @throws InvalidRegisterException 
	 */
	private boolean mov_mr_to_from_register(short instruction) throws InvalidMemException, UnkInstructionException, InvalidRegisterException
	{
		short mr_r_mask = 0xFC;

		if (!((instruction & mr_r_mask) == Inst8086.MOV_RM_R))
			return false;

		/* Get direction bit */
		short direction_mask = 0x2;
		boolean to_register = false;
		if ((instruction & direction_mask) == direction_mask)
			to_register = true;

		/* Get word bit */
		short word_mask = 0x1;
		boolean word = false;
		if ((instruction & word_mask) == word_mask)
			word = true;

		/* read second byte instruction */
		int sec_byte = read_current_ip();

		/* Get mod bits */
		short mod_mask = 0xC0;
		int mod = ((sec_byte & mod_mask) >> 6);

		/* Get Register bits */
		short reg_mask = 0x38;
		int reg_value = ((sec_byte & reg_mask) >> 3);
		Reg8086 reg = Reg8086.convert_reg(reg_value, word);
	
		/* Get r/m bits */
		int rm_mask = 0x7;
		int rm = sec_byte & rm_mask;

		switch (mod) {
		case 0x3: // r/m is a register
			if (to_register == false) {
				int value = registers.getRegister(reg);
				Reg8086 reg2 = Reg8086.convert_reg(rm, word);
				if (reg2 == Reg8086.REG_AH ||
					reg2 == Reg8086.REG_BH ||
					reg2 == Reg8086.REG_CH ||
					reg2 == Reg8086.REG_DH) {
					registers.setRegister_H(reg2, value);
					return true;
				}
				if (reg2 == Reg8086.REG_AL ||
					reg2 == Reg8086.REG_BL ||
					reg2 == Reg8086.REG_CL ||
					reg2 == Reg8086.REG_DL) {
					registers.setRegister_L(reg2, value);
						return true;
					}
				if (reg2 == Reg8086.REG_AX ||
					reg2 == Reg8086.REG_BX ||
					reg2 == Reg8086.REG_CX ||
					reg2 == Reg8086.REG_DX) {
					registers.setRegister_X(reg2, value);
						return true;
					}
			}
			break;
		}

		throw new UnkInstructionException("type of mov");
	}

	/**
	 * Data Transfer instruction dispatcher.
	 * 
	 * @param instruction current instruction
	 * @return true if the instructions was executed, false otherwise
	 * @throws InvalidMemException 
	 * @throws InvalidRegisterException 
	 * @throws UnkInstructionException 
	 */
	private boolean data_transfer(short instruction) throws InvalidMemException, InvalidRegisterException, UnkInstructionException
	{		
		if (mov_immediate_to_register(instruction))
			return true;

		if (mov_accumulator_to_memory(instruction))
			return true;

		if (mov_memory_to_accumulator(instruction))
			return true;

		if (mov_mr_to_from_register(instruction))
			return true;

		return false;
	}

	/**
	 * INC instruction
	 * 
	 * @param instruction
	 * @return true if the instructions was executed, false otherwise
	 * @throws InvalidRegisterException 
	 */
	private boolean inc_reg(short instruction) throws InvalidRegisterException
	{
		short inc_r_mask = 0xF8;

		if (!((instruction & inc_r_mask) == Inst8086.INC_REG))
			return false;
	
		// Get register
		int reg_code = instruction & 0x7;
		Reg8086 reg = Reg8086.convert_reg(reg_code, true);

		// increment
		int value = registers.getRegister(reg);
		registers.setRegister_X(reg, ++value);

		return true;
	}

	/**
	 * DEC instruction
	 * 
	 * @param instruction
	 * @return true if the instructions was executed, false otherwise
	 * @throws InvalidRegisterException 
	 */
	private boolean dec_reg(short instruction) throws InvalidRegisterException
	{
		short dec_r_mask = 0xF8;

		if (!((instruction & dec_r_mask) == Inst8086.DEC_REG))
			return false;
	
		// Get register
		int reg_code = instruction & 0x7;
		Reg8086 reg = Reg8086.convert_reg(reg_code, true);

		// decrement
		int value = registers.getRegister(reg);
		registers.setRegister_X(reg, --value);

		return true;
	}

	/**
	 * ADD instruction
	 * 
	 * @param instruction
	 * @return true if the instructions was executed, false otherwise
	 * @throws InvalidMemException 
	 * @throws UnkInstructionException 
	 * @throws InvalidRegisterException 
	 */
	private boolean add(short instruction) throws InvalidMemException, UnkInstructionException, InvalidRegisterException
	{
		short add_r_mask = 0xFC;

		if (!((instruction & add_r_mask) == Inst8086.ADD_REG))
			return false;

		// Get direction
		boolean to_register = false;
		int reg_mask = 0x2;
		if ((instruction & reg_mask) == reg_mask)
			to_register = true;

		// Get word
		boolean word = true;
		int word_mask = 0x1;
		if ((instruction & word_mask) != word_mask)
			throw new UnkInstructionException("add type");
 
		/* read second byte instruction */
		int sec_byte_inst = read_current_ip();

		// XXX: Only accepts registers for now
		int mod_mask = 0xC0;
		if ((sec_byte_inst & mod_mask) != mod_mask)
			throw new UnkInstructionException("add type");

		// Get source
		int reg_sec_mask = 0x38;
		int reg_source = ((sec_byte_inst & reg_sec_mask) >> 3);
		Reg8086 reg = Reg8086.convert_reg(reg_source, word);

		// Get target
		int rm_mask = 0x3;
		int reg_rm = sec_byte_inst & rm_mask; 
		Reg8086 reg_target = Reg8086.convert_reg(reg_rm, word);

		// Add them!
		int result = registers.getRegister(reg) +
							registers.getRegister(reg_target);
		registers.setRegister_X(reg_target, result);

		return true;
	}

	/**
	 * SUB instruction
	 * 
	 * @param instruction
	 * @return true if the instructions was executed, false otherwise
	 * @throws InvalidMemException 
	 * @throws UnkInstructionException 
	 * @throws InvalidRegisterException 
	 */
	private boolean sub(short instruction) throws InvalidMemException, UnkInstructionException, InvalidRegisterException
	{
		short sub_r_mask = 0xFC;

		if (!((instruction & sub_r_mask) == Inst8086.SUB_REG))
			return false;

		// Get direction
		boolean to_register = false;
		int reg_mask = 0x2;
		if ((instruction & reg_mask) == reg_mask)
			to_register = true;

		// Get word
		boolean word = true;
		int word_mask = 0x1;
		if ((instruction & word_mask) != word_mask)
			throw new UnkInstructionException("add type");
 
		/* read second byte instruction */
		int sec_byte_inst = read_current_ip();

		// XXX: Only accepts registers for now
		int mod_mask = 0xC0;
		if ((sec_byte_inst & mod_mask) != mod_mask)
			throw new UnkInstructionException("add type");

		// Get source
		int reg_sec_mask = 0x38;
		int reg_source = ((sec_byte_inst & reg_sec_mask) >> 3);
		Reg8086 reg = Reg8086.convert_reg(reg_source, word);

		// Get target
		int rm_mask = 0x3;
		int reg_rm = sec_byte_inst & rm_mask; 
		Reg8086 reg_target = Reg8086.convert_reg(reg_rm, word);

		// Add them!
		int result = registers.getRegister(reg) -
							registers.getRegister(reg_target);
		registers.setRegister_X(reg_target, result);

		return true;
	}

	/**
	 * Arithmetic instruction dispatcher.
	 * 
	 * @param instruction current instruction
	 * @return true if the instructions was executed, false otherwise
	 * @throws InvalidRegisterException 
	 * @throws UnkInstructionException 
	 * @throws InvalidMemException 
	 */
	private boolean arithmetic(short instruction) throws InvalidRegisterException, InvalidMemException, UnkInstructionException
	{
		if (inc_reg(instruction))
			return true;

		if (dec_reg(instruction))
			return true;

		if (add(instruction))
			return true;

		if (sub(instruction))
			return true;

		return false;
	}

	/**
	 * Execute specified instruction.
	 * 
	 * @param instruction instruction to be executed
	 * @throws UnkInstructionException, InvalidMemException 
	 * @throws InvalidRegisterException 
	 */
	private void executeInst(short instruction) throws UnkInstructionException, UnkInstructionException, InvalidMemException, InvalidRegisterException
	{
		if (data_transfer(instruction))
			return;

		if (processor_control(instruction))
			return;

		if (arithmetic(instruction))
			return;

		throw new UnkInstructionException(instruction);
	}

	/**
	 * Execute next instruction.
	 * @throws UnkInstructionException, InvalidMemException 
	 * @throws InvalidRegisterException 
	 */
	public void executeNext() throws UnkInstructionException, UnkInstructionException, InvalidMemException, InvalidRegisterException
	{
		executeInst((short) read_current_ip());
	}

	/**
	 * Write all memory contents to stdout.
	 */
	public void dumpRAM()
	{
		ram.dump();
	}

	/**
	 * Write one byte into the specified RAM address.
	 * 
	 * @param addr RAM address to write to
	 * @param value new contents for the specified address
	 * @throws InvalidMemException 
	 */
	public void writeRAM(int addr, short value) throws InvalidMemException
	{
		ram.write(addr, value);
	}

	/**
	 * Read one byte from the specified RAM address.
	 * 
	 * @param addr RAM address to read from
	 * @return address contents
	 * @throws InvalidMemException 
	 */
	public short readRAM(int addr) throws InvalidMemException
	{
		return ram.read(addr);
	}

	/**
	 * Get Processor RAM memory.
	 * 
	 * @return processor RAM memory
	 */
	public RAMMemory getRAM()
	{
		return ram;
	}

	/**
	 * Set the Processor RAM memory object
	 * 
	 * @param new_ram new ram memory object
	 */
	public void setRAM(RAMMemory new_ram)
	{
		ram = new_ram;
	}

	/**
	 * Get Processor status.
	 * 
	 * @return processor status
	 */
	public ProcStatus getStatus()
	{
		return registers;
	}

	/**
	 * Set Processor status
	 * 
	 * @param new_status new processor status
	 */
	public void setStatus(ProcStatus new_status)
	{
		registers = new_status;
	}

	/**
	 * Constructor.
	 */
	public Processor()
	{
		ram = new RAMMemory(100);
		registers = new ProcStatus();
		halted = false;
	}
}