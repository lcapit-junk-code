import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Implements a 8086 Emulator shell based.
 * 
 * @author Luiz Fernando N. Capitulino
 */
public class EmulatorShell {
	private static Emulator emulator;
	private static BufferedReader cmdfile;
	private static boolean verbose;
	private static boolean batchmode;

	private static void emulatorInit()
	{
		verbose = false;
		emulator = new Emulator();
	}

	private static void shellPrint(String msg)
	{
		if (!batchmode)
			System.out.println("-> " + msg);
	}

	private static void shellError(String msg)
	{
		System.err.println("-> ERROR: " + msg);
		if (batchmode)
			System.exit(1);
	}

	private static void fileCommandOpen(String file)
	{
		try {
			cmdfile = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}

	private static void showHelp()
	{
		System.out.println("load < filepath >");
		System.out.println("next");
		System.out.println("print < regs | mem < addr > >");
		System.out.println("reset");
		System.out.println("restore < name >");
		System.out.println("save < name >");
		System.out.println("restore < name >");
		System.out.println("run");
		System.out.println("quit");
		System.out.println("verbose");
	}

	private static boolean process(String cmd)
	{
		if (cmd == null)
			return true;

		if (cmd.length() == 0 || cmd.startsWith("#"))
			return false;

		if (verbose)
			shellPrint("cmd: " + cmd);

		try {
			if (cmd.startsWith("quit")) {
				return true;
			} else if (cmd.startsWith("load")) {
				if (cmd.indexOf(' ') == -1) {
					shellError("hm? No filename to load?");
					return false;
				}
				String prog = cmd.substring(cmd.indexOf(' ') + 1);
				shellPrint("loading: " + prog);
				emulator.loadProgram(prog);
			} else if (cmd.startsWith("run")) {
				emulator.run();
			} else if (cmd.startsWith("verbose")) {
				verbose = !verbose;
			} else if (cmd.startsWith("print")) {
				if (cmd.indexOf(' ') == -1) {
					shellError("hm? Print what?");
					return false;
				}
				String arg = cmd.substring(cmd.indexOf(' ') + 1);
				if (arg.startsWith("flags")) {
					ProcStatus status = emulator.getStatus();
					status.dump_flags();
				} else if (arg.startsWith("regs")) {
					ProcStatus status = emulator.getStatus();
					status.dump_registers();
				} else if (arg.startsWith("mem")) {
					if (arg.indexOf(' ') == -1) {
						shellError("hm? Print what address?");
						return false;
					}
					String saddr = arg.substring(cmd.indexOf(' ') + 1);
					int addr = Integer.parseInt(saddr, 16);
					RAMMemory ram = emulator.getRAM();
					ram.show(addr);
				} else {
					shellError("invalid print command");
				}
			} else if (cmd.startsWith("reset")) {
				emulatorInit();
			} else if (cmd.startsWith("next")) {
				if (emulator.ExecuteNext() == false)
					shellPrint("processor halted");
			} else if (cmd.startsWith("save")) {
				if (cmd.indexOf(' ') == -1) {
					shellError("hm? What name?");
					return false;
				}
				String name = cmd.substring(cmd.indexOf(' ') + 1);
				String statedir = "/home/lcapitulino/.emu8086-states";
				emulator.saveState(statedir, name);
			} else if (cmd.startsWith("restore")) {
				if (cmd.indexOf(' ') == -1) {
					shellError("hm? What name?");
					return false;
				}
				String name = cmd.substring(cmd.indexOf(' ') + 1);
				String statedir = "/home/lcapitulino/.emu8086-states";
				emulator.loadState(statedir, name);
			} else if (cmd.startsWith("help")) {
				showHelp();
			} else {
				shellError("unknown command: " + cmd);
			}
		} catch (Exception e) {
			shellError(e.getMessage());
		}

		return false;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		String file = "/dev/stdin";

		if (args.length > 0) {
			if (args.length != 2 ||
				!args[0].startsWith("-b") ||
				args[1].length() == 0) {
					shellError("emu8086 [ -b < file > ]");
			}

			// Batch mode
			file = args[1];
			batchmode = true;
		}

		fileCommandOpen(file);
		emulatorInit();

		shellPrint("Emulator is ready!");
		// main shell loop
		try {
			for (;;) {
				if (!batchmode)
					System.out.print(">>> ");
				if (process(cmdfile.readLine()) == true) {
					shellPrint("Exiting");
					cmdfile.close();
					System.exit(0);
				}
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}