
public class InvalidRegisterException extends Exception {
	private int register;
	
	public String getMessage()
	{
		return "invalid register " + register;
	}

	public InvalidRegisterException(int reg)
	{
		register = reg;
	}
}
