import java.io.*;

/**
 * Stores 8086 registers and flags status.
 * 
 * @author Luiz Fernando N. Capitulino
 */
public class ProcStatus implements Serializable {
	/**
	 * All registers contents.
	 */
	private int[] registers;

	/**
	 * flags register contents.
	 */
	private boolean[] flags_register;

	/**
	 * Write all flags registers contents to stdout.
	 */
	public void dump_flags()
	{
		String[] flag_names = { "AF", "CF", "DF", "IF",
								"OF", "PF", "SF",  "ZF" };
		StringBuilder result = new StringBuilder();
		result.append("[FLAGS] ");
	
		for (int i = 0; i < flag_names.length; i++) {
			result.append(flag_names[i] + ": ");
			if (flags_register[i])
				result.append("1 ");
			else
				result.append("0 ");
		}

		System.out.println(result.toString());
	}

	/**
	 * Write all registers (except flags) contents to stdout.
	 */
	public void dump_registers()
	{
		String[] reg_names = {  "AX", "AH", "AL",
								"CX", "CH", "CL",
								"DX", "DH", "DL",
								"BX", "BH", "BL",
								"SI", "DI", "ES",
								"SP", "BP", "SS",
								"IP", "CS", "DS" };
		StringBuilder result = new StringBuilder();

		result.append("[REGS]\n");
		for (int i = 0; i < reg_names.length; i++) {
			result.append(reg_names[i] + ": ");
			result.append("0x" + Integer.toHexString(registers[i]));
			if ((i % 3) == 2)
				result.append('\n');
			else
				result.append(" ");
		}

		System.out.print(result.toString());
	}

	/**
	 * Set register's contents.
	 * 
	 * @param reg Registers to set the content
	 * @param value Content
	 */
	public void setRegister(Reg8086 reg, int value)
	{
		registers[reg.ordinal()] = value;
	}

	/**
	 * Get register contents.
	 * 
	 * @param reg Register to get the content
	 * @return Register's content
	 */
	public int getRegister(Reg8086 reg)
	{
		return registers[reg.ordinal()];
	}

	/**
	 * Set a 16-bit register and its high and low byte
	 * 
	 * @param reg register to be set
	 * @param value value to set
	 * @throws InvalidRegisterException 
	 */
	public void setRegister_X(Reg8086 reg, int value) throws InvalidRegisterException
	{
		// set 16-bit value
		setRegister(reg, value);
	
		// set 8-bit high byte
		int high = (value & 0xFF00) >> 8;
		Reg8086 regh = Reg8086.convert(reg.ordinal() + 1);
		setRegister(regh, high);

		// set 8-bit low byte
		int low = value & 0xFF;
		Reg8086 regl = Reg8086.convert(reg.ordinal() + 2);
		setRegister(regl, low);
	}

	/**
	 * Set a 8-bit "high" register and its X
	 * 
	 * @param reg register to be set
	 * @param value value to set
	 * @throws InvalidRegisterException 
	 */
	public void setRegister_H(Reg8086 reg, int value) throws InvalidRegisterException
	{
		// set 8-bit high byte
		setRegister(reg, value);

		// set 16-bit value
		Reg8086 regx = Reg8086.convert(reg.ordinal() - 1);
		setRegister(regx, value);
	}

	/**
	 * Set a 8-bit "low" register and its X
	 * 
	 * @param reg register to be set
	 * @param value value to set
	 * @throws InvalidRegisterException 
	 */
	public void setRegister_L(Reg8086 reg, int value) throws InvalidRegisterException
	{
		// set 8-bit low byte
		setRegister(reg, value);

		// set 16-bit value
		Reg8086 regx = Reg8086.convert(reg.ordinal() - 2);
		setRegister(regx, value);
	}

	/**
	 * Set flags register flag.
	 * 
	 * @param flag flag to set.
	 */
	public void setFlags(RegFlags flag)
	{
		flags_register[flag.ordinal()] = true;
	}

	/**
	 * Unset flags register flag.
	 * 
	 * @param flag flag to unset.
	 */
	public void unsetFlags(RegFlags flag)
	{
		flags_register[flag.ordinal()] = false;
	}

	/**
	 * Get flags register flag.
	 * 
	 * @param flag flag to get.
	 * @return Flag content.
	 */
	public boolean getFlag(RegFlags flag)
	{
		return flags_register[flag.ordinal()];
	}

	/**
	 * Constructor.
	 */
	public ProcStatus()
	{
		registers = new int[21];
		flags_register = new boolean[8];
	}
}