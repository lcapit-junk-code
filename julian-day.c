/* Show the day number (julian calendar) */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int main(void)
{
	char datastr[16];
	struct tm *tm;
	struct tm date;
	time_t timep;

	timep = time(NULL);
	tm = localtime(&timep);
	if (!tm) {
		perror("localtime()");
		exit(1);
	}

	memset(&date, 0, sizeof(date));
	date.tm_yday = tm->tm_yday;
	date.tm_year = tm->tm_year;

	datastr[0] = '\0';
	strftime(datastr, sizeof(datastr), "%j/%Y", &date);
	printf("%s\n", datastr);
	return 0;
}
