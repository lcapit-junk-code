//
// Prim algorithm implementation in C#, for more info have a
// a look at:
//
// 	http://en.wikipedia.org/wiki/Prim_algorithm
//
// Licensed under the GPLv2 only.
//
// Luiz Fernando N. Capitulino
// <lcapitulino@gmail.com>

using System;
using System.IO;
using System.Collections.Generic;

namespace Prim
{

class Edge
{
	private int cost;
	private bool border;
	private bool discarded;
	public bool Solution;
	public bool Printed;
	public bool Vacant;

	public void MarkPrinted()
	{
		Printed = true;
	}

	public void ResetPrinted()
	{
		Printed = false;
	}

	public int GetCost()
	{
		return cost;
	}

	public void MarkBorder()
	{
		border = true;
	}

	public void MarkSolution()
	{
		Solution = true;
	}

	public void discard()
	{
		discarded = true;
	}

	public bool Alive()
	{
		return (!discarded && !Solution && border);
	}

	public override string ToString()
	{
		return string.Format("{0}", GetCost());
	}

	public Edge()
	{
		Vacant = true;
	}

	public Edge(int c)
	{
		cost = c;
		Vacant = Printed = false;
		discarded = border = Solution = false;
	}
}

class EdgeMatrixQ
{
	public int Rows;
	public int Cols;
	private Edge[,] edge_matrix;

	public Edge GetEdgeAt(int row, int col)
	{
		return edge_matrix[row, col];
	}

	public void Add(Edge edge, int row, int col)
	{
		edge_matrix[row, col] = edge_matrix[col, row] = edge;
	}

	private void AllocMatrix()
	{
		Edge edge = new Edge();

		edge_matrix = new Edge[Rows, Cols];

		for (int i = 0; i < Rows; i++)
			for (int j = 0; j < Cols; j++)
				edge_matrix[i,j] = edge;
	}

	public void ResetPrinted()
	{
		foreach(Edge e in edge_matrix)
			e.ResetPrinted();
	}

	public EdgeMatrixQ(int rows)
	{
		Rows = Cols = rows;
		AllocMatrix();
	}
}

class Node
{
	private int cost;
	private string name;
	public bool Connected;

	public void SetCost(int c)
	{
		cost = c;
	}

	public int GetCost()
	{
		return cost;
	}

	public string GetName()
	{
		return name;
	}

	public void MarkConnected()
	{
		Connected = true;
	}

	public override string ToString()
	{
		return string.Format("{0}: {1}", GetName(), GetCost());
	}

	public Node(string n)
	{
		name = n.ToUpper();
		Connected = false;
		SetCost(int.MaxValue); // infinite
	}
}

class NodeList : List<Node>
{
	public int IndexOf(string name)
	{
		Node node = Find(delegate(Node n) {
				return n.GetName() == name.ToUpper(); });
		if (node == null)
			return -1;
		return IndexOf(node);
	}

	public bool Exists(Node node)
	{
		return Exists(delegate(Node n) {
				return n.GetName() == node.GetName(); });
	}

	public void Add(string[] line)
	{
		for (int i = 0; i < 2; i++){
			Node node = new Node(line[i]);
			if (!Exists(node))
				Add(node);
		}
	}

	public override string ToString()
	{
		string ret = "";

		foreach(Node n in this)
			ret += string.Format("{0}\n", n);

		return ret;
	}

	public bool AllConnected()
	{
		foreach(Node node in this)
			if (!node.Connected)
				return false;
		return true;
	}
}

class Graph
{
	private Node current;
	private NodeList nodes;
	private EdgeMatrixQ graph;
	private List<string> plot_append;
	private enum LineCont { NODE1, NODE2, EDGE };

	public void Plot(string filename)
	{
		TextWriter tw = new StreamWriter(filename);

		tw.WriteLine("digraph A {\n");
		tw.WriteLine("nodesep = 1.0");
		tw.WriteLine("node [ shape = box, width = .20, height = .20 ]");
		tw.WriteLine("edge [ dir = none, color = gray ]");
		tw.WriteLine("graph[page=\"8.5,11\",size=\"7.5,7\",ratio=fill,center=1]");
		tw.WriteLine("\n");

		for (int i = 0; i < graph.Rows; i++) {
			for (int j = 0; j < graph.Cols; j++) {
				Edge edge = graph.GetEdgeAt(i, j);

				if (edge.Vacant || edge.Printed)
					continue;

				tw.Write("{0}->{1} [ label = {2}",
						nodes[i].GetName(), nodes[j].GetName(),
						edge.GetCost());

				if (edge.Solution)
					tw.WriteLine(", color = green ]");
				else
					tw.WriteLine(" ]");

				edge.MarkPrinted();
			}
		}

		tw.Write("\n");

		foreach(string line in plot_append)
			tw.WriteLine(line);

		tw.WriteLine("\n}");
		tw.Close();

		graph.ResetPrinted();
	}

	private bool CheckLine(string[] line)
	{
		if (line.Length <= 0 || line.Length > 3)
			return false;

		if (line[(int)LineCont.NODE1] ==
				line[(int)LineCont.NODE2])
			return false;

		int edge = Convert.ToInt32(line[(int)LineCont.EDGE]);
		if (edge < 0)
			return false;

		return true;
	}

	private void AddEdge(string[] content)
	{
		int row, col;
		Edge edge = new Edge(Convert.ToInt32(content[2]));

		row = nodes.IndexOf(content[0]);
		col = nodes.IndexOf(content[1]);

		graph.Add(edge, row, col);
	}

	public void ShowNodes()
	{
		Console.Write(nodes);
	}

	public void ShowGraph(bool show_Solution)
	{
		for (int i = 0; i < graph.Rows; i++) {
			for (int j = 0; j < graph.Cols; j++) {
				Edge edge = graph.GetEdgeAt(i, j);

				if (edge.Vacant || edge.Printed)
					continue;

				if (show_Solution && !edge.Solution)
					continue;

				Console.WriteLine("{0} {1} {2}",
						nodes[i].GetName(),
						nodes[j].GetName(),
						edge.GetCost());
				edge.MarkPrinted();
			}
		}

		graph.ResetPrinted();
		Console.Write("\n");
	}

	private void SpreadCosts()
	{
		int lin;

		lin = nodes.IndexOf(current.GetName());

		for (int j = 0; j < nodes.Count; j++) {
			Node node = nodes[j];
			Edge edge = graph.GetEdgeAt(lin, j);

			if (edge.Vacant || edge.Solution)
				continue;

			if (edge.GetCost() < node.GetCost()) {
				node.SetCost(edge.GetCost());
				edge.MarkBorder();
			} else {
				edge.discard();
			}
		}
	}

	private void ChangeCurrent()
	{
		int lin, col;
		Edge smaller = new Edge(int.MaxValue);

		lin = col = -1;

		for (int i = 0; i < graph.Rows; i++) {
			for (int j = 0; j < graph.Cols; j++) {
			 	Edge edge = graph.GetEdgeAt(i, j); 

				if (edge.Vacant || !edge.Alive())
					continue;

				if (edge.GetCost() < smaller.GetCost()) {
					smaller = edge;
					lin = i;
					col = j;
				}
			}
		}

		current = nodes[lin].Connected ? nodes[col] : nodes[lin];

		current.MarkConnected();
		smaller.MarkSolution();
	}

	private void SetFirstCurrent()
	{
		current = nodes[0];
		current.SetCost(0);
		current.MarkConnected();
	}

	public void MinimumSpanningTree()
	{
		while (true) {
			SpreadCosts();
			ChangeCurrent();
			if (nodes.AllConnected())
				return;
		}
	}

	private void ReadGraph(string[] file_contents)
	{
		int line_nr = 0;

		plot_append = new List<string>();

		foreach (string line in file_contents) {
			line_nr++;

			if (line.Length == 0 || line[0] == '#')
				continue;

			if (line[0] == '{') {
				plot_append.Add(line);
				continue;
			}

			string[] info = line.Split();
			if (CheckLine(info) == false) {
				Console.WriteLine("ERROR on line {0}", line_nr);
				System.Environment.Exit(1);
			}

			nodes.Add(info);
		}
	}

	private void BuildGraph(string[] file_contents)
	{
		graph = new EdgeMatrixQ(nodes.Count);

		foreach(string line in file_contents) {
			if (line.Length == 0 || line[0] == '#' ||
					line[0] == '{')
				continue;
			AddEdge(line.Split());
		}
	}

	public Graph(string filename)
	{
		string[] lines;

		nodes = new NodeList();
		lines = File.ReadAllLines(filename);

		ReadGraph(lines);
		BuildGraph(lines);
		SetFirstCurrent();
	}
}

class MainClass
{
	private static void Usage()
	{
		Console.WriteLine("usage: prim < graph-file > [ -p ]");
	}

	public static void Main(string[] args)
	{
		Graph graph;

		if (args.Length == 0 || args.Length > 2) {
			Usage();
			Environment.Exit(1);
		}

		graph = new Graph(args[0]);

		graph.ShowGraph(false);

		graph.MinimumSpanningTree();

		graph.ShowGraph(true);
		graph.ShowNodes();

		if (args.Length == 2)
			graph.Plot("graph.dot");
	}
}

} // end namespace Prim
