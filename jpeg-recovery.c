/*
 * A really stupid jpeg file recover for Linux.
 *
 * You do not want to use this, you want to use photorec instead:
 *
 * 	http://www.cgsecurity.org/wiki/PhotoRec
 *
 * This program is licensed under the GPLv2.
 *
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>

// Start of Image, according to the spec
#define IMG_START 0xd8ff

// End of Image, according to hexdump :P
// Some images do not have the EOI specified by the standard (0xffd9),
// the value bellow worked for a lot of images.
#define IMG_END 0xff3f

static int open_file(void)
{
	char filename[12];
	static int counter = 0;

	snprintf(filename, sizeof(filename), "img%d.jpeg", counter++);
	return open(filename, O_RDWR | O_CREAT, 0444);
}

int main(int argc, char *argv[])
{
	ssize_t bytes;
	unsigned short c;
	int found, amount, fd, fd2;

	if (argc != 2) {
		fprintf(stderr, "you have to specify the device name\n");
		exit(1);
	}

	fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		perror("open()");
		exit(1);
	}

	found = amount = 0;

	while (read(fd, &c, 2) > 0) {
		if (c == IMG_START) {
			found = 1;
			fd2 = open_file();
			if (fd2 < 0) {
				perror("open_file()");
				exit(1);
			}
			fprintf(stderr, "Found %d\r", amount++);
		}

		if (found) {
			bytes = write(fd2, &c, 2);
			if (bytes < 0) {
				perror("write()");
				exit(1);
			}

			if (c == IMG_END) {
				close(fd2);
				found = 0;
			}
		}
	}

	fprintf(stderr, "\n");
	return 0;
}
