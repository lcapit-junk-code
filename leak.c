/*
 * Leaks memory in SIZE chunks
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define SIZE (1024 * 1024)

int main(void)
{
	char *p;

	for (;;) {
		p = malloc(SIZE);
		if (!p) {
			perror("malloc()");
			exit(1);
		}
		memset(p, 0, SIZE);
	}

	return 0;
}
