#!/usr/bin/python
#
# Python version of the CEP browser program
#
# Note: dict type in python is implemented using a hash
# table.
#
# Luiz Fernando N. Capitulino
# <lcapitulino@gmail.com>

import sys

class Address:
	def dump(self):
		print '%s;%s;%s;%s;%s' % \
		(self.__cep, self.__uf, self.__cidade, \
		self.__bairro, self.__rua)
	
	def get_cep(self):
		return self.__cep

	def __cmp__(self, y):
		return self.__cep == y;

	def __repr__(self):
		str = '\n'
		str += 'CEP:    ' + self.cep + '\n'
		str += 'UF:     ' + self.uf + '\n'
		str += 'Cidade: ' + self.cidade + '\n'
		str += 'Bairro: ' + self.bairro + '\n'
		str += 'Rua:    ' + self.rua + '\n'
		return str

	def __init__(self, line):
		items = line.split(";")
		self.__rua = items.pop()
		self.__bairro = items.pop()
		self.__cidade = items.pop()
		self.__uf = items.pop()
		self.__cep = items.pop()

class AddressList(list):
	def lookup(self, cep):
		for entry in self:
			if entry == cep:
				return entry
		return None

	def __init__(self, contents):
		list.__init__(self)
		for line in contents:
			addr = Address(line.strip('\n\r'))
			self.append(addr)

class AddressDict(dict):
	lookup = dict.get

	def __init__(self, contents):
		dict.__init__(self)
		for line in contents:
			addr = Address(line.strip('\n\r'))
			self.setdefault(addr.get_cep(), addr)

def read_db(dbpath):
	db = open(dbpath)
	contents = db.readlines()
	db.close()
	return contents

def do_test(addresses, contents):
	failed = False
	for line in contents:
		cep = line[:line.find(';')]
		ret = addresses.lookup(cep)
		if ret is None:
			failed = True
			break
	if failed:
		print 'FAILED'
	else:
		print 'PASSED'


def shell(addresses):
	while True:
		sys.stdout.write('>>> ')
		line = sys.stdin.readline()

		if len(line) == 0:
			print
			return

		if line == '\n':
			continue

		line = line.strip('\n')
		entry = addresses.lookup(line)
		if entry is not None:
			print entry
		else:
			print line + ' : not found'
def usage():
	print 'usage: cep-browser.py [ -l | -d ] [ -t ] < filename >'
	sys.exit(1)

def main():

	if len(sys.argv) < 3 or len(sys.argv) > 4:
		usage()

	opt = sys.argv[1]
	if opt != "-l" and opt != "-d":
		usage()

	run_test = False
	file = sys.argv[2]

	if sys.argv[2] == "-t":
		run_test = True
		file = sys.argv[3]

	contents = read_db(file)

	if opt == "-l":
		addresses = AddressList(contents)
	else:
		addresses = AddressDict(contents)

	if run_test is True:
		do_test(addresses, contents)
		sys.exit(0)

	shell(addresses)

if __name__ == "__main__":
	main()
