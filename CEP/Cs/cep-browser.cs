//
// C# Version of the CEP browser program
//
// Luiz Fernando N. Capitulino
// <lcapitulino@gmail.com>

using System;
using System.IO;
using System.Collections.Generic;

namespace Cep
{

class Address
{
	private string rua;
	private string bairro;
	private string cidade;
	private string uf;
	private string cep;

	public string GetCep()
	{
		return cep;
	}

	public void Dump()
	{
		Console.WriteLine("{0};{1};{2};{3};{4}", cep, uf, cidade,
				bairro, rua);
	}

	public override string ToString()
	{
		string fmt = "";

		fmt += "CEP:    {0}\n";
		fmt += "UF:     {1}\n";
		fmt += "Cidade: {2}\n";
		fmt += "Bairro: {3}\n";
		fmt += "Rua:    {4}";

		return string.Format(fmt, cep, uf, cidade, bairro, rua);
	}

	public Address(string line)
	{
		string[] items;
		
		items = line.Split(';');

		cep = items[0];
		uf = items[1];
		cidade = items[2];
		bairro = items[3];
		rua = items[4];
	}
}

class Program
{
	private static Dictionary<string, Address> Addrs;

	static void ReadDb(string path)
	{
		string line;

		StreamReader st = File.OpenText(path);
		Addrs = new Dictionary<string, Address>();

		while ((line = st.ReadLine()) != null) {
			Address p = new Address(line);
			Addrs.Add(p.GetCep(), p);
		}

		st.Close();
	}

	static void Shell()
	{
		string line;

		while (true) {
			Console.Write(">>> ");
			line = Console.ReadLine();

			if (line.Length == 0)
				continue;

			if (line == "exit" || line == "quit")
				return;

			try {
				Console.WriteLine("\n{0}\n", Addrs[line]);
			}
			catch (KeyNotFoundException) {
				Console.WriteLine("{0}: not found", line);
			}
		}
	}

	static void Main(string[] args)
	{
		if (args.Length != 1) {
			Console.WriteLine("cep-browser.exe < file >");
			Environment.Exit(1);
		}

		ReadDb(args[0]);
		Shell();
	}
}

} // end of namspace Cep
