#
# Run with: awk -f cep-search.awk -v cep=cep-number filename
#

BEGIN {
	if (!cep) {
		printf("usage: awk -f cep-search.awk ")
		printf("-v cep=cep-number filename\n")
	}

	FS = ";"
	found = 0
}

$1 == cep {
	printf("\nCEP: %s\nEstado: %s\nCidade: %s\n", $1, $2, $3)
	printf("Bairro: %s\nRua: %s\n", $4, $5)
	found = 1
}

END {
	if (!found)
		printf("%s: not found", cep)
	printf("\n")
}
