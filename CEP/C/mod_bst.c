#include <stdio.h>
#include <stdlib.h>

#include "misc.h"
#include "module.h"
#include "mod_bst.h"

struct node {
	void *data;
	struct node *left;
	struct node *right;
};

struct bs_tree {
	int size;
	struct node *root;
	int (*compare)(const void *key1, const void *key2);
	void (*destroy)(void *data);
	void (*dump)(const void *data);
};

static struct bs_tree cep_bs_tree;

static struct node *bst_node_alloc(void *data)
{
	struct node *new;

	new = malloc(sizeof(*new));
	if (!new)
		fatal("Could not allocate memory");

	new->data = data;
	new->left = new->right = NULL;

	return new;
}

static struct node *__bst_insert(void *data, struct node *node)
{
	int cmpval;

	if (!node)
		return bst_node_alloc(data);

	cmpval = cep_bs_tree.compare(data, node->data);
	if (cmpval < 0) {
		/* go to the left */
		node->left = __bst_insert(data, node->left);
	} else if (cmpval > 0) {
		/* go to the right */
		node->right = __bst_insert(data, node->right);
	}

	/* we ignore duplicates */

	return node;
}

static void bst_insert(void *data)
{
	cep_bs_tree.root = __bst_insert(data, cep_bs_tree.root);
	cep_bs_tree.size++;
}

static void *lookup(const void *data, struct node *node)
{
	void *ret;
	int cmpval;

	if (!node) {
		/* not found */
		return NULL;
	}

	cmpval = cep_bs_tree.compare(data, node->data);
	if (cmpval < 0) {
		/* move to the left */
		ret = lookup(data, node->left);
	} else if (cmpval > 0) {
		/* move to the right */
		ret = lookup(data, node->right);
	} else {
		/* found */
		ret = node->data;
	}

	return ret;
}

static void *bst_lookup(const void *data)
{
	return lookup(data, cep_bs_tree.root);
}

static void bst_init(int (*compare)(const void *key1, const void *key2),
		     void (*destroy)(void *data),
		     void (*dump)(const void *data))
		     
{
	cep_bs_tree.size = 0;
	cep_bs_tree.root = NULL;
	cep_bs_tree.compare = compare;
	cep_bs_tree.destroy = destroy;
	cep_bs_tree.dump  = dump;
}

static size_t bst_size(void)
{
	return cep_bs_tree.size;
}

static void destroy(struct node *node)
{
	if (!node)
		return;

	destroy(node->left);
	destroy(node->right);

	cep_bs_tree.destroy(node->data);
	free(node);
}

static void bst_destroy(void)
{
	destroy(cep_bs_tree.root);
}

static void dump(struct node *node)
{
	if (!node)
		return;

	dump(node->left);

	cep_bs_tree.dump(node->data);

	dump(node->right);
}

static void bst_dump(void)
{
	dump(cep_bs_tree.root);
}

struct module bst_module = {
	.mod_name    = "BINARY SEARCH TREE",
	.mod_init    = bst_init,
	.mod_destroy = bst_destroy,
	.mod_insert  = bst_insert,
	.mod_lookup  = bst_lookup,
	.mod_dump    = bst_dump,
	.mod_size    = bst_size,
};
