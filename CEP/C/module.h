/*
 * This header file has one of the main data structures used by the
 * cep-browser program: the struct module.
 *
 * Each module has to define its own struct module and export it
 * so that cep-browser can use the module.
 * 
 * Licensed under the GPLv2.
 */
#ifndef __MODULE_H
#define __MODULE_H

struct module {
	const char *mod_name;
	void (*mod_init)(int  (*compare)(const void *key1, const void *key2),
			 void (*destroy)(void *data),
			 void (*dump)(const void *data));
	void (*mod_destroy)(void);
	void (*mod_insert)(void *data);
	void *(*mod_lookup)(const void *data);
	void (*mod_dump)(void);
	size_t (*mod_size)(void);
};

#endif /* __MODULE_H */
