/*
 * Exports struct list_module.
 * 
 * Licensed under the GPLv2.
 */
#ifndef __LIST_H
#define __LIST_H

#include "module.h"

extern struct module list_module;

#endif /* __LIST_H */
