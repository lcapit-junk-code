/*
 * cep-browser: Address search based on CEP.
 * 
 * A CEP is a sequence of numbers which correspond to a specific
 * address. It's the system of postal codes used by the Correios
 * (Brazil's Postal Service).
 * 
 * This program supports more than one data structure to store
 * and search the addresses, it can be used to experiment
 * different data structures and algorithms.
 * 
 * This program is lincesed under the GPLv2, see COPYING for
 * more information.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#include "db.h"
#include "misc.h"
#include "module.h"
#include "mod_list.h"
#include "mod_avl.h"
#include "mod_bst.h"
#include "mod_aa.h"

/* module being used */
static struct module *cep_module;

/* usage(): print usage info */
static void usage(void)
{
	printf(
	       "usage: cep-browser [options] < file >\n\n"
	       "  options:\n"
	       "            -h  this text\n"
	       "            -l  use list module\n"
	       "            -a  use AVL tree module\n"
	       "            -b  use BST tree module\n"
	       "            -A  use AA tree module\n"
	       "            -D  dump the database using choosen module\n"
	       "            -r  raw database dump\n"
	       "            -t  run test for selected data structure\n"
	       "\n"
	       );
}

/* cep_compare(): return -1 if key1 < key2, 1 if key1 > key2 and
 * 0 if key1 == key2. Where key is the struct db_entry and what
 * gets compared is its cep member */
static int cep_compare(const void *key1, const void *key2)
{
	const struct db_entry *entry1, *entry2;

	entry1 = (const struct db_entry *) key1;
	entry2 = (const struct db_entry *) key2;

	if (entry1->cep < entry2->cep)
		return -1;
	else if (entry1->cep > entry2->cep)
		return 1;
	return 0;
}

/* cep_destroy(): wrapper to db_free_entry() which can be used by
 * the module's destroy function */
static void cep_destroy(void *data)
{
	struct db_entry *entry;

	entry = (struct db_entry *) data;
	db_free_entry(&entry);
}

/* cep_dump(): wrapper for db_dump_entry() which can be used by
 the module's dump function */
static void cep_dump(const void *data)
{
	db_dump_entry((const struct db_entry *) data);
}

/* Begin HACK
 * 
 * cep_is_duplicated(): return a positive integer if 'cep' is
 * in the duplicated list, otherwise return 0.
 *
 * This function should only be used in do_test().
 * 
 * The problem here is that the Brazil's full CEP list file has
 * some entries which have the same CEP number for different
 * addresses, but the module's insert() function will only insert
 * the first occurence of the duplicated entry, silently ignoring
 * the others.
 * 
 * This can make the do_test() function fail because there won't
 * be a match for the sencond entry.
 * 
 * So we just ignore duplicated CEPs in do_test().
 * 
 * End HACK
 */
static inline int cep_is_duplicated(int cep)
{
	switch (cep) {
	case 72821470:
	case 72855007:
	case 72855034:
	case 72855140:
	case 72856560:
	case 72859134:
	case 29965990:
		return 1;
	}

	return 0;
}

/* do_test(): run a simple test to assure that basic functionality
 * is working. The test resets the database file and does a lookup()
 * for each CEP in the file. */
static void do_test(FILE *db)
{
	void *ret;
	int failed;
	struct db_entry *entry;

	rewind(db);

	printf("%s LOOKUP TEST: ", cep_module->mod_name);

	failed = 0;
	for (;;) {
		entry = db_next_entry(db);
		if (!entry)
			break;

		if (cep_is_duplicated(entry->cep))
			continue;

		ret = cep_module->mod_lookup((void *) &entry->cep);
		if (!ret)
			failed = 1;
		if (ret && !db_entries_match(entry, (struct db_entry *) ret))
			failed = 1;

		db_free_entry(&entry);
	}

	if (failed)
		printf("FAILED\n");
	else
		printf("PASSED\n");
}

/* user_command(): executes a command entered by the user in the
 * interactive shell. Return 1 if the command was executed or
 * 0 if the command doesn't exist. */
static int user_command(const char *cmd)
{
	if (!strcmp(cmd, "size")) {
		printf("%d\n", cep_module->mod_size());
		return 1;
	}

	return 0;
}

static int eof_input(FILE *stream, char c)
{
	(void) stream;

	return (c == '\n' ? 1 : 0);
}

static char *user_shell(const char *prompt)
{
	printf("%s", prompt);
	return read_chars(stdin, eof_input);
}

int main(int argc, char *argv[])
{
	FILE *db;
	struct db_entry *entry;
	int opt, verbose, raw_dump, dump, run_test;

	cep_module = NULL;
	raw_dump = dump = run_test = verbose = 0;

	while ((opt = getopt(argc, argv, "hlAabrDtv")) != -1 ) {
		switch (opt) {
		case 'h':
			usage();
			exit(0);
		case 'l':
			cep_module = &list_module;
			break;
		case 'a':
			cep_module = &avl_module;
			break;
		case 'A':
			cep_module = &aa_module;
			break;
		case 'b':
			cep_module = &bst_module;
			break;
		case 'r':
			raw_dump = 1;
			break;
		case 'D':
			dump = 1;
			break;
		case 't':
			run_test = 1;
			break;
		case 'v':
			verbose = 1;
			break;
		default:
			usage();
			exit(1);
		}
	}

	if (optind >= argc)
		fatal("you have to specify a filename");

	if ((dump && !cep_module) || (!cep_module && !raw_dump))
		fatal("you have to select a module");

	if (cep_module && raw_dump)
		fatal("you can't use a module to do raw database dump");

	db = fopen(argv[optind], "r");
	if (!db) {
		fatal("can't open file '%s': %s", argv[optind],
		      strerror(errno));
	}

	if (raw_dump) {
		db_dump_csv(db);
		fclose(db);
		exit(0);
	}

	/*
	 * Setup done, here we go
	 */

	cep_module->mod_init(cep_compare, cep_destroy, cep_dump);

	if (verbose)
		putchar('\n');

	/* read all entries */
	for (;;) {
		entry = db_next_entry(db);
		if (!entry)
			break;

		cep_module->mod_insert((void *) entry);

		if (verbose) {
			printf("-> reading entries: %d\r",
			       cep_module->mod_size());
		}
	}

	if (dump) {
		if (cep_module->mod_dump == NULL) {
			fatal("module '%s' doesn't support database dump",
			      cep_module->mod_name);
		}

		if (run_test) {
			fatal("you can't run the test if you want to dump "
			      "the database");
		}

		cep_module->mod_dump();
		fclose(db);
		goto out;
	}

	if (run_test) {
		do_test(db);
		fclose(db);
		goto out;
	}

	fclose(db);

	setlinebuf(stdout);

	printf("\n-> using %s module [ with %d entries ]\n\n",
	       cep_module->mod_name, cep_module->mod_size());

	for (;;) {
		void *data;
		char *line;
		unsigned int cep;

		line = user_shell(">>> ");
		if (!line) {
			putchar('\n');
			break;
		}

		if (line[0] == '\0') {
			free(line);
			continue;
		}

		if (user_command(line)) {
			free(line);
			continue;
		}

		cep = strtol(line, NULL, 10);

		data = cep_module->mod_lookup((void *) &cep);
		if (data)
			db_print_entry((struct db_entry *) data);
		else
			printf("%s: not found\n", line);

		free(line);
	}

out:
	if (cep_module->mod_destroy)
		cep_module->mod_destroy();
	return 0;
}
