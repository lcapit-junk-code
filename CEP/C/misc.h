#ifndef __MISC_H
#define __MISC_H

#include <stdio.h>

void fatal(const char *err, ...);
char *read_chars(FILE *db, int (*eof_chars)(FILE *db, char c));

#endif /* __MISC_H */
