#include <stdio.h>
#include <stdlib.h>

#include "misc.h"
#include "module.h"
#include "mod_avl.h"

enum avl_factor {
	AVL_RGT_HEAVY = -1,
	AVL_BALANCED,
	AVL_LFT_HEAVY
};

struct node {
	void *data;
	enum avl_factor factor;
	struct node *left;
	struct node *right;
};

struct avl_tree {
	int size;
	struct node *root;
	int (*compare)(const void *key1, const void *key2);
	void (*destroy)(void *data);
	void (*dump)(const void *data);
};

static struct avl_tree cep_avl_tree;

static struct node *avl_node_alloc(void *data)
{
	struct node *new;

	new = malloc(sizeof(*new));
	if (!new)
		fatal("Could not allocate memory");

	new->data = data;
	new->left = new->right = NULL;
	new->factor = AVL_BALANCED;

	return new;
}

static struct node *rotate_left(struct node *node)
{
	struct node *left, *grandchild;

	left = node->left;

	if (left->factor == AVL_LFT_HEAVY) {
		/* LL rotation */
		node->left = left->right;
		left->right = node;
		node->factor = AVL_BALANCED;
		left->factor = AVL_BALANCED;
		node = left;
	} else {
		/* LR rotation */
		grandchild = left->right;
		left->right = grandchild->left;
		grandchild->left = left;
		node->left = grandchild->right;
		grandchild->right = node;

		switch (grandchild->factor) {
		case AVL_LFT_HEAVY:
			node->factor = AVL_RGT_HEAVY;
			left->factor = AVL_BALANCED;
			break;
		case AVL_BALANCED:
			node->factor = AVL_BALANCED;
			left->factor = AVL_BALANCED;
			break;
		case AVL_RGT_HEAVY:
			node->factor = AVL_BALANCED;
			left->factor = AVL_LFT_HEAVY;
			break;
		default:
			abort();
		}

		grandchild->factor = AVL_BALANCED;
		node = grandchild;
	}

	return node;
}

static struct node *rotate_right(struct node *node)
{
	struct node *right, *grandchild;

	right = node->right;

	if (right->factor == AVL_RGT_HEAVY) {
		/* RR rotation */
		node->right = right->left;
		right->left = node;
		node->factor = AVL_BALANCED;
		right->factor = AVL_BALANCED;
		node = right;
	} else {
		/* RL rotation */
		grandchild = right->left;
		right->left = grandchild->right;
		grandchild->right = right;
		node->right = grandchild->left;
		grandchild->left = node;

		switch (grandchild->factor) {
		case AVL_LFT_HEAVY:
			node->factor = AVL_BALANCED;
			right->factor = AVL_RGT_HEAVY;
			break;
		case AVL_BALANCED:
			node->factor = AVL_BALANCED;
			right->factor = AVL_BALANCED;
			break;
		case AVL_RGT_HEAVY:
			node->factor = AVL_LFT_HEAVY;
			right->factor = AVL_BALANCED;
			break;
		}

		grandchild->factor = AVL_BALANCED;
		node = grandchild;
	}

	return node;
}

static struct node *__avl_insert(void *data, struct node *node, int *balanced)
{
	int cmpval;

	cmpval = cep_avl_tree.compare(data, node->data);
	if (cmpval < 0) {
		/* move to the left */
		if (!node->left) {
			node->left = avl_node_alloc(data);
			*balanced = 0;
		} else {
			node->left = __avl_insert(data, node->left, balanced);
		}

		if (!(*balanced)) {
			switch (node->factor) {
			case AVL_LFT_HEAVY:
				node = rotate_left(node);
				*balanced = 1;
				break;
			case AVL_BALANCED:
				node->factor = AVL_LFT_HEAVY;
				break;
			case AVL_RGT_HEAVY:
				node->factor = AVL_BALANCED;
				*balanced = 1;
				break;
			}
		}
	} else if (cmpval > 0) {
		/* move to the right */
		if (!node->right) {
			node->right = avl_node_alloc(data);
			*balanced = 0;
		} else {
			node->right = __avl_insert(data, node->right,balanced);
		}

		if (!(*balanced)) {
			switch (node->factor) {
			case AVL_LFT_HEAVY:
				node->factor = AVL_BALANCED;
				*balanced = 1;
				break;
			case AVL_BALANCED:
				node->factor = AVL_RGT_HEAVY;
				break;
			case AVL_RGT_HEAVY:
				node = rotate_right(node);
				*balanced = 1;
			}
		}
	} else {
		/* duplicated */
		*balanced = 1;
	}

	return node;
}

static void avl_insert(void *data)
{
	int balanced;

	cep_avl_tree.size++;

	if (!cep_avl_tree.root) {
		cep_avl_tree.root = avl_node_alloc(data);
		return;
	}

	balanced = 0;

	cep_avl_tree.root = __avl_insert(data, cep_avl_tree.root, &balanced);
}

static void *lookup(const void *data, struct node *node)
{
	void *ret;
	int cmpval;

	if (!node) {
		/* not found */
		return NULL;
	}

	cmpval = cep_avl_tree.compare(data, node->data);
	if (cmpval < 0) {
		/* move to the left */
		ret = lookup(data, node->left);
	} else if (cmpval > 0) {
		/* move to the right */
		ret = lookup(data, node->right);
	} else {
		/* found */
		ret = node->data;
	}

	return ret;
}

static void *avl_lookup(const void *data)
{
	return lookup(data, cep_avl_tree.root);
}

static void avl_init(int (*compare)(const void *key1, const void *key2),
		     void (*destroy)(void *data),
		     void (*dump)(const void *data))
		     
{
	cep_avl_tree.size = 0;
	cep_avl_tree.root = NULL;
	cep_avl_tree.compare = compare;
	cep_avl_tree.destroy = destroy;
	cep_avl_tree.dump  = dump;
}

static size_t avl_size(void)
{
	return cep_avl_tree.size;
}

static void destroy(struct node *node)
{
	if (!node)
		return;

	destroy(node->left);
	destroy(node->right);

	cep_avl_tree.destroy(node->data);
	free(node);
}

static void avl_destroy(void)
{
	destroy(cep_avl_tree.root);
}

static void dump(struct node *node)
{
	if (!node)
		return;

	dump(node->left);

	cep_avl_tree.dump(node->data);

	dump(node->right);
}

static void avl_dump(void)
{
	dump(cep_avl_tree.root);
}

struct module avl_module = {
	.mod_name    = "AVL TREE",
	.mod_init    = avl_init,
	.mod_destroy = avl_destroy,
	.mod_insert  = avl_insert,
	.mod_lookup  = avl_lookup,
	.mod_dump    = avl_dump,
	.mod_size    = avl_size,
};
