#ifndef __DB_H
#define __DB_H

#include <stdio.h>

/* exported values */
#define DB_UF_LEN  3
#define DB_CEP_LEN 8 /* defined by the Correios */

/* each line of db file */
struct db_entry {
	unsigned int cep;
	char uf[DB_UF_LEN];
	char *cidade;
	char *bairro;
	char *desc;
};

/* exported functions */
struct db_entry *db_next_entry(FILE *db);
void db_free_entry(struct db_entry **entry);
void db_dump_entry(const struct db_entry *entry);
void db_print_entry(const struct db_entry *entry);
void db_dump_csv(FILE *db);
int db_entries_match(const struct db_entry *ent1,
		     const struct db_entry *ent2);

#endif /* __DB_H */
