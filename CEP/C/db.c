/*
 * db: functions to retrieve data from the database file.
 * 
 * The database is a CSV list, each line has the following format
 * (pt_BR for now):
 * 
 * CEP;UF;Cidade;Bairro;Rua
 *
 * A line is called an entry, each value in the line is called
 * an info.
 * 
 * This file is licensed under the GPLv2 license.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "db.h"
#include "misc.h"

/* eof_info(): return 1 if 'c' is an end of info character,
 * otherwise return 0. */
static int eof_info(FILE *db, char c)
{
	if (c == ';' || c == '\n')
		return 1;

	if (c == '\r') {
		/* remove trailing '\n' */
		c = fgetc(db);
		if (c != '\n')
			fatal("DOS file seems corrupted");
		return 1;
	}

	return 0;
}

/* read_info(): Return the next info from the 'db' file,
 * aborts program execution on error. */
static char *read_info(FILE *db)
{
	char *info;

	info = read_chars(db, eof_info);
	if (!info)
		fatal("didn't read expected EOF from db file");

	return info;
}


/* read_buf(): read 'len' characters from 'db' file into 'buf',
 * the characters have to be part of an info (ie, they're
 * terminated by ';' or '\n') otherwise program execution is
 * aborted. */
static void read_buf(FILE *db, char *buf, size_t len)
{
	int c;
	size_t i;

	for (i = 0; i < (len - 1); i++)
		buf[i] = fgetc(db);

	buf[i] = '\0';

	c = fgetc(db);
	if (c != ';')
		fatal("db file is corrupted");
}

/* read_cep(): read CEP info from the 'db' file. */
static unsigned int read_cep(FILE *db)
{
	char cep_str[DB_CEP_LEN + 1];

	read_buf(db, cep_str, sizeof(cep_str));

	/* XXX: error check */
	return strtol(cep_str, NULL, 10);
}

/* read_uf(): read UF info from the 'db' file. */
static void read_uf(FILE *db, char *uf)
{
	read_buf(db, uf, DB_UF_LEN);
}

/* file_eof(): return 1 if next character from 'db' is EOF,
 * otherwise return 0. */
static int file_eof(FILE *db)
{
	int c;

	c = fgetc(db);
	if (c == EOF)
		return 1;

	ungetc(c, db);
	return 0;
}

/* db_next_entry(): return the next entry from 'db' file, if
 * EOF is found return NULL. */
struct db_entry *db_next_entry(FILE *db)
{
	struct db_entry *entry;

	if (file_eof(db))
		return NULL;

	entry = malloc(sizeof(*entry));
	if (!entry)
		fatal("cannot allocate memory");

	entry->cep = read_cep(db);
	read_uf(db, entry->uf);
	entry->cidade = read_info(db);
	entry->bairro = read_info(db);
	entry->desc   = read_info(db);

	return entry;
}

/* db_free_entry(): free all the memory allocated by 'entry */
void db_free_entry(struct db_entry **entry)
{
	free((*entry)->cidade);
	free((*entry)->bairro);
	free((*entry)->desc);
	free(*entry);
}

/* db_dump_entry(): dump 'entry' info in CSV format to stdout */
void db_dump_entry(const struct db_entry *entry)
{
	printf("%u;%s;%s;%s;%s\n", entry->cep, entry->uf,
	       entry->cidade, entry->bairro, entry->desc);
}

/* db_print_entry(): print 'entry' info in human readable format
 * to stdout */
void db_print_entry(const struct db_entry *entry)
{
	putchar('\n');

	printf(
	       "CEP:    %d\n"
	       "UF:     %s\n"
	       "Cidade: %s\n"
	       "Bairro: %s\n"
	       "Desc:   %s\n",
	       entry->cep, entry->uf, entry->cidade,
	       entry->bairro, entry->desc);

	putchar('\n');
}

/* db_dump_csv(): dump all entries in 'db' file in CSV format
 * to stdout */
void db_dump_csv(FILE *db)
{
	struct db_entry *entry;

	for (;;) {
		entry = db_next_entry(db);
		if (!entry)
			break;

		db_dump_entry(entry);
		db_free_entry(&entry);
	}
}

/* db_entries_match(): return 1 if ent1 == ent2, return
 * 0 otherwise */
int db_entries_match(const struct db_entry *ent1,
		     const struct db_entry *ent2)
{
	if (ent1->cep != ent2->cep)
		return 0;

	if (strcmp(ent1->uf, ent2->uf))
		return 0;

	if (strcmp(ent1->cidade, ent2->cidade))
		return 0;

	if (strcmp(ent1->bairro, ent2->bairro))
		return 0;

	if (strcmp(ent1->desc, ent2->desc))
		return 0;

	return 1;
}
