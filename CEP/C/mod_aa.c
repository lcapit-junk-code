#include <stdio.h>
#include <stdlib.h>

#include "misc.h"
#include "module.h"
#include "mod_aa.h"

struct node {
	void *data;
	int level;
	struct node *left;
	struct node *right;
};

struct aa_tree {
	int size;
	struct node *root;
	int (*compare)(const void *key1, const void *key2);
	void (*destroy)(void *data);
	void (*dump)(const void *data);
};

static struct aa_tree cep_aa_tree;

static struct node *aa_node_alloc(void *data)
{
	struct node *new;

	new = malloc(sizeof(*new));
	if (!new)
		fatal("Could not allocate memory");

	new->data = data;
	new->left = new->right = NULL;
	new->level = 1;

	return new;
}

static struct node *skew(struct node *node)
{
	struct node *left;

	if (!node || !node->left || !node->left->right)
		goto out;

	if (node->left->level == node->level) {
		/*
		 * Swap the pointers of horizontal left
		 * links.
		 */

		left = node->left;
		node->left = left->right;
		left->right = node;

		return left;
	}

out:
	return node;
}

static struct node *split(struct node *node)
{
	struct node *right;

	if (!node || !node->right ||
	    !node->right->right || !node->right->left)
		goto out;

	if (node->level == node->right->right->level) {
		/*
		 * We have three horizontal right links.
		 * Take the middle element, elevate it, and return
		 * that.
		 */

		right = node->right;
		node->right = right->left;
		right->left = node;
		right->level++;

		return right;
	}

out:
	return node;
}

static struct node *__aa_insert(void *data, struct node *node)
{
	int cmpval;

	if (!node)
		return aa_node_alloc(data);

	cmpval = cep_aa_tree.compare(data, node->data);
	if (cmpval < 0) {
		/* move to the left */
		node->left = __aa_insert(data, node->left);
	} else if (cmpval > 0) {
		/* move to the right */
		node->right = __aa_insert(data, node->right);
	} else {
		/* duplicated, just ignore it */
		goto out;
	}

	node = skew(node);
	node = split(node);

out:
	return node;
}

static void aa_insert(void *data)
{
	cep_aa_tree.root = __aa_insert(data, cep_aa_tree.root);
	cep_aa_tree.size++;
}

static void *lookup(const void *data, struct node *node)
{
	void *ret;
	int cmpval;

	if (!node) {
		/* not found */
		return NULL;
	}

	cmpval = cep_aa_tree.compare(data, node->data);
	if (cmpval < 0) {
		/* move to the left */
		ret = lookup(data, node->left);
	} else if (cmpval > 0) {
		/* move to the right */
		ret = lookup(data, node->right);
	} else {
		/* found */
		ret = node->data;
	}

	return ret;
}

static void *aa_lookup(const void *data)
{
	return lookup(data, cep_aa_tree.root);
}

static void aa_init(int (*compare)(const void *key1, const void *key2),
		    void (*destroy)(void *data),
		    void (*dump)(const void *data))
		     
{
	cep_aa_tree.size = 0;
	cep_aa_tree.root = NULL;
	cep_aa_tree.compare = compare;
	cep_aa_tree.destroy = destroy;
	cep_aa_tree.dump  = dump;
}

static size_t aa_size(void)
{
	return cep_aa_tree.size;
}

static void dump(struct node *node)
{
	if (!node)
		return;

	dump(node->left);

	cep_aa_tree.dump(node->data);

	dump(node->right);
}

static void aa_dump(void)
{
	dump(cep_aa_tree.root);
}

static void destroy(struct node *node)
{
	if (!node)
		return;

	destroy(node->left);
	destroy(node->right);

	cep_aa_tree.destroy(node->data);
	free(node);
}

static void aa_destroy(void)
{
	destroy(cep_aa_tree.root);
}

struct module aa_module = {
	.mod_name    = "AA TREE",
	.mod_init    = aa_init,
	.mod_destroy = aa_destroy,
	.mod_insert  = aa_insert,
	.mod_lookup  = aa_lookup,
	.mod_dump    = aa_dump,
	.mod_size    = aa_size,
};
