#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "misc.h"

void fatal(const char *err, ...)
{
	va_list params;

	fputs("ERROR: ", stderr);

	va_start(params, err);
	vfprintf(stderr, err, params);
	va_end(params);

	putc('\n', stderr);

	exit(1);
}

char *read_chars(FILE *db, int (*eof_chars)(FILE *db, char c))
{
	int c;
	char *info;
	size_t len, i;
	const int def_len = 12;
	const int grow_hate = 2;

	len = def_len;

	info = malloc(len);
	if (!info)
		fatal("Could not allocate memory");

	i = 0;
	for (;;) {
		for (; i < len; i++) {
			c = fgetc(db);
			if (c == EOF) {
				free(info);
				return NULL;
			} else if (eof_chars(db, c)) {
				info[i] = '\0';
				return info;
			}

			/*
			 * we probably don't want these characters
			 * in the buffer
			 */
			assert(c != '\0');
			assert(c != '\r');
			assert(c != '\n');

			info[i] = c;
		}

		len *= grow_hate;
		info = realloc(info, len);
		if (!info)
			fatal("Could not reallocate memory");
	}

	/* should never get here */
	abort();

	return NULL; /* gcc */
}
