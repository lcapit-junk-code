/*
 * mod_list: Implements linked list module for cep-browser.
 * 
 * The mod_list should be used for basic tests because its
 * main operations (insert() and lookup()) are quite slow and
 * expansive.
 * 
 * Licensed under the GPLv2 license.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "misc.h"
#include "module.h"
#include "mod_list.h"

/* list node */
struct node {
	void *data;
	struct node *next;
};

/* list data */
struct list {
	size_t size;
	struct node *head;
	struct node *tail;
	int (*compare)(const void *key1, const void *key2);
	void (*destroy)(void *data);
	void (*dump)(const void *data);
};

static struct list cep_list;

static struct node *list_node_alloc(void *data, struct node *next)
{
	struct node *new;

	new = malloc(sizeof(*new));
	if (!new)
		fatal("Could not allocate memory");

	new->data = data;
	new->next = next;

	return new;
}

static void list_init(int (*compare)(const void *key1, const void *key2),
		      void (*destroy)(void *data),
		      void (*dump)(const void *data))
{
	struct node *p;

	p = list_node_alloc(NULL, NULL);

	cep_list.head = cep_list.tail = p;
	cep_list.compare = compare;
	cep_list.destroy = destroy;
	cep_list.dump = dump;
	cep_list.size = 0;
}

static void list_destroy(void)
{
	struct node *p, *tmp;

	/* free place holder */
	tmp = cep_list.head->next;
	free(cep_list.head);

	for (p = tmp; p != NULL;) {
		tmp = p->next;

		cep_list.destroy(p->data);
		free(p);

		p = tmp;
	}
}

static size_t list_size(void)
{
	return cep_list.size;
}

static void list_insert(void *data)
{
	struct node *new;

	/*
	 * XXX: Accept duplicated entries
	 * 
	 * Insertions would be considerably slower if we
	 * call list_lookup() to catch duplicated
	 * insertions.
	 * 
	 * Instead, we just ignore duplicates and
	 * inserts new nodes in the tail of the list.
	 */

	new = list_node_alloc(data, NULL);

	cep_list.tail->next = new;
	cep_list.tail = new;
	cep_list.size++;
}

static void list_dump(void)
{
	struct node *p;

	for (p = cep_list.head->next; p != NULL; p = p->next)
		cep_list.dump(p->data);
}

static void *list_lookup(const void *data)
{
	int ret;
	struct node *p;

	for (p = cep_list.head->next; p != NULL; p = p->next) {
		ret = cep_list.compare(p->data, data);
		if (ret == 0)
			return p->data;
	}

	return NULL;
}

struct module list_module = {
	.mod_name    = "LIST",
	.mod_init    = list_init,
	.mod_destroy = list_destroy,
	.mod_insert  = list_insert,
	.mod_lookup  = list_lookup,
	.mod_dump    = list_dump,
	.mod_size    = list_size,
};
