#include <stdio.h>

#define MAX_STACK_SIZE 16

struct data {
	int code;
};

struct stack {
	struct data data[MAX_STACK_SIZE];
	int top;
};

static void stack_init(struct stack *stack)
{
	stack->top = 0;
}

static int stack_emty(const struct stack *stack)
{
	return stack->top == 0;
}

static int stack_size(const struct stack *stack)
{
	return stack->top;
}

static int stack_push(struct stack *stack, struct data *data)
{
	if (stack_size(stack) >= MAX_STACK_SIZE)
		return -1;

	stack->data[stack->top] = *data;
	stack->top++;
	return 0;
}

static int stack_pop(struct stack *stack, struct data *data)
{
	if (stack_emty(stack))
		return -1;

	stack->top--;

	if (data)
		*data = stack->data[stack->top];
	return 0;
}

static void fill_data(struct data *data, int code)
{
	data->code = code;
}

int main(void)
{
	struct stack stack;
	struct data data, data1, data2, data3, data4;

	stack_init(&stack);

	if (stack_emty(&stack))
		printf("Stack is empty\n");
	else
		printf("Stack is not empty\n");

	fill_data(&data1, 1);
	fill_data(&data2, 2);
	fill_data(&data3, 3);
	fill_data(&data4, 4);

	stack_push(&stack, &data1);
	stack_push(&stack, &data2);
	stack_push(&stack, &data3);
	stack_push(&stack, &data4);

	if (stack_emty(&stack))
		printf("Stack is empty\n");
	else
		printf("Stack is not empty\n");

	printf("Stack's size: %d\n", stack_size(&stack));

	while (stack_pop(&stack, &data) == 0)
		printf("%d\n", data.code);

	if (stack_emty(&stack))
		printf("Stack is empty\n");
	else
		printf("Stack is not empty\n");

	return 0;
}
