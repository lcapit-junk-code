/*
 * list: A static list implementation
 *
 * Note that this code uses the simplest implementation: elements are
 * _copied_ into the list's array. We could get a speedup by using
 * pointers though.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>

#define MAX_LIST_SIZE 16

struct data {
	int code;
};

struct list {
	struct data data[MAX_LIST_SIZE];
	int first;
	int last;
};

/* list_init(): Make 'list' ready to use */
static void list_init(struct list *list)
{
	list->first = list->last = 0;
}

/* list_empty(): Return 1 if 'list' is empty, 0 otherwise */
static int list_empty(const struct list *list)
{
	return list->first == list->last;
}

/* list_insert(): Insert element 'el' at the end of 'list'.
 * Returns 0 on success, -1 otherwise */
static int list_insert(struct list *list, struct data *el)
{
	if (list->last == MAX_LIST_SIZE)
		return -1;

	list->data[list->last] = *el; /* copy */
	list->last++;
	return 0;
}

/* list_remove(): Remove element at position 'pos' from 'list'
 * and returns it in 'ret'.
 * Returns 0 on success, -1 otherwise */
static int list_remove(struct list *list, int pos, struct data *ret)
{
	int i;

	if (list_empty(list) || pos < 0 || pos >= list->last)
		return -1;

	*ret = list->data[pos];

	list->last--;
	for (i = pos; i < list->last; i++)
		list->data[i] = list->data[i + 1];

	return 0;
}

/* list_print(): Print the contents of 'list' on stdout */
static void list_print(const struct list *list)
{
	int i;

	if (list_empty(list))
		return;

	for (i = list->first; i < list->last; i++)
		printf("%d ", list->data[i].code);
	printf("\n");
}

static void fill_data(struct data *data, int code)
{
	data->code = code;
}

int main(void)
{
	struct list list;
	struct data data1, data2, data3, data_ret;

	list_init(&list);

	if (list_empty(&list))
		printf("List is empty\n");
	else
		printf("List is not empty\n");

	fill_data(&data1, 0);
	fill_data(&data2, 1);
	fill_data(&data3, 2);

	list_insert(&list, &data1);
	list_insert(&list, &data2);
	list_insert(&list, &data3);

	if (list_empty(&list))
		printf("List is empty\n");
	else
		printf("List is not empty\n");

	list_print(&list);

	list_remove(&list, 0, &data_ret);
	printf("Item removed: %d\n", data_ret.code);

	list_print(&list);

	return 0;
}
