#include <stdio.h>

#define MAX_QUEUE_SIZE 16

struct data {
	int code;
};

struct queue {
	struct data data[MAX_QUEUE_SIZE];
	int first;
	int last;
};

static void queue_init(struct queue *queue)
{
	queue->first = queue->last = 0;
}

static int queue_empty(const struct queue *queue)
{
	return queue->first == queue->last;
}

static int queue_insert(struct queue *queue, struct data *data)
{
	if (queue->last == MAX_QUEUE_SIZE)
		return -1;

	queue->data[queue->last] = *data;
	queue->last++;

	return 0;
}

static int queue_remove(struct queue *queue, struct data *ret)
{
	int i;

	if (queue_empty(queue))
		return -1;

	if (ret)
		*ret = queue->data[queue->first];

	queue->last--;
	for (i = queue->first; i < queue->last; i++)
		queue->data[i] = queue->data[i + 1];

	return 0;
}

static void queue_print(const struct queue *queue)
{
	int i;

	if (queue_empty(queue))
		return;

	for (i = queue->first; i < queue->last; i++)
		printf("%d ", queue->data[i].code);
	printf("\n");
}

static void fill_data(struct data *data, int code)
{
	data->code = code;
}

int main(void)
{
	struct queue queue;
	struct data data1, data2, data3, data_ret;

	queue_init(&queue);

	if (queue_empty(&queue))
		printf("Queue is empty\n");
	else
		printf("Queue is not empty\n");

	fill_data(&data1, 0);
	fill_data(&data2, 1);
	fill_data(&data3, 2);

	queue_insert(&queue, &data1);
	queue_insert(&queue, &data2);
	queue_insert(&queue, &data3);

	if (queue_empty(&queue))
		printf("Queue is empty\n");
	else
		printf("Queue is not empty\n");

	queue_print(&queue);

	queue_remove(&queue, &data_ret);
	printf("Item removed: %d\n", data_ret.code);

	queue_print(&queue);

	return 0;
}
