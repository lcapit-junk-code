#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define MAXLINE 4096

int main(int argc, char *argv[])
{
	ssize_t ret;
	time_t ticks;
	char buf[MAXLINE];
	int err, len, listenfd, connfd;
	struct sockaddr_in servaddr, cliaddr;

	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	if (listenfd < 0) {
		perror("socket()");
		exit(1);
	}

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(13);

	err = bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr));
	if (err < 0) {
		perror("bind()");
		exit(1);
	}

	err = listen(listenfd, 3);
	if (err < 0) {
		perror("listen()");
		exit(1);
	}

	for (;;) {
		len = sizeof(cliaddr);
		connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &len);
		if (connfd < 0) {
			perror("accept()");
			exit(1);
		}
		printf("connection from %s, port %d\n",
		       inet_ntop(AF_INET, &cliaddr.sin_addr, buf, sizeof(buf)),
		       ntohs(cliaddr.sin_port));

		ticks = time(NULL);
		memset(buf, '\0', sizeof(buf));
		snprintf(buf, sizeof(buf), "%.24s", ctime(&ticks));
		printf("%s: Handling a connection\n", buf);

		memset(buf, '\0', sizeof(buf));
		snprintf(buf, sizeof(buf), "%.24s\r\n", ctime(&ticks));
		ret = write(connfd, buf, strlen(buf));
		if (ret != strlen(buf)) {
			perror("write()");
			exit(1);
		}

		err = close(connfd);
		if (err < 0) {
			perror("close()");
			exit(1);
		}
	}

	exit(0);
}
