#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define MAXLINE 4096

int main(int argc, char *argv[])
{
	int sockfd, err, n;
	char recvline[MAXLINE +1];
	struct sockaddr_in servaddr;

	if (argc != 2) {
		fprintf(stderr, "usage: a.out <IP address>");
		exit(1);
	}

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		perror("socket()");
		exit(1);
	}

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port   = htons(13);
	err = inet_pton(AF_INET, argv[1], &servaddr.sin_addr);
	if (err < 0) {
		perror("inet_pton()");
		close(sockfd);
		exit(1);
	}

	err = connect(sockfd, (struct sockaddr *) &servaddr,
		      sizeof(servaddr));
	if (err < 0) {
		perror("connect()");
		close(sockfd);
		exit(1);
	}

	while ( (n = read(sockfd, recvline, MAXLINE) ) > 0) {
		recvline[n] = '\0'; /* NULL terminate */
		if (fputs(recvline, stdout) == EOF)
			perror("fputs()");
	}
	if (n < 0)
		perror("read()");

	exit(0);
}
