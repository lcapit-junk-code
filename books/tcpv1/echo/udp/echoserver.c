/*
 * echoserver.c: An UDP echo server implementation.
 *
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "udp_echo.h"

/**
 * dg_echo - Datagram echo
 *
 * This function receives UDP datagram from clients and sends it back,
 * it's a classical echo server implementation.
 *
 * Note that this function is protocol-independent (ie, works with
 * IPv4 and IPv6).
 *
 * @param cliaddrp pointer to the client socket address structure
 * @param clilen   length of the client address structure
 */
static void dg_echo(int sockfd, struct sockaddr *cliaddrp,
		    socklen_t clilen)
{
	int err, n;
	socklen_t len;
	char mesg[UDP_ECHO_LINE_MAX];

	for (;;) {
		len = clilen;

		n = recvfrom(sockfd, mesg, sizeof(mesg), 0, cliaddrp,
			     &len);
		if (n < 0) {
			perror("recvfrom() error");
			exit(1);
		}

		err = sendto(sockfd, mesg, n, 0, cliaddrp, len);
		if (err < 0) {
			perror("sendto() error");
			exit(1);
		}
	}
}

int main(void)
{
	int err, sockfd;
	struct sockaddr_in servaddr, cliaddr;

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0) {
		perror("Can't create socket");
		exit(1);
	}

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(UDP_ECHO_SERV_PORT);

	err = bind(sockfd, (struct sockaddr *) &servaddr,
		   sizeof(servaddr));
	if (err) {
		perror("Can't bind socket");
		exit(1);
	}

	dg_echo(sockfd, (struct sockaddr *) &cliaddr, sizeof(servaddr));

	exit(0);
}
