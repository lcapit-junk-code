/*
 * echoclient.c: An UDP echo client implementation.
 *
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "udp_echo.h"

/**
 * dg_cli - Datagram client
 *
 * This function does the client job: reads from standard input,
 * sends the datagram to the server, reads it back and then prints it
 * to the standard output.
 *
 * Note that this function is protocol-independent (ie, works with
 * IPv4 and IPv6).
 *
 * @param fp        file-pointer (usually stdin)
 * @param servaddrp pointer to the server socket address structure
 * @param servlen   length of the server address structure
 *
 * @return 0 on success, -1 otherwise
 */
static int dg_cli(FILE *fp, int sockfd,
		  const struct sockaddr *servaddrp, socklen_t servlen)
{
	int err, n;
	char sendline[UDP_ECHO_LINE_MAX];
	char recvline[UDP_ECHO_LINE_MAX + 1];

	while (fgets(sendline, sizeof(sendline), fp) != NULL) {
		err = sendto(sockfd, sendline, strlen(sendline), 0,
			     servaddrp, servlen);
		if (err < 0) {
			perror("Cann't send data");
			exit(1);
		}

		/*
		 * FIXME(I): If we loose datagrams from server or if our
		 * datagram never reaches it (due to router discard)
		 * we'll block forever here. The simpler solution is to
		 * place a timeout, but it is not the better solution
		 * because we wont know what happened. TCPv1 does a
		 * better solution in section 22.5
		 *
		 * FIXME(II): We aren't checking for the server's address
		 * reply. This is a dangerous, because if another knows
		 * our ephemeral port, it could send a datagram to us.
		 * Solution is in TCPv1 8.7 and 22.6.
		 */

		n = recvfrom(sockfd, recvline, sizeof(recvline), 0,
			     NULL, NULL);
		if (n < 0) {
			perror("Can't receive data");
			exit(1);
		}

		recvline[n] = '\0';
		puts(recvline);
	}

	if (ferror(fp))
		return -1;

	return 0;
}

/**
 * dg_cli_con - Datagram client connected
 *
 * Similar to dg_cli(), except that uses connect() and read()/write().
 *
 * @param fp        file-pointer (usually stdin)
 * @param servaddrp pointer to the server socket address structure
 * @param servlen   length of the server address structure
 *
 * @return 0 on success, -1 otherwise
 */
static int dg_cli_con(FILE *fp, int sockfd,
		      const struct sockaddr *servaddrp,
		      socklen_t servlen)
{
	int err;
	ssize_t ret;
	char sendline[UDP_ECHO_LINE_MAX];
	char recvline[UDP_ECHO_LINE_MAX + 1];

	err = connect(sockfd, servaddrp, servlen);
	if (err)
		return -1;

	while (fgets(sendline, sizeof(sendline), fp) != NULL) {
		/* TODO: write and read in a loop */

		ret = write(sockfd, sendline, strlen(sendline));
		if (ret < 0)
			return -1;

		ret = read(sockfd, recvline, sizeof(recvline));
		if (ret < 0)
			return -1;

		recvline[ret] = '\0';
		puts(recvline);
	}

	return 0;
}

/**
 * usage - Prints usage info
 */
static void usage(void)
{
	printf("usage: udpcli <IPaddress> [ -c ]\n");
}

int main(int argc, char *argv[])
{
	int err, sockfd;
	int use_connect = 0;
	struct sockaddr_in servaddr;

	if (argc < 2 || argc > 3) {
		fprintf(stderr, "Wrong number of arguments!\n");
		usage();
		exit(1);
	}

	if (argc == 3) {
		if (strncmp(argv[1], "-c", 3) &&
		    strncmp(argv[2], "-c", 3)) {
			fprintf(stderr, "Invalid argments!\n");
			usage();
			exit(1);
		}

		use_connect = 1;
	}

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0) {
		perror("Can't create socket");
		exit(1);
	}

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(UDP_ECHO_SERV_PORT);
	err = inet_pton(AF_INET, argv[1], &servaddr.sin_addr);
	if (err < 1) {
		perror("Can't convert IPaddress");
		exit(1);
	}

	if (use_connect)
		err = dg_cli_con(stdin, sockfd,
				 (struct sockaddr *) &servaddr,
				 sizeof(servaddr));
	else
		err = dg_cli(stdin, sockfd,
			     (struct sockaddr *) &servaddr,
			     sizeof(servaddr));

	if (err) {
		perror("Client error");
		exit(1);
	}

	exit(0);
}
