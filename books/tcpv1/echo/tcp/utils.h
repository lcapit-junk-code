/*
 * utils.h: utils.c exported functions
 */
#ifndef __UTILS_H
#define __UTILS_H

void verbose_write(const char *format, ...);
void verbose_set(void);
void verbose_unset(void);

int write_data(int fd, char *data, size_t size);
int read_data(int fd, char *data, size_t size);

int connect_to_server(const char *ip, unsigned short port,
		      unsigned short from_port);
void close_server_connection(int fd);

#endif /* __UTILS_H */
