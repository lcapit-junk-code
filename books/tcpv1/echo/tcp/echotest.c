/*
 * echotest.c: An echo server test program.
 *
 * This program reads all data from a specified file, writes it to the
 * echo server, reads it back and checks if it is the same data
 * previously written: if it is, we are ok, otherwise the server may be
 * buggy.
 *
 * TODO: Implements verbose mode.
 *
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include "utils.h"

#define BUF_SIZE 8192

/**
 * echo_test - Echo server test function
 *
 * This functions does the test by reading BUF_SIZE bytes from the
 * specified file, sending these bytes to the echo server and reading
 * the bytes back. In each iteration, a check is made to see if the
 * data sent by the echo server is the same that was actually written.
 *
 * @param sfd   file-descriptor connected to the echo server
 * @param file  file to read data from
 *
 * @return 0 on success (all data written matches with the received
 * data), -1 on error or 2 if the read data doesn't match with the
 * written one.
 */
static int echo_test(int sfd, const char *file)
{
	int fd, err;
	ssize_t bytes;
	char buf[BUF_SIZE], rbuf[BUF_SIZE];

	if (sfd < 0 || !file) {
		errno = EINVAL;
		return -1;
	}

	fd = open(file, O_RDONLY);
	if (fd < 0)
		return -1;

	for (;;) {
		memset(buf, 0, sizeof(buf));
		bytes = read(fd, buf, sizeof(buf));
		if (!bytes) {
			err = 0;
			break;
		} else if (bytes < 0) {
			err = -1;
			break;
		}

		err = write_data(sfd, buf, bytes);
		if (err < 0)
			break;

		memset(rbuf, 0, sizeof(rbuf));
		err = read_data(sfd, rbuf, bytes);
		if (err < 0)
			break;

		if (memcmp(buf, rbuf, bytes)) {
			/* Doesn't match */
			err = 2;
			break;
		}
	}

	close(fd);
	return err;
}

/**
 * usage - Prints usage info
 */
static void usage(void)
{
	printf(
		"usage: echotest [ -i ip_address ] < -f file > [ -v ] [ -h ]\n"
		"-i <ip_address>  IP address to connect to\n"
		"-f <file>        File to be read\n"
		"-v               Verbose mode\n"
		"-h               This message\n"
		);
}

int main(int argc, char *argv[])
{
	int ret, sfd, opt;
	char *file = NULL;
	char *srv_ip = NULL;
	const unsigned short srv_port = 7;

	while ( (opt = getopt(argc, argv, "i:f:vh")) > 0) {
		switch (opt) {
		case 'i':
			srv_ip = optarg;
			break;
		case 'v':
			verbose_set();
			break;
		case 'f':
			file = optarg;
			break;
		case 'h':
			usage();
			exit(0);
		default:
			fprintf(stderr, "Invalid option: %c\n", opt);
			usage();
			exit(1);
		}
	}

	if (!file) {
		fprintf(stderr, "You must specify a file to be read!\n");
		usage();
		exit(1);
	}

	if (!srv_ip) {
		fprintf(stderr,
			"You must specify a server IP address\n");
		usage();
		exit(1);
	}

	sfd = connect_to_server(srv_ip, srv_port, 0);
	if (sfd < 0) {
		fprintf(stderr, "Can't connect to '%s:%d': %s", srv_ip,
			srv_port, strerror(errno));
		exit(1);
	}

	verbose_write("Connected to the server!\n");

	ret = echo_test(sfd, file);
	if (ret < 0) {
		fprintf(stderr, "Can't proceed with the test: %s",
			strerror(errno));
		ret = 1;
	} else if (ret == 1) {
		fprintf(stderr, "Doesn't match!\n");
		ret = 2;
	}

	close_server_connection(sfd);

	verbose_write("Connection closed!\n");

	exit(ret);
}
