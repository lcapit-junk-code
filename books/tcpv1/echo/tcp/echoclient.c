/*
 * echoclient.c: An echo client program
 *
 * This program reads bytes from standard input, writes it to the echo
 * server, reads it back and prints it.
 *
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/select.h>
#include <sys/socket.h>
#include "utils.h"

#define ECHO_SRV_PORT   7
#define MAXLINE       256

static void usage(void)
{
	printf("echoclient < server ip > [ from port ]\n");
}

int main(int argc, char *argv[])
{
	ssize_t len;
	fd_set rset;
	unsigned short from_port = 0;
	char buf[MAXLINE], line[MAXLINE];
	int stdineof, maxfdp1, err, fd, fdin;

	if (argc != 2 && argc != 3) {
		fprintf(stderr, "You must specify the server's IP!\n");
		usage();
		exit(1);
	}

	if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
		usage();
		exit(1);
	}

	if (signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
		perror("signal()");
		exit(1);
	}

	if (argc == 3) {
		/*
		 * Don't use an ephemeral port,
		 * uses argv[2] instead
		 */
		from_port = strtol(argv[2], (char **) NULL, 10);
	}

	fd = connect_to_server(argv[1], ECHO_SRV_PORT, from_port);
	if (fd < 0) {
		perror("Can't connect to the echo server");
		exit(1);
	}

	stdineof = 0;
	fdin = fileno(stdin);

	FD_ZERO(&rset);

	for (;;) {
		if (!stdineof)
			FD_SET(fdin, &rset);
		FD_SET(fd, &rset);

		maxfdp1 = fdin > fd ? fdin : fd;
		maxfdp1 += 1;

		err = select(maxfdp1, &rset, NULL, NULL, NULL);
		if (err < 0) {
			perror("select()");
			exit(1);
		}

		if (FD_ISSET(fdin, &rset)) {
			len = read(fdin, line, sizeof(line));
			if (!len) {
				stdineof = 1;
				err = shutdown(fd, SHUT_WR);
				if (err) {
					perror("shutdown()");
					exit(1);
				}
				FD_CLR(fdin, &rset);
				continue;
			} else if (len < 0) {
				perror("read()");
				exit(1);
			}

			err = write_data(fd, line, len);
			if (err) {
				perror("write_data()");
				exit(1);
			}
		}

		if (FD_ISSET(fd, &rset)) {
			len = read(fd, buf, sizeof(buf));
			if (!len) {
				if (stdineof) {
					/* normal termination */
					exit(0);
				}

				fprintf(stderr,
					"Server has closed the connection.\n");
				exit(1);
			}
			else if (len < 0) {
				perror("read()");
				exit(1);
			}

			err = write(fileno(stdout), buf, len);
			if (err < 0) {
				perror("write()");
				exit(1);
			}
		}
	}

	return 0;
}
