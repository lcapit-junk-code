/*
 * utils.c: utils functions used by the echo project
 */
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "utils.h"

/*
 * Verbose print
 */

/* Set in verbose mode (see verbose_write() comment, XXX: Global variable) */
static int verbose;

/**
 * verbose_set - Enable verbose mode
 */
void verbose_set(void)
{
	verbose = 1;
}

/**
 * verbose_unset - Disable verbose mode
 */
void verbose_unset(void)
{
	verbose = 0;
}

/**
 * verbose_write - Writes a formatted string to the stdout
 *
 * The format of the string is 'HOUR: string', also note that
 * is 'verbose' is not set, nothing is printed.
 *
 * @param format  string format
 * @param ...     parameters of 'format'
 */
void verbose_write(const char *format, ...)
{
	va_list ap;
	time_t ticks;
	struct tm *tm;
	char buf[32];

	if (!verbose)
		return;

	ticks = time(NULL);
	tm = localtime(&ticks);
	memset(buf, '\0', sizeof(buf));
	strftime(buf, sizeof(buf), "%T", tm);
	fprintf(stdout, "%s: ", buf);

	va_start(ap, format);
	vfprintf(stdout, format, ap);
	va_end(ap);
}

/*
 * I/O
 */

/**
 * write_data - Writes data to a file-descriptor
 *
 * @param fd    file-descriptor to send the data
 * @param data  data to be written
 * @param size  amount of bytes to be written
 *
 * @return 0 on success (all 'size' bytes were written), -1 otherwise
 */
int write_data(int fd, char *data, size_t size)
{
	char *p;
	ssize_t ret;
	size_t bytes;

	if (fd < 0 || !data || !size) {
		errno = EINVAL;
		return -1;
	}

	p = data;
	bytes = size;

	while (bytes > 0) {
		ret = write(fd, p, bytes);
		if (ret < 0)
			return -1;

		bytes -= ret;
		p += ret;
	}

	if (bytes != 0) {
		errno = ERANGE;
		return -1;
	}

	return 0;
}

/**
 * read_data - Receives data from a file-descriptor
 *
 * @param fd    file-descriptor to receive the data
 * @param data  data to be receive
 * @param size  amount of bytes to be receive
 *
 * @return 0 on success (all 'size' bytes were received), -1 otherwise
 */
int read_data(int fd, char *data, size_t size)
{
	char *p;
	ssize_t ret;
	size_t bytes;

	if (fd < 0 || !data || !size) {
		errno = EINVAL;
		return -1;
	}

	p = data;
	bytes = size;

	while (bytes > 0) {
		ret = read(fd, p, bytes);
		if (ret < 0)
			return -1;
		else if (!ret) {
			/*
			 * FIXME: This is the only way I find to tell the
			 * calling function the server has closed the
			 * connection.
			 */
			return -2;
		}

		bytes -= ret;
		p += ret;
	}

	if (bytes != 0) {
		errno = ERANGE;
		return -1;
	}

	return 0;
}

/*
 * Connection
 */

/**
 * connect_to_server - Creates an IPv4 connection to a server
 *
 * @param ip         server's IP address
 * @param port       server's port
 * @param from_port  port to originate the connection (not used yet)
 *
 * @return a connected file-descriptor, -1 otherwise
 */
int connect_to_server(const char *ip, unsigned short port,
		      unsigned short from_port)
{
	int berrno, err, fd;
	struct sockaddr_in server;

	if (!ip || !port) {
		errno = EINVAL;
		return -1;
	}

	fd = socket(PF_INET, SOCK_STREAM, 0);
	if (fd < 0)
		return -1;

	if (from_port) {
		struct sockaddr_in client;

		memset(&client, 0, sizeof(client));
		client.sin_family = PF_INET;
		client.sin_port   = htons(from_port);
		err = inet_pton(PF_INET, "0.0.0.0", &client.sin_addr);
		if (err < 0)
			goto out;

		err = bind(fd, (struct sockaddr *) &client, sizeof(client));
		if (err)
			goto out;
	}

	memset(&server, 0, sizeof(server));
	server.sin_family = PF_INET;
	server.sin_port   = htons(port);
	err = inet_pton(PF_INET, ip, &server.sin_addr);
	if (err < 0)
		goto out;

	err = connect(fd, (struct sockaddr *) &server, sizeof(server));

 out:
	if (err) {
		berrno = errno;
		close(fd);
		fd = -1;
		errno = berrno;
	}

	return fd;
}

/**
 * close_server_connection - Closes a previously connection made with
 * connect_to_server()
 */
void close_server_connection(int fd)
{
	if (fd >= 0)
		close(fd);
}
