/*
 * echoserver.c: An echo server implementation.
 *
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "utils.h"

#define MAX_ECHO_BUF 8192

/**
 * handler - echoserver's signal handler
 *
 * This function collects echoserver's zombies exit status, note that:
 *
 * 1- We cannot just use SIG_IGN for SIGCHLD to not generate zombies
 * because this behaivor is _not_ POSIX;
 *
 * 2- The use of waitpid() in a loop as coded here, is the correct
 * way to collect _all_ zombies states; IOW, this implementation
 * does not left zombies around (this would happen with wait(), for
 * example).
 *
 * @param signo  signal number
 */
static void handler(int signo)
{
	while (waitpid(-1, NULL, WNOHANG) > 0)
		/* NOTHING */ ;

	signo = signo; /* Makes GCC happy */
}

/**
 * echo_data - Reads data from 'fd' and writes it back
 *
 * This is the 'echoserver' main function: it reads data from the
 * client and writes it back. Note that this function only finishes
 * when the client closes the connection.
 *
 * @param fd      file-descriptor to work with
 * @param rbytes  returns the amount of bytes read
 * @param wbytes  returns the amount of bytes written
 *
 * @return 0 on success, -1 otherwise
 */
static int echo_data(int fd, int *rbytes, int *wbytes)
{
	int err;
	ssize_t ret;
	size_t bytes;
	char buf[MAX_ECHO_BUF];

	*rbytes = *wbytes = 0;

	for (;;) {
		memset(buf, '\0', sizeof(buf));

		ret = read(fd, buf, sizeof(buf));
		if (ret < 0) {
			fprintf(stderr, "Warning: read() failed: %s\n",
				strerror(errno));
			return -1;
		} else if (!ret) {
			/* Client has closed the connection */
			return 0;
		}

		/* 'ret' cannot be negative */
		bytes = (size_t) ret;

		*rbytes += bytes;

		err = write_data(fd, buf, bytes);
		if (err)
			return -1;
		*wbytes += bytes;
	}
}

/**
 * usage - Prints usage info
 */
static void usage(void)
{
	printf(
		"usage: echoserver [ -i ip_address ] [ -v ]\n"
		"-i <ip_address>  IP address to be used\n"
		"-v               verbose mode\n"
		"-h               this message\n"
		);
}

int main(int argc, char *argv[])
{
	char *ip_addr = NULL;
	int reuse, lfd, err, opt;
	struct sockaddr_in iface;

	/* Read command-line argments */
	while ( (opt = getopt(argc, argv, "i:vh")) > 0) {
		switch (opt) {
		case 'i':
			ip_addr = optarg;
			break;
		case 'v':
			verbose_set();
			break;
		case 'h':
			usage();
			exit(0);
		default:
			fprintf(stderr, "Invalid option: %c\n", opt);
			usage();
			exit(1);
		}
	}

	/* Catches SIGCHLD */
	if (signal(SIGCHLD, handler) == SIG_ERR) {
		perror("signal()");
		exit(1);
	}

	if (!ip_addr)
		ip_addr = "0.0.0.0";

	verbose_write("IP address: %s\n", ip_addr);

	/*
	 * Does all the required setup to start the
	 * server
	 */
	lfd = socket(PF_INET, SOCK_STREAM, 0);
	if (lfd < 0) {
		perror("socket()");
		exit(1);
	}

	reuse = 1;
	err = setsockopt(lfd, SOL_SOCKET, SO_REUSEADDR, (void *) &reuse,
			 sizeof(reuse));
	if (err) {
		perror("setsockopt()");
		exit(1);
	}

	memset(&iface, 0, sizeof(iface));
	iface.sin_family = PF_INET;
	iface.sin_port   = htons(7);
	err = inet_pton(PF_INET, ip_addr, &iface.sin_addr);
	if (err < 0) {
		perror("inet_pton()");
		exit(1);
	}

	err = bind(lfd, (struct sockaddr *) &iface, sizeof(iface));
	if (err < 0) {
		perror("bind()");
		exit(1);
	}

	err = listen(lfd, 5);
	if (err < 0) {
		perror("listen()");
		exit(1);
	}

	verbose_write("Ready for connections...\n\n");

	/* Runs forever... */
	for (;;) {
		int len, cfd;
		char cip[INET_ADDRSTRLEN];
		struct sockaddr_in client;

		len = sizeof(client);
		memset(&client, 0, len);
		cfd = accept(lfd, (struct sockaddr *) &client, &len);
		if (cfd < 0) {
			if (errno == EINTR) {
				/* Restart accept() */
				continue;
			}

			fprintf(stderr, "accept() failed: %s",
				strerror(errno));
			exit(1);
		}

		memset(cip, '\0', sizeof(cip));
		inet_ntop(PF_INET, &client.sin_addr, cip, sizeof(cip));

		if (fork() == 0) {
			int wbytes, rbytes;

			/* child */
			close(lfd);

			verbose_write(
				"[%d] Opened Connection for %s:%d\n",
				getpid(), cip, ntohs(client.sin_port));

			echo_data(cfd, &rbytes, &wbytes);

			verbose_write("[%d] %s:%d %d read, wrote %d bytes, closed\n", getpid(), cip, ntohs(client.sin_port), rbytes, wbytes);

			close(cfd);
			exit(0);
		}

		/* Parent */
		close(cfd);
	}

	return 0;
}
