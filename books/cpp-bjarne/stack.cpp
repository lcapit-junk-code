#include <iostream>

class Stack {
	private:
		char *v;
		int top;
		int max_size;
	public:
		class Underflow { }; // exception
		class Overflow { };  // exception
		class Bad_size { };  // exception

		Stack(int s);       // constructor
		~Stack();           // destructor

		void push(char c);
		char pop();
};

Stack::Stack(int s)
{
	top = 0;

	if (s > 1000)
		throw Bad_size();
	max_size = s;
	v = new char[s]; // allocate elements on the free store (heap)
}

Stack::~Stack()
{
	delete[] v;
}

void Stack::push(char c)
{
	if (top == max_size)
		throw Overflow();
	std::cout << v[top];
	v[top++] = c;
}

char Stack::pop()
{
	if (top == 0)
		throw Underflow();
	return v[--top];
}

Stack s_var1(10); // global stack with 10 elements

int main(void)
{
	Stack s_var2(5); // local stack with 5 elements
	Stack *s_ptr = new Stack(20); // pointer to allocated stack

	s_var1.push('f');
	s_var2.push('a');
	s_ptr->push('d');

	std::cout << s_ptr->pop();
	std::cout << "\n";

	try {
		std::cout << s_ptr->pop();
	}
	catch (Stack::Underflow) {
		std::cerr << "ERROR: No elements in the stack!\n";
		return 1;
	}

	return 0;
}
