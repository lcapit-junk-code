#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
	int fd;

	fd = open("./teste.fifo", O_WRONLY);
	if (fd < 0) {
		perror("open()");
		exit(1);
	}

	write(fd, "hello world!", 12);

	close(fd);
	return 0;
}
