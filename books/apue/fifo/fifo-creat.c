#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
	int fd, err;
	char line[1024];

	err = mkfifo("./teste.fifo", 0777);
	if (err) {
		perror("mkfifo()");
		exit(1);
	}

	fd = open("./teste.fifo", O_RDONLY);
	if (fd < 0) {
		perror("open()");
		exit(1);
	}

	memset(line, '\0', sizeof(line));
	read(fd, line, sizeof(line));
	printf("read from FIFO: %s\n", line);

	close(fd);
	return 0;
}
