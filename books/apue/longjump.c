/*
 * setjmp() and longjmp() example usage
 */

#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>

static jmp_buf jmpbuffer;

static void f2(void)
{
	longjmp(jmpbuffer, 1);
}

static void f1(int i, int j, int k)
{
	printf("in f1(): count = %d, val = %d, sum = %d\n", i, j, k);
	f2();
}

int main(void)
{
	int count;
	register int val;
	volatile int sum;

	count = 2; val = 3; sum = 4;
	if (setjmp(jmpbuffer) != 0) {
		printf("after longjmp: count = %d, val = %d, sum = %d\n",
		       count, val, sum);
		exit(0);
	}

	count = 97; val = 98; sum = 99;
	f1(count, val, sum); 	/* never returns */
	return 0;
}
