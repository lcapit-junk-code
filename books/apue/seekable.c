/*
 * Check if a file specified in stdin is 'seekable'
 */ 
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

int main(void)
{
	if (lseek(STDIN_FILENO, 0, SEEK_CUR) == -1 && errno == ESPIPE)
		printf("cannot seek\n");
	else
		printf("seek OK\n");

	return 0;
}
