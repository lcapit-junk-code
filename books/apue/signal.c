/*
 * 10.11: Demonstrates the use of sigpending() and sigismember()
 * by installing a handler for SIGQUIT, blocking it and checking
 * if it was sent.
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

static void sig_quit(int signo)
{
	fprintf(stderr, "caught SIGQUIT\n");

	if (signal(SIGQUIT, SIG_DFL) == SIG_ERR) {
		fprintf(stderr, "can't reset SIGQUIT\n");
		exit(1);
	}
}

int main(void)
{
	sigset_t newmask, oldmask, pendmask;

	/* Installs sig_quit() as our handler for SIGQUIT */
	if (signal(SIGQUIT, sig_quit) == SIG_ERR) {
		fprintf(stderr, "can't catch SIGQUIT\n");
		exit(1);
	}

	/* Blocks SIGQUIT */
	sigemptyset(&newmask);
	sigaddset(&newmask, SIGQUIT);

	if (sigprocmask(SIG_BLOCK, &newmask, &oldmask) < 0) {
		fprintf(stderr, "SIG_BLOCK error\n");
		exit(1);
	}

	/* 
	 * Sleeps for five seconds. In this period we should
	 * receive a SIGQUIT, but as it's blocked we continue
	 * to sleep
	 */
	sleep(5);

	if (sigpending(&pendmask) < 0) {
		fprintf(stderr, "sigpending error\n");
		exit(1);
	}

	if (sigismember(&pendmask, SIGQUIT))
		printf("SIGQUIT pending\n");

	if (sigprocmask(SIG_SETMASK, &oldmask, NULL) < 0) {
		fprintf(stderr, "SIG_SETMASK error\n");
		exit(1);
	}
	printf("SIGQUIT unblocked\n");

	exit(0);
}
