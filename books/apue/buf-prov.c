/*
 * This program proves the behaivor of  buffered, line-buffered
 * and unbufferd I/O. Read the comments to understand it.
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(void)
{
	char c;
	FILE *file;

	/*
	 * stdout is line-buffered, it means the that data is flushed
	 * when a new-line char is found. So, you should see the line
	 * bellow.
	 */
	fprintf(stdout, "stdout is line-buffered\n");

	/* ... and you shouldn't see the next line */
	fprintf(stdout, "--- The hidden line ---");

	/* stderr is unbuffered, you will see the line */
	fprintf(stderr, "this is stderr in action");

	/*
	 * opens a file and write-data in it. When we call pause()
	 * you'll see that nothing was written to the file.
	 */
	file = fopen("./buf_test.txt", "w+");
	if (file) {
		fprintf(file, "File contents\nFully buffered\n");
	} else {
		perror("fopen()");
		exit(1);
	}

	read(0, &c, 1);
	return 0;
}
