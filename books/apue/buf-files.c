/*
 * Print buffering for various standard I/O streams
 */

#include <stdio.h>
#include <stdlib.h>

void pr_stdio(const char *name, FILE *fp)
{
	printf("stream = %s, ", name);

	if (fp->_flags & _IONBF)
		printf("unbuffered");
	else if (fp->_flags & _IOLBF)
		printf("line buffered");
	else
		printf("fully buffered");

	printf("\n");
}

int main(void)
{
	FILE *fp;

	fputs("enter any character\n", stdout);
	if (getchar() == EOF) {
		perror("getchar()");
		exit(1);
	}
	fputs("one line to standard error\n", stderr);

	pr_stdio("stdin", stdin);
	pr_stdio("stdout", stdout);
	pr_stdio("stderr", stderr);

	if ((fp = fopen("/etc/passwd", "r")) == NULL) {
		fprintf(stderr, "fopen() error");
		exit(1);
	}
	if (getc(fp) == EOF) {
		fprintf(stderr, "getc() error");
		exit(1);
	}

	pr_stdio("/etc/passwd", fp);
	fclose(fp);

	return 0;
}
