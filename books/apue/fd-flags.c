/*
 * Print flags for specified file-descriptor
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
	int accmode, val;

	if (argc != 2) {
		fprintf(stderr, "usage: a.out <descriptor#>\n");
		exit(1);
	}

	if ((val = fcntl(atoi(argv[1]), F_GETFL, 0)) < 0) {
		fprintf(stderr, "fcntl error for fd %d: %s\n", atoi(argv[1]),
			strerror(errno));
		exit(1);
	}

	accmode = val & O_ACCMODE;
	if (accmode == O_RDONLY)
		printf("read only");
	else if (accmode == O_WRONLY)
		printf("write only");
	else if (accmode == O_RDWR)
		printf("read write");
	else
		printf("unknow access mode");

	if (val & O_APPEND)
		printf(", append");

	if (val & O_NONBLOCK)
		printf(", nonblocking");

	if (val & O_SYNC)
		printf(", synchornous writes");

	putchar('\n');
	return 0;
}
