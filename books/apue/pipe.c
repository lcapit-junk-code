#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	pid_t pid;
	int err, n, fd[2];
	char line[1024];

	err = pipe(fd);
	if (err) {
		perror("pipe()");
		exit(1);
	}

	pid = fork();
	if (pid < 0) {
		perror("fork()");
		exit(1);
	} else if (!pid) {
		/* Child */
		close(fd[1]);
		memset(line, '\0', sizeof(line));
		n = read(fd[0], line, sizeof(line));
		write(STDOUT_FILENO, line, n);
	} else {
		/* Parent */
		close(fd[0]);
		write(fd[1], "hello world!\n", 13);
	}

	exit(0);
}
