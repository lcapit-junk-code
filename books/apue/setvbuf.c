/*
 * setvbuf() usage example
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(void)
{
	int err;
	FILE *file;
	char buf[BUFSIZ];

	/* open the stream */
	file = fopen("./buf_test2.txt", "w+");
	if (!file) {
		fprintf(stderr, "file cannot be openned\n");
		exit(1);
	}

	/* set the stream buffer and the it as fully-buffered */
	memset(buf, '\0', sizeof(buf));
	err = setvbuf(file, buf, _IOFBF, sizeof(buf));
	if (err) {
		fprintf(stderr, "sevbuf() failed\n");
		exit(1);
	}

	/* write data and exit */
	fprintf(file, "Go, go! Goooooo Jhonny go, go! Goooooo Jhonny go, go!\n");
	fclose(file);

	/* shows the contents of 'buf' */
	fprintf(stdout, "buf: %s", buf);

	return 0;
};
