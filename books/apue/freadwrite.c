/*
 * A fread() and fwrite() example
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NAME_MAX 32

struct data {
	int id;
	int count;
	char name[NAME_MAX];
};

static void usage(const char *progname)
{
	printf("Usage: %s < -w | -r > < filename >\n", progname);
}

int main(int argc, char *argv[])
{
	size_t ret;
	FILE *file;
	int i;
	struct data data;
	char *filepath, *progname;

	if (argc != 3) {
		fprintf(stderr, "Wrong number of arguments\n");
		usage(argv[0]);
		exit(1);
	}

	progname = argv[0];
	filepath = argv[2];
	if (!memcmp(argv[1], "-w", 2)) {
		struct data datav[2];

		/*
		 * Write the structures using fwrite()
		 */

		file = fopen(filepath, "w+");
		if (!file) {
			perror("fopen()");
			exit(1);
		}

		/* fill datav[] */
		memset(datav, 0, sizeof(data));
		datav[0].id = 0;
		datav[0].count = 42;
		strncpy(datav[0].name, "luiz", NAME_MAX);

		datav[1].id = 1;
		datav[1].count = 666;
		strncpy(datav[1].name, "capitulino", NAME_MAX);

		ret = fwrite((void *) datav, sizeof(struct data),
			     2, file);
		if (ret != 2) {
			perror("fwrite()");
			exit(1);
		}
		
		goto out;
	}

	/*
	 * Write the structures using fwrite()
	 */

	file = fopen(filepath, "r");
	if (!file) {
		perror("fopen()");
		exit(1);
	}

	for (i = 0; i < 2; i++) {
		ret = fread((void *) &data, sizeof(data), 1, file);
		if (ret != 1) {
			if (feof(file)) {
				fprintf(stderr, "finished reading\n");
				goto out;
			}

			perror("fread()");
			exit(1);
		}

		printf("id = %d\ncount = %d\nname = %s\n\n", data.id,
		       data.count, data.name);
	}

 out:
	fclose(file);
	return 0;
}
