/*
 * cp example using mmap()
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

int main(int argc, char *argv[])
{
	char *src, *dst;
	int err, fdin, fdout;
	struct stat statbuf;

	if (argc != 3) {
		fprintf(stderr, "usage: 12.14 <fromfile> <tofile>\n");
		exit(EXIT_FAILURE);
	}

	fdin = open(argv[1], O_RDONLY);
	if (fdin < 0) {
		fprintf(stderr, "can't open %s for reading", argv[1]);
		exit(EXIT_FAILURE);
	}

	fdout = open(argv[2], O_RDWR | O_CREAT | O_TRUNC, 0666);
	if (fdin < 0) {
		fprintf(stderr, "can't open %s for reading", argv[2]);
		exit(EXIT_FAILURE);
	}

	err = fstat(fdin, &statbuf);
	if (err) {
		perror("fstat()");
		exit(EXIT_FAILURE);
	}

	/* Allocates the space needed */
	err = lseek(fdout, statbuf.st_size -1, SEEK_SET);
	if (err == (off_t) -1) {
		perror("lseek()");
		exit(EXIT_FAILURE);
	}

	err = write(fdout, "", 1);
	if (err != 1) {
		perror("write()");
		exit(EXIT_FAILURE);
	}

	src = (char *) mmap((void *) 0, statbuf.st_size,
			    PROT_READ, MAP_SHARED, fdin, 0);
	if (src == MAP_FAILED) {
		perror("first mmap()");
		exit(EXIT_FAILURE);
	}

	dst = (char *) mmap((void *) 0, statbuf.st_size,
			    PROT_READ | PROT_WRITE, MAP_SHARED, fdout, 0);
	if (dst == MAP_FAILED) {
		perror("second mmap()");
		exit(EXIT_FAILURE);
	}

	memcpy(dst, src, statbuf.st_size);

	munmap(src, statbuf.st_size);
	munmap(dst, statbuf.st_size);

	return 0;
}
