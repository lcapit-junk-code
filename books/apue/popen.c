#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	FILE *file;
	char buf[1024];

	file = popen("ls *.c", "r");
	if (!file) {
		perror("open()");
		exit(1);
	}

	memset(buf, '\0', sizeof(buf));
	while (fgets(buf, sizeof(buf), file) != NULL) {
		printf("%s", buf);
		memset(buf, '\0', sizeof(buf));
	}

	pclose(file);
	return 0;
}
