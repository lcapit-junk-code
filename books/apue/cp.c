/*
 * Copy standard input to standard output (slow way)
 * 
 * Would probably faster if we:
 * 
 *  1. get the BUFSIZE value from stat's st_blksize member
 *  2. use the standard I/O functions
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#define BUFSIZE 8192

int main(void)
{
	int n;
	char buf[BUFSIZE];

	while ((n = read(STDIN_FILENO, buf, BUFSIZE)) > 0)
		if (write(STDOUT_FILENO, buf, n) != n)
			strerror(errno);

	if (n < 0)
		strerror(errno);

	return 0;
}
