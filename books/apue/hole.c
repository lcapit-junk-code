/*
 * Creates a file with a hole
 */

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

char buf1[] = "abcdefghij";
char buf2[] = "ABCDEFGHIJ";

int main(void)
{
	int fd;

	if ((fd = creat("file.hole", 0666)) < 0) {
		perror("creat()");
		exit(1);
	}

	if (write(fd, buf1, 10) != 10) {
		perror("buf1 write error");
		exit(1);
	}
	/* offset now == 10 */

	if (lseek(fd, 40, SEEK_SET) == -1) {
		perror("lseek()");
		exit(1);
	}
	/* offset now == 40 */

	if (write(fd, buf2, 10) != 10) {
		perror("buff2 write error");
		exit(1);
	}
	/* offset now == 50 */

	return 0;
}
