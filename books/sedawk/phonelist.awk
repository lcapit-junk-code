# phonelist.awk -- print name and phone number
# input file -- name, company, street, city, state and zip, phone

BEGIN { FS = "," }
{ print $1 ", " $6 }
