BEGIN { print "Bytes", "\t", "File" }
{ 
	sum += $5
	++filenum
	print $5, "\t", $8 
}
END { print "Total : ", sum, "bytes (" filenum " files)" }
