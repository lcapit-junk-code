#ifndef CLIST_H
#define CLIST_H

/*
 * Main structures: CList element and CList
 */
typedef struct CListElmt_ {
	void *data;
	struct CListElmt_ *next;
} CListElmt;

typedef struct CList_ {
	int size;
	void (*destroy)(void *data);
	CListElmt *head;
} CList;

/*
 * Public interface
 */

void clist_init(CList *clist, void (*destroy)(void *data));
int clist_ins_next(CList *clist, CListElmt *el, void *data);
int clist_rem_next(CList *clist, CListElmt *el, void **data);
void clist_destroy(CList *clist);

#define clist_size(clist)           ((clist)->size)
#define clist_head(clist)           ((clist)->head)
#define clist_element_data(element) ((element)->data)
#define clist_element_next(element) ((element)->next)

#endif /* CLIST_H */
