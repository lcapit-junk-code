#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "clist.h"

void clist_init(CList *clist, void (*destroy)(void *data))
{
	clist->size = 0;
	clist->destroy = destroy;
	clist->head = NULL;
}

int clist_ins_next(CList *clist, CListElmt *el, void *data)
{
	CListElmt *new_el;

	new_el = malloc(sizeof(CListElmt));
	if (!new_el)
		return -1;
	new_el->data = data;

	if (!el) {
		if (clist_size(clist)) {
			free(new_el);
			errno = EINVAL;
			return -1;
		}
		new_el->next = new_el;
		clist->head  = new_el;
	} else {
		new_el->next = el->next;
		el->next = new_el;
	}

	clist->size++;
	return 0;
}

int clist_rem_next(CList *clist, CListElmt *el, void **data)
{
	CListElmt *old_el;

	if (!el || !clist_size(clist)) {
		errno = EINVAL;
		return -1;
	}

	old_el = el->next;
	*data = old_el->data;

	if (clist_size(clist) == 1)
		clist->head = NULL;
	else if (clist_head(clist) == old_el)
		clist->head = old_el->next;

	el->next = old_el->next;

	free(old_el);
	clist->size--;
	return 0;
}

void clist_destroy(CList *clist)
{
	int err;
	void *data;

	while (clist_size(clist) > 0) {
		err = clist_rem_next(clist, clist_head(clist), (void **)&data);
		if (!err && clist->destroy)
			clist->destroy(data);
	}
}
