#include <check.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "clist.h"

#define ELEMENTS_MAX 15

START_TEST(clist_transversal_test)
{
	CList l;
	CListElmt *p, *last;
	int i, ret, end, data[ELEMENTS_MAX];

	clist_init(&l, NULL);

	last = NULL;
	end = 0;

	/* Adds all the elements */
	for (i = 0; i < ELEMENTS_MAX; i++) {
		data[i] = i;
		ret = clist_ins_next(&l, last, &data[i]);
		fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
			    ret, strerror(errno));
		if (!last)
			last = clist_head(&l);
		else
			last = last->next;
	}

	/* Transverse it three times */
	for (p = clist_head(&l);; p = clist_element_next(p)) {
		int *value = clist_element_data(p);
		if (*value == ELEMENTS_MAX-1) {
			if (end == 3)
				break;
			end++;
		}
	}

	fail_unless(end == 3, "list wasn't tranversed three times [%d]\n",
		    end);
	clist_destroy(&l);
}
END_TEST

static int destroy_called;

static void my_destroy2(void *data)
{
	free(data);
	destroy_called++;
}

START_TEST(clist_destroy_test)
{
	CList l;
	void *p;
	int ret, i;
	const int max_objs = 20;

	clist_init(&l, my_destroy2);

	for (i = 0; i < max_objs; i++) {
		p = malloc(sizeof(int));
		fail_unless(p != NULL,
			    "Could not allocate memory: %s\n",
			    strerror(errno));

		ret = clist_ins_next(&l, clist_head(&l), p);
		fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
			    ret, strerror(errno));
	}

	clist_destroy(&l);

	fail_unless(clist_size(&l) == 0, "list size is %d but should be 0\n",
		    clist_size(&l));
	fail_unless(clist_head(&l) == NULL,
		    "list head is %p but should be NULL\n", clist_head(&l));
	fail_unless(destroy_called == max_objs,
		    "destroy function called %d times, but should be %d\n",
		    destroy_called, max_objs);
}
END_TEST

START_TEST(clist_rem_next_mid_test)
{
	CList l;
	void *data;
	CListElmt *p;
	int ret, head_data, sec_data, thi_data;

	clist_init(&l, NULL);

	ret = clist_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = clist_ins_next(&l, clist_head(&l), &thi_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = clist_ins_next(&l, clist_head(&l), &sec_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	/* removes the second element */
	ret = clist_rem_next(&l, clist_head(&l), &data);
	fail_unless(ret == 0, "clist_rem_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(data == &sec_data,
		    "removed data is %p but should be %p\n",
		    data, &sec_data);
	fail_unless(clist_size(&l) == 2,
		    "list size is %d but should be 2\n", clist_size(&l));

	data = clist_element_data(clist_head(&l));
	fail_unless(data == &head_data,
		    "list head data is %p but should be %p\n",
		    data, &head_data);

	p = clist_element_next(clist_head(&l));
	data = clist_element_data(p);
	fail_unless(data == &thi_data,
		    "list head next data is %p but should be %p\n",
		    data, &thi_data);

	p = clist_element_next(p);
	fail_unless(p == clist_head(&l),
		    "sec's next [%p] is not pointing to the head [%p]\n",
		    p, clist_head(&l));

	free(clist_element_next(clist_head(&l)));
	free(clist_head(&l));
}
END_TEST

START_TEST(clist_rem_next_head_test)
{
	CList l;
	void *data;
	CListElmt *p;
	int ret, head_data, sec_data;

	clist_init(&l, NULL);

	ret = clist_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = clist_ins_next(&l, clist_head(&l), &sec_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	/* removes the head */
	p = clist_element_next(clist_head(&l));
	ret = clist_rem_next(&l, p, &data);
	fail_unless(ret == 0, "clist_rem_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(data == &head_data,
		    "removed data is %p but should be %p\n",
		    data, &head_data);
	fail_unless(clist_size(&l) == 1,
		    "list size is %d but should be 0\n", clist_size(&l));

	data = clist_element_data(clist_head(&l));
	fail_unless(data == &sec_data,
		    "list head data is %p but should be %p\n",
		    data, &sec_data);
	p = clist_element_next(clist_head(&l));
	fail_unless(p == clist_head(&l),
		    "head's next [%p] is not pointing to the head [%p]\n",
		    p, clist_head(&l));

	free(clist_head(&l));
}
END_TEST

START_TEST(clist_rem_next_sec_test)
{
	CList l;
	void *data;
	CListElmt *p;
	int ret, head_data, sec_data;

	clist_init(&l, NULL);

	ret = clist_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = clist_ins_next(&l, clist_head(&l), &sec_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	/* removes the second element */
	ret = clist_rem_next(&l, clist_head(&l), &data);
	fail_unless(ret == 0, "clist_rem_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(data == &sec_data,
		    "removed data is %p but should be %p\n",
		    data, &sec_data);
	fail_unless(clist_size(&l) == 1,
		    "list size is %d but should be 0\n", clist_size(&l));

	data = clist_element_data(clist_head(&l));
	fail_unless(data == &head_data,
		    "list head data is %p but should be %p\n",
		    data, &head_data);
	p = clist_element_next(clist_head(&l));
	fail_unless(p == clist_head(&l),
		    "head's next [%p] is not pointing to the head [%p]\n",
		    p, clist_head(&l));

	free(clist_head(&l));
}
END_TEST

START_TEST(clist_rem_next_single_test)
{
	CList l;
	void *data;
	int ret, head_data;

	clist_init(&l, NULL);

	ret = clist_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = clist_rem_next(&l, clist_head(&l), &data);
	fail_unless(ret == 0, "clist_rem_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(data == &head_data,
		    "removed data is %p but should be %p\n",
		    data, &head_data);
	fail_unless(clist_size(&l) == 0,
		    "list size is %d but should be 0\n", clist_size(&l));
	fail_unless(clist_head(&l) == NULL,
		    "list head is %p but should be NULL\n", clist_head(&l));
}
END_TEST

START_TEST(clist_rem_next_no_el_test)
{
	CList l;
	CListElmt *el;
	void *data;
	int ret;

	clist_init(&l, NULL);

	ret = clist_rem_next(&l, el, data);
	fail_unless(ret == -1, "ret %d\n", ret);
	fail_unless(errno == EINVAL, "errno: %s\n", strerror(errno));
}
END_TEST

START_TEST(clist_rem_next_empty_test)
{
	CList l;
	int ret;
	void *data;

	clist_init(&l, NULL);

	ret = clist_rem_next(&l, NULL, data);
	fail_unless(ret == -1, "ret %d\n", ret);
	fail_unless(errno == EINVAL, "errno: %s\n", strerror(errno));
}
END_TEST

START_TEST(clist_ins_next_mid_test)
{
	CList l;
	void *data;
	CListElmt *p;
	int ret, head_data, sec_data, thi_data;

	clist_init(&l, NULL);

	ret = clist_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = clist_ins_next(&l, clist_head(&l), &sec_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	p = clist_element_next(clist_head(&l));

	ret = clist_ins_next(&l, p, &thi_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(clist_size(&l) == 3,
		    "list size is %d but should be 3\n", clist_size(&l));

	data = clist_element_data(clist_head(&l));
	fail_unless(data == &head_data,
		    "list head data is %p but should be %p\n",
		    data, &head_data);

	p = clist_element_next(clist_head(&l));
	data = clist_element_data(p);
	fail_unless(data == &sec_data,
		    "list sec data is %p but should be %p\n",
		    data, &sec_data);

	p = clist_element_next(p);
	data = clist_element_data(p);
	fail_unless(data == &thi_data,
		    "list thi data is %p but should be %p\n",
		    data, &thi_data);

	p = clist_element_next(p);
	fail_unless(p == clist_head(&l),
		    "thi's next [%p] is not pointing to the head [%p]\n",
		    p, clist_head(&l));

	p = clist_element_next(clist_head(&l));
	p = clist_element_next(p);
	free(p);
	free(clist_element_next(clist_head(&l)));
	free(clist_head(&l));
}
END_TEST

START_TEST(clist_ins_next_head_mid_test)
{
	CList l;
	void *data;
	CListElmt *p;
	int ret, head_data, sec_data, thi_data;

	clist_init(&l, NULL);

	ret = clist_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = clist_ins_next(&l, clist_head(&l), &thi_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = clist_ins_next(&l, clist_head(&l), &sec_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(clist_size(&l) == 3,
		    "list size is %d but should be 3\n", clist_size(&l));

	data = clist_element_data(clist_head(&l));
	fail_unless(data == &head_data,
		    "list head data is %p but should be %p\n",
		    data, &head_data);

	p = clist_element_next(clist_head(&l));
	data = clist_element_data(p);
	fail_unless(data == &sec_data,
		    "list sec data is %p but should be %p\n",
		    data, &sec_data);

	p = clist_element_next(p);
	data = clist_element_data(p);
	fail_unless(data == &thi_data,
		    "list thi data is %p but should be %p\n",
		    data, &thi_data);

	p = clist_element_next(p);
	fail_unless(p == clist_head(&l),
		    "thi's next [%p] is not pointing to the head [%p]\n",
		    p, clist_head(&l));

	p = clist_element_next(clist_head(&l));
	p = clist_element_next(p);
	free(p);
	free(clist_element_next(clist_head(&l)));
	free(clist_head(&l));
}
END_TEST

START_TEST(clist_ins_next_two_test)
{
	CList l;
	void *data;
	CListElmt *p;
	int ret, head_data, sec_data;

	clist_init(&l, NULL);

	ret = clist_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = clist_ins_next(&l, clist_head(&l), &sec_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(clist_size(&l) == 2,
		    "list size is %d but should be 2\n", clist_size(&l));
	data = clist_element_data(clist_head(&l));
	fail_unless(data == &head_data,
		    "list head data is %p but should be %p\n",
		    data, &head_data);

	p = clist_element_next(clist_head(&l));
	data = clist_element_data(p);
	fail_unless(data == &sec_data,
		    "list head next data is %p but should be %p\n",
		    data, &sec_data);
	p = clist_element_next(p);
	fail_unless(p == clist_head(&l),
		    "sec's next [%p] is not pointing to the head [%p]\n",
		    p, clist_head(&l));

	free(clist_element_next(clist_head(&l)));
	free(clist_head(&l));
}
END_TEST

START_TEST(clist_ins_next_null_test)
{
	CList l;
	int ret, data;

	clist_init(&l, NULL);

	ret = clist_ins_next(&l, NULL, &data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = clist_ins_next(&l, NULL, &data);
	fail_unless(ret == -1, "clist_ins_next() didn't fail: %d\n", ret);
	fail_unless(errno == EINVAL, "errno is %d but should be %d\n",
		    errno, EINVAL);
	fail_unless(clist_size(&l) == 1,
		    "list size is %d but should be 1\n", clist_size(&l));

	free(clist_head(&l));
}
END_TEST

START_TEST(clist_ins_next_one_test)
{
	CList l;
	void *data;
	CListElmt *p;
	int ret, head_data;

	clist_init(&l, NULL);

	ret = clist_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "clist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(clist_size(&l) == 1,
		    "list size is %d but should be 1\n", clist_size(&l));
	data = clist_element_data(clist_head(&l));
	fail_unless(data == &head_data,
		    "list head data is %p but should be %p\n",
		    data, &head_data);
	p = clist_element_next(clist_head(&l));
	fail_unless(p == clist_head(&l),
		    "head's next [%p] is not pointing to the head [%p]\n",
		    p, clist_head(&l));

	free(clist_head(&l));
}
END_TEST

static void my_destroy(void *data)
{
	/* makes gcc happy */
	data = data;
}

START_TEST(clist_init_test)
{
	CList l;

	clist_init(&l, my_destroy);
	fail_unless(clist_size(&l) == 0, "list size is %d but should be 0\n",
		    clist_size(&l));
	fail_unless(l.destroy == my_destroy,
		    "list destroy is %p but should be %p\n", l.destroy,
		    my_destroy);
	fail_unless(clist_head(&l) == NULL,
		    "list head is %p but should be NULL\n", clist_head(&l));
}
END_TEST

START_TEST(clist_element_next_test)
{
	CListElmt el, el_next;

	el.next = &el_next;
	fail_unless(clist_element_next(&el) ==  (void *) &el_next,
		    "Element's next %p doesn't match [%p]\n", el.next,
		    &el_next);
}
END_TEST

START_TEST(clist_element_data_test)
{
	int data;
	CListElmt el;

	el.data = (void *) &data;
	fail_unless(clist_element_data(&el) == (void *) &data,
		    "Element's data %p doesn't match [%p]\n", el.data, data);
}
END_TEST

START_TEST(clist_head_test)
{
	CList l;
	CListElmt el;

	l.head = &el;
	fail_unless(clist_head(&l) == &el,
		    "Element %p is not the head [%p]\n", &el, l.head);
}
END_TEST

START_TEST(clist_size_test)
{
	const int size = 15;
	int ret;
	CList l;

	l.size = size;
	ret = clist_size(&l);
	fail_unless(ret == size, "list size is %d but should be %d\n",
		    ret, size);
}
END_TEST

static Suite *clinked_list_suite(void)
{
	Suite *s;
	TCase *tc_interface;

	s = suite_create("Circular linked list implementation test-suite");

	/*
	 * This suite makes the basic interface test, it's goal
	 * is to assure that the basic functionality is working
	 */
	tc_interface = tcase_create("Basic functionality");
	suite_add_tcase(s, tc_interface);
	tcase_add_test(tc_interface, clist_size_test);
	tcase_add_test(tc_interface, clist_head_test);
	tcase_add_test(tc_interface, clist_element_data_test);
	tcase_add_test(tc_interface, clist_element_next_test);
	tcase_add_test(tc_interface, clist_init_test);
	tcase_add_test(tc_interface, clist_ins_next_one_test);
	tcase_add_test(tc_interface, clist_ins_next_null_test);
	tcase_add_test(tc_interface, clist_ins_next_two_test);
	tcase_add_test(tc_interface, clist_ins_next_head_mid_test);
	tcase_add_test(tc_interface, clist_ins_next_mid_test);
	tcase_add_test(tc_interface, clist_rem_next_no_el_test);
	tcase_add_test(tc_interface, clist_rem_next_empty_test);
	tcase_add_test(tc_interface, clist_rem_next_single_test);
	tcase_add_test(tc_interface, clist_rem_next_sec_test);
	tcase_add_test(tc_interface, clist_rem_next_head_test);
	tcase_add_test(tc_interface, clist_rem_next_mid_test);
	tcase_add_test(tc_interface, clist_destroy_test);
	tcase_add_test(tc_interface, clist_transversal_test);

	return s;
}

int main(void)
{
	int nf;
	Suite *s;
	SRunner *sr;

	s = clinked_list_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (nf == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
