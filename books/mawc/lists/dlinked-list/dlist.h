#ifndef DLIST_H
#define DLIST_H

/*
 * Main structures: DList element and DList
 */
typedef struct DListElmt_ {
	void *data;
	struct DListElmt_ *next;
	struct DListElmt_ *prev;
} DListElmt;

typedef struct DList_ {
	int size;
	void (*destroy)(void *data);
	DListElmt *head;
	DListElmt *tail;
} DList;

#define dlist_size(dlist)      ((dlist)->size)
#define dlist_is_head(el)      ((el)->prev == NULL)
#define dlist_is_tail(el)      ((el)->next == NULL)
#define dlist_head(dlist)      ((dlist)->head)
#define dlist_tail(dlist)      ((dlist)->tail)
#define dlist_element_data(el) ((el)->data)
#define dlist_element_next(el) ((el)->next)
#define dlist_element_prev(el) ((el)->prev)

void dlist_init(DList *dlist, void (*destroy)(void *data));
int dlist_ins_next(DList *dlist, DListElmt *element, void *data);
int dlist_ins_prev(DList *dlist, DListElmt *element, void *data);
int dlist_remove(DList *dlist, DListElmt *el, void **data);
void dlist_destroy(DList *dlist);
#endif /* DLIST_H */
