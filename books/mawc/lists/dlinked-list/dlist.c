#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include "dlist.h"

void dlist_init(DList *dlist, void (*destroy)(void *data))
{
	dlist->size = 0;
	dlist->destroy = destroy;
	dlist->head = dlist->tail = NULL;
}

int dlist_ins_next(DList *dlist, DListElmt *el, void *data)
{
	DListElmt *new_el;

	new_el = malloc((sizeof(DListElmt)));
	if (!new_el)
		return -1;

	new_el->data = data;
	new_el->prev = el;

	if (!el) {
		if (dlist_size(dlist)) {
			free(new_el);
			errno = EINVAL;
			return -1;
		}
		/* First Element */
		new_el->next = NULL;
		dlist->head = dlist->tail = new_el;
	} else {
		new_el->next = el->next;
		if (dlist_is_tail(el))
			dlist->tail = new_el;
		else
			el->next->prev = new_el;
		el->next = new_el;
	}

	dlist->size++;
	return 0;
}

int dlist_ins_prev(DList *dlist, DListElmt *el, void *data)
{
	DListElmt *new_el;

	new_el = malloc((sizeof(DListElmt)));
	if (!new_el)
		return -1;

	new_el->data = data;
	new_el->next = el;

	if (!el) {
		if (dlist_size(dlist)) {
			free(new_el);
			errno = EINVAL;
			return -1;
		}
		/* First Element */
		new_el->prev = NULL;
		dlist->head = dlist->tail = new_el;
	} else {
		new_el->prev = el->prev;
		if (dlist_is_head(el))
			dlist->head = new_el;
		else
			el->prev->next = new_el;
		el->prev = new_el;
	}

	dlist->size++;
	return 0;
}

int dlist_remove(DList *dlist, DListElmt *el, void **data)
{
	if (!el || dlist_size(dlist) == 0) {
		errno = EINVAL;
		return -1;
	}

	*data = el->data;

	if (dlist_size(dlist) == 1) {
		dlist->head = dlist->tail = NULL;
	} else if (dlist_is_head(el)) {
		/* New head */
		el->next->prev = NULL;
		dlist->head = el->next;
	} else if (dlist_is_tail(el)) {
		/* New tail */
		el->prev->next = NULL;
		dlist->tail = el->prev;
	} else {
		el->next->prev = el->prev;
		el->prev->next = el->next;
	}

	free(el);
	dlist->size--;

	return 0;
}

void dlist_destroy(DList *dlist)
{
	int err;
	void *data;

	if (!dlist)
		return;

	while (dlist_size(dlist) > 0) {
		err = dlist_remove(dlist, dlist_head(dlist), &data);
		if (!err && dlist->destroy)
			dlist->destroy(data);
	}
}
