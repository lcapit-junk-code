#include <stdlib.h>
#include <check.h>
#include <errno.h>
#include <string.h>
#include "dlist.h"

static int destroy_called;


START_TEST(dlist_remove_infinite_loop_test)
{
	DList l;

	/*
	 * If list's size becomes negative for whatever reason,
	 * we should not enter in a infinite loop
	 */

	dlist_init(&l, NULL);
	l.size = -1;

	dlist_destroy(&l);
	fail_unless(dlist_size(&l) == -1, "list size is %d but should be -1\n",
		    dlist_size(&l));
}
END_TEST

static void my_destroy3(void *data)
{
	data = data; /* makes gcc happy */
	destroy_called++;
}

START_TEST(dlist_remove_null_data_test)
{
	DList l;
	int i, ret;
	const int max_objs = 3;

	destroy_called = 0;

	/*
	 * The destroy method must be called even when
	 * data is NULL
	 */

	dlist_init(&l, my_destroy3);

	for (i = 0; i < max_objs; i++) {
		ret = dlist_ins_next(&l, dlist_head(&l), NULL);
		fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
			    ret, strerror(errno));
	}

	dlist_destroy(&l);
	fail_unless(destroy_called == max_objs,
		    "destroy function was called %d times but should be %d\n",
		    destroy_called, max_objs);
}
END_TEST

#define TRANS_TEST_NR_NODES 3072

START_TEST(dlist_left_to_right_test)
{
	DList l;
	DListElmt *p;
	int i, ret, el_data, data[TRANS_TEST_NR_NODES];

	dlist_init(&l, NULL);

	/* Fill the list (left-to-right) */
	for (i = 0; i < TRANS_TEST_NR_NODES; i++) {
		data[i] = i;
		ret = dlist_ins_prev(&l, dlist_head(&l), &data[i]);
		fail_unless(ret == 0, "dlist_ins_prev() failed [%d]: %s\n",
			    ret, strerror(errno));
	}

	/* Check list's data (left-to-right) */
	for (p = dlist_tail(&l), i = 0; p; p = dlist_element_prev(p), i++) {
		el_data = *(int *) dlist_element_data(p);
		fail_unless(el_data == i,
			    "Element %d data is %d but should be %d\n",
			    i, el_data, i);
	}

	dlist_destroy(&l);
	fail_unless(dlist_size(&l) == 0, "list size is %d but should be 0\n",
		    dlist_size(&l));
}
END_TEST

START_TEST(dlist_right_to_left_test)
{
	DList l;
	DListElmt *p;
	int i, ret, el_data, data[TRANS_TEST_NR_NODES];

	dlist_init(&l, NULL);

	/* Fill the list (right-to-left) */
	for (i = 0; i < TRANS_TEST_NR_NODES; i++) {
		data[i] = i;
		ret = dlist_ins_next(&l, dlist_tail(&l), &data[i]);
		fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
			    ret, strerror(errno));
	}

	/* Check list's data (right-to-left) */
	for (p = dlist_head(&l), i = 0; p; p = dlist_element_next(p), i++) {
		el_data = *(int *) dlist_element_data(p);
		fail_unless(el_data == i,
			    "Element %d data is %d but should be %d\n",
			    i, el_data, i);
	}

	dlist_destroy(&l);
	fail_unless(dlist_size(&l) == 0, "list size is %d but should be 0\n",
		    dlist_size(&l));
}
END_TEST

START_TEST(dlist_ins_next_error_test)
{
	int ret;
	DList l;

	dlist_init(&l, NULL);

	ret = dlist_ins_next(&l, NULL, NULL);
	fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_ins_next(&l, NULL, NULL);
	fail_unless(ret == -1, "dlist_ins_next() didn't fail: %d\n", ret);
	fail_unless(errno == EINVAL, "errno: %d\n", errno);

	free(dlist_head(&l));
}
END_TEST

START_TEST(dlist_ins_prev_error_test)
{
	int ret;
	DList l;

	dlist_init(&l, NULL);

	ret = dlist_ins_prev(&l, NULL, NULL);
	fail_unless(ret == 0, "dlist_ins_prev() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_ins_prev(&l, NULL, NULL);
	fail_unless(ret == -1, "dlist_ins_prev() didn't fail: %d\n", ret);
	fail_unless(errno == EINVAL, "errno: %d\n", errno);

	free(dlist_head(&l));
}
END_TEST

START_TEST(dlist_remove_empty_test)
{
	int ret;
	DList l;
	void *data;
	DListElmt el;

	dlist_init(&l, NULL);

	ret = dlist_remove(&l, &el, &data);
	fail_unless(ret == -1, "dlist_remove() didn't fail: %d\n", ret);
	fail_unless(errno == EINVAL, "errno is %d but should be %d\n",
		    errno, EINVAL);
}
END_TEST

START_TEST(dlist_destroy_empty_test)
{
	DList l;

	dlist_init(&l, NULL);
	dlist_destroy(&l);
	fail_unless(dlist_size(&l) == 0, "list size is %d but should be 0\n",
		    dlist_size(&l));
}
END_TEST

START_TEST(dlist_destroy_null_test)
{
	dlist_destroy(NULL);
}
END_TEST

static void my_destroy2(void *data)
{
	free(data);
	destroy_called++;
}

START_TEST(dlist_destroy_test)
{
	DList l;
	void *p;
	int ret, i;
	const int max_objs = 20;

	destroy_called = 0;

	dlist_init(&l, my_destroy2);

	for (i = 0; i < max_objs; i++) {
		p = malloc(sizeof(int));
		fail_unless(p != NULL,
			    "Could not allocate memory: %s\n",
			    strerror(errno));

		ret = dlist_ins_next(&l, dlist_tail(&l), p);
		fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
			    ret, strerror(errno));
	}

	dlist_destroy(&l);

	fail_unless(dlist_size(&l) == 0, "list size is %d but should be 0\n",
		    dlist_size(&l));
	fail_unless(destroy_called == max_objs,
		    "destroy function called %d times, but should be %d\n",
		    destroy_called, max_objs);
}
END_TEST

START_TEST(dlist_remove_mid_test)
{
	DList l;
	void *data;
	DListElmt *p;
	int ret, head_data, mid_data, tail_data;

	dlist_init(&l, NULL);

	ret = dlist_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_ins_next(&l, dlist_head(&l), &tail_data);
	fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_ins_prev(&l, dlist_tail(&l), &mid_data);
	fail_unless(ret == 0, "dlist_ins_prev() failed [%d]: %s\n",
		    ret, strerror(errno));

	p = dlist_element_prev(dlist_tail(&l));
	ret = dlist_remove(&l, p, &data);
	fail_unless(ret == 0, "dlist_remove() failed [%d]: %s\n",
		    ret, strerror(errno));
	fail_unless(data == &mid_data,
		    "returned data is %p but should be %p\n",
		    data, &tail_data);
	fail_unless(dlist_size(&l) == 2,
		    "list size is %d but should be 2\n", dlist_size(&l));

	p = dlist_element_next(dlist_head(&l));
	fail_unless(p == dlist_tail(&l),
		    "head's next [%p] is not the tail [%p]\n",
		    p, dlist_tail(&l));
	p = dlist_element_prev(dlist_head(&l));
	fail_unless(p == NULL, "head's prev is %p but should be NULL\n", p);
	data = dlist_element_data(dlist_head(&l));
	fail_unless(data == &head_data,
		    "tail data is %p but should be %p\n", data, tail_data);

	p = dlist_element_prev(dlist_tail(&l));
	fail_unless(p == dlist_head(&l),
		    "tail's prev [%p] is not the head [%p]\n",
		    p, dlist_head(&l));
	p = dlist_element_next(dlist_tail(&l));
	fail_unless(p == NULL, "tail's next is %p but should be NULL\n", p);
	data = dlist_element_data(dlist_tail(&l));
	fail_unless(data == &tail_data,
		    "tail data is %p but should be %p", data, &tail_data);

	free(dlist_head(&l));
	free(dlist_tail(&l));
}
END_TEST

START_TEST(dlist_remove_tail_test)
{
	DList l;
	void *data;
	DListElmt *p;
	int ret, head_data, tail_data;

	dlist_init(&l, NULL);

	ret = dlist_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_ins_next(&l, dlist_head(&l), &tail_data);
	fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_remove(&l, dlist_tail(&l), &data);
	fail_unless(ret == 0, "dlist_remove() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(data == &tail_data,
		    "returned data is %p but should be %p\n",
		    data, &tail_data);
	fail_unless(dlist_size(&l) == 1,
		    "list size is %d but should be 1\n", dlist_size(&l));

	fail_unless(dlist_head(&l) == dlist_tail(&l),
		    "list head [%p] and tail [%p] differ\n",
		    dlist_head(&l), dlist_tail(&l));
	fail_unless(dlist_head(&l) != NULL, "list tail is NULL\n");
	data = dlist_element_data(dlist_head(&l));
	fail_unless(data == &head_data,
		    "tail data is %p but should be %p\n", data, tail_data);

	p = dlist_element_prev(dlist_head(&l));
	fail_unless(p == NULL,
		    "Element's prev is %p but should be NULL\n", p);
	p = dlist_element_next(dlist_head(&l));
	fail_unless(p == NULL,
		    "Element's next is %p but should be NULL\n", p);

	free(dlist_head(&l));
}
END_TEST

START_TEST(dlist_remove_head_test)
{
	DList l;
	void *data;
	DListElmt *p;
	int ret, head_data, tail_data;

	dlist_init(&l, NULL);

	ret = dlist_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_ins_next(&l, dlist_head(&l), &tail_data);
	fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_remove(&l, dlist_head(&l), &data);
	fail_unless(ret == 0, "dlist_remove() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(data == &head_data,
		    "returned data is %p but should be %p\n",
		    data, &head_data);
	fail_unless(dlist_size(&l) == 1,
		    "list size is %d but should be 1\n", dlist_size(&l));

	fail_unless(dlist_head(&l) == dlist_tail(&l),
		    "list head [%p] and tail [%p] differ\n",
		    dlist_head(&l), dlist_tail(&l));
	fail_unless(dlist_tail(&l) != NULL, "list tail is NULL\n");
	data = dlist_element_data(dlist_tail(&l));
	fail_unless(data == &tail_data,
		    "tail data is %p but should be %p\n", data, tail_data);

	p = dlist_element_prev(dlist_tail(&l));
	fail_unless(p == NULL,
		    "Element's prev is %p but should be NULL\n", p);
	p = dlist_element_next(dlist_tail(&l));
	fail_unless(p == NULL,
		    "Element's next is %p but should be NULL\n", p);

	free(dlist_tail(&l));
}
END_TEST

START_TEST(dlist_remove_single_test)
{
	DList l;
	void *data;
	int ret, head_data;

	dlist_init(&l, NULL);

	ret = dlist_ins_prev(&l, NULL, &head_data);
	fail_unless(ret == 0, "dlist_ins_prev() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_remove(&l, dlist_head(&l), &data);
	fail_unless(ret == 0, "dlist_remove() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(data == &head_data,
		    "returned data is %p but should be %p\n",
		    data, &head_data);
	fail_unless(dlist_size(&l) == 0,
		    "list size is %d but should be 0\n", dlist_size(&l));
	fail_unless(dlist_head(&l) == NULL,
		    "head is %p but should be NULL", dlist_head(&l));
	fail_unless(dlist_tail(&l) == NULL,
		    "tail is %p but should be NULL", dlist_tail(&l));
}
END_TEST

START_TEST(dlist_ins_prev_mid_test)
{
	DList l;
	void *data;
	DListElmt *p;
	int ret, mid_data, head_data, tail_data;

	dlist_init(&l, NULL);

	/* Will go to the tail */
	ret = dlist_ins_prev(&l, NULL, &tail_data);
	fail_unless(ret == 0, "dlist_ins_prev() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_ins_prev(&l, dlist_head(&l), &head_data);
	fail_unless(ret == 0, "dlist_ins_prev() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_ins_prev(&l, dlist_tail(&l), &mid_data);
	fail_unless(ret == 0, "dlist_ins_prev() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(dlist_size(&l) == 3,
		    "list size is %d but should be 3\n", dlist_size(&l));

	data = dlist_element_data(dlist_head(&l));
	fail_unless(data == &head_data,
		    "head data is %p but should be %p", data, &head_data);
	p = dlist_element_next(dlist_head(&l));
	fail_unless(p == (dlist_tail(&l))->prev,
		    "head next is %p but should be %p",
		    p, (dlist_tail(&l))->prev);
	p = dlist_element_prev(dlist_head(&l));
	fail_unless(p == NULL, "head next is %p but should be NULL\n", p);

	data = dlist_element_data(dlist_tail(&l));
	fail_unless(data == &tail_data,
		    "tail data is %p but should be %p", data, &tail_data);
	p = dlist_element_next(dlist_tail(&l));
	fail_unless(p == NULL, "head prev is %p but should be NULL\n", p);

	p = dlist_element_next(dlist_head(&l));
	data = dlist_element_data(p);
	fail_unless(data == &mid_data,
		    "mid data is %p but should be %p", data, &mid_data);
	fail_unless(p->prev == dlist_head(&l),
		    "Element's prev [%p] should be the head [%p]\n",
		    p->prev, dlist_head(&l));
	fail_unless(p->next == dlist_tail(&l),
		    "Element's next [%p] should be the tail [%p]\n",
		    p->next, dlist_tail(&l));

	free(p);
	free(dlist_head(&l));
	free(dlist_tail(&l));
}
END_TEST

START_TEST(dlist_ins_prev_head_test)
{
	DList l;
	DListElmt *p;
	void *data;
	int ret, head_data, tail_data;

	dlist_init(&l, NULL);

	/* Will go to the tail */
	ret = dlist_ins_prev(&l, NULL, &tail_data);
	fail_unless(ret == 0, "dlist_ins_prev() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_ins_prev(&l, dlist_head(&l), &head_data);
	fail_unless(ret == 0, "dlist_ins_prev() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(dlist_size(&l) == 2,
		    "list size is %d but should be 2\n", dlist_size(&l));

	data = dlist_element_data(dlist_head(&l));
	fail_unless(data == &head_data,
		    "head data is %p but should be %p", data, &head_data);
	p = dlist_element_next(dlist_head(&l));
	fail_unless(p == dlist_tail(&l),
		    "head next is %p but should be %p", p, dlist_tail(&l));
	p = dlist_element_prev(dlist_head(&l));
	fail_unless(p == NULL, "head next is %p but should be NULL\n", p);

	data = dlist_element_data(dlist_tail(&l));
	fail_unless(data == &tail_data,
		    "tail data is %p but should be %p", data, &tail_data);
	p = dlist_element_prev(dlist_tail(&l));
	fail_unless(p == dlist_head(&l),
		    "tail prev is %p but should be %p", p, dlist_tail(&l));
	p = dlist_element_next(dlist_tail(&l));
	fail_unless(p == NULL, "head prev is %p but should be NULL\n", p);

	free(dlist_head(&l));
	free(dlist_tail(&l));
}
END_TEST

START_TEST(dlist_ins_prev_empty_test)
{
	DList l;
	DListElmt *p;
	void *el_data;
	int ret, data;

	dlist_init(&l, NULL);

	ret = dlist_ins_prev(&l, NULL, &data);
	fail_unless(ret == 0, "dlist_ins_prev() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(dlist_size(&l) == 1,
		    "list size is %d but should be 1\n", dlist_size(&l));

	fail_unless(dlist_head(&l) == dlist_tail(&l),
		    "list head [%p] and tail [%p] are different\n",
		    dlist_head(&l), dlist_tail(&l));
	el_data = dlist_element_data(dlist_head(&l));
	fail_unless(el_data == &data,
		    "list head data is %p but should be %p\n", el_data, data);

	p = dlist_element_next(dlist_head(&l));
	fail_unless(p == NULL, "head next is %p but should be NULL\n", p);
	p = dlist_element_prev(dlist_head(&l));
	fail_unless(p == NULL, "head prev is %p but should be NULL\n", p);

	free(dlist_head(&l));
}
END_TEST

START_TEST(dlist_ins_next_mid_test)
{
	DList l;
	void *data;
	DListElmt *p;
	int ret, head_data, tail_data, mid_data;

	dlist_init(&l, NULL);

	ret = dlist_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_ins_next(&l, dlist_tail(&l), &tail_data);
	fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_ins_next(&l, dlist_head(&l), &mid_data);
	fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(dlist_size(&l) == 3,
		    "list size is %d but should be 3\n", dlist_size(&l));

	data = dlist_element_data(dlist_head(&l));
	fail_unless(data == &head_data,
		    "head data is %p but should be %p", data, &head_data);
	p = dlist_element_next(dlist_head(&l));
	fail_unless(p == (dlist_tail(&l))->prev,
		    "head next is %p but should be %p",
		    p, (dlist_tail(&l))->prev);
	p = dlist_element_prev(dlist_head(&l));
	fail_unless(p == NULL, "head next is %p but should be NULL\n", p);

	data = dlist_element_data(dlist_tail(&l));
	fail_unless(data == &tail_data,
		    "tail data is %p but should be %p", data, &tail_data);
	p = dlist_element_next(dlist_tail(&l));
	fail_unless(p == NULL, "head prev is %p but should be NULL\n", p);

	p = dlist_element_next(dlist_head(&l));
	data = dlist_element_data(p);
	fail_unless(data == &mid_data,
		    "mid data is %p but should be %p", data, &mid_data);
	fail_unless(p->prev == dlist_head(&l),
		    "Element's prev [%p] should be the head [%p]\n",
		    p->prev, dlist_head(&l));
	fail_unless(p->next == dlist_tail(&l),
		    "Element's next [%p] should be the tail [%p]\n",
		    p->next, dlist_tail(&l));

	free(p);
	free(dlist_head(&l));
	free(dlist_tail(&l));
}
END_TEST

START_TEST(dlist_ins_next_tail_test)
{
	DList l;
	DListElmt *p;
	void *data;
	int ret, head_data, tail_data;

	dlist_init(&l, NULL);

	ret = dlist_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = dlist_ins_next(&l, dlist_tail(&l), &tail_data);
	fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(dlist_size(&l) == 2,
		    "list size is %d but should be 2\n", dlist_size(&l));

	data = dlist_element_data(dlist_head(&l));
	fail_unless(data == &head_data,
		    "head data is %p but should be %p", data, &head_data);
	p = dlist_element_next(dlist_head(&l));
	fail_unless(p == dlist_tail(&l),
		    "head next is %p but should be %p", p, dlist_tail(&l));
	p = dlist_element_prev(dlist_head(&l));
	fail_unless(p == NULL, "head next is %p but should be NULL\n", p);

	data = dlist_element_data(dlist_tail(&l));
	fail_unless(data == &tail_data,
		    "tail data is %p but should be %p", data, &tail_data);
	p = dlist_element_prev(dlist_tail(&l));
	fail_unless(p == dlist_head(&l),
		    "tail prev is %p but should be %p", p, dlist_tail(&l));
	p = dlist_element_next(dlist_tail(&l));
	fail_unless(p == NULL, "head prev is %p but should be NULL\n", p);

	free(dlist_head(&l));
	free(dlist_tail(&l));
}
END_TEST

START_TEST(dlist_ins_next_empty_test)
{
	DList l;
	DListElmt *p;
	void *el_data;
	int ret, data;

	dlist_init(&l, NULL);

	ret = dlist_ins_next(&l, NULL, &data);
	fail_unless(ret == 0, "dlist_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(dlist_size(&l) == 1,
		    "list size is %d but should be 1\n", dlist_size(&l));

	fail_unless(dlist_head(&l) == dlist_tail(&l),
		    "list head [%p] and tail [%p] are different\n",
		    dlist_head(&l), dlist_tail(&l));
	el_data = dlist_element_data(dlist_head(&l));
	fail_unless(el_data == &data,
		    "list head data is %p but should be %p\n", el_data, data);

	p = dlist_element_next(dlist_head(&l));
	fail_unless(p == NULL, "head next is %p but should be NULL\n", p);
	p = dlist_element_prev(dlist_head(&l));
	fail_unless(p == NULL, "head prev is %p but should be NULL\n", p);

	free(dlist_head(&l));
}
END_TEST

static void my_destroy(void *data)
{
	/* makes gcc happy */
	data = data;
}

START_TEST(dlist_init_test)
{
	DList l;

	dlist_init(&l, my_destroy);
	fail_unless(dlist_size(&l) == 0, "list size is %d but should be 0\n",
		    dlist_size(&l));
	fail_unless(l.destroy == my_destroy,
		    "list destroy is %p but should be %p\n", l.destroy,
		    my_destroy);
	fail_unless(dlist_head(&l) == NULL,
		    "list head is %p but should be NULL\n", dlist_head(&l));
	fail_unless(dlist_tail(&l) == NULL,
		    "list tail is %p but should be NULL\n", dlist_tail(&l));
}
END_TEST

START_TEST(dlist_element_prev_test)
{
	DListElmt el, el_prev;

	el.prev = &el_prev;
	fail_unless(dlist_element_prev(&el) == (void *) &el_prev,
		    "Element's prev %p doesn't match [%p]\n", el.prev,
		    &el_prev);
}
END_TEST

START_TEST(dlist_element_next_test)
{
	DListElmt el, el_next;

	el.next = &el_next;
	fail_unless(dlist_element_next(&el) == (void *) &el_next,
		    "Element's next %p doesn't match [%p]\n", el.next,
		    &el_next);
}
END_TEST

START_TEST(dlist_element_data_test)
{
	int a;
	DListElmt el;

	el.data = &a;
	fail_unless(dlist_element_data(&el) == (void *) &a,
		    "Element's data %p doesn't match [%p]\n", el.data,
		    &a);
}
END_TEST

START_TEST(dlist_tail_test)
{
	DList l;
	DListElmt el;

	l.tail = &el;
	fail_unless(dlist_tail(&l) == &el,
		    "Element %p is not the tail [%p]\n", &el, l.tail);
}
END_TEST

START_TEST(dlist_head_test)
{
	DList l;
	DListElmt el;

	l.head = &el;
	fail_unless(dlist_head(&l) == &el,
		    "Element %p is not the head [%p]\n", &el, l.head);
}
END_TEST

START_TEST(dlist_is_tail_test)
{
	DList l;
	DListElmt el;

	l.tail = &el;
	fail_unless(dlist_is_tail(&el),
		    "Element %p is not the tail [%p]\n", &el, l.tail);
}
END_TEST

START_TEST(dlist_is_head_test)
{
	DList l;
	DListElmt el;

	l.head = &el;
	fail_unless(dlist_is_head(&el),
		    "Element %p is not the head [%p]\n", &el, l.head);
}
END_TEST

START_TEST(dlist_size_test)
{
	const int size = 15;
	int ret;
	DList l;

	l.size = size;
	ret = dlist_size(&l);
	fail_unless(ret == size, "list size is %d but should be %d\n",
		    ret, size);
}
END_TEST

static Suite *dlinked_list_suite(void)
{
	Suite *s;
	TCase *tc_interface;
	TCase *tc_errors;
	TCase *tc_stress;
	TCase *tc_regression;

	s = suite_create("Doubly-Linked list implementation test-suite");

	/*
	 * This suite makes the basic interface test, it's goal
	 * is to assure that the basic functionality is working
	 */
	tc_interface = tcase_create("Basic functionality");
	suite_add_tcase(s, tc_interface);
	tcase_add_test(tc_interface, dlist_size_test);
	tcase_add_test(tc_interface, dlist_is_head_test);
	tcase_add_test(tc_interface, dlist_is_tail_test);
	tcase_add_test(tc_interface, dlist_head_test);
	tcase_add_test(tc_interface, dlist_tail_test);
	tcase_add_test(tc_interface, dlist_element_data_test);
	tcase_add_test(tc_interface, dlist_element_next_test);
	tcase_add_test(tc_interface, dlist_element_prev_test);
	tcase_add_test(tc_interface, dlist_init_test);
	tcase_add_test(tc_interface, dlist_ins_next_empty_test);
	tcase_add_test(tc_interface, dlist_ins_next_tail_test);
	tcase_add_test(tc_interface, dlist_ins_next_mid_test);
	tcase_add_test(tc_interface, dlist_ins_prev_empty_test);
	tcase_add_test(tc_interface, dlist_ins_prev_head_test);
	tcase_add_test(tc_interface, dlist_ins_prev_mid_test);
	tcase_add_test(tc_interface, dlist_remove_single_test);
	tcase_add_test(tc_interface, dlist_remove_head_test);
	tcase_add_test(tc_interface, dlist_remove_tail_test);
	tcase_add_test(tc_interface, dlist_remove_mid_test);
	tcase_add_test(tc_interface, dlist_destroy_test);

	/*
	 * Error tests
	 */
	tc_errors = tcase_create("Error tests");
	suite_add_tcase(s, tc_errors);
	tcase_add_test(tc_errors, dlist_ins_next_error_test);
	tcase_add_test(tc_errors, dlist_ins_prev_error_test);
	tcase_add_test(tc_errors, dlist_remove_empty_test);
	tcase_add_test(tc_errors, dlist_destroy_empty_test);
	tcase_add_test(tc_errors, dlist_destroy_null_test);

	/*
	 * Stress tests
	 */
	tc_stress = tcase_create("Stress tests");
	tcase_set_timeout(tc_stress, 0);
	suite_add_tcase(s, tc_stress);
	tcase_add_test(tc_stress, dlist_right_to_left_test);
	tcase_add_test(tc_stress, dlist_left_to_right_test);

	/*
	 * Regression tests
	 *
	 * Any bug found after the first implementation (or missing
	 * featues) must have a regression test here.
	 * 
	 * The bug (or feature) is explained in the test's code itself.
	 */
	tc_regression = tcase_create("Regression tests");
	suite_add_tcase(s, tc_regression);
	tcase_add_test(tc_regression, dlist_remove_null_data_test);
	tcase_add_test(tc_regression, dlist_remove_infinite_loop_test);

	return s;
}

int main(void)
{
	int nf;
	Suite *s;
	SRunner *sr;

	s = dlinked_list_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (nf == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
