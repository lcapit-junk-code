#ifndef LIST_H
#define LIST_H

/*
 * Main structures: List element and List
 */
typedef struct ListElmt_ {
	void *data;
	struct ListElmt_ *next;
} ListElmt;

typedef struct List_ {
	int size;
	void (*destroy)(void *data);
	ListElmt *head;
	ListElmt *tail;
} List;

/*
 * Public interface
 */

#define list_size(list)  ((list)->size)
#define list_is_head(list, element) ((list)->head == (element))
#define list_is_tail(list, element) ((list)->tail == (element))
#define list_head(list) ((list)->head)
#define list_tail(list) ((list)->tail)
#define list_element_data(element) ((element)->data)
#define list_element_next(element) ((element)->next)

void list_init(List *list, void (*destroy)(void *data));
int list_ins_next(List *list, ListElmt *element, void *data);
int list_rem_next(List *list, ListElmt *element, void **data);
void list_destroy(List *list);

#endif /* LIST_H */
