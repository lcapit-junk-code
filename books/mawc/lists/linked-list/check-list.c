#include <stdlib.h>
#include <check.h>
#include <string.h>
#include <errno.h>
#include "list.h"

/*
 * Ordering
 */

#define ORD_TEST_NR_NODES 3072

START_TEST(reverse_order_test)
{
	List l;
	ListElmt *el;
	int i, ret, data[ORD_TEST_NR_NODES];

	list_init(&l, NULL);

	for (i = 0; i < ORD_TEST_NR_NODES; i++) {
		data[i] = i;
		ret = list_ins_next(&l, NULL, &data[i]);
		fail_unless(ret == 0, "insert of %d failed with %d\n", i, ret);
	}

	for (el = list_head(&l), i = ORD_TEST_NR_NODES-1; el;
	     el = list_element_next(el), i--) {
		int *p = (int *) list_element_data(el);
		fail_unless(*p == i, "Returned %d but should be %d\n", *p, i);
	}

	list_destroy(&l);
}
END_TEST

START_TEST(sequential_order_test)
{
	List l;
	ListElmt *el;
	int i, ret, data[ORD_TEST_NR_NODES];

	list_init(&l, NULL);

	for (i = 0; i < ORD_TEST_NR_NODES; i++) {
		data[i] = i;
		ret = list_ins_next(&l, list_tail(&l), &data[i]);
		fail_unless(ret == 0, "insert of %d failed with %d\n", i, ret);
	}

	for (el = list_head(&l), i = 0; el; i++, el = list_element_next(el)) {
		int *p = (int *) list_element_data(el);
		fail_unless(*p == i, "Returned %d but should be %d\n", *p, i);
	}

	list_destroy(&l);
}
END_TEST

/*
 * Basic functionality
 */

static int destroy_called;

static void my_destroy2(void *data)
{
	free(data);
	destroy_called++;
}

START_TEST(list_destroy_test)
{
	List l;
	void *p;
	int ret, i;
	const int max_objs = 20;

	list_init(&l, my_destroy2);

	for (i = 0; i < max_objs; i++) {
		p = malloc(sizeof(int));
		fail_unless(p != NULL,
			    "Could not allocate memory: %s\n",
			    strerror(errno));

		ret = list_ins_next(&l, list_tail(&l), p);
		fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
			    ret, strerror(errno));
	}

	list_destroy(&l);

	fail_unless(list_size(&l) == 0, "list size is %d but should be 0\n",
		    list_size(&l));
	fail_unless(destroy_called == max_objs,
		    "destroy function called %d times, but should be %d\n",
		    destroy_called, max_objs);
}
END_TEST

START_TEST(list_rem_next_mid_test)
{
	List l;
	void *data;
	ListElmt *p;
	int ret, head_data, mid_data, tail_data;

	list_init(&l, NULL);

	ret = list_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));
	
	ret = list_ins_next(&l, list_tail(&l), &mid_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = list_ins_next(&l, list_tail(&l), &tail_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = list_rem_next(&l, list_head(&l), &data);
	fail_unless(ret == 0, "list_rem_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(list_size(&l) == 2, "list size is %d but should be 2\n",
		    list_size(&l));

	fail_unless(data == &mid_data,
		    "Removed data is %d but should be %d\n", &mid_data, data);

	data = list_element_data(list_head(&l));
	fail_unless(data == &head_data,
		    "head data is %p but should be %p", data, &head_data);
	p = list_element_next(list_head(&l));
	fail_unless(p == list_tail(&l),
		    "head next is %p but should be %p", p, list_tail(&l));

	data = list_element_data(list_tail(&l));
	fail_unless(data == &tail_data,
		    "tail data is %p but should be %p", data, &tail_data);
	p = list_element_next(list_tail(&l));
	fail_unless(p == NULL, "tail next is but should be NULL", p);

	free(list_head(&l));
	free(list_tail(&l));
}
END_TEST

START_TEST(list_rem_next_tail_test)
{
	List l;
	void *data;
	ListElmt *p;
	int ret, head_data, tail_data;

	list_init(&l, NULL);

	ret = list_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));
	
	ret = list_ins_next(&l, list_head(&l), &tail_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = list_rem_next(&l, list_head(&l), &data);
	fail_unless(ret == 0, "list_rem_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(list_size(&l) == 1, "list size is %d but should be 1\n",
		    list_size(&l));

	fail_unless(data == &tail_data,
		    "Removed data is %d but should be %d\n", &tail_data, data);

	fail_unless(list_head(&l) == list_tail(&l),
		    "list head [%p] and tail [%p] are different\n",
		    list_head(&l), list_tail(&l));
	data = list_element_data(list_head(&l));
	fail_unless(data == &head_data,
		    "list head data is %p but should be %p\n",
		    data, &head_data);

	p = list_element_next(list_tail(&l));
	fail_unless(p == NULL, "tail next is %p but should be NULL\n", p);

	free(list_head(&l));
}
END_TEST

START_TEST(list_rem_next_head_test)
{
	List l;
	void *data;
	ListElmt *p;
	int ret, head_data, tail_data;

	list_init(&l, NULL);

	ret = list_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = list_ins_next(&l, list_head(&l), &tail_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = list_rem_next(&l, NULL, &data);
	fail_unless(ret == 0, "list_rem_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(list_size(&l) == 1, "list size is %d but should be 1\n",
		    list_size(&l));

	fail_unless(data == &head_data,
		    "Removed data is %d but should be %d\n", &head_data, data);

	fail_unless(list_head(&l) == list_tail(&l),
		    "list head [%p] and tail [%p] are different\n",
		    list_head(&l), list_tail(&l));
	data = list_element_data(list_tail(&l));
	fail_unless(data == &tail_data,
		    "list tail data is %p but should be %p\n",
		    data, &tail_data);

	p = list_element_next(list_tail(&l));
	fail_unless(p == NULL, "tail next is %p but should be NULL\n", p);

	free(list_head(&l));
}
END_TEST

START_TEST(list_rem_next_single_test)
{
	List l;
	void *data;
	int ret, head_data;

	list_init(&l, NULL);

	ret = list_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = list_rem_next(&l, NULL, &data);
	fail_unless(ret == 0, "list_rem_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(list_size(&l) == 0, "list size is %d but should be 0\n",
		    list_size(&l));

	fail_unless(data == &head_data,
		    "Removed data is %d but should be %d\n", &head_data, data);

	fail_unless(list_head(&l) == NULL,
		    "list head is %p but should be NULL\n", list_head(&l));
	fail_unless(list_tail(&l) == NULL,
		    "list tail is %p but should be NULL\n", list_tail(&l));
}
END_TEST

START_TEST(list_rem_next_empty_test)
{
	List l;
	int ret;
	void *data;

	list_init(&l, NULL);

	ret = list_rem_next(&l, NULL, data);
	fail_unless(ret == -1, "ret %d\n", ret);
	fail_unless(errno == EINVAL, "errno: %s\n", strerror(errno));
}
END_TEST

START_TEST(list_ins_next_tail_test)
{
	List l;
	void *data;
	ListElmt *p;
	int ret, head_data, mid_data, tail_data;

	list_init(&l, NULL);

	ret = list_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));
	
	ret = list_ins_next(&l, list_head(&l), &mid_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = list_ins_next(&l, list_tail(&l), &tail_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(list_size(&l) == 3, "list size is %d but should be 3\n",
		    list_size(&l));

	data = list_element_data(list_head(&l));
	fail_unless(data == &head_data,
		    "head data is %p but should be %p", data, &head_data);

	p = list_element_next(list_head(&l));
	data = list_element_data(p);
	fail_unless(data == &mid_data,
		    "head data is %p but should be %p", data, &mid_data);
	p = list_element_next(p);
	fail_unless(p == list_tail(&l),
		    "Element's next is %p but should be %p\n",
		    p, list_tail(&l));

	data = list_element_data(list_tail(&l));
	fail_unless(data == &tail_data,
		    "tail data is %p but should be %p", data, &tail_data);
	p = list_element_next(list_tail(&l));
	fail_unless(p == NULL, "tail next is %p but should be NULL\n", p);

	free(list_element_next(list_head(&l)));
	free(list_head(&l));
	free(list_tail(&l));
}
END_TEST

START_TEST(list_ins_next_mid_test)
{
	List l;
	void *data;
	ListElmt *p;
	int ret, head_data, mid_data, tail_data;

	list_init(&l, NULL);

	/* Will go to the tail */
	ret = list_ins_next(&l, NULL, &tail_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = list_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = list_ins_next(&l, list_head(&l), &mid_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(list_size(&l) == 3, "list size is %d but should be 3\n",
		    list_size(&l));

	data = list_element_data(list_head(&l));
	fail_unless(data == &head_data,
		    "head data is %p but should be %p", data, &head_data);

	p = list_element_next(list_head(&l));
	data = list_element_data(p);
	fail_unless(data == &mid_data,
		    "head data is %p but should be %p", data, &mid_data);
	p = list_element_next(p);
	fail_unless(p == list_tail(&l),
		    "Element's next is %p but should be %p\n",
		    p, list_tail(&l));

	data = list_element_data(list_tail(&l));
	fail_unless(data == &tail_data,
		    "tail data is %p but should be %p", data, &tail_data);
	p = list_element_next(list_tail(&l));
	fail_unless(p == NULL, "tail next is %p but should be NULL\n", p);

	free(list_element_next(list_head(&l)));
	free(list_head(&l));
	free(list_tail(&l));
}
END_TEST

START_TEST(list_ins_next_head_test)
{
	List l;
	void *data;
	ListElmt *p;
	int ret, head_data, tail_data;

	list_init(&l, NULL);

	/* Will go to the tail */
	ret = list_ins_next(&l, NULL, &tail_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));
	
	ret = list_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(list_size(&l) == 2, "list size is %d but should be 2\n",
		    list_size(&l));

	data = list_element_data(list_head(&l));
	fail_unless(data == &head_data,
		    "head data is %p but should be %p", data, &head_data);
	p = list_element_next(list_head(&l));
	fail_unless(p == list_tail(&l),
		    "head next is %p but should be %p", p, list_tail(&l));

	data = list_element_data(list_tail(&l));
	fail_unless(data == &tail_data,
		    "tail data is %p but should be %p", data, &tail_data);
	p = list_element_next(list_tail(&l));
	fail_unless(p == NULL, "tail next is but should be NULL", p);

	free(list_tail(&l));
	free(list_head(&l));
}
END_TEST

START_TEST(list_ins_next_empty_test)
{
	List l;
	ListElmt *p;
	void *data;
	int ret, head_data;

	list_init(&l, NULL);

	ret = list_ins_next(&l, NULL, &head_data);
	fail_unless(ret == 0, "list_ins_next() failed [%d]: %s\n",
		    ret, strerror(errno));

	fail_unless(list_size(&l) == 1, "list size is %d but should be 1\n",
		    list_size(&l));

	fail_unless(list_head(&l) == list_tail(&l),
		    "list head [%p] and tail [%p] are different\n",
		    list_head(&l), list_tail(&l));
	data = list_element_data(list_head(&l));
	fail_unless(data == &head_data,
		    "list head data is %p but should be %p\n",
		    data, &head_data);

	p = list_element_next(list_head(&l));
	fail_unless(p == NULL, "head next is %p but should be NULL\n", p);

	free(list_head(&l));
}
END_TEST

static void my_destroy(void *data)
{
	/* makes gcc happy */
	data = data;
}

START_TEST(list_init_test)
{
	List l;

	list_init(&l, my_destroy);
	fail_unless(list_size(&l) == 0, "list size: %d\n", list_size(&l));
	fail_unless(l.destroy == my_destroy, "list destroy: %p\n", l.destroy);
	fail_unless(list_head(&l) == NULL, "list head: %p\n", list_head(&l));
	fail_unless(list_tail(&l) == NULL, "list tail: %p\n", list_tail(&l));
}
END_TEST

START_TEST(list_element_next_test)
{
	ListElmt el, el_next;

	el.next = &el_next;
	fail_unless(list_element_next(&el) ==  (void *) &el_next,
		    "Element's next %p doesn't match [%p]\n", el.next,
		    &el_next);
}
END_TEST

START_TEST(list_element_data_test)
{
	void *data;
	ListElmt el;

	el.data = data;
	fail_unless(list_element_data(&el) == data,
		    "Element's data %p doesn't match [%p]\n", el.data, data);
}
END_TEST

START_TEST(list_tail_test)
{
	List l;
	ListElmt el;

	l.tail = &el;
	fail_unless(list_tail(&l) == &el,
		    "Element %p is not the tail [%p]\n", &el, l.tail);
}
END_TEST

START_TEST(list_head_test)
{
	List l;
	ListElmt el;

	l.head = &el;
	fail_unless(list_head(&l) == &el,
		    "Element %p is not the head [%p]\n", &el, l.head);
}
END_TEST

START_TEST(list_is_tail_test)
{
	List l;
	ListElmt el;

	l.tail = &el;
	fail_unless(list_is_tail(&l, &el),
		    "Element %p is not the tail [%p]\n", &el, l.tail);
}
END_TEST

START_TEST(list_is_head_test)
{
	List l;
	ListElmt el;

	l.head = &el;
	fail_unless(list_is_head(&l, &el),
		    "Element %p is not the head [%p]\n", &el, l.head);
}
END_TEST

START_TEST(list_size_test)
{
	const int size = 15;
	int ret;
	List l;

	l.size = size;
	ret = list_size(&l);
	fail_unless(ret == size, "list size did not match: %d\n", ret);
}
END_TEST

static Suite *linked_list_suite(void)
{
	Suite *s;
	TCase *tc_interface;
	TCase *tc_ordering;

	s = suite_create("Linked list implementation test-suite");

	/*
	 * This suite makes the basic interface test, it's goal
	 * is to assure that the basic functionality is working
	 */
	tc_interface = tcase_create("Basic functionality");
	suite_add_tcase(s, tc_interface);
	tcase_add_test(tc_interface, list_size_test);
	tcase_add_test(tc_interface, list_is_head_test);
	tcase_add_test(tc_interface, list_is_tail_test);
	tcase_add_test(tc_interface, list_head_test);
	tcase_add_test(tc_interface, list_tail_test);
	tcase_add_test(tc_interface, list_element_data_test);
	tcase_add_test(tc_interface, list_element_next_test);
	tcase_add_test(tc_interface, list_init_test);
	tcase_add_test(tc_interface, list_ins_next_empty_test);
	tcase_add_test(tc_interface, list_ins_next_head_test);
	tcase_add_test(tc_interface, list_ins_next_mid_test);
	tcase_add_test(tc_interface, list_ins_next_tail_test);
	tcase_add_test(tc_interface, list_rem_next_empty_test);
	tcase_add_test(tc_interface, list_rem_next_single_test);
	tcase_add_test(tc_interface, list_rem_next_head_test);
	tcase_add_test(tc_interface, list_rem_next_tail_test);
	tcase_add_test(tc_interface, list_rem_next_mid_test);
	tcase_add_test(tc_interface, list_destroy_test);

	/*
	 * This suite assures that the insertion ordering is
	 * being followed as it should
	 */
	tc_ordering = tcase_create("Ordering");
	suite_add_tcase(s, tc_ordering);
	tcase_add_test(tc_ordering, sequential_order_test);
	tcase_add_test(tc_ordering, reverse_order_test);

	return s;
}

int main(void)
{
	int nf;
	Suite *s;
	SRunner *sr;

	s = linked_list_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (nf == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
