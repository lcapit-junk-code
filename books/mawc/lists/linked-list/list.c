#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "list.h"

void list_init(List *list, void (*destroy)(void *data))
{
	list->size = 0;
	list->destroy = destroy;
	list->head = list->tail = NULL;
}

int list_ins_next(List *list, ListElmt *el, void *data)
{
	ListElmt *new_el;

	new_el = malloc(sizeof(ListElmt));
	if (!new_el)
		return -1;
	new_el->data = data;

	if (el) {
		if (el == list_tail(list))
			list->tail = new_el; /* Inserts in the tail */

		new_el->next = el->next;
		el->next = new_el;
	} else {
		/* Inserts in the head */
		if (!list_head(list))
			list->tail = new_el; /* First element! */

		new_el->next = list->head;
		list->head = new_el;
	}

	list->size++;
	return 0;
}

int list_rem_next(List *list, ListElmt *el, void **data)
{
	ListElmt *old_el;

	if (!list_size(list)) {
		errno = EINVAL;
		return -1;
	}

	if (!el) {
		/* Removes from the head */
		old_el = list->head;

		*data = list->head->data;
		list->head = list->head->next;

		if (list_size(list) == 1)
			list->tail = NULL; /* Becomes empty */
	} else {
		old_el = el->next;

		*data = el->next->data;
		el->next = el->next->next;

		if (list_tail(list) == old_el)
			list->tail = el; /* New tail */
	}

	free(old_el);
	list->size--;
	return 0;
}

void list_destroy(List *list)
{
	int err;
	void *data;

	while (list_size(list) > 0) {
		err = list_rem_next(list, NULL, (void **)&data);
		if (!err && list->destroy)
			list->destroy(data);
	}
}
