#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int compare(const void *key1, const void *key2)
{
	if (*((const long *) key1) > *((const long *) key2))
		return 1;
	else if (*((const long *) key1) < *((const long *) key2))
		return -1;
	else
		return 0;
}

int insertion_sort(void *data, int size, int esize,
		   int (*compare) (const void *key1, const void *key2))
{
	char *a = data;
	void *key;
	int i, j;

	key = (char *) malloc(esize);
	if (!key)
		return -1;

	for (j = 1; j < size; j++) {
		memcpy(key, &a[j * esize], esize);
		i = j -1;

		while (i >= 0 && compare(&a[i * esize], key) > 0) {
			memcpy(&a[(i + 1) * esize], &a[i * esize], esize);
			i--;
		}

		memcpy(&a[(i + 1) * esize], key, esize);
	}

	free(key);
	return 0;
}

int main(int argc, char *argv[])
{
	int ret;
	long *nums;
	size_t len;
	unsigned int i;

	if (argc == 1) {
		printf("Usage: insertion-sort <num1> <num2> ... <numN>\n");
		exit(1);
	}

	len = argc - 1;
	nums = calloc(len, sizeof(long));
	if (!nums) {
		perror("calloc()");
		exit(1);
	}

	for (i = 0; i < len; i++) {
		nums[i] = strtol(argv[i + 1], (char **)NULL, 10);
		printf("%ld ", nums[i]);
	}
	printf("\n");

	ret = insertion_sort((void *) nums, len, sizeof(long), compare);
	if (ret) {
		perror("insertion_sort()");
		exit(1);
	}

	for (i = 0; i < len; i++)
		printf("%ld ", nums[i]);
	printf("\n");

	free(nums);
	return 0;
}
