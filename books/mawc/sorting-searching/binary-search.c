#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int bisearch(void *sorted, const void *target, int size, int esize,
		    int (*compare)(const void *key1, const void *key2))
{
	int left, middle, right;

	left = 0;
	right = size - 1;

	while (left <= right) {
		middle = (left + right) / 2;

		switch (compare((char *) sorted + (esize * middle), target)) {
		case -1:
			left = middle + 1;
			break;
		case 1:
			right = middle - 1;
			break;

		case 0:
			return middle;
		}
	}

	return -1;
}

int my_compare(const void *key1, const void *key2)
{
	int a = *((int *) key1);
	int b = *((int *) key2);

	if (a > b)
		return 1;

	if (a == b)
		return 0;

	return -1;
}

#define MAX_SIZE 360

int main(int argc, char *argv[])
{
	int i, idx, num, numbers[MAX_SIZE];

	if (argc != 2) {
		printf("Usage: binary-search < num >\n");
		return 1;
	}

	num = atoi(argv[1]);

	for (i = 0; i < MAX_SIZE; i++)
		numbers[i] = i;

	idx = bisearch(numbers, &num, MAX_SIZE, sizeof(int), my_compare);
	if (idx < 0)
		printf("Not found\n");
	else
		printf("found: %d\n", numbers[idx]);

	return 0;
}
