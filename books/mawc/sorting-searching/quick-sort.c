#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int compare(const void *key1, const void *key2)
{
	if (*((const long *) key1) > *((const long *) key2))
		return 1;
	else if (*((const long *) key1) < *((const long *) key2))
		return -1;
	else
		return 0;
}

int insertion_sort(void *data, int size, int esize,
		   int (*compare) (const void *key1, const void *key2))
{
	char *a = data;
	void *key;
	int i, j;

	key = (char *) malloc(esize);
	if (!key)
		return -1;

	for (j = 1; j < size; j++) {
		memcpy(key, &a[j * esize], esize);
		i = j -1;

		while (i >= 0 && compare(&a[i * esize], key) > 0) {
			memcpy(&a[(i + 1) * esize], &a[i * esize], esize);
			i--;
		}

		memcpy(&a[(i + 1) * esize], key, esize);
	}

	free(key);
	return 0;
}

int qpartition(void *data, int esize, int i, int k,
	       int (*compare) (const void *key1, const void *key2))
{
	char *a = data;
	void *pval, *tmp;
	int z, r[3];

	pval = malloc(esize);
	if (!pval)
		return -1;

	tmp = malloc(esize);
	if (!tmp) {
		free(pval);
		return -1;
	}

	for (z = 0; z < 3; z++)
		r[z] = (rand() % (k - i + 1)) + i;

	insertion_sort(r, 3, sizeof(int), compare);
	memcpy(pval, &a[r[1] * esize], esize);

	/* create two partitions around the value */
	i--, k++;

	while (1) {

		/* move left 'til an element is found in the wrong partition*/
		do {
			k--;
		} while (compare(&a[k * esize], pval) > 0);

		/* move right 'til an element is found in the wrong partition*/
		do {
			i++;
		} while (compare(&a[i * esize], pval) < 0);

		if (i >= k) {
			/* stop partitioning when the left and right cross */
			break;
		} else {
			/* swap the elements now under the left and right counters*/
			memcpy(tmp, &a[i * esize], esize);
			memcpy(&a[i * esize], &a[k * esize], esize);
			memcpy(&a[k * esize], tmp, esize);
		}
	}

	free(pval);
	free(tmp);

	/* position dividing the two partitions */
	return k;
}

int quick_sort(void *data, int size, int esize, int i, int k,
	       int (*compare) (const void *key1, const void *key2))
{
	int ret, j;

	while (i < k) {

		/* determine where to partition the elements */
		j = qpartition(data, esize, i, k, compare);
		if (j < 0)
			return -1;

		/* recursively sort the left partition */
		ret = quick_sort(data, size, esize, i, j, compare);
		if (ret < 0)
			return -1;

		i = j + 1;
	}

	return 0;
}

int set_seed(void)
{
	int fd, ret;
	size_t bytes;
	unsigned int seed;

	fd = open("/dev/random", O_RDONLY);
	if (fd < 0)
		return -1;

	bytes = read(fd, &seed, sizeof(seed));
	if (bytes != sizeof(seed)) {
		ret = -1;
		goto out;
	}

	ret = 0;
	srand(seed);

out:
	close(fd);
	return ret;
}

int main(int argc, char *argv[])
{
	int ret;
	long *nums;
	size_t len;
	unsigned int i;

	if (argc == 1) {
		printf("Usage: quick-sort <num1> <num2> ... <numN>\n");
		exit(1);
	}

	ret = set_seed();
	if (ret) {
		perror("set_seed()");
		free(nums);
		exit(1);
	}

	len = argc - 1;
	nums = calloc(len, sizeof(long));
	if (!nums) {
		perror("calloc()");
		exit(1);
	}

	for (i = 0; i < len; i++) {
		nums[i] = strtol(argv[i+1], (char **)NULL, 10);
		printf("%ld ", nums[i]);
	}
	printf("\n");

	ret = quick_sort((void *) nums, len, sizeof(long), 0, len - 1,
			 compare);
	if (ret) {
		perror("quick_sort()");
		free(nums);
		exit(1);
	}

	for (i = 0; i < len; i++)
		printf("%ld ", nums[i]);
	printf("\n");

	free(nums);
	return 0;
}
