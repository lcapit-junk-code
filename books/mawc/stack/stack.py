class Stack(list):
	def push(self, value):
		self.append(value)
	def size(self):
		return len(self)
	def peek(self):
		return self[len(self)-1]
	def destroy(self):
		del self[0:]
