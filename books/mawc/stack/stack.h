#ifndef STACK_H
#define STACK_H

#include <stdio.h>
#include <list.h>

typedef List Stack;

#define stack_init    list_init
#define stack_size    list_size
#define stack_destroy list_destroy

#define stack_peek(stack) ((stack)->head == NULL ? NULL : (stack)->head->data)

int stack_push(Stack *stack, void *data);
int stack_pop(Stack *stack, void **data);

#endif /* STACK_H */
