#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "stack.h"

static void show_prompt(void)
{
	printf(">>> ");
}

static int push_value(Stack *stack, int value)
{
	int *data;

	data = malloc(sizeof(int));
	if (!data)
		return 1;

	memcpy(data, &value, sizeof(int));

	return stack_push(stack, data);
}

#define CMD_SIZE 16

int main(void)
{
	Stack stack;
	int *data, ret;
	char cmd[CMD_SIZE];

	stack_init(&stack, free);

	for (;;) {
		cmd[0] = '\0';
		show_prompt();
		fgets(cmd, CMD_SIZE, stdin);

		if (cmd[0] == 'q') {
			break;
		} else if (cmd[0] == '\0') {
			putc('\n', stdout);
			break;
		} else if (cmd[0] == 'p' && cmd[1] == 'u') {
			int value = 0;

			sscanf(cmd+2, "%d", &value);

			ret = push_value(&stack, value);
			if (ret)
				perror("push_value()");
		} else if (cmd[0] == 'p' && cmd[1] == 'o') {
			ret = stack_pop(&stack, (void **) &data);
			if (ret) {
				perror("stack_pop()");
				continue;
			}

			printf("%d\n", *data);
			free(data);
		} else if (cmd[0] == 'k') {
			data = stack_peek(&stack);
			printf("%d\n", *data);
		} else if (cmd[0] == 'd') {
			stack_destroy(&stack);
		} else if (cmd[0] == 's') {
			printf("size: %d\n", stack_size(&stack));
		} else if (cmd[0] == 'P' && cmd[1] == 'u') {
			int i, max;

			sscanf(cmd+2, "%d", &max);
			printf("Power push for %d elements\n", max);

			for (i = 0; i < max; i++) {
				ret = push_value(&stack, i);
				if (ret) {
					perror("push_value()");
					exit(1);
				}
			}
		} else if (cmd[0] == 'P' && cmd[1] == 'o') {
			int i, max;

			printf("Power popping:\n");

			max = stack_size(&stack);

			for (i = 0; i < max; i++) {
				ret = stack_pop(&stack, (void **) &data);
				if (ret) {
					perror("stack_pop()");
					exit(1);
				}
				printf("%d\n", *data);
				free(data);
			}
		}
	}

	stack_destroy(&stack);
	return 0;
}
