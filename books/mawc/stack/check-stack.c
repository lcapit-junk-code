#include <stdlib.h>
#include <string.h>
#include <check.h>
#include <errno.h>
#include "stack.h"

static int destroy_called;

static void my_destroy2(void *data)
{
	data = data;
	destroy_called++;
}

START_TEST(stack_destroy_test)
{
	Stack s;
	int i, ret, value;
	const int max_objs = 20;

	stack_init(&s, my_destroy2);

	for (i = 0; i < max_objs; i++) {
		ret = stack_push(&s, &value);
		fail_unless(ret == 0, "stack_push() failed [%d]: %s\n",
			    ret, strerror(errno));
	}

	stack_destroy(&s);

	fail_unless(stack_size(&s) == 0, "stack size is %d but should be 0\n",
		    stack_size(&s));
	fail_unless(destroy_called == max_objs,
		    "destroy function called %d times, but should be %d\n",
		    destroy_called, max_objs);
}
END_TEST

START_TEST(stack_peek_head_null_test)
{
	Stack s;
	void *data;

	stack_init(&s, NULL);

	data = stack_peek(&s);
	fail_unless(data == NULL,
		    "stack's top is %p but should be NULL\n", data);
}
END_TEST

START_TEST(stack_peek_test)
{
	Stack s;
	void *data;
	int ret, value;

	stack_init(&s, NULL);

	ret = stack_push(&s, &value);
	fail_unless(ret == 0, "stack_push() failed [%d]: %s\n",
		    ret, strerror(errno));

	data = stack_peek(&s);
	fail_unless(data == &value,
		    "stack's top is %p but should be %p\n", data, &value);

	ret = stack_pop(&s, &data);
	fail_unless(ret == 0, "stack_pop() failed [%d]: %s\n",
		    ret, strerror(errno));
	fail_unless(data == &value,
		    "stack's top is %p but should be %p\n", data, &value);
}
END_TEST

START_TEST(stack_pop_three_test)
{
	Stack s;
	void *data;
	int ret, fvalue, svalue, tvalue;

	stack_init(&s, NULL);

	ret = stack_push(&s, &fvalue);
	fail_unless(ret == 0, "stack_push() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = stack_push(&s, &svalue);
	fail_unless(ret == 0, "stack_push() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = stack_push(&s, &tvalue);
	fail_unless(ret == 0, "stack_push() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = stack_pop(&s, &data);
	fail_unless(ret == 0, "stack_pop() failed [%d]: %s\n",
		    ret, strerror(errno));
	fail_unless(data == &tvalue,
		    "stack's top is %p but should be %p\n", data, &tvalue);

	ret = stack_pop(&s, &data);
	fail_unless(ret == 0, "stack_pop() failed [%d]: %s\n",
		    ret, strerror(errno));
	fail_unless(data == &svalue,
		    "stack's second data is %p but should be %p\n",
		    data, &svalue);

	ret = stack_pop(&s, &data);
	fail_unless(ret == 0, "stack_pop() failed [%d]: %s\n",
		    ret, strerror(errno));
	fail_unless(data == &fvalue,
		    "stack's third data is %p but should be %p\n",
		    data, &fvalue);
}
END_TEST

START_TEST(stack_pop_single_test)
{
	Stack s;
	void *data;
	int ret, value;

	stack_init(&s, NULL);

	ret = stack_push(&s, &value);
	fail_unless(ret == 0, "stack_push() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = stack_pop(&s, &data);
	fail_unless(ret == 0, "stack_pop() failed [%d]: %s\n",
		    ret, strerror(errno));
	fail_unless(data == &value,
		    "stack's top is %p but should be %p\n", data, &value);
}
END_TEST

START_TEST(stack_push_two_test)
{
	Stack s;
	void *data;
	ListElmt *p;
	int ret, tvalue, svalue;

	stack_init(&s, NULL);

	ret = stack_push(&s, &svalue);
	fail_unless(ret == 0, "stack_push() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = stack_push(&s, &tvalue);
	fail_unless(ret == 0, "stack_push() failed [%d]: %s\n",
		    ret, strerror(errno));

	data = list_element_data(list_head(&s));
	fail_unless(data == &tvalue,
		    "stack's top is %p but should be %p\n", data, &tvalue);

	p = list_element_next(list_head(&s));
	data = list_element_data(p);
	fail_unless(data == &svalue,
		    "stack's second data is %p but should be %p\n",
		    data, &svalue);

	free(p);
	free(list_head(&s));
}
END_TEST

START_TEST(stack_push_single_test)
{
	Stack s;
	void *data;
	int ret, value;

	stack_init(&s, NULL);

	ret = stack_push(&s, &value);
	fail_unless(ret == 0, "stack_push() failed [%d]: %s\n",
		    ret, strerror(errno));

	data = list_element_data(list_head(&s));
	fail_unless(data == &value,
		    "stack's top is %p but should be %p\n", data, &value);
	free(list_head(&s));
}
END_TEST

static void my_destroy(void *data)
{
	/* makes gcc happy */
	data = data;
}

START_TEST(stack_init_test)
{
	Stack s;

	stack_init(&s, my_destroy);
	fail_unless(stack_size(&s) == 0,
		    "stack size is %d but should be 0\n", stack_size(&s));
	fail_unless(s.destroy == my_destroy,
		    "stack destroy is %p but should be %p\n", s.destroy);
}
END_TEST

START_TEST(stack_size_test)
{
	const int size = 15;
	int ret;
	Stack s;

	s.size = size;
	ret = stack_size(&s);
	fail_unless(ret == size, "Stack size is %d but should be %d\n",
		    ret, size);
}
END_TEST

static Suite *stack_suite(void)
{
	Suite *s;
	TCase *st_interface;

	s = suite_create("Stack implementation test-suite");

	/*
	 * These tests are very simple because they only check
	 * the stack's _interface_. The _implementation_ tests
	 * are performed by the Linked List's test suite.
	 */
	st_interface = tcase_create("Stack interface test case");
	suite_add_tcase(s, st_interface);
	tcase_add_test(st_interface, stack_size_test);
	tcase_add_test(st_interface, stack_init_test);
	tcase_add_test(st_interface, stack_push_single_test);
	tcase_add_test(st_interface, stack_push_two_test);
	tcase_add_test(st_interface, stack_pop_single_test);
	tcase_add_test(st_interface, stack_pop_three_test);
	tcase_add_test(st_interface, stack_peek_test);
	tcase_add_test(st_interface, stack_peek_head_null_test);
	tcase_add_test(st_interface, stack_destroy_test);

	return s;
}

int main(void)
{
	int nf;
	Suite *s;
	SRunner *sr;

	s = stack_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (nf == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
