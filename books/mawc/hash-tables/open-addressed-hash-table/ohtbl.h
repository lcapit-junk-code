#ifndef OHTBL_H
#define OHTBL_H

#include "list.h"

typedef struct OHTBL_ {
	int positions;
	void *vacated;

	int (*h1)(const void *data);
	int (*h2)(const void *data);
	int (*match)(const void *data1, const void *data2);
	void (*destroy)(void *data);

	int size;
	void **table;
} OHTbl;

#define OHTBL_VACATED    ((void *)0xDEADBEEF)
#define ohtbl_size(htbl)       ((htbl)->size)

int ohtbl_init(OHTbl *htbl, int positions, int (*h1)(const void *key),
	       int (*h2)(const void *key),
	       int (*match)(const void *key1, const void *key2),
	       void (*destroy)(void *data));
int ohtbl_lookup(OHTbl *htbl, void **data);
int ohtbl_insert(OHTbl *htbl, void *data);
int ohtbl_remove(OHTbl *htbl, void **data);
void ohtbl_destroy(OHTbl *htbl);

#endif /* OHTBL_H */
