#include <stdio.h>
#include <stdlib.h>
#include <check.h>
#include <errno.h>
#include "ohtbl.h"

/*
 * NOTE: everything here (from the hash functions to the
 * tests) depends on the HASH_TABLE_SIZE. If you change its
 * value, you'll have to review most of the tests
 */
#define HASH_TABLE_SIZE 5 /* MUST be prime */

static int hash_function1(const void *data)
{
	if (!data)
		return -1;

	return ((*(const int *)data) % HASH_TABLE_SIZE);
}

static int hash_function2(const void *data)
{
	int hash;

	if (!data)
		return -1;

	hash = ((*(const int *)data) % (HASH_TABLE_SIZE - 2)) + 1;
	return hash;
}

static int match_function(const void *data1, const void *data2)
{
	if (!data1 || !data2)
		return -1;

	return ((*(const int *)data1) == (*(const int *)data2));
}

static OHTbl hash_table;

static void ht_setup(void)
{
	int ret;

	ret = ohtbl_init(&hash_table, HASH_TABLE_SIZE, hash_function1,
			 hash_function2, match_function, NULL);
	fail_unless(ret == 0, "ohtbl_init() failed: %d\n", ret);
}

static void ht_teardown(void)
{
	free(hash_table.table);
}

/*
 * Destroy tests
 */

static int destroy_called;

static void my_destroy(void *data)
{
	data = data;
	destroy_called++;
}

static void destroy_setup(void)
{
	int ret;

	ret = ohtbl_init(&hash_table, HASH_TABLE_SIZE, hash_function1,
			 hash_function2, match_function, my_destroy);
	fail_unless(ret == 0, "ohtbl_init() failed: %d\n", ret);
}

START_TEST(destroy_no_method_test)
{
	OHTbl htable;
	int ret, el1, el2;

	ret = ohtbl_init(&htable, HASH_TABLE_SIZE, hash_function1,
			 hash_function2, match_function, NULL);
	fail_unless(ret == 0, "ohtbl_init() failed: %d\n", ret);

	el1 = 0;
	ret = ohtbl_insert(&htable, (void *) &el1);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	el2 = 1;
	ret = ohtbl_insert(&htable, (void *) &el2);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	/* by hand... */
	htable.table[3] = OHTBL_VACATED;

	/* should not explode */
	ohtbl_destroy(&htable);
	fail_unless(htable.table == NULL, "table is %p but should be NULL\n",
		    htable.table);
}
END_TEST

START_TEST(destroy_misc_table_test)
{
	int ret, el1, el2;

	el1 = 0;
	ret = ohtbl_insert(&hash_table, (void *) &el1);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	el2 = 3;
	ret = ohtbl_insert(&hash_table, (void *) &el2);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	/* by hand... */
	hash_table.table[2] = OHTBL_VACATED;

	destroy_called = 0;
	ohtbl_destroy(&hash_table);
	fail_unless(destroy_called == 2,
		    "destroy_called is %d but should be 2\n", destroy_called);
	fail_unless(hash_table.table == NULL,
		    "table is %p but should be NULL\n", hash_table.table);
}
END_TEST

START_TEST(destroy_full_table_test)
{
	int i;

	/*
	 * Fills the table by hand
	 * 
	 * We're inserting 'i' in all the positions, but it
	 * doesn't matter it's only a test
	 */
	for (i = 0; i < hash_table.positions; i++)
		hash_table.table[i] = (void *) &i;
	hash_table.size = hash_table.positions;

	ohtbl_destroy(&hash_table);
	fail_unless(destroy_called == hash_table.size,
		    "destroy function called %d times, but should be %d\n",
		    destroy_called, hash_table.size);
	fail_unless(hash_table.table == NULL,
		    "table is %p but should be NULL\n", hash_table.table);
}
END_TEST

START_TEST(destroy_empty_test)
{
	ohtbl_destroy(&hash_table);
	fail_unless(destroy_called == 0,
		    "destroy_called is %d but should be 0\n", destroy_called);
	fail_unless(hash_table.table == NULL,
		    "table is %p but should be NULL\n", hash_table.table);
}
END_TEST

START_TEST(destroy_null_test)
{
	/* should not explode */
	ohtbl_destroy(NULL);
}
END_TEST

/*
 * Remove tests
 */

START_TEST(remove_vacated_test)
{
	int ret, pos, value, el1, el2;
	void *data;

	el1 = 3;
	ret = ohtbl_insert(&hash_table, (void *) &el1);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	/* by hand... */
	hash_table.table[0] = OHTBL_VACATED;
	hash_table.table[2] = OHTBL_VACATED;

	/*
	 * insert by hand, otherwise ohtbl_insert() will insert in
	 * slot 0
	 */
	el2 = 13;
	hash_table.table[4] = (void *) &el2;
	hash_table.size++;

	value = 13;
	data = &value;
	ret = ohtbl_remove(&hash_table, &data);
	fail_unless(ret == 0, "ohtbl_remove() failed: %d\n", ret);

	fail_unless(ohtbl_size(&hash_table) == 1,
		    "hash table's size is %d but should be 1\n",
		    ohtbl_size(&hash_table));
	fail_unless(data == (void *) &el2,
		    "returned data is %p but should be %p\n", data, &el2);
	pos = 4;
	fail_unless(hash_table.table[pos] == OHTBL_VACATED,
		    "table[%d] should be %p but is %p\n",
		    pos, OHTBL_VACATED, hash_table.table[pos]);
}
END_TEST

START_TEST(remove_collision_not_found_test)
{
	int ret, value, el1, el2;
	void *data;

	el1 = 12;
	ret = ohtbl_insert(&hash_table, (void *) &el1);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	el2 = 0;
	ret = ohtbl_insert(&hash_table, (void *) &el2);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	value = 32;
	data = &value;
	ret = ohtbl_remove(&hash_table, &data);
	fail_unless(ret == -1, "ohtbl_remove() didn't fail: %d\n", ret);

	fail_unless(ohtbl_size(&hash_table) == 2,
		    "hash table's size is %d but should be 2\n",
		    ohtbl_size(&hash_table));
}
END_TEST

START_TEST(remove_collision_test)
{
	int ret, pos, value, el1, el2;
	void *data;

	el1 = 12;
	ret = ohtbl_insert(&hash_table, (void *) &el1);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	el2 = 22;
	ret = ohtbl_insert(&hash_table, (void *) &el2);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	value = 22;
	data = &value;
	ret = ohtbl_remove(&hash_table, &data);
	fail_unless(ret == 0, "ohtbl_remove() failed: %d\n", ret);

	fail_unless(ohtbl_size(&hash_table) == 1,
		    "hash table's size is %d but should be 1\n",
		    ohtbl_size(&hash_table));
	fail_unless(data == (void *) &el2,
		    "returned data is %p but should be %p\n", data, &el2);
	pos = 4;
	fail_unless(hash_table.table[pos] == OHTBL_VACATED,
		    "table[%d] should be %p but is %p\n",
		    pos, OHTBL_VACATED, hash_table.table[pos]);
}
END_TEST

START_TEST(remove_not_found_test)
{
	int ret, value, num;
	void *data;

	/* to not hit 'size == 0' check */
	num = 4;
	ret = ohtbl_insert(&hash_table, (void *) &num);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	value = 2;
	data = &value;
	ret = ohtbl_remove(&hash_table, &data);
	fail_unless(ret == -1, "ohtbl_remove() didn't fail: %d\n", ret);

	fail_unless(ohtbl_size(&hash_table) == 1,
		    "hash table's size is %d but should be 1\n",
		    ohtbl_size(&hash_table));
}
END_TEST

START_TEST(remove_normal_test)
{
	int ret, pos, value, el;
	void *data;

	el = 12;
	ret = ohtbl_insert(&hash_table, (void *) &el);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	value = 12;
	data = &value;
	ret = ohtbl_remove(&hash_table, &data);
	fail_unless(ret == 0, "ohtbl_remove() failed: %d\n", ret);

	fail_unless(ohtbl_size(&hash_table) == 0,
		    "hash table's size is %d but should be 0\n",
		    ohtbl_size(&hash_table));
	fail_unless(data == (void *) &el,
		    "returned data is %p but should be %p\n", data, &el);
	pos = 2;
	fail_unless(hash_table.table[pos] == OHTBL_VACATED,
		    "table[%d] should be %p but is %p\n",
		    pos, OHTBL_VACATED, hash_table.table[pos]);
}
END_TEST

START_TEST(remove_empty_table_test)
{
	int ret, value;
	void *data;

	value = 0;
	data = &value;
	ret = ohtbl_remove(&hash_table, &data);
	fail_unless(ret == -1, "ohtbl_remove() didn't fail: %d\n", ret);
}
END_TEST

START_TEST(remove_null_test)
{
	int ret;

	ret = ohtbl_remove(NULL, NULL);
	fail_unless(ret == -1, "ohtbl_remove() didn't fail: %d\n", ret);
	fail_unless(errno == EINVAL, "errno is %d but should be %d\n",
		    errno, EINVAL);
}
END_TEST

/*
 * Insert tests
 */

START_TEST(insert_table_full_test)
{
	int i, ret, value;

	/*
	 * Fills the table by hand
	 * 
	 * We're inserting 'i' in all the positions, but it
	 * doesn't matter it's only a test
	 */
	for (i = 0; i < hash_table.positions; i++)
		hash_table.table[i] = (void *) &i;
	hash_table.size = hash_table.positions;

	value = 204;
	ret = ohtbl_insert(&hash_table, (void *) &value);
	fail_unless(ret == -1, "ohtbl_insert() didn't fail: %d\n", ret);
	fail_unless(ohtbl_size(&hash_table) == hash_table.positions,
		    "hash table's size is %d but should be %d\n",
		    hash_table.positions);
}
END_TEST

START_TEST(insert_already_in_table_test)
{
	int ret, el1, el2;

	el1 = el2 = 12;

	ret = ohtbl_insert(&hash_table, (void *) &el1);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	ret = ohtbl_insert(&hash_table, (void *) &el2);
	fail_unless(ret == 1,
		    "ohtbl_insert() should return 1 but returned: %d\n", ret);
	fail_unless(ohtbl_size(&hash_table) == 1,
		    "hash table's size is %d but should be 1\n",
		    ohtbl_size(&hash_table));
}
END_TEST

START_TEST(insert_vacated_test)
{
	int ret, pos, el;

	/* by hand... */
	hash_table.table[2] = OHTBL_VACATED;

	el = 12;
	ret = ohtbl_insert(&hash_table, (void *) &el);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	fail_unless(ohtbl_size(&hash_table) == 1,
		    "hash table's size is %d but should be 1\n",
		    ohtbl_size(&hash_table));

	pos = 2;
	fail_unless(hash_table.table[pos] != NULL, "table[%d] is NULL\n", pos);
	fail_unless(hash_table.table[pos] == (void *) &el,
		    "table[%d] is %p but should be %p\n",
		    pos, hash_table.table[pos], &el);
}
END_TEST

START_TEST(insert_collision_test)
{
	int ret, el1, el2, pos;

	el1 = 8;
	ret = ohtbl_insert(&hash_table, (void *) &el1);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	el2 = 13;
	ret = ohtbl_insert(&hash_table, (void *) &el2);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	fail_unless(ohtbl_size(&hash_table) == 2,
		    "hash table's size is %d but should be 2\n",
		    ohtbl_size(&hash_table));

	pos = 3;
	fail_unless(hash_table.table[pos] != NULL, "table[%d] is NULL\n", pos);
	fail_unless(hash_table.table[pos] == (void *) &el1,
		    "table[%d] is %p but should be %p\n",
		    pos, hash_table.table[pos], &el1);

	pos = 0;
	fail_unless(hash_table.table[pos] != NULL, "table[%d] is NULL\n", pos);
	fail_unless(hash_table.table[pos] == (void *) &el2,
		    "table[%d] is %p but should be %p\n",
		    pos, hash_table.table[pos], &el2);
}
END_TEST

START_TEST(insert_normal_test)
{
	int ret, el;

	el = 666;
	ret = ohtbl_insert(&hash_table, (void *) &el);
	fail_unless(ret == 0, "ohtbl_insert() failed: %d\n", ret);

	fail_unless(ohtbl_size(&hash_table) == 1,
		    "hash table's size is %d but should be 1\n",
		    ohtbl_size(&hash_table));
	fail_unless(hash_table.table[1] != NULL, "table[1] is NULL\n");
	fail_unless(hash_table.table[1] == (void *) &el,
		    "table[1] is %p but should be %p\n",
		    hash_table.table[1], &el);
}
END_TEST

START_TEST(insert_null_test)
{
	int ret;

	ret = ohtbl_insert(NULL, (void *) 0xFFFFF);
	fail_unless(ret == -1, "ohtbl_insert() didn't fail: %d\n", ret);
	fail_unless(errno == EINVAL, "errno is %d but should be %d\n",
		    errno, EINVAL);
}
END_TEST

/*
 * Lookup tests
 */

START_TEST(lookup_collision_not_found_test)
{
	int ret;
	void *data;
	int value, el;

	el = 1; /* just for testing */

	/* insert by hand */
	hash_table.table[4] = OHTBL_VACATED;
	hash_table.table[1] = (void *) &el;
	hash_table.table[3] = (void *) &el;
	hash_table.size = 2;

	value = 4;
	data = (void *) &value;
	ret = ohtbl_lookup(&hash_table, &data);
	fail_unless(ret == 0,
		    "ohtbl_lookup() returned %d but should return 0\n", ret);
}
END_TEST

START_TEST(lookup_vacated_match_test)
{
	int ret;
	void *data;
	int value, el;

	value = el = 6;

	/* insert by hand */
	hash_table.table[1] = OHTBL_VACATED;
	hash_table.table[2] = (void *) &el;
	hash_table.size = 1;

	data = (void *) &value;
	ret = ohtbl_lookup(&hash_table, &data);
	fail_unless(ret == 1,
		    "ohtbl_lookup() returned %d but should return 1\n", ret);
	fail_unless(data == (void *) &el,
		    "returned data is %p but should be %p\n", data, &el);
}
END_TEST

START_TEST(lookup_collision_match_test)
{
	int ret;
	void *data;
	int value, el1, el2;

	el1 = 1;
	el2 = 6;

	/* insert by hand */
	hash_table.table[1] = (void *) &el1;
	hash_table.table[2] = (void *) &el2;
	hash_table.size = 2;

	value = 6;
	data = (void *) &value;
	ret = ohtbl_lookup(&hash_table, &data);
	fail_unless(ret == 1,
		    "ohtbl_lookup() returned %d but should return 1\n", ret);
	fail_unless(data == (void *) &el2,
		    "returned data is %p but should be %p\n", data, &el2);
}
END_TEST

START_TEST(lookup_match_test)
{
	int ret;
	void *data;
	int value, el;

	value = el = 1;

	/* insert by hand */
	hash_table.table[1] = (void *) &el;
	hash_table.size = 1;

	data = (void *) &value;
	ret = ohtbl_lookup(&hash_table, &data);
	fail_unless(ret == 1,
		    "ohtbl_lookup() returned %d but should return 1\n", ret);
	fail_unless(data == (void *) &el,
		    "returned data is %p but should be %p\n", data, &el);
}
END_TEST

START_TEST(lookup_not_found_test)
{
	int value, ret;
	void *data;

	/* ugly. Just to force the table lookup */
	hash_table.size = 1;

	value = 5;
	data = (void *) &value;
	ret = ohtbl_lookup(&hash_table, &data);
	fail_unless(ret == 0,
		    "ohtbl_lookup() returned %d but should return 0\n", ret);
	fail_unless(data == NULL,
		    "returned data is %p but should be NULL\n", data);
}
END_TEST

START_TEST(lookup_empty_table_test)
{
	int ret;
	void *data;

	ret = ohtbl_lookup(&hash_table, &data);
	fail_unless(ret == 0,
		    "ohtbl_lookup() returned %d but should return 0\n", ret);
}
END_TEST

START_TEST(lookup_null_table_test)
{
	int ret;

	ret = ohtbl_lookup(NULL, NULL);
	fail_unless(ret == -1, "ohtbl_lookup() didn't fail: %d\n", ret);
	fail_unless(errno == EINVAL, "errno is %d but should be %d\n",
		    errno, EINVAL);
}
END_TEST

/*
 * Simple function tests
 */

START_TEST(ohtbl_init_test)
{
	int i, ret;
	OHTbl t;

	ret = ohtbl_init(&t, HASH_TABLE_SIZE, hash_function1, hash_function2,
			 match_function, free);
	fail_unless(ret == 0, "ohtbl_init() failed: %d\n", ret);
	fail_unless(t.positions == HASH_TABLE_SIZE,
		    "table's positions is %d but should be %d\n",
		    t.positions, HASH_TABLE_SIZE);
	fail_unless(t.vacated == OHTBL_VACATED,
		    "table's vacated member is %p but should be %p\n",
		    t.vacated, OHTBL_VACATED);
	fail_unless(t.h1 == hash_function1,
		    "table's hash_function1() is %p but should be %p\n",
		    t.h1, hash_function1);
	fail_unless(t.h2 == hash_function2,
		    "table's hash_function2() is %p but should be %p\n",
		    t.h2, hash_function2);
	fail_unless(t.match == match_function,
		    "table's match_function() is %p but should be %p\n",
		    t.match, match_function);
	fail_unless(t.destroy == free,
		    "table's destroy_function() is %p but should be %p\n",
		    t.destroy, free);
	fail_unless(t.size == 0, "table's size is %d but should be 0\n",
		    t.size);
	fail_unless(t.table != NULL, "table  is NULL\n", t.table);
	for (i = 0; i < t.positions; i++)
		fail_unless(t.table[i] == NULL,
			    "table[i] is %p but should be NULL\n", t.table[i]);

	free(t.table);
}
END_TEST

START_TEST(ohtbl_size_test)
{
	const int size = 15;
	int ret;
	OHTbl t;

	t.size = size;
	ret = ohtbl_size(&t);
	fail_unless(ret == size, "Table size is %d but should be %d\n",
		    ret, size);
}
END_TEST

/*
 * User defined functions tests
 */

START_TEST(match_no_match_test)
{
	int ret, value1, value2;

	value1 = 1;
	value2 = 2;

	ret = match_function((void *) &value1, (void *) &value2);
	fail_unless(ret == 0, "return code is %d but should be 0\n", ret);
}
END_TEST

START_TEST(match_ok_test)
{
	int ret, value1, value2;

	value1 = value2 = 666;

	ret = match_function((void *) &value1, (void *) &value2);
	fail_unless(ret == 1, "return code is %d but should be 1\n", ret);
}
END_TEST

START_TEST(match_func_data2_null_test)
{
	int ret, value;

	ret = match_function((void *) &value, NULL);
	fail_unless(ret == -1, "return code is %d but should be -1\n", ret);
}
END_TEST

START_TEST(match_func_data1_null_test)
{
	int ret, value;

	ret = match_function(NULL, (void *) &value);
	fail_unless(ret == -1, "return code is %d but should be -1\n", ret);
}
END_TEST

START_TEST(hash_func2_hcode_test)
{
	int ret, value;

	value = 19;
	ret = hash_function2((void *) &value);
	fail_unless(ret == 2, "hash code is %d but should be %d\n", ret, 2);
}
END_TEST

START_TEST(hash_func2_null_test)
{
	int ret;

	ret = hash_function2(NULL);
	fail_unless(ret == -1, "hash_function2() didn't fail: %d\n", ret);
}
END_TEST

START_TEST(hash_func1_hcode_test)
{
	int ret, value;

	value = 14;
	ret = hash_function1((void *) &value);
	fail_unless(ret == 4, "hash code is %d but should be %d\n", ret, 4);

	value = 19;
	ret = hash_function1((void *) &value);
	fail_unless(ret == 4, "hash code is %d but should be %d\n", ret, 4);
}
END_TEST

START_TEST(hash_func1_null_test)
{
	int ret;

	ret = hash_function1(NULL);
	fail_unless(ret == -1, "hash_function1() didn't fail: %d\n", ret);
}
END_TEST


static Suite *ohtbl_suite(void)
{
	Suite *s;
	TCase *ohtbl_user_func;
	TCase *ohtbl_simple_func;
	TCase *ohtbl_lookup_tcase;
	TCase *ohtbl_insert_tcase;
	TCase *ohtbl_remove_tcase;
	TCase *ohtbl_destroy_tcase;

	s = suite_create("Open addressed hash table implementation suite");

	/*
	 * Tests the user defined functions which will be used
	 * by the other test suites
	 */
	ohtbl_user_func = tcase_create("User defined functions test-case");
	suite_add_tcase(s, ohtbl_user_func);
	tcase_add_test(ohtbl_user_func, hash_func1_null_test);
	tcase_add_test(ohtbl_user_func, hash_func1_hcode_test);
	tcase_add_test(ohtbl_user_func, hash_func2_null_test);
	tcase_add_test(ohtbl_user_func, hash_func2_hcode_test);
	tcase_add_test(ohtbl_user_func, match_func_data1_null_test);
	tcase_add_test(ohtbl_user_func, match_func_data2_null_test);
	tcase_add_test(ohtbl_user_func, match_ok_test);
	tcase_add_test(ohtbl_user_func, match_no_match_test);

	ohtbl_simple_func = tcase_create("Simple functions test-case");
	suite_add_tcase(s, ohtbl_simple_func);
	tcase_add_test(ohtbl_simple_func, ohtbl_size_test);
	tcase_add_test(ohtbl_simple_func, ohtbl_init_test);

	ohtbl_lookup_tcase = tcase_create("Lookup test-case");
	suite_add_tcase(s, ohtbl_lookup_tcase);
	tcase_add_checked_fixture(ohtbl_lookup_tcase, ht_setup, ht_teardown);
	tcase_add_test(ohtbl_lookup_tcase, lookup_null_table_test);
	tcase_add_test(ohtbl_lookup_tcase, lookup_empty_table_test);
	tcase_add_test(ohtbl_lookup_tcase, lookup_not_found_test);
	tcase_add_test(ohtbl_lookup_tcase, lookup_match_test);
	tcase_add_test(ohtbl_lookup_tcase, lookup_collision_match_test);
	tcase_add_test(ohtbl_lookup_tcase, lookup_vacated_match_test);
	tcase_add_test(ohtbl_lookup_tcase, lookup_collision_not_found_test);

	ohtbl_insert_tcase = tcase_create("Insert test-case");
	suite_add_tcase(s, ohtbl_insert_tcase);
	tcase_add_checked_fixture(ohtbl_insert_tcase, ht_setup, ht_teardown);
	tcase_add_test(ohtbl_insert_tcase, insert_null_test);
	tcase_add_test(ohtbl_insert_tcase, insert_normal_test);
	tcase_add_test(ohtbl_insert_tcase, insert_collision_test);
	tcase_add_test(ohtbl_insert_tcase, insert_vacated_test);
	tcase_add_test(ohtbl_insert_tcase, insert_already_in_table_test);
	tcase_add_test(ohtbl_insert_tcase, insert_table_full_test);

	ohtbl_remove_tcase = tcase_create("Remove test-case");
	suite_add_tcase(s, ohtbl_remove_tcase);
	tcase_add_checked_fixture(ohtbl_remove_tcase, ht_setup, ht_teardown);
	tcase_add_test(ohtbl_remove_tcase, remove_null_test);
	tcase_add_test(ohtbl_remove_tcase, remove_empty_table_test);
	tcase_add_test(ohtbl_remove_tcase, remove_normal_test);
	tcase_add_test(ohtbl_remove_tcase, remove_not_found_test);
	tcase_add_test(ohtbl_remove_tcase, remove_collision_test);
	tcase_add_test(ohtbl_remove_tcase, remove_collision_not_found_test);
	tcase_add_test(ohtbl_remove_tcase, remove_vacated_test);

	ohtbl_destroy_tcase = tcase_create("Destroy test-case");
	suite_add_tcase(s, ohtbl_destroy_tcase);
	tcase_add_checked_fixture(ohtbl_destroy_tcase, destroy_setup, NULL);
	tcase_add_test(ohtbl_destroy_tcase, destroy_null_test);
	tcase_add_test(ohtbl_destroy_tcase, destroy_empty_test);
	tcase_add_test(ohtbl_destroy_tcase, destroy_full_table_test);
	tcase_add_test(ohtbl_destroy_tcase, destroy_misc_table_test);
	tcase_add_test(ohtbl_destroy_tcase, destroy_no_method_test);

	return s;
}

int main(void)
{
	int nf;
	Suite *s;
	SRunner *sr;

	s = ohtbl_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (nf == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
