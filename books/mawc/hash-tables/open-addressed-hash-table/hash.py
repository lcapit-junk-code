def hash_function1(key):
	return (key % 5)

def hash_function2(key):
	return ((key % 3)+1)

def double_hash(key, i):
	hash1 = hash_function1(key)
	hash2 = hash_function2(key)
	return ((hash1 + i*hash2) % 5)
