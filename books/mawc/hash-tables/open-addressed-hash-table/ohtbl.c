#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "ohtbl.h"

int ohtbl_init(OHTbl *htbl, int positions, int (*h1)(const void *key),
	       int (*h2)(const void *key),
	       int (*match)(const void *key1, const void *key2),
	       void (*destroy)(void *data))
{
	int i;

	htbl->table = malloc(sizeof(void *) * positions);
	if (!htbl->table)
		return -1;

	for (i = 0; i < positions; i++)
		htbl->table[i] = NULL;

	htbl->positions = positions;
	htbl->vacated = (void *) OHTBL_VACATED;
	htbl->h1 = h1;
	htbl->h2 = h2;
	htbl->match = match;
	htbl->destroy = destroy;
	htbl->size = 0;

	return 0;
}

int ohtbl_lookup(OHTbl *htbl, void **data)
{
	int i, hash1, hash2;

	if (!htbl || !data) {
		errno = EINVAL;
		return -1;
	}

	if (ohtbl_size(htbl) == 0)
		return 0;

	hash1 = htbl->h1(*data);
	if (hash1 < 0)
		return -1;

	hash2 = htbl->h2(*data);
	if (hash2 < 0)
		return -1;

	for (i = 0; i < htbl->positions; i++) {
		void *tmp;
		int ret, hash;

		hash = (hash1 + i*hash2) % htbl->positions;

		tmp = htbl->table[hash];
		if (!tmp) {
			*data = NULL;
			return 0;
		}

		if (tmp == htbl->vacated)
			continue;

		ret = htbl->match(*data, tmp);
		if (ret == 1) {
			*data = tmp;
			return 1;
		} else if (ret < 0) {
			return -1;
		}
	}

	return 0;
}

int ohtbl_insert(OHTbl *htbl, void *data)
{
	int i, ret, hash1, hash2;
	void *tmp;

	if (!htbl || !data) {
		errno = EINVAL;
		return -1;
	}

	if (ohtbl_size(htbl) == htbl->positions) {
		/* table is full */
		return -1;
	}

	tmp = data;
	ret = ohtbl_lookup(htbl, &tmp);
	if (ret == 1)
		return 1;

	hash1 = htbl->h1(data);
	if (hash1 < 0)
		return -1;

	hash2 = htbl->h2(data);
	if (hash2 < 0)
		return -1;

	for (i = 0; i < htbl->positions; i++) {
		int hash;

		hash = (hash1 + i*hash2) % htbl->positions;

		tmp = htbl->table[hash];
		if (!tmp || tmp == htbl->vacated) {
			htbl->table[hash] = data;
			htbl->size++;
			return 0;
		}
	}

	return -1;
}

int ohtbl_remove(OHTbl *htbl, void **data)
{
	int i, hash1, hash2;

	if (!htbl || !data) {
		errno = EINVAL;
		return -1;
	}

	if (ohtbl_size(htbl) == 0)
		return -1;

	hash1 = htbl->h1(*data);
	if (hash1 < 0)
		return -1;

	hash2 = htbl->h2(*data);
	if (hash2 < 0)
		return -1;

	for (i = 0; i < htbl->positions; i++) {
		void *tmp;
		int ret, hash;

		hash = (hash1 + i*hash2) % htbl->positions;

		tmp = htbl->table[hash];
		if (!tmp) {
			*data = NULL;
			return -1;
		}

		if (tmp == htbl->vacated)
			continue;

		ret = htbl->match(*data, tmp);
		if (ret == 1) {
			*data = tmp;
			htbl->table[hash] = htbl->vacated;
			htbl->size--;
			return 0;
		} else if (ret < 0) {
			return -1;
		}
	}

	return -1;
}

void ohtbl_destroy(OHTbl *htbl)
{
	int i;

	if (!htbl)
		return;

	if (htbl->destroy == NULL)
		goto out;

	for (i = 0; i < htbl->positions; i++) {
		void *p = htbl->table[i];

		if (p != NULL && p != htbl->vacated)
			htbl->destroy(p);
	}

out:
	free(htbl->table);
	htbl->table = NULL;
}
