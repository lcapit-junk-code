#ifndef CHTBL_H
#define CHTBL_H

#include "list.h"

typedef struct CHTbl_ {
	int buckets;
	int (*h)(const void *key);
	int (*match)(const void *key1, const void *key2);
	void (*destroy)(void *data);
	int size;
	List *table;
} CHTbl;

#define chtbl_size(htbl) ((htbl)->size)

int chtbl_init(CHTbl *htbl, int buckets, int (*h)(const void *key),
	       int (*match)(const void *key1, const void *key2),
	       void (*destroy)(void *data));
int chtbl_insert(CHTbl *htbl, void *data);
int chtbl_lookup(CHTbl *htbl, void **data);
int chtbl_remove(CHTbl *htbl, void **data);
void chtbl_destroy(CHTbl *htbl);

#endif /* CHTBL_H */
