#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <check.h>
#include <errno.h>
#include "chtbl.h"

struct foobar {
	const char *key;
};

static int hash_function(const void *data)
{
	const char *key;

	if (!data)
		return -1;

	key = ((const struct foobar *)data)->key;

	if (!strncmp(key, "zero", 3))
		return 0;
	else if (!strncmp(key, "one", 3))
		return 1;
	else if (!strncmp(key, "two", 3))
		return 2;

	return -1;
}

static int match_function(const void *data1, const void *data2)
{
	const char *key1, *key2;

	if (!data1 || !data2)
		return -1;

	key1 = ((const struct foobar *)data1)->key;
	key2 = ((const struct foobar *)data2)->key;

	if (!strcmp(key1, key2))
		return 1;

	return 0;
}

static const char *const keys[] = { "zero", "one", "two" };

static CHTbl hash_table;

static void ht_setup(void)
{
	int ret;
	const int buckets = 3;

	ret = chtbl_init(&hash_table, buckets, hash_function,
			 match_function, NULL);
	fail_unless(ret == 0, "chtbl_init() failed: %d\n", ret);
}

static void ht_teardown(void)
{
	free(hash_table.table);
}

/*
 * Destroy tests
 */

START_TEST(destroy_test)
{
	const char *const keyscol[] = { "one-SEC", "one-THI" };
	struct foobar el0, el1, el1sec, el1thi, el2;
	const int buckets = 3;
	int ret;

	ret = chtbl_init(&hash_table, buckets, hash_function,
			 match_function, NULL);
	fail_unless(ret == 0, "chtbl_init() failed: %d\n", ret);

	el0.key = keys[0];
	ret = chtbl_insert(&hash_table, &el0);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1.key = keys[1];
	ret = chtbl_insert(&hash_table, &el1);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1sec.key = keyscol[0];
	ret = chtbl_insert(&hash_table, &el1sec);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1thi.key = keyscol[1];
	ret = chtbl_insert(&hash_table, &el1thi);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el2.key = keys[2];
	ret = chtbl_insert(&hash_table, &el2);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	chtbl_destroy(&hash_table);

	fail_unless(chtbl_size(&hash_table) == 0,
		    "hash table's size is %d but should be 0\n",
		    chtbl_size(&hash_table));
}
END_TEST

START_TEST(destroy_empty_table_test)
{
	int ret;
	const int buckets = 3;

	ret = chtbl_init(&hash_table, buckets, hash_function,
			 match_function, NULL);
	fail_unless(ret == 0, "chtbl_init() failed: %d\n", ret);

	chtbl_destroy(&hash_table);
}
END_TEST

/*
 * Remove tests
 */

START_TEST(remove_collision_head_test)
{
	const char *const keyscol[] = { "one-SEC", "one-THI" };
	struct foobar *data, el0, el1, el1sec, el1thi;
	struct foobar el2, els;
	List *list;
	int ret;

	el0.key = keys[0];
	ret = chtbl_insert(&hash_table, &el0);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1.key = keys[1];
	ret = chtbl_insert(&hash_table, &el1);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1sec.key = keyscol[0];
	ret = chtbl_insert(&hash_table, &el1sec);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1thi.key = keyscol[1];
	ret = chtbl_insert(&hash_table, &el1thi);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el2.key = keys[2];
	ret = chtbl_insert(&hash_table, &el2);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	els.key = keys[1];
	data = &els;
	ret = chtbl_remove(&hash_table, (void **) &data);
	fail_unless(ret == 0, "chtbl_remove() failed: %d\n", ret);
	fail_unless(data == &el1, "data is %p but should be %p\n",data, &el1);
	fail_unless(strncmp(data->key, keys[1], 3) == 0,
		    "data's key is %s but should be %s\n",
		    data->key, keys[1]);

	fail_unless(chtbl_size(&hash_table) == 4,
		    "hash table's size is %d but should be 4\n",
		    chtbl_size(&hash_table));

	list = hash_table.table + 1;
	fail_unless(list_size(list) == 2, "list size is %d but should be 2\n",
		    list_size(list));

	list_destroy(hash_table.table);
	list_destroy(hash_table.table + 1);
	list_destroy(hash_table.table + 2);
}
END_TEST

START_TEST(remove_collision_test)
{
	const char *const keyscol[] = { "one-SEC", "one-THI" };
	struct foobar *data, el0, el1, el1sec, el1thi;
	struct foobar el2, els;
	List *list;
	int ret;

	el0.key = keys[0];
	ret = chtbl_insert(&hash_table, &el0);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1.key = keys[1];
	ret = chtbl_insert(&hash_table, &el1);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1sec.key = keyscol[0];
	ret = chtbl_insert(&hash_table, &el1sec);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1thi.key = keyscol[1];
	ret = chtbl_insert(&hash_table, &el1thi);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el2.key = keys[2];
	ret = chtbl_insert(&hash_table, &el2);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	els.key = keyscol[1];
	data = &els;
	ret = chtbl_remove(&hash_table, (void **) &data);
	fail_unless(ret == 0, "chtbl_remove() failed: %d\n", ret);
	fail_unless(data == &el1thi, "data is %p but should be %p\n",
		    data, &el1thi);
	fail_unless(strncmp(data->key, keyscol[1], 7) == 0,
		    "data's key is %s but should be %s\n",
		    data->key, keyscol[1]);

	fail_unless(chtbl_size(&hash_table) == 4,
		    "hash table's size is %d but should be 4\n",
		    chtbl_size(&hash_table));

	list = hash_table.table + 1;
	fail_unless(list_size(list) == 2, "list size is %d but should be 2\n",
		    list_size(list));

	list_destroy(hash_table.table);
	list_destroy(hash_table.table + 1);
	list_destroy(hash_table.table + 2);
}
END_TEST

START_TEST(remove_direct_addressed_table_test)
{
	struct foobar *data, el0, el1, el2, el3;
	List *list;
	int ret;

	el0.key = keys[0];
	ret = chtbl_insert(&hash_table, &el0);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1.key = keys[1];
	ret = chtbl_insert(&hash_table, &el1);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el2.key = keys[2];
	ret = chtbl_insert(&hash_table, &el2);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el3.key = keys[2];
	data = &el3;
	ret = chtbl_remove(&hash_table, (void **) &data);
	fail_unless(ret == 0, "chtbl_remove() failed: %d\n", ret);
	fail_unless(data == &el2, "data is %p but should be %p\n",data, &el2);
	fail_unless(strncmp(data->key, keys[2], 3) == 0,
		    "data's key is %s but should be %s\n",
		    data->key, keys[2]);

	fail_unless(chtbl_size(&hash_table) == 2,
		    "hash table's size is %d but should be 2\n",
		    chtbl_size(&hash_table));

	list = hash_table.table + 2;
	fail_unless(list_size(list) == 0, "list size is %d but should be 0\n",
		    list_size(list));

	list_destroy(hash_table.table);
	list_destroy(hash_table.table + 1);
}
END_TEST

START_TEST(remove_empty_table_test)
{
	int ret;
	void *data;

	ret = chtbl_remove(&hash_table, &data);
	fail_unless(ret == -1,
		    "chtbl_remove() didn't fail: %d\n", ret);
}
END_TEST

/*
 * Lookup tests
 */

START_TEST(lookup_fail_collision_test)
{
	const char *const keyscol[] = { "one-SEC", "one-THI", "one-FOR" };
	struct foobar *data, el0, el1, el1sec, el1thi;
	struct foobar el2, els;
	int ret;

	el0.key = keys[0];
	ret = chtbl_insert(&hash_table, &el0);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1.key = keys[1];
	ret = chtbl_insert(&hash_table, &el1);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1sec.key = keyscol[0];
	ret = chtbl_insert(&hash_table, &el1sec);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1thi.key = keyscol[1];
	ret = chtbl_insert(&hash_table, &el1thi);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el2.key = keys[2];
	ret = chtbl_insert(&hash_table, &el2);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	els.key = keyscol[2];
	data = &els;
	ret = chtbl_lookup(&hash_table, (void **) &data);
	fail_unless(ret == 0, "value found by chtbl_lookup(): %d\n", ret);

	list_destroy(hash_table.table);
	list_destroy(hash_table.table + 1);
	list_destroy(hash_table.table + 2);
}
END_TEST

START_TEST(lookup_collision_test)
{
	const char *const keyscol[] = { "one-SEC", "one-THI" };
	struct foobar *data, el0, el1, el1sec, el1thi;
	struct foobar el2, els;
	int ret;

	el0.key = keys[0];
	ret = chtbl_insert(&hash_table, &el0);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1.key = keys[1];
	ret = chtbl_insert(&hash_table, &el1);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1sec.key = keyscol[0];
	ret = chtbl_insert(&hash_table, &el1sec);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1thi.key = keyscol[1];
	ret = chtbl_insert(&hash_table, &el1thi);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el2.key = keys[2];
	ret = chtbl_insert(&hash_table, &el2);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	els.key = keyscol[1];
	data = &els;
	ret = chtbl_lookup(&hash_table, (void **) &data);
	fail_unless(ret == 1, "value not found by chtbl_lookup(): %d\n", ret);
	fail_unless(data == &el1thi, "data is %p but should be %p\n",
		    data, &el1thi);
	fail_unless(strncmp(data->key, keyscol[1], 7) == 0,
		    "data's key is %s but should be %s\n",
		    data->key, keyscol[1]);

	list_destroy(hash_table.table);
	list_destroy(hash_table.table + 1);
	list_destroy(hash_table.table + 2);
}
END_TEST

START_TEST(lookup_not_found_test)
{
	struct foobar *data, el0, el1;
	int ret;

	el0.key = keys[0];
	ret = chtbl_insert(&hash_table, &el0);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1.key = keys[1];
	data = &el1;
	ret = chtbl_lookup(&hash_table, (void **) &data);
	fail_unless(ret == 0, "value found by chtbl_lookup(): %d\n", ret);

	list_destroy(hash_table.table);
}
END_TEST

START_TEST(lookup_direct_addressed_table_test)
{
	struct foobar *data, el0, el1, el2, el3;
	int ret;

	el0.key = keys[0];
	ret = chtbl_insert(&hash_table, &el0);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1.key = keys[1];
	ret = chtbl_insert(&hash_table, &el1);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el2.key = keys[2];
	ret = chtbl_insert(&hash_table, &el2);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el3.key = keys[1];
	data = &el3;
	ret = chtbl_lookup(&hash_table, (void **) &data);
	fail_unless(ret == 1, "value not found by chtbl_lookup(): %d\n", ret);
	fail_unless(data == &el1, "data is %p but should be %p\n",data, &el1);
	fail_unless(strncmp(data->key, keys[1], 3) == 0,
		    "data's key is %s but should be %s\n",
		    data->key, keys[1]);

	list_destroy(hash_table.table);
	list_destroy(hash_table.table + 1);
	list_destroy(hash_table.table + 2);
}
END_TEST

START_TEST(lookup_empty_table_test)
{
	int ret;
	void *data;

	ret = chtbl_lookup(&hash_table, &data);
	fail_unless(ret == 0,
		    "chtbl_lookup() returned %d but should return 0\n", ret);
}
END_TEST

/*
 * Insertion tests
 */

START_TEST(insert_collision_test)
{
	const char *const mykeys[] = { "two0", "two1", "two2" };
	struct foobar *p, el0, el1, el2;
	ListElmt *next;
	void *data;
	int ret;
	List *t;

	el0.key = mykeys[0];
	ret = chtbl_insert(&hash_table, &el0);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el1.key = mykeys[1];
	ret = chtbl_insert(&hash_table, &el1);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	el2.key = mykeys[2];
	ret = chtbl_insert(&hash_table, &el2);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	fail_unless(chtbl_size(&hash_table) == 3,
		    "hash table's size is %d but should be 3\n",
		    chtbl_size(&hash_table));

	t = hash_table.table + 2;
	data = list_element_data(list_head(t));
	fail_unless(data == &el2,
		    "third bucket's head data is %p but should be %p\n",
		    data, &el2);
	p = (struct foobar *) data;
	fail_unless(strncmp(p->key, mykeys[2], 4) == 0,
		    "third bucket's head data key is %s but should be %s\n",
		    p->key, mykeys[2]);

	next = list_element_next(list_head(t));
	data = list_element_data(next);
	fail_unless(data == &el1,
		    "third bucket's second data is %p but should be %p\n",
		    data, &el1);
	p = (struct foobar *) data;
	fail_unless(strncmp(p->key, mykeys[1], 4) == 0,
		    "third bucket's second data key is %s but should be %s\n",
		    p->key, mykeys[1]);

	next = list_element_next(next);
	data = list_element_data(next);
	fail_unless(data == &el0,
		    "third bucket's third data is %p but should be %p\n",
		    data, &el0);
	p = (struct foobar *) data;
	fail_unless(strncmp(p->key, mykeys[0], 4) == 0,
		    "third bucket's third data key is %s but should be %s\n",
		    p->key, mykeys[0]);

	next = list_element_next(next);
	fail_unless(next == NULL, "third's next is %p but should be NULL\n",
		    next);

	list_destroy(t);
}
END_TEST

START_TEST(insert_mid_data_test)
{
	const char key[] = "one";
	struct foobar *p, el;
	ListElmt *next;
	void *data;
	int ret;
	List *t;

	el.key = key;
	ret = chtbl_insert(&hash_table, &el);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	fail_unless(chtbl_size(&hash_table) == 1,
		    "hash table's size is %d but should be 1\n",
		    chtbl_size(&hash_table));

	t = hash_table.table + 1;
	data = list_element_data(list_head(t));
	fail_unless(data == &el,
		    "second bucket's data is %p but should be %p\n",
		    data, &el);
	p = (struct foobar *) data;
	fail_unless(strncmp(p->key, key, 3) == 0,
		    "second bucket's data key is %s but should be %s\n",
		    p->key, key);

	next = list_element_next(list_head(t));
	fail_unless(next == NULL, "head's next is %p but should be NULL\n",
		    next);

	list_destroy(t);
}
END_TEST

START_TEST(insert_first_data_test)
{
	const char key[] = "zero";
	struct foobar *p, el;
	ListElmt *next;
	void *data;
	int ret;
	List *t;

	el.key = key;

	ret = chtbl_insert(&hash_table, &el);
	fail_unless(ret == 0, "chtbl_insert() failed: %d\n", ret);

	fail_unless(chtbl_size(&hash_table) == 1,
		    "hash table's size is %d but should be 1\n",
		    chtbl_size(&hash_table));

	t = hash_table.table;
	data = list_element_data(list_head(t));
	fail_unless(data == &el,
		    "first bucket's data is %p but should be %p\n",
		    data, &el);
	p = (struct foobar *) data;
	fail_unless(strncmp(p->key, key, 3) == 0,
		    "first bucket's data key is %s but should be %s\n",
		    p->key, key);

	next = list_element_next(list_head(t));
	fail_unless(next == NULL, "head's next is %p but should be NULL\n",
		    next);

	list_destroy(t);
}
END_TEST

START_TEST(insert_empty_test)
{
	int ret;

	ret = chtbl_insert(NULL, (void *) 0xFFFFF);
	fail_unless(ret == -1, "chtbl_insert() didn't fail: %d\n", ret);
	fail_unless(errno == EINVAL, "errno is %d but should be %d\n",
		    ret, EINVAL);
}
END_TEST


/*
 * Simple function tests
 */

START_TEST(chtbl_size_test)
{
	const int size = 15;
	int ret;
	CHTbl t;

	t.size = size;
	ret = chtbl_size(&t);
	fail_unless(ret == size, "Table size is %d but should be %d\n",
		    ret, size);
}
END_TEST

START_TEST(chtbl_init_test)
{
	const int buckets = 3;
	int ret, size;
	CHTbl t;

	ret = chtbl_init(&t, buckets, hash_function, match_function, free);
	fail_unless(ret == 0, "chtbl_init() failed: %d\n", ret);
	fail_unless(t.buckets == buckets,
		    "table's buckets is %d but should be %d\n", t.buckets,
		    buckets);
	fail_unless(t.h == hash_function,
		    "table's hash_function() is %p but should be %p\n",
		    t.h, hash_function);
	fail_unless(t.match == match_function,
		    "table's match_function() is %p but should be %p\n",
		    t.match, match_function);
	fail_unless(t.destroy == free,
		    "table's destroy_function() is %p but should be %p\n",
		    t.destroy, free);
	fail_unless(t.size == 0, "table's size is %d but should be 0\n",
		    t.size);
	fail_unless(t.table != NULL, "table's list is NULL\n", t.table);
	size = (t.table)->size;
	fail_unless(size == 0, "first table's size is %d but should be 0\n",
		    size);
	size = (t.table+1)->size;
	fail_unless(size == 0, "second table's size is %d but should be 0\n",
		    size);
	size = (t.table+2)->size;
	fail_unless(size == 0, "third table's size is %d but should be 0\n",
		    size);

	free(t.table);
}
END_TEST

/*
 * User defined functions tests
 */

START_TEST(match_no_match_test)
{
	int ret;
	struct foobar bar1;
	struct foobar bar2;

	bar1.key = "one";
	bar2.key = "two";

	ret = match_function((void *) &bar1, (void *) &bar2);
	fail_unless(ret == 0, "return code is %d but should be 0\n", ret);
}
END_TEST

START_TEST(match_ok_test)
{
	int ret;
	struct foobar bar1;
	struct foobar bar2;

	bar1.key = "one";
	bar2.key = "one";

	ret = match_function((void *) &bar1, (void *) &bar2);
	fail_unless(ret == 1, "return code is %d but should be 1\n", ret);
}
END_TEST

START_TEST(match_func_data2_null_test)
{
	int ret;
	struct foobar bar;

	ret = match_function((void *) &bar, NULL);
	fail_unless(ret == -1, "return code is %d but should be -1\n", ret);
}
END_TEST

START_TEST(match_func_data1_null_test)
{
	int ret;
	struct foobar bar;

	ret = match_function(NULL, (void *) &bar);
	fail_unless(ret == -1, "return code is %d but should be -1\n", ret);
}
END_TEST

START_TEST(hash_func_unknow_input_test)
{
	int ret;
	struct foobar bar;

	bar.key = "four";

	ret = hash_function((void *) &bar);
	fail_unless(ret == -1, "return code is %d but should be -1\n", ret);
}
END_TEST

START_TEST(hash_func_ret_two_test)
{
	int ret;
	struct foobar bar;

	bar.key = "two";

	ret = hash_function((void *) &bar);
	fail_unless(ret == 2, "return code is %d but should be 2\n", ret);
}
END_TEST

START_TEST(hash_func_ret_one_test)
{
	int ret;
	struct foobar bar;

	bar.key = "one";

	ret = hash_function((void *) &bar);
	fail_unless(ret == 1, "return code is %d but should be 1\n", ret);
}
END_TEST

START_TEST(hash_func_ret_zero_test)
{
	int ret;
	struct foobar bar;

	bar.key = "zero";

	ret = hash_function((void *) &bar);
	fail_unless(ret == 0, "return code is %d but should be 0\n", ret);
}
END_TEST

START_TEST(hash_func_null_input_test)
{
	int ret;

	ret = hash_function(NULL);
	fail_unless(ret == -1, "return code is %d but should be -1\n", ret);
}
END_TEST

static Suite *chtbl_suite(void)
{
	Suite *s;
	TCase *chtbl_user_func;
	TCase *chtbl_simple_func;
	TCase *chtbl_insert_tcase;
	TCase *chtbl_lookup_tcase;
	TCase *chtbl_remove_tcase;
	TCase *chtbl_destroy_tcase;

	s = suite_create("Chained hash table implementation test-suite");

	/*
	 * Tests the user defined functions which will be used
	 * by the other test suites
	 */
	chtbl_user_func = tcase_create("User defined functions test-case");
	suite_add_tcase(s, chtbl_user_func);
	tcase_add_test(chtbl_user_func, hash_func_null_input_test);
	tcase_add_test(chtbl_user_func, hash_func_ret_zero_test);
	tcase_add_test(chtbl_user_func, hash_func_ret_one_test);
	tcase_add_test(chtbl_user_func, hash_func_ret_two_test);
	tcase_add_test(chtbl_user_func, hash_func_unknow_input_test);
	tcase_add_test(chtbl_user_func, match_func_data1_null_test);
	tcase_add_test(chtbl_user_func, match_func_data2_null_test);
	tcase_add_test(chtbl_user_func, match_ok_test);
	tcase_add_test(chtbl_user_func, match_no_match_test);

	chtbl_simple_func = tcase_create("Simple functions test-case");
	suite_add_tcase(s, chtbl_simple_func);
	tcase_add_test(chtbl_simple_func, chtbl_size_test);
	tcase_add_test(chtbl_simple_func, chtbl_init_test);

	chtbl_insert_tcase = tcase_create("Insert test-case");
	suite_add_tcase(s, chtbl_insert_tcase);
	tcase_add_checked_fixture(chtbl_insert_tcase, ht_setup, ht_teardown);
	tcase_add_test(chtbl_insert_tcase, insert_empty_test);
	tcase_add_test(chtbl_insert_tcase, insert_first_data_test);
	tcase_add_test(chtbl_insert_tcase, insert_mid_data_test);
	tcase_add_test(chtbl_insert_tcase, insert_collision_test);

	chtbl_lookup_tcase = tcase_create("Lookup test-case");
	suite_add_tcase(s, chtbl_lookup_tcase);
	tcase_add_checked_fixture(chtbl_lookup_tcase, ht_setup, ht_teardown);
	tcase_add_test(chtbl_lookup_tcase, lookup_empty_table_test);
	tcase_add_test(chtbl_lookup_tcase, lookup_direct_addressed_table_test);
	tcase_add_test(chtbl_lookup_tcase, lookup_not_found_test);
	tcase_add_test(chtbl_lookup_tcase, lookup_collision_test);
	tcase_add_test(chtbl_lookup_tcase, lookup_fail_collision_test);

	chtbl_remove_tcase = tcase_create("Remove test-case");
	suite_add_tcase(s, chtbl_remove_tcase);
	tcase_add_checked_fixture(chtbl_remove_tcase, ht_setup, ht_teardown);
	tcase_add_test(chtbl_remove_tcase, remove_empty_table_test);
	tcase_add_test(chtbl_remove_tcase, remove_direct_addressed_table_test);
	tcase_add_test(chtbl_remove_tcase, remove_collision_test);
	tcase_add_test(chtbl_remove_tcase, remove_collision_head_test);

	chtbl_destroy_tcase = tcase_create("Destroy test-case");
	suite_add_tcase(s, chtbl_destroy_tcase);
	tcase_add_test(chtbl_destroy_tcase, destroy_empty_table_test);
	tcase_add_test(chtbl_destroy_tcase, destroy_test);

	return s;
}

int main(void)
{
	int nf;
	Suite *s;
	SRunner *sr;

	s = chtbl_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (nf == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
