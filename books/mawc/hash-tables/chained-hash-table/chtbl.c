#include <stdlib.h>
#include <errno.h>
#include "chtbl.h"

int chtbl_init(CHTbl *htbl, int buckets, int (*h)(const void *key),
	       int (*match)(const void *key1, const void *key2),
	       void (*destroy)(void *data))
{
	int i;

	htbl->table = malloc(buckets * sizeof(CHTbl));
	if (!htbl->table)
		return -1;

	for (i = 0; i < buckets; i++)
		list_init(htbl->table + i, destroy);

	htbl->buckets = buckets;
	htbl->h = h;
	htbl->match = match;
	htbl->destroy = destroy;
	htbl->size = 0;

	return 0;
}

int chtbl_insert(CHTbl *htbl, void *data)
{
	int ret, hash;

	if (!htbl) {
		errno = EINVAL;
		return -1;
	}

	hash = htbl->h(data);
	if (hash < 0)
		return -1;
	hash %= htbl->buckets;

	ret = list_ins_next(htbl->table + hash, NULL, data);
	if (ret)
		return -1;

	htbl->size++;
	return 0;
}

int chtbl_lookup(CHTbl *htbl, void **data)
{
	ListElmt *p;
	List *list;
	int hash;

	if (!htbl) {
		errno = EINVAL;
		return -1;
	}

	if (htbl->size == 0)
		return 0;

	hash = htbl->h(*data);
	if (hash < 0)
		return -1;
	hash %= htbl->buckets;

	list = htbl->table + hash;

	for (p = list_head(list); p; p = list_element_next(p)) {
		void *tmp = list_element_data(p);
		int ret;

		ret = htbl->match(tmp, *data);
		if (ret < 0) {
			return -1;
		} else if (ret == 1) {
			*data = tmp;
			return 1;
		}
	}

	return 0;
}

int chtbl_remove(CHTbl *htbl, void **data)
{
	ListElmt *p, *prev;
	List *list;
	int hash;

	if (!htbl) {
		errno = EINVAL;
		return -1;
	}

	if (htbl->size == 0)
		return -1;

	hash = htbl->h(*data);
	if (hash < 0)
		return -1;
	hash %= htbl->buckets;

	prev = NULL;
	list = htbl->table + hash;

	for (p = list_head(list); p; p = list_element_next(p)) {
		void *tmp = list_element_data(p);
		int ret;

		ret = htbl->match(tmp, *data);
		if (ret < 0) {
			return -1;
		} else if (ret == 1) {
			ret = list_rem_next(list, prev, data);
			if (ret)
				return -1;

			htbl->size--;
			return 0;
		}

		prev = p;
	}

	return -1;
}

void chtbl_destroy(CHTbl *htbl)
{
	int i;

	for (i = 0; i < htbl->buckets; i++)
		list_destroy(htbl->table + i);

	free(htbl->table);
	htbl->size = 0;
}
