class Deque(list):
	def push_back(self, value):
		self.append(value)
	def push_front(self, value):
		self.insert(0, value)
	def pop_back(self):
		return self.pop()
	def pop_front(self):
		return self.pop(0)
	def peek_back(self):
		return self[len(self)-1]
	def peek_front(self):
		return self[0]
	def size(self):
		return len(self)
	def destroy(self):
		del self[0:]

class Queue(Deque):
	def enqueue(self, value):
		self.push_back(value)
	def dequeue(self):
		return self.pop_front()
	def peek(self):
		return self.peek_front()

class Stack(Deque):
	def push(self, value):
		self.push_back(value)
	def peek(self):
		return self.peek_back()
