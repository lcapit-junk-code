#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "queue.h"

static void show_prompt(void)
{
	printf(">>> ");
}

static int enqueue_value(Queue *queue, int value)
{
	int *data;

	data = malloc(sizeof(int));
	if (!data)
		return 1;

	memcpy(data, &value, sizeof(int));

	return queue_enqueue(queue, data);
}

#define CMD_SIZE 16

int main(void)
{
	Queue queue;
	int *data, ret;
	char cmd[CMD_SIZE];

	queue_init(&queue, free);

	for (;;) {
		cmd[0] = '\0';
		show_prompt();
		fgets(cmd, CMD_SIZE, stdin);

		if (cmd[0] == 'q') {
			break;
		} else if (cmd[0] == '\0') {
			putc('\n', stdout);
			break;
		} else if (cmd[0] == 'e') {
			int value = 0;

			sscanf(cmd+1, "%d", &value);

			ret = enqueue_value(&queue, value);
			if (ret)
				perror("enqueue_value()");
		} else if (cmd[0] == 'd') {
			ret = queue_dequeue(&queue, (void **) &data);
			if (ret) {
				perror("queue_dequeue()");
				continue;
			}

			printf("%d\n", *data);
			free(data);
		} else if (cmd[0] == 'k') {
			data = queue_peek(&queue);
			printf("%d\n", *data);
		} else if (cmd[0] == 'D') {
			queue_destroy(&queue);
		} else if (cmd[0] == 's') {
			printf("size: %d\n", queue_size(&queue));
		} else if (cmd[0] == 'P' && cmd[1] == 'e') {
			int i, max;

			sscanf(cmd+2, "%d", &max);
			printf("Power enqueue for %d elements\n", max);

			for (i = 0; i < max; i++) {
				ret = enqueue_value(&queue, i);
				if (ret) {
					perror("enqueue_value()");
					exit(1);
				}
			}
		} else if (cmd[0] == 'P' && cmd[1] == 'd') {
			int i, max;

			printf("Power dequeing:\n");

			max = queue_size(&queue);

			for (i = 0; i < max; i++) {
				ret = queue_dequeue(&queue, (void **) &data);
				if (ret) {
					perror("queue_dequeue()");
					exit(1);
				}
				printf("%d\n", *data);
				free(data);
			}
		}
	}

	queue_destroy(&queue);
	return 0;
}
