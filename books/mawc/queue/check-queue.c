#include <stdlib.h>
#include <string.h>
#include <check.h>
#include <errno.h>
#include "queue.h"

static int destroy_called;

static void my_destroy2(void *data)
{
	data = data;
	destroy_called++;
}

START_TEST(queue_destroy_test)
{
	Queue q;
	int i, ret, value;
	const int max_objs = 20;

	queue_init(&q, my_destroy2);

	for (i = 0; i < max_objs; i++) {
		ret = queue_enqueue(&q, &value);
		fail_unless(ret == 0, "queue_enqueue() failed [%d]: %s\n",
			    ret, strerror(errno));
	}

	queue_destroy(&q);

	fail_unless(queue_size(&q) == 0, "queue size is %d but should be 0\n",
		    queue_size(&q));
	fail_unless(destroy_called == max_objs,
		    "destroy function called %d times, but should be %d\n",
		    destroy_called, max_objs);
}
END_TEST

START_TEST(queue_peek_head_null_test)
{
	Queue q;
	void *data;

	queue_init(&q, NULL);

	data = queue_peek(&q);
	fail_unless(data == NULL,
		    "stack's top is %p but should be NULL\n", data);
}
END_TEST

START_TEST(queue_peek_test)
{
	Queue q;
	void *data;
	int ret, value;

	queue_init(&q, NULL);

	ret = queue_enqueue(&q, &value);
	fail_unless(ret == 0, "queue_enqueue() failed [%d]: %s\n",
		    ret, strerror(errno));

	data = queue_peek(&q);
	fail_unless(data == &value,
		    "queue's top is %p but should be %p\n", data, &value);

	ret = queue_dequeue(&q, &data);
	fail_unless(ret == 0, "queue_dequeue() failed [%d]: %s\n",
		    ret, strerror(errno));
	fail_unless(data == &value,
		    "queue's top is %p but should be %p\n", data, &value);
}
END_TEST

START_TEST(queue_dequeue_three_test)
{
	Queue q;
	void *data;
	int ret, fvalue, svalue, tvalue;

	queue_init(&q, NULL);

	ret = queue_enqueue(&q, &fvalue);
	fail_unless(ret == 0, "queue_enqueue() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = queue_enqueue(&q, &svalue);
	fail_unless(ret == 0, "queue_enqueue() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = queue_enqueue(&q, &tvalue);
	fail_unless(ret == 0, "queue_enqueue() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = queue_dequeue(&q, &data);
	fail_unless(ret == 0, "queue_dequeue() failed [%d]: %s\n",
		    ret, strerror(errno));
	fail_unless(data == &fvalue,
		    "queue's first data is %p but should be %p\n",
		    data, &fvalue);

	ret = queue_dequeue(&q, &data);
	fail_unless(ret == 0, "queue_dequeue() failed [%d]: %s\n",
		    ret, strerror(errno));
	fail_unless(data == &svalue,
		    "queue's second data is %p but should be %p\n",
		    data, &svalue);

	ret = queue_dequeue(&q, &data);
	fail_unless(ret == 0, "queue_dequeue() failed [%d]: %s\n",
		    ret, strerror(errno));
	fail_unless(data == &tvalue,
		    "queue's third data is %p but should be %p\n",
		    data, &tvalue);
}
END_TEST

START_TEST(queue_dequeue_single_test)
{
	Queue q;
	void *data;
	int ret, value;

	queue_init(&q, NULL);

	ret = queue_enqueue(&q, &value);
	fail_unless(ret == 0, "queue_enqueue() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = queue_dequeue(&q, &data);
	fail_unless(ret == 0, "queue_dequeue() failed [%d]: %s\n",
		    ret, strerror(errno));
	fail_unless(data == &value,
		    "queue's first data is %p but should be %p\n",
		    data, &value);
}
END_TEST

START_TEST(queue_enqueue_two_test)
{
	Queue q;
	void *data;
	int ret, fvalue, svalue;

	queue_init(&q, NULL);

	ret = queue_enqueue(&q, &fvalue);
	fail_unless(ret == 0, "queue_enqueue() failed [%d]: %s\n",
		    ret, strerror(errno));

	ret = queue_enqueue(&q, &svalue);
	fail_unless(ret == 0, "queue_enqueue() failed [%d]: %s\n",
		    ret, strerror(errno));

	data = list_element_data(list_head(&q));
	fail_unless(data == &fvalue,
		    "queue's first data is %p but should be %p\n",
		    data, &fvalue);

	data = list_element_data(list_tail(&q));
	fail_unless(data == &svalue,
		    "queue's second data is %p but should be %p\n",
		    data, &svalue);

	free(list_head(&q));
	free(list_tail(&q));
}
END_TEST

START_TEST(queue_enqueue_single_test)
{
	Queue q;
	void *data;
	int ret, value;

	queue_init(&q, NULL);

	ret = queue_enqueue(&q, &value);
	fail_unless(ret == 0, "queue_enqueue() failed [%d]: %s\n",
		    ret, strerror(errno));

	data = list_element_data(list_head(&q));
	fail_unless(data == &value,
		    "queue's first data is %p but should be %p\n",
		    data, &value);
	free(list_head(&q));
}
END_TEST

static void my_destroy(void *data)
{
	/* makes gcc happy */
	data = data;
}

START_TEST(queue_init_test)
{
	Queue q;

	queue_init(&q, my_destroy);
	fail_unless(queue_size(&q) == 0,
		    "Queue size is %d but should be 0\n", queue_size(&q));
	fail_unless(q.destroy == my_destroy,
		    "Queue destroy is %p but should be %p\n", q.destroy);
}
END_TEST

START_TEST(queue_size_test)
{
	const int size = 15;
	int ret;
	Queue q;

	q.size = size;
	ret = queue_size(&q);
	fail_unless(ret == size, "queue size is %d but should be %d\n",
		    ret, size);
}
END_TEST

static Suite *queue_suite(void)
{
	Suite *s;
	TCase *qe_interface;

	s = suite_create("Queue implementation test-suite");

	/*
	 * These tests are very simple because they only check
	 * the queue's _interface_. The _implementation_ tests
	 * are performed by the Linked List's test suite.
	 */
	qe_interface = tcase_create("Queue interface test case");
	suite_add_tcase(s, qe_interface);
	tcase_add_test(qe_interface, queue_size_test);
	tcase_add_test(qe_interface, queue_init_test);
	tcase_add_test(qe_interface, queue_enqueue_single_test);
	tcase_add_test(qe_interface, queue_enqueue_two_test);
	tcase_add_test(qe_interface, queue_dequeue_single_test);
	tcase_add_test(qe_interface, queue_dequeue_three_test);
	tcase_add_test(qe_interface, queue_peek_test);
	tcase_add_test(qe_interface, queue_peek_head_null_test);
	tcase_add_test(qe_interface, queue_destroy_test);

	return s;
}

int main(void)
{
	int nf;
	Suite *s;
	SRunner *sr;

	s = queue_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (nf == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
