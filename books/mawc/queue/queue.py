class Queue(list):
	def enqueue(self, value):
		self.append(value)
	def dequeue(self):
		return self.pop(0)
	def size(self):
		return len(self)
	def peek(self):
		return self[0]
	def destroy(self):
		del self[0:]
