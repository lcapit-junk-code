#include <stdlib.h>
#include <check.h>
#include "bitree.h"

static BiTree global_bitree;
static char global_values[] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I' };

static void setup(void)
{
	bitree_init(&global_bitree, NULL);
}

static int destroy_called;

static void my_destroy(void *data)
{
	data = data;
	destroy_called++;
}

static void pop_setup(void)
{
	int ret;
	BiTreeNode *node;

	bitree_init(&global_bitree, my_destroy);

	ret = bitree_ins_left(&global_bitree, NULL, (void *)&global_values[5]);
	fail_unless(ret == 0, "bitree_ins_left() failed: %d\n", ret);
	ret = bitree_ins_left(&global_bitree, bitree_root(&global_bitree),
			      (void *) &global_values[1]);
	fail_unless(ret == 0, "bitree_ins_left() failed: %d\n", ret);
	ret = bitree_ins_right(&global_bitree, bitree_root(&global_bitree),
			      (void *) &global_values[6]);
	fail_unless(ret == 0, "bitree_ins_right() failed: %d\n", ret);
	ret = bitree_ins_left(&global_bitree,
			      bitree_node_left(global_bitree.root),
			      (void *) &global_values[0]);
	fail_unless(ret == 0, "bitree_ins_left() failed: %d\n", ret);
	ret = bitree_ins_right(&global_bitree,
			       bitree_node_left(global_bitree.root),
			      (void *) &global_values[3]);
	fail_unless(ret == 0, "bitree_ins_right() failed: %d\n", ret);
	node = bitree_node_left(global_bitree.root)->right;
	ret = bitree_ins_left(&global_bitree, node, (void *)&global_values[2]);
	fail_unless(ret == 0, "bitree_ins_left() failed: %d\n", ret);
	ret = bitree_ins_right(&global_bitree, node,(void *)&global_values[4]);
	fail_unless(ret == 0, "bitree_ins_right() failed: %d\n", ret);
	ret = bitree_ins_right(&global_bitree,
			       bitree_node_right(global_bitree.root),
			      (void *) &global_values[8]);
	fail_unless(ret == 0, "bitree_ins_right() failed: %d\n", ret);
	node = bitree_node_right(global_bitree.root)->right;
	ret = bitree_ins_left(&global_bitree, node, (void *)&global_values[7]);
	fail_unless(ret == 0, "bitree_ins_left() failed: %d\n", ret);
}

/*
 * Remotion functions tests
 */

START_TEST(bitree_rem_right_null_test)
{
	int max_objs;

	destroy_called = 0;
	max_objs = sizeof(global_values);

	bitree_rem_right(&global_bitree, NULL);
	fail_unless(bitree_size(&global_bitree) == 0,
		    "Binary tree's size is %d but should be 0\n",
		    bitree_size(&global_bitree));
	fail_unless(bitree_root(&global_bitree) == NULL,
		    "Binary tree's root node is %p but should be NULL\n",
		    bitree_root(&global_bitree));
	fail_unless(destroy_called == max_objs,
		    "destroy function called %d times, but should be %d\n",
		    destroy_called, max_objs);
}
END_TEST

START_TEST(bitree_rem_right_test)
{
	BiTreeNode *node;

	destroy_called = 0;
	node = bitree_root(&global_bitree)->left;

	bitree_rem_right(&global_bitree, node);
	fail_unless(bitree_size(&global_bitree) == 6,
		    "Binary tree's size is %d but should be 6\n",
		    bitree_size(&global_bitree));
	fail_unless(node->right == NULL,
		    "Node's right pointer is %p but should be NULL\n",
		    node->right);
	fail_unless(destroy_called == 3,
		    "destroy function called %d times, but should be %d\n",
		    destroy_called, 3);

	bitree_destroy(&global_bitree);
}
END_TEST

START_TEST(bitree_rem_right_null_empty_test)
{
	BiTree tree;

	bitree_init(&tree, NULL);

	/* Should not explode */
	bitree_rem_right(NULL, NULL);
	bitree_rem_right(&tree, NULL);
}
END_TEST

START_TEST(bitree_rem_left_null_test)
{
	int max_objs;

	destroy_called = 0;
	max_objs = sizeof(global_values);

	bitree_rem_left(&global_bitree, NULL);
	fail_unless(bitree_size(&global_bitree) == 0,
		    "Binary tree's size is %d but should be 0\n",
		    bitree_size(&global_bitree));
	fail_unless(bitree_root(&global_bitree) == NULL,
		    "Binary tree's root node is %p but should be NULL\n",
		    bitree_root(&global_bitree));
	fail_unless(destroy_called == max_objs,
		    "destroy function called %d times, but should be %d\n",
		    destroy_called, max_objs);
}
END_TEST

START_TEST(bitree_rem_left_test)
{
	BiTreeNode *node;

	destroy_called = 0;
	node = bitree_root(&global_bitree)->left;

	bitree_rem_left(&global_bitree, node);
	fail_unless(bitree_size(&global_bitree) == 8,
		    "Binary tree's size is %d but should be 8\n",
		    bitree_size(&global_bitree));
	fail_unless(node->left == NULL,
		    "Node's left pointer is %p but should be NULL\n",
		    node->left);
	fail_unless(destroy_called == 1,
		    "destroy function called %d times, but should be %d\n",
		    destroy_called, 1);

	bitree_destroy(&global_bitree);
}
END_TEST

START_TEST(bitree_rem_left_null_empty_test)
{
	BiTree tree;

	bitree_init(&tree, NULL);

	/* Should not explode */
	bitree_rem_left(NULL, NULL);
	bitree_rem_left(&tree, NULL);
}
END_TEST

/*
 * Destroy function tests
 */

START_TEST(bitree_test)
{
	int max_objs;

	destroy_called = 0;
	max_objs = sizeof(global_values);

	bitree_destroy(&global_bitree);
	fail_unless(bitree_size(&global_bitree) == 0,
		    "Binary tree's size is %d but should be 0\n",
		    bitree_size(&global_bitree));
	fail_unless(bitree_root(&global_bitree) == NULL,
		    "Binary tree's root node is %p but should be NULL\n",
		    bitree_root(&global_bitree));
	fail_unless(destroy_called == max_objs,
		    "destroy function called %d times, but should be %d\n",
		    destroy_called, max_objs);
}
END_TEST

START_TEST(bitree_destroy_null_empty_test)
{
	BiTree tree;

	bitree_init(&tree, NULL);

	/* should not explode */
	bitree_destroy(NULL);
	bitree_destroy(&tree);
}
END_TEST

/*
 * Merge function tests
 */

START_TEST(bitree_merge_test)
{
	BiTree btleft, btright;
	BiTreeNode *btlroot, *btrroot, *root_node;
	int value, btrvalue, btlvalue, size, ret;

	bitree_init(&btleft, NULL);
	ret = bitree_ins_left(&btleft, NULL, (void *) &btlvalue);
	fail_unless(ret == 0, "bitree_ins_left() failed: %d\n", ret);
	btlroot = bitree_root(&btleft);
	ret = bitree_ins_left(&btleft, btlroot, (void *) &btlvalue);
	fail_unless(ret == 0, "bitree_ins_left() failed: %d\n", ret);
	ret = bitree_ins_right(&btleft, btlroot, (void *) &btlvalue);
	fail_unless(ret == 0, "bitree_ins_right() failed: %d\n", ret);

	bitree_init(&btright, NULL);
	ret = bitree_ins_left(&btright, NULL, (void *) &btrvalue);
	fail_unless(ret == 0, "bitree_ins_left() failed: %d\n", ret);
	btrroot = bitree_root(&btright);
	ret = bitree_ins_left(&btright, btrroot, (void *) &btrvalue);
	fail_unless(ret == 0, "bitree_ins_left() failed: %d\n", ret);
	ret = bitree_ins_right(&btright, btrroot, (void *) &btrvalue);
	fail_unless(ret == 0, "bitree_ins_right() failed: %d\n", ret);

	size = btleft.size + btright.size + 1;

	ret = bitree_merge(&global_bitree, &btleft, &btright, (void *)&value);
	fail_unless(ret == 0, "bitree_merge() failed: %d\n", ret);
	fail_unless(bitree_size(&global_bitree) == size,
		    "Binary tree's size is %d but should be %d\n",
		    bitree_size(&global_bitree), size);
	root_node = bitree_root(&global_bitree);
	fail_unless(root_node != NULL, "Binary tree's root node is NULL\n");
	fail_unless(root_node->data == (void *) &value,
		    "Binary tree root node data is %p but should be %p\n",
		    root_node->data, &value);
	fail_unless(root_node->left == btlroot,
		    "Binary tree root left node is %p but should be %p\n",
		    root_node->left, btlroot);
	fail_unless(root_node->right == btrroot,
		    "Binary tree root right node is %p but should be %p\n",
		    root_node->right, btrroot);

	free(root_node->left->left);
	free(root_node->left->right);
	free(root_node->left);

	free(root_node->right->left);
	free(root_node->right->right);
	free(root_node->right);

	free(root_node);
}
END_TEST

START_TEST(bitree_merge_null_test)
{
	int value, ret;
	BiTree btleft, btright;

	ret = bitree_merge(NULL, &btleft, &btright, (void *) &value);
	fail_unless(ret == -1, "bitree_merge() didn't fail: %d\n", ret);

	ret = bitree_merge(&global_bitree, NULL, &btright, (void *) &value);
	fail_unless(ret == -1, "bitree_merge() didn't fail: %d\n", ret);

	ret = bitree_merge(&global_bitree, &btleft, NULL, (void *) &value);
	fail_unless(ret == -1, "bitree_merge() didn't fail: %d\n", ret);

	ret = bitree_merge(NULL, NULL, NULL, (void *) &value);
	fail_unless(ret == -1, "bitree_merge() didn't fail: %d\n", ret);
}
END_TEST

/*
 * Insertion functions' tests
 */

START_TEST(bitree_ins_right_root_test)
{
	int value, ret;
	BiTreeNode *root_node;

	ret = bitree_ins_right(&global_bitree, NULL, (void *) &value);
	fail_unless(ret == 0, "bitree_ins_right() failed: %d\n", ret);
	fail_unless(bitree_size(&global_bitree) == 1,
		    "Binary tree's size is %d but should be 1\n",
		    bitree_size(&global_bitree));

	root_node = bitree_root(&global_bitree);
	fail_unless(root_node != NULL, "root node is NULL\n");
	fail_unless(root_node->data == (void *) &value,
		    "root node's data is %p but should be %p\n",
		    root_node->data, &value);
	fail_unless(root_node->right == NULL,
		    "root node's right pointer is not NULL");
	fail_unless(root_node->left == NULL,
		    "root node's left pointer is not NULL");

	free(root_node);
}
END_TEST

START_TEST(bitree_ins_right_root_error_test)
{
	int value, ret;

	/* to trigger the error */
	global_bitree.size = 1;

	ret = bitree_ins_right(&global_bitree, NULL, (void *) &value);
	fail_unless(ret == -1, "bitree_ins_right() didn't fail: %d\n", ret);
	fail_unless(bitree_size(&global_bitree) == 1,
		    "Binary tree's size is %d but should be 1\n",
		    bitree_size(&global_bitree));
}
END_TEST

START_TEST(bitree_ins_right_test)
{
	int value, ret;
	BiTreeNode node;

	node.right = NULL;

	ret = bitree_ins_right(&global_bitree, &node, (void *) &value);
	fail_unless(ret == 0, "bitree_ins_right() failed: %d\n", ret);
	fail_unless(bitree_size(&global_bitree) == 1,
		    "Binary tree's size is %d but should be 1\n",
		    bitree_size(&global_bitree));
	fail_unless(node.right != NULL, "node's right is NULL\n");
	fail_unless(node.right->data == (void *) &value,
		    "node's right data is %p but should be %p\n",
		    node.right->data, &value);
	fail_unless(node.right->right == NULL,
		    "node's right right pointer is not NULL");
	fail_unless(node.right->left == NULL,
		    "node's right left pointer is not NULL");

	free(node.right);
}
END_TEST

START_TEST(bitree_ins_right_error_test)
{
	int value, ret;
	BiTreeNode node, rnode;

	node.right = &rnode;

	ret = bitree_ins_right(&global_bitree, &node, (void *) &value);
	fail_unless(ret == -1, "bitree_ins_right() didn't fail: %d\n", ret);
	fail_unless(bitree_size(&global_bitree) == 0,
		    "Binary tree's size is %d but should be 0\n",
		    bitree_size(&global_bitree));
}
END_TEST

START_TEST(bitree_ins_right_null_tree_test)
{
	int value, ret;
	BiTreeNode node;

	ret = bitree_ins_right(NULL, &node, (void *) &value);
	fail_unless(ret == -1, "bitree_ins_right() didn't fail: %d\n", ret);
	fail_unless(bitree_size(&global_bitree) == 0,
		    "Binary tree's size is %d but should be 0\n",
		    bitree_size(&global_bitree));
}
END_TEST

START_TEST(bitree_ins_left_root_test)
{
	int value, ret;
	BiTreeNode *root_node;

	ret = bitree_ins_left(&global_bitree, NULL, (void *) &value);
	fail_unless(ret == 0, "bitree_ins_left() failed: %d\n", ret);
	fail_unless(bitree_size(&global_bitree) == 1,
		    "Binary tree's size is %d but should be 1\n",
		    bitree_size(&global_bitree));

	root_node = bitree_root(&global_bitree);
	fail_unless(root_node != NULL, "root node is NULL\n");
	fail_unless(root_node->data == (void *) &value,
		    "root node's data is %p but should be %p\n",
		    root_node->data, &value);
	fail_unless(root_node->left == NULL,
		    "root node's left pointer is not NULL");
	fail_unless(root_node->right == NULL,
		    "root node's right pointer is not NULL");

	free(root_node);
}
END_TEST

START_TEST(bitree_ins_left_root_error_test)
{
	int value, ret;

	/* to trigger the error */
	global_bitree.size = 1;

	ret = bitree_ins_left(&global_bitree, NULL, (void *) &value);
	fail_unless(ret == -1, "bitree_ins_left() didn't fail: %d\n", ret);
	fail_unless(bitree_size(&global_bitree) == 1,
		    "Binary tree's size is %d but should be 1\n",
		    bitree_size(&global_bitree));
}
END_TEST

START_TEST(bitree_ins_left_test)
{
	int value, ret;
	BiTreeNode node;

	node.left = NULL;

	ret = bitree_ins_left(&global_bitree, &node, (void *) &value);
	fail_unless(ret == 0, "bitree_ins_left() failed: %d\n", ret);
	fail_unless(bitree_size(&global_bitree) == 1,
		    "Binary tree's size is %d but should be 1\n",
		    bitree_size(&global_bitree));
	fail_unless(node.left != NULL, "node's left is NULL\n");
	fail_unless(node.left->data == (void *) &value,
		    "node's left data is %p but should be %p\n",
		    node.left->data, &value);
	fail_unless(node.left->left == NULL,
		    "node's left left pointer is not NULL");
	fail_unless(node.left->right == NULL,
		    "node's left right pointer is not NULL");

	free(node.left);
}
END_TEST

START_TEST(bitree_ins_left_error_test)
{
	int value, ret;
	BiTreeNode node, lnode;

	node.left = &lnode;

	ret = bitree_ins_left(&global_bitree, &node, (void *) &value);
	fail_unless(ret == -1, "bitree_ins_left() didn't fail: %d\n", ret);
	fail_unless(bitree_size(&global_bitree) == 0,
		    "Binary tree's size is %d but should be 0\n",
		    bitree_size(&global_bitree));
}
END_TEST

START_TEST(bitree_ins_left_null_tree_test)
{
	int value, ret;
	BiTreeNode node;

	ret = bitree_ins_left(NULL, &node, (void *) &value);
	fail_unless(ret == -1, "bitree_ins_left() didn't fail: %d\n", ret);
	fail_unless(bitree_size(&global_bitree) == 0,
		    "Binary tree's size is %d but should be 0\n",
		    bitree_size(&global_bitree));
}
END_TEST

/*
 * Initialization functions' tests
 */

START_TEST(bitree_init_test)
{
	BiTree t;

	bitree_init(&t, free);
	fail_unless(t.size == 0,
		    "Binary tree's size is %d but should be 0\n", t.size);
	fail_unless(t.destroy == free,
		    "Binary tree's destroy function is %p but should be %p\n",
		    t.destroy, free);
	fail_unless(t.root == NULL,
		    "Binary tree's root member is %p but should be NULL\n",
		    t.root);
}
END_TEST

START_TEST(bitree_init_null_test)
{
	/* should not explode */
	bitree_init(NULL, NULL);
}
END_TEST

/*
 * Simple functons' tests
 */

START_TEST(bitree_node_is_not_eob_test)
{
	int ret;
	BiTreeNode node;

	ret = bitree_node_is_eob(&node);
	fail_unless(ret == 0, "node is not a end-of-branch\n");
}
END_TEST

START_TEST(bitree_node_is_eob_test)
{
	int ret;

	ret = bitree_node_is_eob(NULL);
	fail_unless(ret == 1, "node is a end-of-branch\n");
}
END_TEST

START_TEST(bitree_node_is_not_leaf_btest)
{
	int ret;
	BiTreeNode node, lnode, rnode;

	node.left = &lnode;
	node.right = &rnode;

	ret = bitree_node_is_leaf(&node);
	fail_unless(ret == 0, "node is a leaf\n");
}
END_TEST

START_TEST(bitree_node_is_not_leaf_ltest)
{
	int ret;
	BiTreeNode node, lnode;

	node.left = &lnode;
	node.right = NULL;

	ret = bitree_node_is_leaf(&node);
	fail_unless(ret == 0, "node is a leaf\n");
}
END_TEST

START_TEST(bitree_node_is_not_leaf_rtest)
{
	int ret;
	BiTreeNode node, rnode;

	node.left = NULL;
	node.right = &rnode;

	ret = bitree_node_is_leaf(&node);
	fail_unless(ret == 0, "node is a leaf\n");
}
END_TEST

START_TEST(bitree_node_is_leaf_test)
{
	int ret;
	BiTreeNode node;

	node.left = node.right = NULL;

	ret = bitree_node_is_leaf(&node);
	fail_unless(ret == 1, "node is not a leaf\n");
}
END_TEST

START_TEST(bitree_node_right_test)
{
	BiTreeNode node, rnode, *ret_node;

	node.right = &rnode;
	ret_node = bitree_node_right(&node);
	fail_unless(ret_node == &rnode, "right node is %p but should be %p\n",
		    ret_node, &rnode);
}
END_TEST

START_TEST(bitree_node_left_test)
{
	BiTreeNode node, lnode, *ret_node;

	node.left = &lnode;
	ret_node = bitree_node_left(&node);
	fail_unless(ret_node == &lnode, "left node is %p but should be %p\n",
		    ret_node, &lnode);
}
END_TEST

START_TEST(bitree_node_data_test)
{
	void *data;
	int value;
	BiTreeNode node;

	node.data = (void *) &value;
	data = bitree_node_data(&node);
	fail_unless(data == (void *) &value,
		    "node's data is %p but should be %p\n", data, &value);
}
END_TEST

START_TEST(bitree_root_test)
{
	BiTreeNode root_node, *node;
	BiTree t;

	t.root = &root_node;
	node = bitree_root(&t);
	fail_unless(node == &root_node,
		    "Binary tree's root node is %p but should be %p\n",
		    node, &root_node);
}
END_TEST

START_TEST(bitree_size_test)
{
	const int size = 15;
	int ret;
	BiTree t;

	t.size = size;
	ret = bitree_size(&t);
	fail_unless(ret == size, "Binary tree's size is %d but should be %d\n",
		    ret, size);
}
END_TEST

static Suite *bitree_suite(void)
{
	Suite *s;
	TCase *simple_func;
	TCase *bitree_init_tcase;
	TCase *bitree_ins_tcase;
	TCase *bitree_mg_tcase;
	TCase *bitree_dest_tcase;
	TCase *bitree_rem_tcase;

	s = suite_create("Binary tree implementation test-suite");

	simple_func = tcase_create("Simple functions test-case");
	suite_add_tcase(s, simple_func);
	tcase_add_test(simple_func, bitree_size_test);
	tcase_add_test(simple_func, bitree_root_test);
	tcase_add_test(simple_func, bitree_node_data_test);
	tcase_add_test(simple_func, bitree_node_left_test);
	tcase_add_test(simple_func, bitree_node_right_test);
	tcase_add_test(simple_func, bitree_node_is_leaf_test);
	tcase_add_test(simple_func, bitree_node_is_not_leaf_rtest);
	tcase_add_test(simple_func, bitree_node_is_not_leaf_ltest);
	tcase_add_test(simple_func, bitree_node_is_not_leaf_btest);
	tcase_add_test(simple_func, bitree_node_is_eob_test);
	tcase_add_test(simple_func, bitree_node_is_not_eob_test);

	bitree_init_tcase = tcase_create("Initialization function test-case");
	suite_add_tcase(s, bitree_init_tcase);
	tcase_add_test(bitree_init_tcase, bitree_init_null_test);
	tcase_add_test(bitree_init_tcase, bitree_init_test);

	bitree_ins_tcase = tcase_create("Insertion functions test-case");
	suite_add_tcase(s, bitree_ins_tcase);
	tcase_add_checked_fixture(bitree_ins_tcase, setup, NULL);
	tcase_add_test(bitree_ins_tcase, bitree_ins_left_null_tree_test);
	tcase_add_test(bitree_ins_tcase, bitree_ins_left_error_test);
	tcase_add_test(bitree_ins_tcase, bitree_ins_left_test);
	tcase_add_test(bitree_ins_tcase, bitree_ins_left_root_error_test);
	tcase_add_test(bitree_ins_tcase, bitree_ins_left_root_test);
	tcase_add_test(bitree_ins_tcase, bitree_ins_right_null_tree_test);
	tcase_add_test(bitree_ins_tcase, bitree_ins_right_error_test);
	tcase_add_test(bitree_ins_tcase, bitree_ins_right_test);
	tcase_add_test(bitree_ins_tcase, bitree_ins_right_root_error_test);
	tcase_add_test(bitree_ins_tcase, bitree_ins_right_root_test);

	bitree_mg_tcase = tcase_create("Merge function test-case");
	suite_add_tcase(s, bitree_mg_tcase);
	tcase_add_checked_fixture(bitree_mg_tcase, setup, NULL);
	tcase_add_test(bitree_mg_tcase, bitree_merge_null_test);
	tcase_add_test(bitree_mg_tcase, bitree_merge_test);

	bitree_dest_tcase = tcase_create("Destroy function test-case");
	suite_add_tcase(s, bitree_dest_tcase);
	tcase_add_checked_fixture(bitree_dest_tcase, pop_setup, NULL);
	tcase_add_test(bitree_dest_tcase, bitree_destroy_null_empty_test);
	tcase_add_test(bitree_dest_tcase, bitree_test);

	bitree_rem_tcase = tcase_create("Remotion function test-case");
	suite_add_tcase(s, bitree_rem_tcase);
	tcase_add_checked_fixture(bitree_rem_tcase, pop_setup, NULL);
	tcase_add_test(bitree_rem_tcase, bitree_rem_left_null_empty_test);
	tcase_add_test(bitree_rem_tcase, bitree_rem_left_test);
	tcase_add_test(bitree_rem_tcase, bitree_rem_left_null_test);
	tcase_add_test(bitree_rem_tcase, bitree_rem_right_null_empty_test);
	tcase_add_test(bitree_rem_tcase, bitree_rem_right_test);
	tcase_add_test(bitree_rem_tcase, bitree_rem_right_null_test);

	return s;
}

int main(void)
{
	int nf;
	Suite *s;
	SRunner *sr;

	s = bitree_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (nf == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
