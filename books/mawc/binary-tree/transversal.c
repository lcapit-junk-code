#include <stdio.h>
#include <stdlib.h>
#include "bitree.h"

static void pre_order(BiTree *tree, BiTreeNode *node)
{
	if (!node)
		return;

	printf("%2d ", *((int *)node->data));

	pre_order(tree, node->left);
	pre_order(tree, node->right);
}

static void post_order(BiTree *tree, BiTreeNode *node)
{
	if (!node)
		return;

	post_order(tree, node->left);
	post_order(tree, node->right);

	printf("%2d ", *((int *)node->data));
}

static void in_order(BiTree *tree, BiTreeNode *node)
{
	if (!node)
		return;

	printf("%2d ", *((int *)node->data));

	in_order(tree, node->left);
	in_order(tree, node->right);
}

int main(void)
{
	int err;
	BiTree tree;
	int values[] = { 1, 5, 7, 9, 11, 15, 20, 44, 53, 79 };

	bitree_init(&tree, NULL);

	// Root element
	err = bitree_ins_left(&tree, NULL, (void *) &values[6]);
	if (err) {
		fprintf(stderr, "bitree_ins_left() failed\n");
		exit(1);
	}

	err = bitree_ins_left(&tree, bitree_root(&tree), (void *) &values[3]);
	if (err) {
		fprintf(stderr, "bitree_ins_left() failed\n");
		exit(1);
	}

	err = bitree_ins_right(&tree, bitree_root(&tree), (void *) &values[8]);
	if (err) {
		fprintf(stderr, "bitree_ins_right() failed\n");
		exit(1);
	}

	err = bitree_ins_left(&tree, bitree_node_left(tree.root),
			      (void *) &values[1]);
	if (err) {
		fprintf(stderr, "bitree_ins_left() failed\n");
		exit(1);
	}

	err = bitree_ins_right(&tree, bitree_node_left(tree.root),
			      (void *) &values[5]);
	if (err) {
		fprintf(stderr, "bitree_ins_right() failed\n");
		exit(1);
	}

	err = bitree_ins_left(&tree, bitree_node_right(tree.root),
			      (void *) &values[7]);
	if (err) {
		fprintf(stderr, "bitree_ins_left() failed\n");
		exit(1);
	}

	err = bitree_ins_right(&tree, bitree_node_right(tree.root),
			       (void *) &values[9]);
	if (err) {
		fprintf(stderr, "bitree_ins_right() failed\n");
		exit(1);
	}

	printf("\nPre-order : ");
	pre_order(&tree, bitree_root(&tree));

	printf("\nPost-order: ");
	post_order(&tree, bitree_root(&tree));

	printf("\nIn-order  : ");
	in_order(&tree, bitree_root(&tree));
	printf("\n");

	bitree_destroy(&tree);
	return 0;
}
