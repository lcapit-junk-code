#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "bitree.h"

static BiTreeNode *create_node(BiTreeNode *left, BiTreeNode *right,
			       void *data)
{
	BiTreeNode *node;

	node = malloc(sizeof(BiTreeNode));
	if (!node)
		return NULL;

	node->left  = left;
	node->right = right;
	node->data  = data;

	return node;
}

/* Removes leafs using post-order transversal */
static void destroy_subtree(BiTree *tree, BiTreeNode *node)
{
	if (node->left)
		destroy_subtree(tree, node->left);
	if (node->right)
		destroy_subtree(tree, node->right);

	if (tree->destroy)
		tree->destroy(node->data);
	tree->size--;
	free(node);
}

void bitree_init(BiTree *tree, void (*destroy)(void *data))
{
	if (!tree)
		return;

	tree->destroy = destroy;
	tree->root = NULL;
	tree->size = 0;
}

int bitree_ins_left(BiTree *tree, BiTreeNode *node, void *data)
{
	BiTreeNode **position;

	if (!tree)
		goto out_err;

	if (!node) {
		if (bitree_size(tree))
			goto out_err;
		position = &tree->root;
	} else {
		if (node->left)
			goto out_err;
		position = &node->left;
	}

	*position = create_node(NULL, NULL, data);
	if (!*position)
		return -1;

	tree->size++;
	return 0;

out_err:
	errno = EINVAL;
	return -1;
}

int bitree_ins_right(BiTree *tree, BiTreeNode *node, void *data)
{
	BiTreeNode **position;

	if (!tree)
		goto out_err;

	if (!node) {
		if (bitree_size(tree))
			goto out_err;
		position = &tree->root;
	} else {
		if (node->right)
			goto out_err;
		position = &node->right;
	}

	*position = create_node(NULL, NULL, data);
	if (!*position)
		return -1;

	tree->size++;
	return 0;

out_err:
	errno = EINVAL;
	return -1;
}

int bitree_merge(BiTree *merge, BiTree *left, BiTree *right, void *data)
{
	if (!merge || !left || !right)
		goto out_err;

	if (bitree_size(merge) || !bitree_size(left) || !bitree_size(right))
		goto out_err;

	merge->root = create_node(bitree_root(left), bitree_root(right), data);
	if (!merge->root)
		return -1;

	merge->size = bitree_size(left) + bitree_size(right) + 1;

	/* Just to avoid confusion */
	left->root = right->root = NULL;
	left->size = right->size = 0;

	return 0;

out_err:
	errno = EINVAL;
	return -1;
}

void bitree_destroy(BiTree *tree)
{
	if (!tree || !bitree_size(tree))
		return;

	destroy_subtree(tree, tree->root);
	tree->root = NULL;
}

void bitree_rem_left(BiTree *tree, BiTreeNode *node)
{
	if (!tree || !bitree_size(tree))
		return;

	if (!node) {
		bitree_destroy(tree);
		return;
	}

	if (!node->left)
		return;

	destroy_subtree(tree, node->left);
	node->left = NULL;
}

void bitree_rem_right(BiTree *tree, BiTreeNode *node)
{
	if (!tree || !bitree_size(tree))
		return;

	if (!node) {
		bitree_destroy(tree);
		return;
	}

	if (!node->right)
		return;

	destroy_subtree(tree, node->right);
	node->right = NULL;
}
