#ifndef __SCULL_H
#define __SCULL_H

#define SCULL_MAJOR   0
#define SCULL_MINOR   0
#define SCULL_NR_DEVS 4

/*
 * The bare device is a variable-length region of memory.
 * Use a linked list of indirect blocks.
 *
 * "scull_dev->data" points to an array of pointers, each
 * pointer refers to a memory area of SCULL_QUANTUM bytes.
 *
 * The array (quantum-set) is SCULL_QSET long.
 */
#define SCULL_QUANTUM 4000
#define SCULL_QSET    1000

#endif /* __SCULL_H */
