/*
 * check_membuffer: membuffer kernel module interface test-suite
 *
 * Compile with:
 * $ cc -o check_membuffer check_membuffer.c -Wall -lcheck
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <check.h>
#include "membuffer.h"

static int fd;
static char *port = "/dev/membuffer0";

void setup(void)
{
	fd = open(port, O_RDWR);
	fail_unless(fd >= 0, "Is the membuffer module loaded?\n");
}

void teardown(void)
{
	int ret;

	ret = close(fd);
	fail_unless(ret == 0, NULL);
}

START_TEST(test_read_write)
{
	ssize_t ret;
	char *buf;
	const char str[] = "Coutdown to Extinction";

	/* write test */
	ret = write(fd, str, sizeof(str));
	fail_unless(ret == sizeof(str), NULL);

	/* reset the position in the the file */
	close(fd);
	fd = open(port, O_RDWR);
	fail_unless(fd >= 0, "Is the membuffer module loaded?\n");

	/* read test */
	buf = malloc(sizeof(str));
	fail_unless(buf != NULL, NULL);

	ret = read(fd, buf, sizeof(str));
	fail_unless(ret == sizeof(str));

	/* read and write test */
	fail_unless(strncmp(buf, str, sizeof(str)) == 0, "[%s]\n", buf);

	free(buf);
}
END_TEST

START_TEST(test_lseek)
{
	off_t ret;

	ret = lseek(fd, 13, SEEK_SET);
	fail_unless(ret == 13, NULL);

	ret = lseek(fd, 2, SEEK_CUR);
	fail_unless(ret == 15, NULL);

	ret = lseek(fd, 5, SEEK_END);
	fail_unless(ret == -1, NULL);
	fail_unless(errno == EINVAL, NULL);
}
END_TEST

START_TEST(test_ioctl)
{
	int err;
	size_t size;
	size_t new_size = 2*1024;

	/* MEMBUFFER_RESET, we don't have capabilitie to do that */
	err = ioctl(fd, MEMBUFFER_RESET);
	fail_unless(err == -1, NULL);
	fail_unless(errno == EPERM, NULL);

	/* MEMBUFFER_GET_SIZE */
	err = ioctl(fd, MEMBUFFER_GET_SIZE, &size);
	fail_unless(err == 0, NULL);
	fail_unless(size == MEMBUFFER_DEFAULT_SIZE, NULL);

	/* MEMBUFFER_SET_SIZE */
	err = ioctl(fd, MEMBUFFER_SET_SIZE, &new_size);
	fail_unless(err == 0, NULL);

	err = ioctl(fd, MEMBUFFER_GET_SIZE, &size);
	fail_unless(err == 0, NULL);
	fail_unless(size == new_size, NULL);

	/*
	 * Restore original values, otherwise the others
	 * tests will be dameged
	 */
	new_size = MEMBUFFER_DEFAULT_SIZE;
	err = ioctl(fd, MEMBUFFER_SET_SIZE, &new_size);
	fail_unless(err == 0, NULL);
}
END_TEST

START_TEST(test_write_limits)
{
	size_t ret;
	char buf[MEMBUFFER_DEFAULT_SIZE+3];

	memset(buf, '1', sizeof(buf));
	ret = write(fd, buf, sizeof(buf));
	fail_unless(ret == MEMBUFFER_DEFAULT_SIZE, NULL);

	ret = write(fd, buf, sizeof(buf));
	fail_unless(ret == -1, "written %d bytes\n", ret);
	fail_unless(errno == ENOSPC, NULL);
}
END_TEST

START_TEST(test_read_limits)
{
	size_t ret;
	char buf[MEMBUFFER_DEFAULT_SIZE+4];

	ret = read(fd, buf, sizeof(buf));
	fail_unless(ret == MEMBUFFER_DEFAULT_SIZE, NULL);

	ret = read(fd, buf, sizeof(buf));
	fail_unless(ret == 0, NULL);
}
END_TEST

START_TEST(test_handle_data)
{
	int err, fd;
	ssize_t ret;
	off_t pos;
	char *buf;
	const char data[] = "nsjnsjansjansjansjansjansiiqikajnaknskajasnj    ksad akjnd ";

	fd = open(port, O_RDWR);
	fail_unless(fd >= 0, "Is the membuffer module loaded?\n");

	/* write the data */
	ret = write(fd, data, sizeof(data));
	fail_unless(ret == sizeof(data), NULL);

	/* check the current file position */
	pos = lseek(fd, 0, SEEK_CUR);
	fail_unless(pos == sizeof(data), NULL);

	/* set the position to the start of the file */
	pos = lseek(fd, 0, SEEK_SET);
	fail_unless(pos == 0, NULL);

	/* check the data just written */
	buf = malloc(sizeof(data));
	fail_unless(buf != NULL, NULL);

	ret = read(fd, buf, sizeof(data));
	fail_unless(ret == sizeof(data), NULL);
	fail_unless(strncmp(buf, data, sizeof(data)) == 0, NULL);

	err = close(fd);
	fail_unless(err == 0, NULL);

	free(buf);
}
END_TEST

START_TEST(test_high_level_io)
{
	int err;
	FILE *file;
	const char str[] = "kansdkjndka 42 666    \n";
	char buf[32];

	file = fopen(port, "r+");
	fail_unless(file != NULL, NULL);

	fprintf(file, "%s", str);
	rewind(file);
	fgets(buf, sizeof(buf), file);

	/* the real test */
	fail_unless(strcmp(buf, str) == 0, NULL);

	err = fclose(file);
	fail_unless(err == 0, NULL);
}
END_TEST

START_TEST(test_proc_basic)
{
	int ret, fd;
	char data[1024];

	memset(data, '\0', sizeof(data));
	fd = open("/proc/membuffer", O_RDONLY);
	fail_unless(fd >= 0, "Is the membuffer module loaded?\n");

	ret = read(fd, data, sizeof(data));
	fail_unless(ret > 0, NULL);
	fail_unless(strstr(data, "membuffer") != NULL, NULL);
	fail_unless(strstr(data, "size") != NULL, NULL);
}
END_TEST

START_TEST(test_set_size_reset_pos)
{
	off_t pos;
	ssize_t ret;
	int fd, err;
	size_t new_size = 2*1024;
	const char data[] = "woman";

	fd = open(port, O_RDWR);
	fail_unless(fd >= 0, "Is the membuffer module loaded?\n");

	ret = write(fd, data, sizeof(data));
	fail_unless(ret == sizeof(data), NULL);

	err = ioctl(fd, MEMBUFFER_SET_SIZE, &new_size);
	fail_unless(err == 0, NULL);

	pos = lseek(fd, 0, SEEK_CUR);
	fail_unless(pos == 0, NULL);

	/*
	 * Restore original values, otherwise the others
	 * tests will be dameged
	 */
	new_size = MEMBUFFER_DEFAULT_SIZE;
	err = ioctl(fd, MEMBUFFER_SET_SIZE, &new_size);
	fail_unless(err == 0, NULL);

	err = close(fd);
	fail_unless(err == 0, NULL);
}
END_TEST

Suite *membuffer_suite(void)
{
	Suite *s;
	TCase *tc_basic;
	TCase *tc_limits;
	TCase *tc_complete_usage;
	TCase *tc_proc_support;
	TCase *tc_regression;

	s = suite_create("membuffer test suite");
	tc_basic = tcase_create("Basic usage");
	suite_add_tcase(s, tc_basic);
	tcase_add_test(tc_basic, test_read_write);
	tcase_add_test(tc_basic, test_lseek);
	tcase_add_test(tc_basic, test_ioctl);
	tcase_add_checked_fixture(tc_basic, setup, teardown);

	tc_limits = tcase_create("Limits");
	suite_add_tcase(s, tc_limits);
	tcase_add_test(tc_limits, test_write_limits);
	tcase_add_test(tc_limits, test_read_limits);
	tcase_add_checked_fixture(tc_limits, setup, teardown);

	tc_complete_usage = tcase_create("Complete usage");
	suite_add_tcase(s, tc_complete_usage);
	tcase_add_test(tc_complete_usage, test_handle_data);
	tcase_add_test(tc_complete_usage, test_high_level_io);

	tc_proc_support = tcase_create("Proc support");
	suite_add_tcase(s, tc_proc_support);
	tcase_add_test(tc_proc_support, test_proc_basic);

	tc_regression = tcase_create("Regression tests");
	suite_add_tcase(s, tc_regression);
	tcase_add_test(tc_regression, test_set_size_reset_pos);

	return s;
}

int main(int argc, char *argv[])
{
	int nf;
	Suite *s;
	SRunner *sr;

	if (argc == 2)
		port = argv[1];

	s = membuffer_suite();
	sr = srunner_create(s);

	srunner_run_all(sr, CK_NORMAL);
	nf = srunner_ntests_failed(sr);
	srunner_free(sr);

	return (nf == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
