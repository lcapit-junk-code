/*
 * membuffer: a very simpler version of the LDD3's scull driver.
 *
 * TODO:
 * 	- Multiple device support
 * 	- More userlevel test programs
 * 	- Better proc info
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/proc_fs.h>
#include <linux/capability.h>
#include <asm/semaphore.h>
#include <asm/uaccess.h>
#include "membuffer.h"

#define NUMDEVICES    1
#define MEMBUF_MAJOR  232
static const char DRIVER_NAME[] = "membuffer";

struct membuffer {
	struct cdev cdev;
	struct semaphore mutex;
	size_t size;
	char *data;
};

static dev_t membuffer_devnum;
static struct membuffer membuffer;

static loff_t membuffer_llseek(struct file *filp, loff_t offset, int orig)
{
	loff_t ret;
	struct membuffer *dev = filp->private_data;

	if (down_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	switch (orig) {
	case 0:
		filp->f_pos = offset;
		ret = filp->f_pos;
		break;
	case 1:
		filp->f_pos += offset;
		ret = filp->f_pos;
		break;
	default:
		ret = -EINVAL;
	}

	up(&dev->mutex);
	return ret;
}

static int membuffer_ioctl(struct inode *inop, struct file *filp,
		unsigned int cmd, unsigned long arg)
{
	int err;
	int ret = 0;
	char *p;
	size_t new_size;
	void __user *argp = (void __user *) arg;
	struct membuffer *dev = filp->private_data;

	if (down_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	switch (cmd) {
	case MEMBUFFER_RESET:
		if (!capable(CAP_SYS_ADMIN)) {
			ret = -EPERM;
			break;
		}
		memset(dev->data, '\0', dev->size);
		filp->f_pos = 0;
		break;
	case MEMBUFFER_GET_SIZE:
		err = copy_to_user(argp, &dev->size,
				sizeof(dev->size));
		if (err)
			ret = -EFAULT;
		break;
	case MEMBUFFER_SET_SIZE:
		err = copy_from_user(&new_size, argp, sizeof(new_size));
		if (err) {
			ret = -EFAULT;
			break;
		}

		p = kmalloc(new_size, GFP_KERNEL);
		if (!p) {
			ret = -ENOMEM;
			break;
		}
		memset(p, '\0', new_size);

		kfree(dev->data);
		dev->data = p;
		dev->size = new_size;
		filp->f_pos = 0;
		break;
	default:
		ret = -EINVAL;
	}

	up(&dev->mutex);
	return ret;
}

static ssize_t membuffer_read(struct file *filp, char __user *buf,
		size_t count, loff_t *f_pos)
{
	int ret, err;
	struct membuffer *dev = filp->private_data;

	if (down_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	if (!dev->data) {
		ret = -EFAULT;
		goto out;
	}

	/* end of file */
	if (*f_pos >= dev->size) {
		ret = 0;
		goto out;
	}

	/* available amount to transfer */
	if (count > (dev->size - *f_pos))
		count = dev->size - *f_pos;

	err = copy_to_user(buf, dev->data + *f_pos, count);
	if (err) {
		ret = -EFAULT;
		goto out;
	}

	*f_pos += count;
	ret = count;

out:
	up(&dev->mutex);
	return ret;
}

static ssize_t membuffer_write(struct file *filp, const char __user *buf,
		size_t count, loff_t *f_pos)
{
	int ret, err;
	struct membuffer *dev = filp->private_data;

	if (down_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	if (!dev->data) {
		ret = -EFAULT;
		goto out;
	}

	if ((*f_pos + count) > dev->size) {
		count = dev->size - *f_pos;
		if (!count) {
			ret = -ENOSPC;
			goto out;
		}
	}

	err = copy_from_user(dev->data + *f_pos, buf, count);
	if (err) {
		ret = -EFAULT;
		goto out;
	}

	*f_pos += count;
	ret = count;

out:
	up(&dev->mutex);
	return ret;
}

static int membuffer_open(struct inode *inodep, struct file *filp)
{
	struct membuffer *dev;

	dev = container_of(inodep->i_cdev, struct membuffer, cdev);
	filp->private_data = dev;

	return 0;
}

static int membuffer_release(struct inode *inodep, struct file *filp)
{
	return 0;
}

struct file_operations membuffer_driver_fops = {
	.owner   = THIS_MODULE,
	.llseek  = membuffer_llseek,
	.ioctl   = membuffer_ioctl,
	.read    = membuffer_read,
	.write   = membuffer_write,
	.open    = membuffer_open,
	.release = membuffer_release,
};

static int membuffer_read_proc(char *buf, char **start, off_t offset, int count,
			int *eof, void *data)
{
	int len = 0;

	len = sprintf(buf, "membuffer info\nbuffer size: %d bytes\n",
			membuffer.size);
	*eof = 1;
	return len;
}

static int __init membuffer_driver_init(void)
{
	int err;

	/*
	 * Initializes membuffer data
	 */

	membuffer.size = MEMBUFFER_DEFAULT_SIZE;
	membuffer.data = kmalloc(membuffer.size, GFP_KERNEL);
	if (!membuffer.data)
		return -ENOMEM;
	memset(membuffer.data, 0, membuffer.size);

	init_MUTEX(&membuffer.mutex);

	/*
	 * Creates the /proc entry
	 */
	create_proc_read_entry(DRIVER_NAME, 0 /* default mode */,
			NULL /* parent dir */, membuffer_read_proc,
			NULL /* client data */);

	/*
	 * Register the char number
	 */

	membuffer_devnum = MKDEV(MEMBUF_MAJOR, 0);
	err = register_chrdev_region(membuffer_devnum, NUMDEVICES, DRIVER_NAME);
	if (err)
		goto out_free;

	/*
	 * Register the device
	 */

	cdev_init(&membuffer.cdev, &membuffer_driver_fops);
	membuffer.cdev.owner = THIS_MODULE;

	err = cdev_add(&membuffer.cdev, membuffer_devnum, NUMDEVICES);
	if (err)
		goto out_cdev_add;

	return 0;

 out_cdev_add:
	cdev_del(&membuffer.cdev);
	unregister_chrdev_region(membuffer_devnum, NUMDEVICES);
 out_free:
	remove_proc_entry(DRIVER_NAME, NULL /* parent dir */);
	kfree(membuffer.data);
	return err;
}

static void __exit membuffer_driver_exit(void)
{
	cdev_del(&membuffer.cdev);
	remove_proc_entry(DRIVER_NAME, NULL /* parent dir */);
	unregister_chrdev_region(membuffer_devnum, NUMDEVICES);
	kfree(membuffer.data);
}

module_init(membuffer_driver_init);
module_exit(membuffer_driver_exit);

MODULE_LICENSE("GPL");
