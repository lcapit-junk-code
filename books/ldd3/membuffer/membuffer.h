/*
 * membuffer header file
 */
#ifndef _MEMBUFFER_H
#define _MEMBUFFER_H

#include <linux/ioctl.h>

/* Membuffer default size (in bytes) */
#define MEMBUFFER_DEFAULT_SIZE 1024*50

/* The '|' magic number *seems* to not be in use, try it */
#define MEMBUFFER_IOC_MAGIC '|'

/* ioctl() commands */
#define MEMBUFFER_RESET       _IO(MEMBUFFER_IOC_MAGIC, 0)
#define MEMBUFFER_GET_SIZE    _IOW(MEMBUFFER_IOC_MAGIC, 1, size_t)
#define MEMBUFFER_SET_SIZE    _IOR(MEMBUFFER_IOC_MAGIC, 2, size_t)

#endif /* _MEMBUFFER_H */
