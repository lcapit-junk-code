/*
 * mshort.c
 *
 * A minimalistic version of LDD3's 'short'
 *
 * This module enables user programs to communicate with the
 * PC parallel port with write()s and read()s.
 */

#include <linux/config.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/errno.h>
#include <linux/ioport.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <asm/uaccess.h>
#include <asm/system.h>
#include <asm/io.h>

#define MSHORT_NR_PORTS 8
#define MSHORT_NR_DEVS  1
#define DRIVER_NAME     "mshort_irq"

static unsigned short mshort_base = 0x378;
module_param(mshort_base, short, 0);

static int mshort_debug;
module_param(mshort_debug, int, 0);

static int mshort_probe_irq;
module_param(mshort_probe_irq, int, 0);


static unsigned long mshort_irq = 7;

static int mshort_devnum;

static struct cdev *mshort_cdev;

static struct tasklet_struct mshort_tlet;

static ssize_t mshort_read(struct file *filp, char __user *buf, size_t count,
			   loff_t *f_pos)
{
	int retval;
	unsigned char *kbuf, *p;

	kbuf = kmalloc(count, GFP_KERNEL);
	if (!kbuf)
		return -ENOMEM;

	p = kbuf;
	memset(kbuf, 0, count);
	retval = count;
	while (count--) {
		*p = inb(mshort_base);
		rmb();
		if (mshort_debug)
			printk(KERN_DEBUG "read: 0x%x\n", *p);
		++p;
	}

	if (copy_to_user(buf, kbuf, retval))
		retval = -EFAULT;

	kfree(kbuf);
	return retval;
}

static ssize_t mshort_write(struct file *filp, const char __user *buf,
			    size_t count, loff_t *f_pos)
{
	int retval;
	unsigned char *kbuf, *p;

	kbuf = kmalloc(count, GFP_KERNEL);
	if (!kbuf)
		return -ENOMEM;

	memset(kbuf, 0, count);
	if (copy_from_user(kbuf, buf, count)) {
		kfree(kbuf);
		return -EFAULT;
	}

	p = kbuf;
	retval = count;

	while (count--) {
		if (mshort_debug)
			printk(KERN_DEBUG "writing: 0x%x\n", *p);
		outb(*(p++), mshort_base);
		wmb();
	}

	kfree(kbuf);
	return retval;
}

static int mshort_open(struct inode *inodep, struct file *filp)
{
	return nonseekable_open(inodep, filp);
}

static int mshort_release(struct inode *inodep, struct file *filp)
{
	return 0;
}

struct file_operations mshort_fops = {
	.owner   = THIS_MODULE,
	.llseek  = no_llseek,
	.read    = mshort_read,
	.write   = mshort_write,
	.open    = mshort_open,
	.release = mshort_release,
};

static void mshort_tlet_fn(unsigned long unused)
{
	printk(KERN_INFO "%s() running!\n", __FUNCTION__);
}

static irqreturn_t mshort_handler(int foo, void *bar, struct pt_regs *regs)
{
	printk(KERN_INFO "%s() running!\n", __FUNCTION__);
	tasklet_schedule(&mshort_tlet);
	return IRQ_HANDLED;
}

static int __init probe_irq(void)
{
	int count = 0;
	unsigned long mask;

	do {
		mask = probe_irq_on();
		outb_p(0x10, mshort_base+2); /* enable reporting */
		outb_p(0x00, mshort_base); /* clear the bit */
		outb_p(0xFF, mshort_base); /* generates interrupt */
		outb_p(0x00, mshort_base+2); /* disable reporting */
		udelay(5);
		mshort_irq = probe_irq_off(mask);

		if (!mshort_irq) {
			printk(KERN_INFO "mshort: no IRQ reported by probe\n");
			mshort_irq = -1;
		}
	} while (mshort_irq < 0 && count++ < 5);

	if (mshort_irq < 0) {
		printk("mshort: IRQ probe failed\n");
		return -1;
	}

	return 0;
}

static int __init mshort_init(void)
{
	int err;

	mshort_devnum = MKDEV(232, 0);

	/* Allocates the I/O port */
	if (!request_region(mshort_base, MSHORT_NR_PORTS, DRIVER_NAME)) {
		printk(KERN_INFO "mshort can't get I/O port address: 0x%dx\n",
		       mshort_base);
		return -ENODEV;
	}

	/* Probe IRQ if needed */
	if (mshort_probe_irq) {
		err = probe_irq();
		if (err) {
			err = -ENODEV;
			goto out_request_irq;
		}
	}

	/* Register the tasklet */
	tasklet_init(&mshort_tlet, mshort_tlet_fn, 0);

	/* Interrupt handling */
	err = request_irq(mshort_irq, mshort_handler, SA_INTERRUPT,
			  DRIVER_NAME, NULL);
	if (err) {
		printk(KERN_INFO "mshort can't request irq num: %lu\n",
		       mshort_irq);
		goto out_request_irq;
	}
	outb(0x10, mshort_base+2);

	/* Register the major number */
	err = register_chrdev_region(mshort_devnum, MSHORT_NR_DEVS,
				     DRIVER_NAME);
	if (err) {
		err = -EBUSY;
		goto out_register_chrdev;
	}

	/* Register our operations */
	mshort_cdev = cdev_alloc();
	if (!mshort_cdev) {
		err = -ENOMEM;
		goto out_cdev_alloc;
	}

	mshort_cdev->owner = THIS_MODULE;
	mshort_cdev->ops   = &mshort_fops;

	err = cdev_add(mshort_cdev, mshort_devnum, MSHORT_NR_DEVS);
	if (err) {
		err = -EBUSY;
		goto out_cdev_add;
	}

	return 0;

 out_cdev_add:
	cdev_del(mshort_cdev);
 out_cdev_alloc:
	unregister_chrdev_region(mshort_devnum, MSHORT_NR_DEVS);
 out_register_chrdev:
	free_irq(mshort_irq, NULL);
 out_request_irq:
	release_region(mshort_base, MSHORT_NR_PORTS);
	return err;
}

static void __exit mshort_exit(void)
{
	cdev_del(mshort_cdev);
	unregister_chrdev_region(mshort_devnum, MSHORT_NR_DEVS);
	outb(0x0, mshort_base+2);
	free_irq(mshort_irq, NULL);
	release_region(mshort_base, MSHORT_NR_PORTS);
}

module_init(mshort_init);
module_exit(mshort_exit);

MODULE_LICENSE("GPL");
