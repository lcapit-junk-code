/*
 * ledstate.c
 *
 * This program tests the mshort module by writing or reading 8-bit
 * values to or from the parallel port. For example:
 *
 * $ ./ledstate -s 17
 *
 * Will set the parallel port to: 00010001, and
 *
 * $ ./ledstate -g
 *
 * Will print the current led state
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#define _GNU_SOURCE
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>

static void usage(void)
{
	printf(
		"ledstate [ options ]\n"
		"Get/Set LED state connected to the parallel port "
		"(mshort lkm)\n"
		"  -h,  --help          This message\n"
		"  -s,  --set=<value>  Sets the led state to "
		"'value' (0-255)\n"
		"  -g,  --get           Gets the current LED state\n"
		);
}

int main(int argc, char *argv[])
{
	int ret = 1;
	ssize_t nbytes;
	unsigned long line;
	int opt_idx, fd, c;
	const int base = 10;
	unsigned char data = 0;
	struct option long_options[] = {
		{ "help", 0, 0, 0},
		{ "get", 0, 0, 0},
		{ "set", 1, 0, 0},
		{0, 0, 0, 0}
	};

	fd = open("/dev/mshort0", O_RDWR);
	if (fd < 0) {
		perror("open()");
		exit(1);
	}

	/* get command-line options */
	c = getopt_long(argc, argv, "hgs:", long_options, &opt_idx);
	switch (c) {
	case 's':
		line = strtol(optarg, (void *)NULL, base);
		if (line < 0 || line > 255) {
			fprintf(stderr, "%lu bad value\n", line);
			usage();
			goto out;
		}

		data = (unsigned char) line;
		nbytes = write(fd, &data, sizeof(data));
		if (nbytes != sizeof(data)) {
			perror("write()");
			goto out;
		}
		break;
	case 'g':
		nbytes = read(fd, &data, sizeof(data));
		if (nbytes != sizeof(data)) {
			perror("read()");
			goto out;
		}

		printf("0x%x\n", data);
		break;
	case 'h':
		/* FALLTROUGH */
	default:
		usage();
		goto out;
		// break;
	}

	ret = 0;

 out:
	close(fd);
	exit(ret);
}
