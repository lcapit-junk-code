/*
 * setledsf.c
 *
 * Copy a file to other with 8-bit stepping and waiting 1
 * second between each write(). Example:
 *
 * $ ./setledsf < /etc/passwd > /dev/mshort0
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void)
{
	int ret = 1;
	ssize_t bread = 1;
	ssize_t bwrite;

	while (bread) {
		unsigned char c = 0;

		bread = read(STDIN_FILENO, &c, sizeof(c));
		if (bread < 0) {
			perror("read()");
			goto out;
		}

		bwrite = write(STDOUT_FILENO, &c, sizeof(c));
		if (bwrite < 0) {
			perror("write()");
			goto out;
		}

		usleep(100000);
	}

	ret = 0;

 out:
	exit(ret);
};
