#!/bin/bash
#
# Example of bash select usage.
#
# Luiz Fernando N. Capitulino
# <lcapitulino@gmail.com>

select name in again exitnow; do
	if [ "$name" = "exitnow" ]; then
		exit 0
	fi
	echo $name
done
