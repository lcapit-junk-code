#!/bin/bash
#
# Arrays in bash.
#
# Luiz Fernando N. Capitulino
# <lcapitulino@gmail.com>

###
### First way
###
### name=(value1 value2 valueN)

array1=(a b c d e f g h)

for ((i = 0; i < 8; i++)); do
	echo ${array1[i]}
done

echo

###
### Second way
###
### name[subscript]=value

for ((i = 0; i < 5; i++)); do
	array2[i]=$i
done

for ((i = 0; i < 5; i++)); do
	echo ${array2[i]}
done
