// C++ Hello world
// 
// Compile with:
// 
// $ g++ -o hell hello.cpp

#include <iostream>

int main(void)
{
	std::cout << "Hello, world!\n";
}
