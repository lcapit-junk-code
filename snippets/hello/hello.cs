/* Hello, World in C#
 *
 * To run on Linux using Mono do:
 * 
 * $ mcs hello.cs
 * $ mono hello.exe
 */
using System;

namespace Program
{
	public class HelloWorld
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Hello, World!");
		}
	}
}
