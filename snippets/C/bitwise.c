/*
 * An example of bitwise operations in C
 * 
 * Luiz Fernando N. Capitulino <lcapitulino@mandriva.com.br>
 */
#include <stdio.h>

static void show(unsigned char var)
{
	printf("-> %#x\n", var);
}

static void set_bit(unsigned char *var, unsigned char bits)
{
	*var |= bits;
}

static void unset_bit(unsigned char *var, unsigned char bits)
{
	*var &= ~bits;
}

static void left_shift(unsigned char *var, int positions)
{
	*var <<= positions;
}

static void right_shift(unsigned char *var, int positions)
{
	*var >>= positions;
}

int main(void)
{
	unsigned char var = 0;

	show(var);

	set_bit(&var, 0x1);
	show(var);

	set_bit(&var, 0x8);
	show(var);

	set_bit(&var, 0x80);
	show(var);

	unset_bit(&var, 0x8);
	show(var);

	left_shift(&var, 2);
	show(var);

	right_shift(&var, 2);
	show(var);

	return 0;
}
