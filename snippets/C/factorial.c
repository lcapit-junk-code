/*
 * factorial: Calculate's a number factorial
 */
#include <stdio.h>
#include <stdlib.h>

long double factorial(long n)
{
	int i;
	long double m;

	for (m = 1, i = n; i > 1; i -= 1)
		m *= i;

	return m;
}

int main(int argc, char *argv[])
{
	long n;
	long double f;

	if (argc != 2) {
		printf("Usage: %s < number >\n", argv[0]);
		exit(1);
	}

	n = strtol(argv[1], NULL, 10);
	/* FIXME: Error checking */

	f = factorial(n);
	if (!f) {
		/* FIXME: Explain why */
		fprintf(stderr, "Could not caculate the factorial\n");
		exit(1);
	}

	printf("%ld! = %Le\n", n, f);

	return 0;
}
