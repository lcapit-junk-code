/*
 * Memory allocation with mmap()
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/mman.h>

#define DEFAULT_SIZE 10 /* will be 10k */

static void print(const char *msg, ...)
{
	va_list params;

	fputs("-> ", stderr);

	va_start(params, msg);
	vfprintf(stderr, msg, params);
	va_end(params);

	getchar();
}

int main(int argc, char *argv[])
{
	char *p;
	size_t bytes;

	bytes = DEFAULT_SIZE;
	if (argc == 2)
		bytes = (size_t) strtol(argv[1], NULL, 10);

	bytes *= 1024;

	print("Will allocate %d kBytes", bytes / 1024);

	p = mmap(NULL, bytes, PROT_READ | PROT_WRITE,
		 MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if (p == MAP_FAILED) {
		perror("mmap()");
		exit(1);
	}

	memset(p, 0, bytes);

	print("Allocated %d kBytes", bytes / 1024);

	munmap(p, bytes);

	print("Memory is now freed");

	return 0;
}
