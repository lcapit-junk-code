#include <stdio.h>

static void show_size(const char *msg, int size)
{
	printf("%-15s: %d bytes (%2d bits)\n", msg, size, size * 8);
}

int main(void)
{
	show_size("char", sizeof(char));
	show_size("int", sizeof(int));
	show_size("short int", sizeof(short int));
	show_size("long int", sizeof(long int));
	show_size("long long", sizeof(long long));
	show_size("unsigned int", sizeof(unsigned int));
	show_size("signed int", sizeof(signed int));
	show_size("unsigned char", sizeof(unsigned char));
	show_size("signed char", sizeof(signed char));
	show_size("unsigned long *", sizeof(unsigned long *));

	return 0;
}
