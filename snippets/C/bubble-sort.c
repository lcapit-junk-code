/*
 * bubble-sort: there's no reason to this for anything.
 * 
 * Luiz Fernando N. Capitulino <lcapitulino@mandriva.com.br>
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int compare(const void *key1, const void *key2)
{
	if (*((const long *) key1) > *((const long *) key2))
		return 1;
	else if (*((const long *) key1) < *((const long *) key2))
		return -1;
	else
		return 0;
}

int bubble_sort(void *data, int size, int esize,
		int (*compare) (const void *key1, const void *key2))
{
	void *tmp;
	char *a = data;

	tmp = (char *) malloc(esize);
	if (!tmp)
		return -1;

	for (;;) {
		int i, has_swapped = 0;

		for (i = 0; i < size-1; i++) {
			if (compare(&a[i * esize], &a[(i + 1) * esize]) > 0) {
				memcpy(tmp, &a[i * esize], esize);
				memcpy(&a[i * esize], &a[(i + 1) * esize],
				       esize);
				memcpy(&a[(i + 1) * esize], tmp, esize);
				has_swapped = 1;
			}
		}

		if (!has_swapped)
			break;
	}

	free(tmp);
	return 0;
}

int main(int argc, char *argv[])
{
	int ret;
	long *nums;
	size_t len;
	unsigned int i;

	if (argc == 1) {
		printf("Usage: bubble-sort <num1> <num2> ... <numN>\n");
		exit(1);
	}

	len = argc - 1;
	nums = calloc(len, sizeof(long));
	if (!nums) {
		perror("calloc()");
		exit(1);
	}

	for (i = 0; i < len; i++) {
		nums[i] = strtol(argv[i + 1], (char **)NULL, 10);
		printf("%ld ", nums[i]);
	}
	printf("\n");

	ret = bubble_sort((void *) nums, len, sizeof(long), compare);
	if (ret) {
		perror("bubble_sort()");
		exit(1);
	}

	for (i = 0; i < len; i++)
		printf("%ld ", nums[i]);
	printf("\n");

	free(nums);
	return 0;
}
