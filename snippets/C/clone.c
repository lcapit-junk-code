/*
 * Linux's clone() system call usage example
 */
#include <stdio.h>
#include <stdlib.h>
#include <sched.h>
#include <sys/wait.h>
#include <sys/types.h>

#define UNUSED(x) (x = x)
#define STACK_SIZE 16

int child(void *arg)
{
	UNUSED(arg);

	printf("[child] Hello world!\n");
	return 0;
}

int main(void)
{
	int tid;
	char stack[STACK_SIZE];

	tid = clone(child, (void *) stack, 0, NULL);
	if (tid < 0) {
		perror("clone()");
		exit(1);
	}

	wait(NULL);

	printf("[parent]: exiting\n");
	return 0;
}
