#include <stdio.h>

int main(void)
{
	char buf[] = "capitulino";

	/* %.*s means 'size' specified in the next argument */
	printf("%.*s\n", 3, buf);
	return 0;
}
