/*
 * This program gets its source from stdin when being
 * compiled, for instance:
 * 
 * $ cc -o stdin stdin.c
 * #include <stdio.h>
 * int main(void)
 * {
 * 	printf("Hello, world!\n");
 * 	return 0;
 * }
 * ^D
 * $ ./stdin
 * Hello, world!
 */
#include "/dev/stdin"
