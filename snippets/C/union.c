#include <stdio.h>

union teste {
	char ch;
	int i;
};

struct teste2 {
	char ch;
	int i;
};

int main(void)
{
	union teste teste;
	struct teste2 teste2;

	teste.ch = 'c';
	printf("%c\n", teste.ch);

	teste.i = 666;
	printf("[nada]%c\n", teste.ch);
	printf("%d\n", teste.i);

	printf("\n--> union: %d struct: %d\n", sizeof(teste), sizeof(teste2));

	return 0;
}
