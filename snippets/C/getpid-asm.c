/*
 * Calls getpid() by hand (linux-only probably)
 *
 * 1- Store the system call number in %eax
 * 2- Executes int 0x80
 * 3- Store the return code in 'ret'
 * 
 * Luiz Fernando N. Capitulino <lcapitulino@mandriva.com.br>
 */

#include <stdio.h>
#include <unistd.h>

int main(void)
{

	int ret, num;

	num = 20; /* gerpid()'s number */

	asm("movl %1, %%eax\n\t"
	    "int $0x80\n\t"
	    "movl %%eax, %0"
	    : "=r" (ret)
	    : "r" (num)
	    : "%eax");

	printf("PID: %d\n", ret);
	return 0;
}
