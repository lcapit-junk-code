/*
 * select() example, you can test by doing:
 * 
 * $ mkfifo my-fifo
 * $ cc -o select select.c
 * $ ./select my-fifo
 * 
 * And, in other terminal:
 * 
 * $ echo 'Hello, world!' > my-fifo
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>

int main(int argc, char *argv[])
{
	char c;
	int err, fd;
	fd_set rset;
	ssize_t bytes;

	if (argc != 2) {
		printf("usage: select < fifo-path >");
		exit(1);
	}

	fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		perror("open()");
		exit(1);
	}

	/*
	 * select() receives a set fo file-descriptors to
	 * monitor. The 'fd_set' data type should be used
	 * to define the set variable.
	 * 
	 * Before calling select() you have to:
	 * 
	 * 1. Initialize each set with FD_ZERO()
	 * 2. Add file-descriptors to the set
	 */
	FD_ZERO(&rset);
	FD_SET(fd, &rset);

	/*
	 * The first parameter is always confusing to
	 * most people...
	 * 
	 * The rule is simple: it's the highest-numbered
	 * file descriptor in any of the sets, plus 1.
	 * 
	 * In this example we only have one file-descriptor,
	 * so all we have to do is to add 1 to it. But if you
	 * have, say, 30 file-descriptors, you'll have to
	 * find the highest one and add 1 to it.
	 */
	err = select(fd + 1, &rset, NULL, NULL, NULL);
	if (err < 0) {
		perror("select()");
		exit(1);
	}

	if (FD_ISSET(fd, &rset)) {
		for (;;) {
			bytes = read(fd, &c, 1);
			if (bytes == 0)
				break;
			if (bytes < 0) {
				perror("read()");
				exit(1);
			}
			if (bytes != 1) {
				fprintf(stderr,
					"Something strange has happend\n");
				exit(1);
			}
			printf("%c", c);
		}
	} else {
		fprintf(stderr, "select() returned but fd is not set\n");
		exit(1);
	}

	return 0;
}
