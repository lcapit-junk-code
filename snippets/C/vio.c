/* vio.c: Sort an array using an 'indirect sorting array'.
 * 
 * I don't know whether the right translation is really
 * 'indirect sorting array', but the portuguese name for
 * this is 'vetor indireto de ordenacao'.
 * 
 * The basic idea is: instead of moving array's elements
 * from a position to another, we use another array only
 * to store the indexes and move only the indexes.
 * 
 * This can save time if you're sorting an array of big
 * structures, because you'll only move integers instead
 * of the structs itself.
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* sort_vio(): sort an array of length 'len' pointed by 'p'
 * using an 'indirect sorting array' (vio, in portuguese).
 * Uses insertion sort. */
void sort_vio(int *p, int *vio, int len)
{
	int idx, i, j;

	for (i = 0; i < len; i++)
		vio[i] = i;

	for (i = 1; i < len; i++) {
		idx = i;

		for (j = i - 1; j >= 0 && p[vio[j]] > p[idx]; j--)
			vio[j + 1] = vio[j];

		vio[j + 1] = idx;
	}
}

/* show_array_vio(): print an array of length 'len'
 * pointed by 'p' using an 'indirect sorting array'
 * (vio, in portuguese). */
void show_array_vio(int *p, int *vio, int len)
{
	int i;

	for (i = 0; i < len; i++)
		printf("%d ", p[vio[i]]);
	printf("\n");
}

/* show_array(): print an array of length 'len'
 * pointed by 'p'. */
void show_array(int *p, int len)
{
	int i;

	for (i = 0; i < len; i++)
		printf("%d ", p[i]);
	printf("\n");
}

#define SIZE 10

int main(void)
{
	int array[SIZE], vio[SIZE], i;

	srand(time(NULL));

	for (i = 0; i < SIZE; i++)
		array[i] = rand() % SIZE;

	show_array(array, SIZE);
	sort_vio(array, vio, SIZE);
	show_array_vio(array, vio, SIZE);

	return 0;
}
