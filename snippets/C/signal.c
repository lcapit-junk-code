/*
 * Signal usage example
 * 
 * Luiz Fernando N. Capitulino <lcapitulino@mandriva.com.br>
 */

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>

volatile sig_atomic_t quitflag; /* set by the handler */

static void sig_int(int signo)
{
	if (signo == SIGINT)
		printf("interrupt\n");
	else if (signo == SIGQUIT)
		quitflag = 1;
}

int main(void)
{
	sigset_t newmask, oldmask, zeromask;

	if (signal(SIGINT, sig_int) == SIG_ERR) {
		perror("signal(SIGINT)");
		exit(1);
	}

	if (signal(SIGQUIT, sig_int) == SIG_ERR) {
		perror("signal(SIGQUIT)");
		exit(1);
	}

	sigemptyset(&zeromask);
	sigemptyset(&newmask);
	sigaddset(&newmask, SIGQUIT);

	/*
	 * Block SIGQUIT and save current signal mask
	 */
	if (sigprocmask(SIG_BLOCK, &newmask, &oldmask) < 0) {
		perror("sigprocmask()");
		exit(1);
	}

	while (!quitflag)
		sigsuspend(&zeromask);

	/*
	 * SIGQUIT has been caught and is now blocked; do whatever
	 */
	printf("SIGQUIT caught!\n");

	/*
	 * Reset signal mask which unblocks SIGQUIT
	 */
	if (sigprocmask(SIG_SETMASK, &oldmask, NULL) < 0) {
		perror("sigprocmask()");
		exit(1);
	}

	return 0;
}

