/*
 * Demonstrates Goldbach's conjecture (uses brute-force), more info at:
 * 
 * http://en.wikipedia.org/wiki/Goldbach%27s_conjecture
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <stdlib.h>

#define SIZE 10

int is_prime(int num)
{
	int i;

	if (num < 2)
		return 0;

	for (i = 2; i < num; i++)
		if (!(num % i))
			return 0;
	return 1;
}

int find_prime_sum(int num)
{
	int i, j;

	for (i = 2; i < num; i++)
		if (is_prime(i)) {
			if ((i + i) == num) {
				printf("%d + %d = %d\n", i, i, num);
				return 0;
			}
			for (j = i + 1; j + i <= num; j++)
				if (is_prime(j))
					if ((j + i) == num) {
						printf("%d + %d = %d\n",
						       j, i, num);
						return 0;
					}
		}
	return 1;
}

void golbach(const int *vet, int len)
{
	int i, ret;

	for (i = 0; i < len; i++) {
		int num = vet[i];

		if (!(num % 2) && num > 2) {
			ret = find_prime_sum(num);
			if (ret) {
				fprintf(stderr, "failed for %d\n", num);
				exit(1);
			}
		}
	}
}

int main(void)
{
	int vet[SIZE], i;

	for (i = 0; i < SIZE; i++) {
		printf("next number [%d]: ", i + 1);
		scanf("%d", &vet[i]);
	}

	golbach(vet, SIZE);
	return 0;
}
