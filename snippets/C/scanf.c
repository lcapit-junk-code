#include <stdio.h>

int main(void)
{
	char line[32];

	/* Without this trick, scanf() will stop reading
	 * at the first space character */
	scanf("%[^\n]s", line);
	printf("-> %s\n", line);
	return 0;
}
