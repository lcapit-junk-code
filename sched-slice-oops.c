/* This program used to crash 2.6.24-rc kernels.
 * 
 * See commit 77034937dc4575ca0a76bf209838ecd39e804089 in Linus' tree.
 * 
 * Luiz Fernando N. Capitulino <lcapitulino@mandriva.com.br>
 */
#include <stdio.h>
#include <sched.h>

int main(void)
{
	sched_rr_get_interval(1, NULL);
	return 0;
}
