using System;

namespace ObjArray
{
	class Person
	{
		private int age = 10;
		private string name = "Bukowski";

		public bool set_age(int val)
		{
			if (val <= 0)
				return false;
			age = val;
			return true;
		}

		public int get_age()
		{
			return age;
		}

		public void set_name(string new_name)
		{
			name = new_name;
		}

		public string get_name()
		{
			return name;
		}
	}

	class Program
	{
		static void Main(string[] args)
		{
			Person[] students;
			const int nr_students = 3;

			students = new Person[nr_students];

			for (int i = 0; i < students.Length; i++) {
				students[i] = new Person();

				Console.Write("\nName: ");
				students[i].set_name(Console.ReadLine());

				Console.Write("Age: ");
				int aux = Convert.ToInt32(Console.ReadLine());
				students[i].set_age(aux);
			}

			for (int i = 0; i < students.Length; i++) {
				Console.WriteLine("\nName: {0}",
						students[i].get_name());
				Console.WriteLine("Age: {0}",
						students[i].get_age());
			}
		}
	}
}
