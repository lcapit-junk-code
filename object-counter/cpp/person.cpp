#include "person.h"

int Person::count = 0;

bool Person::set_idade(int value)
{
	if (value < 1 || value > 100)
		return false;

	idade = value;
	return true;
}

int Person::get_idade(void)
{
	return idade;
}

int Person::get_count(void)
{
	return count;
}

Person::Person(void)
{
	idade = 0;
	count++;
}

Person::~Person(void)
{
	count--;
}
