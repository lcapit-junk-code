#include <iostream>
#include "person.h"

using namespace std;

void show_obj_count(void)
{
	cout << "-> Object count ";
	cout << Person::get_count();
	cout << "\n";
}

int main(void)
{
	const int max = 12;
	Person *p[max];

	show_obj_count();

	for (int i = 0; i < max; i++)
		p[i] = new Person();

	show_obj_count();

	for (int i = 0; i < max; i++)
		delete p[i];

	show_obj_count();

	return 0;
}
