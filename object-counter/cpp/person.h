#ifndef PERSON_H
#define PERSON_H

class Person
{
	private:
		int idade;
		static int count;

	public:
		bool set_idade(int value);
		int get_idade(void);
		static int get_count(void);
		Person(void);
		~Person(void);
};

#endif /* PERSON_H */
