using System;

namespace ObjCounter
{
	class Person
	{
		public string name = "void";
		private int age = 0;
		private static int count = 0;

		public bool set_age(int val)
		{
			if (val < 1 || val > 100)
				return false;
			age = val;
			return true;
		}

		public int get_age()
		{
			return age;
		}

		public static int get_count()
		{
			return count;
		}

		public Person()
		{
			count++;
		}

		~Person()
		{
			count--;
		}
	}

	class Program
	{
		static void Main(string []args)
		{
			int aux;
			Person p1, p2, p3;

			p1 = new Person();
			p2 = new Person();
			p3 = new Person();

			do {
				System.Console.Write("Age: ");
				aux = System.Convert.ToInt32(System.Console.ReadLine());
			} while (p1.set_age(aux) == false);

			System.Console.WriteLine("\nAge: {0}", p1.get_age());
			System.Console.WriteLine("count: {0}", Person.get_count());
		}
	}
}
