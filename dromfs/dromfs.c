#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <arpa/inet.h>

struct romfs_sb {
	char name[9];
	uint32_t size;
	uint32_t checksum;
	char vname[17];
};

static void usage(void)
{
	printf("Usage: rromfs < device >\n");
}

static struct romfs_sb *get_romfs_sb(const char *path)
{
	int fd;
	uint8_t *p;
	struct romfs_sb *sb;
	const size_t sb_length = 16;

	fd = open(path, O_RDONLY);
	if (fd < 0) {
		perror("open()");
		exit(1);
	}
	
	sb = malloc(sizeof(struct romfs_sb));
	if (!sb) {
		perror("malloc()");
		goto err_malloc;
	}
	memset(sb, 0, sizeof(struct romfs_sb));
	
	p = mmap(NULL, sb_length, PROT_READ, MAP_PRIVATE, fd, (off_t) 0);
	if (!p) {
		perror("mmap()");
		goto err_mmap;
	}

	/* File-system name */
	memcpy(&sb->name, p, (size_t) 8);

	/* Volume name */
	memcpy(&sb->vname, (char *) p+16, (size_t) 16);

	/*
	 * FIXME: Is there a nicer way to read big
	 * endian values?
	 */

	/* size */
	memcpy(&sb->size, (uint8_t *) p+8, (size_t) 4);
	sb->size = ntohl(sb->size);

	/* checksum */
	memcpy(&sb->checksum, (uint8_t *) p+12, (size_t) 4);
	sb->checksum = ntohl(sb->checksum);

	munmap(p, sb_length);
	close(fd);
	return sb;

err_mmap:
	free(sb);
err_malloc:
	close(fd);
	exit(1);
}

int main(int argc, char *argv[])
{
	struct romfs_sb *sb;
	
	if (argc != 2) {
		usage();
		exit(1);
	}

	sb = get_romfs_sb(argv[1]);
	if (!sb)
		exit(1);

	/* Super-block information */
	printf("sig: %s\n", sb->name);
	printf("size: %d\n", sb->size);
	printf("checksum: %d\n", sb->checksum);
	printf("Volume name: %s\n", sb->vname);

	free(sb);
	return 0;
}
