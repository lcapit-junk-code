##
## Common used functions library
##

# die
#
# $1: Exit code
# $2: Error message
#
# Returns: nothing
#
# Print the error message in standard error output
# and exit
#
die()
{
	exitcode=$1
	str="$2"

	echo -e "Error: $str" >> /dev/stderr
	exit $exitcode
}


# die_on_error
#
# $1: Return code
# $2: Error message
#
# Returns: nothing
#
# Calls die() if return code is not zero
#
die_on_error()
{
	retcode=$1
	errmesg="$2"

	if [ $retcode -ne 0 ]; then
		die 1 "$errmesg"
	fi
}

# send_email_report
#
# $1: Subject
# $2: E-mail contents
#
# Returns: sendmail's exit code
#
# Send an e-mail to the address specified in the TO_ADDR variable
# from the address specified in the FROM_ADDR
#
# Note that sendmail must be proper installed
#
send_email_report()
{
	to=$TO_ADDR
	from=$FROM_ADDR
	subject=$1
	contents=$2
	tmpfile=/tmp/email.XXXXXXXXXX
	emailfile=$(mktemp $tmpfile)

	die_on_error $? "Could not create temporary e-mail file"

	echo -e "To: $to\nFrom: $from" > $emailfile
	echo -e "Subject: $subject" >> $emailfile
	echo -e "Content-type: text/plain\n\n" >> $emailfile
	echo -e "$contents\n." >> $emailfile

	cat $emailfile | $SENDMAIL -t
	ret=$?

	rm -f $emailfile

	return $ret
}
