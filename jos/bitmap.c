/*
 * bitmap: An example of bitmap handling
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>

#define LINES 3
#define COLS  (sizeof(unsigned char) * 8)

static unsigned char bitmap[LINES];

/* bitmap_bit_is_set(): Check if bit 'num' is set.
 * Return 0 if bit is set, 0 otherwise */
static int bitmap_bit_is_set(int num)
{
	return (bitmap[num / COLS] & (1 << (num % COLS)));
}

/* bitmap_set_bit(): Set bit 'num' */
static void bitmap_set_bit(int num)
{
	bitmap[num / COLS] |= 1 << (num % COLS);
}

/* bitmap_unset_bit(): Unset bit 'num' */
static void bitmap_unset_bit(int num)
{
	bitmap[num / COLS] &= ~(1 << (num % COLS));
}

/* bitmap_print(): Print bitmap on stdout
 * 
 * Note that the map is printed on the reverse order.
 * For example, each word in the map is stored in
 * memory this way:
 * 
 *         7 6 5 4 3 2 1 0
 * 
 *         0 0 0 0 0 0 0 0
 * 
 * Where bit number 0 is the LSB one and bit number 7
 * is the MSB one.
 * 
 * But, when printing, we start to print from bit number
 * 0 (the LSB) which gives the following screen for all
 * the bits:
 * 
 *        0 1 2 3 4 5 6 7
 * 
 *        0 0 0 0 0 0 0 0
 *        0 0 0 0 0 0 0 0
 *        0 0 0 0 0 0 0 0
 * 
 * See? We start printing from bit 0 to bit 7.
 */
static void bitmap_print(void)
{
	unsigned int count, j, i;

	for (i = 0; i < COLS; i++)
		printf("%d ", i);
	printf("\n\n");

	count = j = 0;
	for (i = 0; i < (LINES * COLS); i++) {
		printf("%d ", bitmap_bit_is_set(i) ? 1 : 0);
		if (++count == COLS) {
			printf("[%#x]\n", bitmap[j++]);
			count = 0;
		}
	}
}

static void help(void)
{
	printf(
	       "clear               clear bitmap\n"
	       "exit                exit from program\n"
	       "help                this text\n"
	       "print               print bitmap\n"
	       "set <hex value>     set bit correspondent to value\n"
	       "unseset <hex value> unsetset bit correspondent to value\n"
	       );
}

int main(void)
{
	char *cmd;

	for (;;) {
		cmd = readline(">>> ");
		if (!cmd) {
			printf("\n");
			break;
		}

		if (!memcmp(cmd, "clear", 5)) {
			memset(bitmap, 0, LINES * COLS);
		} else if (!memcmp(cmd, "exit", 4)) {
			free(cmd);
			break;
		} else if (!memcmp(cmd, "help", 4)) {
			help();
		} else if (!memcmp(cmd, "print", 5)) {
			bitmap_print();
		} else if (!memcmp(cmd, "set", 3) ||
			   (!memcmp(cmd, "unset", 5))) {
			unsigned int num;
			char *p = strchr(cmd, ' ');
			if (!p) {
				printf("Invalid use of set or unset\n");
			} else {
				num = strtol(p, (char **) NULL, 16);
				if (cmd[0] == 's')
					bitmap_set_bit(num);
				else
					bitmap_unset_bit(num);
			}
		} else {
			printf("Invalid command: %s\n", cmd);
		}

		free(cmd);
	}

	return 0;
}
