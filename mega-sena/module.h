/*
 * This header file has one of the main data structures used by the
 * mega-sena program: the struct module.
 *
 * Each module has to define its own struct module and export it.
 * 
 * Licensed under the GPLv2.
 */
#ifndef __MODULE_H
#define __MODULE_H

struct module {
	const char *mod_name;
	int (*mod_init)(void);
	int (*mod_insert)(char *name);
	int (*mod_lookup)(char *name, unsigned int *h);
	void (*mod_destroy)(void);
	void (*mod_stats)(void);
};

#endif /* __MODULE_H */
