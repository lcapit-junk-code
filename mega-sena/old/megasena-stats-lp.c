#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>

struct ohtbl {
	int positions;
	int size;
	void **table;

	/* debug stuff */
	int gr_collision;
	int nr_collisions;
	int no_collisions;
};

#define HASH_TABLE_SIZE 10240 	       /* 10k */
#define LOTTERY_MAX         6	       /* bytes */
#define NAME_LENGTH        17	       /* bytes */

static sig_atomic_t show_status, reload_table;
static unsigned long nr_runs, nr_found, ulong_max;

static void sig_handler(int signum)
{
	switch (signum) {
	case SIGUSR1:
		show_status = 1;
		break;
	case SIGHUP:
		reload_table = 1;
		break;
	}
}

static inline unsigned int tdb_hash(char *name)
{
	unsigned value;	/* Used to compute the hash value.  */
	unsigned   i;	/* Used to cycle through random values. */

	/* Set the initial value from the key size. */
	for (value = 0x238F13AF * strlen(name), i=0; name[i]; i++)
		value = (value + (((unsigned char *)name)[i] << (i*5 % 24)));

	return (1103515243 * value + 12345);
}

static int ohtbl_init(struct ohtbl *p, int positions)
{
	int i;

	p->table = malloc(sizeof(void *) * positions);
	if (!p->table)
		return -1;

	for (i = 0; i < positions; i++)
		p->table[i] = NULL;

	p->positions = positions;
	p->gr_collision = -1;
	p->size = p->nr_collisions = p->no_collisions = 0;

	return 0;
}

static void ohtbl_destroy(struct ohtbl *p)
{
	int i;

	for (i = 0; i < p->positions; i++)
		if (p->table[i])
			free(p->table[i]);

	free(p->table);
}

static int ohtbl_lookup(struct ohtbl *p, char *name, unsigned int *h)
{
	int i;
	unsigned int hash;

	hash = tdb_hash(name) % p->positions;

	for (i = 0; i < p->positions; i++) {
		if (p->table[hash])
			if (!memcmp(p->table[hash], name, NAME_LENGTH)) {
				/* found */
				if (h)
					*h = hash;
				return 1;
			}

		hash = (hash + 1) % p->positions;
	}

	/* Not found */
	return 0;
}

static int ohtbl_insert(struct ohtbl *p, char *name)
{
	int ret, i;
	unsigned int hash;

	ret = ohtbl_lookup(p, name, NULL);
	if (ret) {
		/* key already exists */
		return 1;
	}

	hash = tdb_hash(name) % p->positions;

	for (i = 0; i < p->positions; i++) {
		if (!p->table[hash]) {
			p->table[hash] = (void *) name;
			p->size++;

			if (!i)
				p->no_collisions++;
			else if (i > p->gr_collision)
				p->gr_collision = i;

			return 0;
		}

		p->nr_collisions++;
		hash = (hash + 1) % p->positions;
	}

	/* Table is full */
	return -1;
}

static void ohtbl_show_stats(struct ohtbl *p)
{
	float lfactor, probes;

	lfactor = (float) p->size / p->positions;
	probes = 1 / (1 - lfactor);

	printf("\n-> Hash table statistics\n");
	printf("Table size:      %4d\n", p->positions);
	printf(" o free:         %4d\n", p->positions - p->size);
	printf(" o in use:       %4d\n", p->size);
	printf("Perfect hashing: %4d\n", p->no_collisions);
	printf("Collisions:      %4d\n", p->nr_collisions);
	printf(" o nr keys:      %4d\n", p->size - p->no_collisions);
	printf(" o greater:      %4d\n", p->gr_collision + 1);
	printf("Load factor:     %4.2f\n", lfactor);
	printf("P. to probe:     %4.2f\n\n", probes);
}

static int ohtbl_load_data_file(struct ohtbl *table, const char *fname)
{
	FILE *file;
	char line[24];

	file = fopen(fname, "r");
	if (!file)
		return -1;

	memset(line, 0, sizeof(line));

	while (fgets(line, sizeof(line), file) != NULL) {
		int err;
		char *p;

		if (line[0] == '#') {
			if (!strchr(line, '\n')) {
				int c;

				while ((c = getc(file)) != '\n')
					/* NOTHING */;
			}
			memset(line, 0, sizeof(line));
			continue;
		}

		p = strchr(line, '\n');
		if (p)
			*p = '\0';

		p = strdup(line);
		if (!p) {
			fclose(file);
			return -1;
		}

		err = ohtbl_insert(table, p);
		if (err) {
			free(p);
			if (err == -1) {
				fclose(file);
				return -1;
			}
			/* else key already exists */
		}
		memset(line, 0, sizeof(line));
	}

	fclose(file);
	return 0;
}

static int set_seed(void)
{
	int fd, ret;
	size_t bytes;
	unsigned int seed;

	fd = open("/dev/random", O_RDONLY);
	if (fd < 0)
		return -1;

	bytes = read(fd, &seed, sizeof(seed));
	if (bytes != sizeof(seed)) {
		ret = -1;
		goto out;
	}

	ret = 0;
	srand(seed);
	printf("\n-> Seed: %#X\n", seed);

out:
	close(fd);
	return ret;
}

static void lottery(char *result, size_t size)
{
	int num, i, j;
	int numbers[LOTTERY_MAX];

	memset(result, 0, size);
	memset(numbers, -1, sizeof(numbers));

	for (i = 0; i < LOTTERY_MAX; i++) {
		for (;;) {
			int found = 0;

			num = 1 + (int) (60.0 * (rand() / (RAND_MAX + 6.0)));

			for (j = 0; j < i; j++)
				if (numbers[j] == num) {
					found = 1;
					break;
				}

			if (!found) {
				numbers[i] = num;
				break;
			}
		}
	}

	snprintf(result, size, "%02d %02d %02d %02d %02d %02d",
		numbers[0], numbers[1], numbers[2],
		numbers[3], numbers[4], numbers[5]);
}

static void show_pid(void)
{
	printf("-> PID: %d\n\n", getpid());
}

int main(int argc, char *argv[])
{
	int ret;
	char result[24];
	unsigned int hash;
	struct ohtbl table;

	if (argc != 2) {
		printf("usage: %s < data-file >\n", argv[0]);
		exit(1);
	}

	setlinebuf(stdout);

	ret = ohtbl_init(&table, HASH_TABLE_SIZE);
	if (ret) {
		perror("ohtabl_init()");
		ret = 1;
		goto out;
	}

	ret = ohtbl_load_data_file(&table, argv[1]);
	if (ret) {
		perror("ohtbl_load_data_file()");
		ret = 1;
		goto out;
	}

	ohtbl_show_stats(&table);

	ret = set_seed();
	if (ret) {
		perror("set_seed()");
		ret = 1;
		goto out;
	}

	show_pid();

	signal(SIGUSR1, sig_handler);
	signal(SIGHUP, sig_handler);

	for (;;) {

		/*
		 * The following checks will probably make the program
		 * run slower, but I've implemented them because they're
		 * cool features (specially the reload table one).
		 * 
		 * If you want full speed you should consider measuring
		 * this thing and removing these features if needed.
		 */

		if (show_status) {
			printf("-> Ran %lu (* %lu) times with %lu matches\n",
			       nr_runs, ulong_max, nr_found);
			show_status = 0;
		}

		if (reload_table) {
			/*
			 * XXX: Reading all the file again isn't a smart
			 * way to do this, we could start reading from
			 * the last line read.
			 */
			ret = ohtbl_load_data_file(&table, argv[1]);
			if (ret) {
				perror("ohtbl_load_data_file()");
				fprintf(stderr, "Aborting reload\n");
			}
			ohtbl_show_stats(&table);
			reload_table = 0;
		}

		lottery(result, sizeof(result));

		ret = ohtbl_lookup(&table, result, &hash);
		if (ret) {
			printf("-> HIT in run %lu: [%d] %s\n",
			       nr_runs, hash, result);
			nr_found++;
		}

		if (++nr_runs == ULONG_MAX) {
			if (++ulong_max == ULONG_MAX) {
				printf("-> ulong_max blew up [%lu]\n",
				       ulong_max);
				ulong_max = 0;
			}
			nr_runs = 0;
		}
	}

out:
	ohtbl_destroy(&table);
	return ret;
}
