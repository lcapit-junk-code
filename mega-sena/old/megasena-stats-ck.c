#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>

struct cuckoo_array {
	void **array;
	unsigned int size;
};

struct cuckoo_tbl {
	int size;
	unsigned int positions;
	struct cuckoo_array first, second;
};

#define HASH_TABLE_SIZE  3072 	       /* 3k */
#define LOTTERY_MAX         6	       /* bytes */
#define NAME_LENGTH        17	       /* bytes */

static sig_atomic_t show_status, reload_table;
static unsigned long nr_runs, nr_found, ulong_max;

static void sig_handler(int signum)
{
	switch (signum) {
	case SIGUSR1:
		show_status = 1;
		break;
	case SIGHUP:
		reload_table = 1;
		break;
	}
}

static inline unsigned int tdb_hash(char *name)
{
	unsigned value;	/* Used to compute the hash value.  */
	unsigned   i;	/* Used to cycle through random values. */

	/* Set the initial value from the key size. */
	for (value = 0x238F13AF * strlen(name), i=0; name[i]; i++)
		value = (value + (((unsigned char *)name)[i] << (i*5 % 24)));

	return (1103515243 * value + 12345);
}

static inline unsigned int joaat_hash(char *name)
{
	int i;
	unsigned int hash = 0;

	for (i = 0; i < (int) strlen(name); i++) {
		hash += name[i];
		hash += (hash << 10);
		hash ^= (hash >> 6);
	}

	hash += (hash << 3);
	hash ^= (hash >> 11);
	hash += (hash << 15);

	return hash;
}

static int cuckoo_init(struct cuckoo_tbl *tbl, unsigned int positions)
{
	unsigned int i;

        tbl->first.array = malloc(sizeof(void *) * positions);
	if (!tbl->first.array)
		return -1;

        tbl->second.array = malloc(sizeof(void *) * positions);
	if (!tbl->second.array) {
		free(tbl->first.array);
		return -1;
	}

	for (i = 0; i < positions; i++)
		tbl->first.array[i] = tbl->second.array[i] = NULL;

	tbl->positions = positions;
	tbl->size = tbl->first.size = tbl->second.size = 0;

	return 0;
}

static void cuckoo_destroy(struct cuckoo_tbl *tbl)
{
	unsigned int i;

	for (i = 0; i < tbl->positions; i++) {
		if (tbl->first.array[i])
			free(tbl->first.array[i]);

		if (tbl->second.array[i])
			free(tbl->second.array[i]);
	}

	free(tbl->first.array);
	free(tbl->second.array);
}

static inline int cuckoo_lookup(struct cuckoo_tbl *tbl, char *name,
				unsigned int *h)
{
	unsigned int hash;

	hash = tdb_hash(name) % tbl->positions;
	if (tbl->first.array[hash] == NULL)
		return 0;

	if (!memcmp(tbl->first.array[hash], name, NAME_LENGTH))
		goto found;

	hash = joaat_hash(name) % tbl->positions;
	if (tbl->second.array[hash] == NULL)
		return 0;

	if (!memcmp(tbl->second.array[hash], name, NAME_LENGTH))
		goto found;

	return 0;

found:
	if (h)
		*h = hash;
	return 1;
}

static int cuckoo_insert(struct cuckoo_tbl *tbl, char *name)
{
	int ret;
	void *el, *tmp;
	unsigned int i, hash;

	ret = cuckoo_lookup(tbl, name, NULL);
	if (ret) {
		/* key already exists */
		return 1;
	}

	el = (void *) name;

	for (i = 0; i < tbl->positions; i++) {

		hash = tdb_hash(el) % tbl->positions;
		if (tbl->first.array[hash] == NULL) {
			tbl->first.array[hash] = el;
			tbl->first.size++;
			goto out;
		}

		tmp = tbl->first.array[hash];
		tbl->first.array[hash] = el;
		el = tmp;

		hash = joaat_hash(el) % tbl->positions;
		if (tbl->second.array[hash] == NULL) {
			tbl->second.array[hash] = el;
			tbl->second.size++;
			goto out;
		}

		tmp = tbl->second.array[hash];
		tbl->second.array[hash] = el;
		el = tmp;
	}

	fprintf(stderr, "Out of Max loop, aborting\n");
	abort();

out:
	tbl->size++;
	return 0;
}

#if 0
static struct cuckoo_tbl *rehash(struct cuckoo_tbl *tbl)
{
	int err;
	unsigned int i;
	struct cuckoo_tbl *new_table;

	new_table = malloc(sizeof(struct cuckoo_tbl));
	if (!new_table)
		return NULL;

	err = cuckoo_init(new_table, tbl->positions * 2);
	if (err)
		goto out_err;

	for (i = 0; i < tbl->positions; i++) {
		if (tbl->first.array[i]) {
			err = cuckoo_insert(new_table,
					    tbl->first.array[i]);
			if (err)
				goto out_err;
		}

		if (tbl->second.array[i]) {
			err = cuckoo_insert(new_table,
					    tbl->second.array[i]);
			if (err)
				goto out_err;
		}
	}

	cuckoo_destroy(tbl);
	free(tbl);

	return new_table;

out_err:
	free(new_table);
	return NULL;
}
#endif

static void cuckoo_print_stats(struct cuckoo_tbl *tbl)
{
	printf("\n-> Hash table statistics\n");
	printf("First table size:  %4d\n", tbl->positions);
	printf(" o in use:         %4d\n", tbl->first.size);
	printf(" o free:           %4d\n", tbl->positions - tbl->first.size);
	printf("Second table size: %4d\n", tbl->positions);
	printf(" o in use:         %4d\n", tbl->second.size);
	printf(" o free:           %4d\n", tbl->positions - tbl->second.size);
	printf("\n");
}

static int cuckoo_load_file(struct cuckoo_tbl *tbl, const char *fname)
{
	FILE *file;
	char line[24];

	file = fopen(fname, "r");
	if (!file)
		return -1;

	memset(line, 0, sizeof(line));

	while (fgets(line, sizeof(line), file) != NULL) {
		int err;
		char *p;

		if (line[0] == '#') {
			if (!strchr(line, '\n')) {
				int c;

				while ((c = getc(file)) != '\n')
					/* NOTHING */;
			}
			memset(line, 0, sizeof(line));
			continue;
		}

		p = strchr(line, '\n');
		if (p)
			*p = '\0';

		p = strdup(line);
		if (!p) {
			fclose(file);
			return -1;
		}

		err = cuckoo_insert(tbl, p);
		if (err) {
			free(p);
			if (err == -1) {
				fclose(file);
				return -1;
			}
			/* else key already exists */
		}
		memset(line, 0, sizeof(line));
	}

	fclose(file);
	return 0;
}

/* FIXME: duplicated from cuckoo_load_file() */
static int cuckoo_self_test(struct cuckoo_tbl *tbl, const char *fname)
{
	int found, ret;
	FILE *file;
	char line[24];

	file = fopen(fname, "r");
	if (!file)
		return -1;

	ret = 0;
	memset(line, 0, sizeof(line));

	while (fgets(line, sizeof(line), file) != NULL) {
		char *p;

		if (line[0] == '#') {
			if (!strchr(line, '\n')) {
				int c;

				while ((c = getc(file)) != '\n')
					/* NOTHING */;
			}
			memset(line, 0, sizeof(line));
			continue;
		}

		p = strchr(line, '\n');
		if (p)
			*p = '\0';

		found = cuckoo_lookup(tbl, line, NULL);
		if (!found) {
			printf("   Could not found: %s\n", line);
			ret = -1;
			break;
		}

		memset(line, 0, sizeof(line));
	}

	fclose(file);

	found = cuckoo_lookup(tbl, "aaaaaaaaaaaaaaaaa", NULL);
	if (found) {
		fprintf(stderr, "found unknow string\n");
		ret = 1;
	}

	return ret;
}

static int set_seed(void)
{
	int fd, ret;
	size_t bytes;
	unsigned int seed;

	fd = open("/dev/random", O_RDONLY);
	if (fd < 0)
		return -1;

	bytes = read(fd, &seed, sizeof(seed));
	if (bytes != sizeof(seed)) {
		ret = -1;
		goto out;
	}

	ret = 0;
	srand(seed);
	printf("\n-> Seed: %#X\n", seed);

out:
	close(fd);
	return ret;
}

static void lottery(char *result, size_t size)
{
	int num, i, j;
	int numbers[LOTTERY_MAX];

	memset(result, 0, size);
	memset(numbers, -1, sizeof(numbers));

	for (i = 0; i < LOTTERY_MAX; i++) {
		for (;;) {
			int found = 0;

			num = 1 + (int) (60.0 * (rand() / (RAND_MAX + 6.0)));

			for (j = 0; j < i; j++)
				if (numbers[j] == num) {
					found = 1;
					break;
				}

			if (!found) {
				numbers[i] = num;
				break;
			}
		}
	}

	snprintf(result, size, "%02d %02d %02d %02d %02d %02d",
		numbers[0], numbers[1], numbers[2],
		numbers[3], numbers[4], numbers[5]);
}

static void show_pid(void)
{
	printf("-> PID: %d\n\n", getpid());
}

int main(int argc, char *argv[])
{
	int stest, ret;
	unsigned int hash;
	struct cuckoo_tbl table;
	char result[24], *fname;

	stest = 0;

	if (argc == 2) {
		fname = argv[1];
	} else if (argc == 3) {
		fname = argv[1];
		stest = 1;
	} else {
		printf("usage: %s < data-file >\n", argv[0]);
		exit(1);
	}

	setlinebuf(stdout);

	ret = cuckoo_init(&table, HASH_TABLE_SIZE);
	if (ret) {
		perror("cuckoo_init()");
		ret = 1;
		goto out;
	}

	ret = cuckoo_load_file(&table, fname);
	if (ret) {
		perror("cuckoo_load_file()");
		ret = 1;
		goto out;
	}

	cuckoo_print_stats(&table);

	if (stest) {
		ret = cuckoo_self_test(&table, fname);
		printf("Performing self-test: ");
		ret ? printf("FAILED!\n") : printf("PASSED!\n");
		exit(0);
	}

	ret = set_seed();
	if (ret) {
		perror("set_seed()");
		ret = 1;
		goto out;
	}

	show_pid();

	signal(SIGUSR1, sig_handler);
	signal(SIGHUP, sig_handler);

	for (;;) {

		/*
		 * The following checks will probably make the program
		 * run slower, but I've implemented them because they're
		 * cool features (specially the reload table one).
		 * 
		 * If you want full speed you should consider measuring
		 * this thing and removing these features if needed.
		 */

		if (show_status) {
			printf("-> Ran %lu (* %lu) times with %lu matches\n",
			       nr_runs, ulong_max, nr_found);
			show_status = 0;
		}

		if (reload_table) {
			/*
			 * XXX: Reading all the file again isn't a smart
			 * way to do this, we could start reading from
			 * the last line read.
			 */
			ret = cuckoo_load_file(&table, argv[1]);
			if (ret) {
				perror("cuckoo_load_file()");
				fprintf(stderr, "Aborting reload\n");
			}
			cuckoo_print_stats(&table);
			reload_table = 0;
		}

		lottery(result, sizeof(result));

		ret = cuckoo_lookup(&table, result, &hash);
		if (ret) {
			printf("-> HIT in run %lu: [%d] %s\n",
			       nr_runs, hash, result);
			nr_found++;
		}

		if (++nr_runs == ULONG_MAX) {
			if (++ulong_max == ULONG_MAX) {
				printf("-> ulong_max blew up [%lu]\n",
				       ulong_max);
				ulong_max = 0;
			}
			nr_runs = 0;
		}
	}

out:
	cuckoo_destroy(&table);
	return ret;
}
