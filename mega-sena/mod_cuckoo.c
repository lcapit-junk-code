#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "module.h"
#include "mod_cuckoo.h"

struct cuckoo_array {
	void **array;
	unsigned int size;
};

struct cuckoo_tbl {
	int size;
	unsigned int positions;
	struct cuckoo_array first, second;
};

static struct cuckoo_tbl cuckoo_tbl;

#define HASH_TABLE_SIZE  3072 	       /* 3k */
#define LOTTERY_MAX         6	       /* bytes */
#define NAME_LENGTH        17	       /* bytes */

static inline unsigned int tdb_hash(char *name)
{
	unsigned value;	/* Used to compute the hash value.  */
	unsigned   i;	/* Used to cycle through random values. */

	/* Set the initial value from the key size. */
	for (value = 0x238F13AF * strlen(name), i=0; name[i]; i++)
		value = (value + (((unsigned char *)name)[i] << (i*5 % 24)));

	return (1103515243 * value + 12345);
}

static inline unsigned int joaat_hash(char *name)
{
	int i;
	unsigned int hash = 0;

	for (i = 0; i < (int) strlen(name); i++) {
		hash += name[i];
		hash += (hash << 10);
		hash ^= (hash >> 6);
	}

	hash += (hash << 3);
	hash ^= (hash >> 11);
	hash += (hash << 15);

	return hash;
}

static void cuckoo_print_stats(void)
{
	struct cuckoo_tbl *tbl = &cuckoo_tbl;

	printf("\n-> Hash table statistics\n");
	printf("First table size:  %4d\n", tbl->positions);
	printf(" o in use:         %4d\n", tbl->first.size);
	printf(" o free:           %4d\n", tbl->positions - tbl->first.size);
	printf("Second table size: %4d\n", tbl->positions);
	printf(" o in use:         %4d\n", tbl->second.size);
	printf(" o free:           %4d\n", tbl->positions - tbl->second.size);
	printf("\n");
}

static int cuckoo_lookup(char *name, unsigned int *h)
{
	unsigned int hash;

	hash = tdb_hash(name) % cuckoo_tbl.positions;
	if (cuckoo_tbl.first.array[hash] == NULL)
		return 0;

	if (!memcmp(cuckoo_tbl.first.array[hash], name, NAME_LENGTH))
		goto found;

	hash = joaat_hash(name) % cuckoo_tbl.positions;
	if (cuckoo_tbl.second.array[hash] == NULL)
		return 0;

	if (!memcmp(cuckoo_tbl.second.array[hash], name, NAME_LENGTH))
		goto found;

	return 0;

found:
	if (h)
		*h = hash;
	return 1;
}

static int cuckoo_insert(char *name)
{
	int ret;
	void *el, *tmp;
	unsigned int i, hash;

	ret = cuckoo_lookup(name, NULL);
	if (ret) {
		/* key already exists */
		return 1;
	}

	el = (void *) name;

	for (i = 0; i < cuckoo_tbl.positions; i++) {

		hash = tdb_hash(el) % cuckoo_tbl.positions;
		if (cuckoo_tbl.first.array[hash] == NULL) {
			cuckoo_tbl.first.array[hash] = el;
			cuckoo_tbl.first.size++;
			goto out;
		}

		tmp = cuckoo_tbl.first.array[hash];
		cuckoo_tbl.first.array[hash] = el;
		el = tmp;

		hash = joaat_hash(el) % cuckoo_tbl.positions;
		if (cuckoo_tbl.second.array[hash] == NULL) {
			cuckoo_tbl.second.array[hash] = el;
			cuckoo_tbl.second.size++;
			goto out;
		}

		tmp = cuckoo_tbl.second.array[hash];
		cuckoo_tbl.second.array[hash] = el;
		el = tmp;
	}

	fprintf(stderr, "Out of Max loop, aborting\n");
	abort();

out:
	cuckoo_tbl.size++;
	return 0;
}

#if 0
static struct cuckoo_tbl *rehash(struct cuckoo_tbl *tbl)
{
	int err;
	unsigned int i;
	struct cuckoo_tbl *new_table;

	new_table = malloc(sizeof(struct cuckoo_tbl));
	if (!new_table)
		return NULL;

	err = cuckoo_init(new_table, tbl->positions * 2);
	if (err)
		goto out_err;

	for (i = 0; i < tbl->positions; i++) {
		if (tbl->first.array[i]) {
			err = cuckoo_insert(new_table,
					    tbl->first.array[i]);
			if (err)
				goto out_err;
		}

		if (tbl->second.array[i]) {
			err = cuckoo_insert(new_table,
					    tbl->second.array[i]);
			if (err)
				goto out_err;
		}
	}

	cuckoo_destroy(tbl);
	free(tbl);

	return new_table;

out_err:
	free(new_table);
	return NULL;
}
#endif

static int cuckoo_init(void)
{
	int i;
	const int positions = HASH_TABLE_SIZE;

        cuckoo_tbl.first.array = malloc(sizeof(void *) * positions);
	if (!cuckoo_tbl.first.array)
		return -1;

        cuckoo_tbl.second.array = malloc(sizeof(void *) * positions);
	if (!cuckoo_tbl.second.array) {
		free(cuckoo_tbl.first.array);
		return -1;
	}

	for (i = 0; i < positions; i++)
		cuckoo_tbl.first.array[i] = cuckoo_tbl.second.array[i] = NULL;

	cuckoo_tbl.positions = positions;
	cuckoo_tbl.size = cuckoo_tbl.first.size = cuckoo_tbl.second.size = 0;

	return 0;
}

static void cuckoo_destroy(void)
{
	unsigned int i;

	for (i = 0; i < cuckoo_tbl.positions; i++) {
		if (cuckoo_tbl.first.array[i])
			free(cuckoo_tbl.first.array[i]);

		if (cuckoo_tbl.second.array[i])
			free(cuckoo_tbl.second.array[i]);
	}

	free(cuckoo_tbl.first.array);
	free(cuckoo_tbl.second.array);
}

struct module cuckoo_module = {
	.mod_name    = "Cuckoo hashing",
	.mod_init    = cuckoo_init,
	.mod_insert  = cuckoo_insert,
	.mod_lookup  = cuckoo_lookup,
	.mod_destroy = cuckoo_destroy,
	.mod_stats   = cuckoo_print_stats,
};
