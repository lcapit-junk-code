#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <signal.h>
#include <limits.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "module.h"
#include "mod_lp.h"
#include "mod_cuckoo.h"

#define LINE_LEN           24
#define LOTTERY_MAX         6	       /* bytes */

static struct module *hash_module;

static sig_atomic_t show_status, reload_table;
static unsigned long nr_runs, nr_found, ulong_max;

static void sig_handler(int signum)
{
	switch (signum) {
	case SIGUSR1:
		show_status = 1;
		break;
	case SIGHUP:
		reload_table = 1;
		break;
	}
}

/* fatal(): print error message and exit */
static void fatal(const char *err, ...)
{
	va_list params;

	fputs("ERROR: ", stderr);

	va_start(params, err);
	vfprintf(stderr, err, params);
	va_end(params);

	putc('\n', stderr);

	exit(1);
}

/* FIXME: duplicated from load_file() */
static int self_test(const char *path)
{
	FILE *file;
	int found, ret;
	char line[LINE_LEN];

	file = fopen(path, "r");
	if (!file)
		return -1;

	ret = 0;
	memset(line, 0, sizeof(line));

	while (fgets(line, sizeof(line), file) != NULL) {
		char *p;

		if (line[0] == '#') {
			if (!strchr(line, '\n')) {
				int c;

				while ((c = getc(file)) != '\n')
					/* NOTHING */;
			}
			memset(line, 0, sizeof(line));
			continue;
		}

		p = strchr(line, '\n');
		if (p)
			*p = '\0';

		found = hash_module->mod_lookup(line, NULL);
		if (!found) {
			printf("   Could not found: %s\n", line);
			ret = -1;
			break;
		}

		memset(line, 0, sizeof(line));
	}

	fclose(file);

	found = hash_module->mod_lookup("aaaaaaaaaaaaaaaaa", NULL);
	if (found) {
		fprintf(stderr, "found unknow string\n");
		ret = 1;
	}

	return ret;
}

/* load the data from the file into the hash table */
static int load_file(const char *path)
{
	FILE *file;
	char line[LINE_LEN];

	file = fopen(path, "r");
	if (!file)
		return -1;

	memset(line, 0, sizeof(line));

	while (fgets(line, sizeof(line), file) != NULL) {
		int err;
		char *p;

		if (line[0] == '#') {
			if (!strchr(line, '\n')) {
				int c;

				while ((c = getc(file)) != '\n')
					/* NOTHING */;
			}
			memset(line, 0, sizeof(line));
			continue;
		}

		p = strchr(line, '\n');
		if (p)
			*p = '\0';

		p = strdup(line);
		if (!p) {
			fclose(file);
			return -1;
		}

		err = hash_module->mod_insert(p);
		if (err) {
			free(p);
			if (err == -1) {
				fclose(file);
				return -1;
			}
			/* else key already exists */
		}

		memset(line, 0, sizeof(line));
	}

	fclose(file);
	return 0;
}

static int set_seed(void)
{
	int fd, ret;
	size_t bytes;
	unsigned int seed;

	fd = open("/dev/random", O_RDONLY);
	if (fd < 0)
		return -1;

	bytes = read(fd, &seed, sizeof(seed));
	if (bytes != sizeof(seed)) {
		ret = -1;
		goto out;
	}

	ret = 0;
	srand(seed);
	printf("\n-> Seed: %#X\n", seed);

out:
	close(fd);
	return ret;
}

static void lottery(char *result, size_t size)
{
	int num, i, j;
	int numbers[LOTTERY_MAX];

	memset(result, 0, size);
	memset(numbers, -1, sizeof(numbers));

	for (i = 0; i < LOTTERY_MAX; i++) {
		for (;;) {
			int found = 0;

			num = 1 + (int) (60.0 * (rand() / (RAND_MAX + 6.0)));

			for (j = 0; j < i; j++)
				if (numbers[j] == num) {
					found = 1;
					break;
				}

			if (!found) {
				numbers[i] = num;
				break;
			}
		}
	}

	snprintf(result, size, "%02d %02d %02d %02d %02d %02d",
		numbers[0], numbers[1], numbers[2],
		numbers[3], numbers[4], numbers[5]);
}

static void show_pid(void)
{
	printf("-> PID: %d\n\n", getpid());
}

/* usage(): print usage info */
static void usage(void)
{
	printf(
	       "usage: mega-sena [options] < -c | -l > < file >\n\n"
	       "  options:\n"
	       "            -h  this text\n"
	       "            -c  use cuckoo hashing\n"
	       "            -l  use linear probing (open addressing)\n"
	       "            -t  run self test for selected hashing algorithm\n"
	       "\n"
	       );
}

int main(int argc, char *argv[])
{
	unsigned int hash;
	int opt, run_stest, err;
	char result[LINE_LEN], *path;

	run_stest = 0;
	hash_module = NULL;

	while ((opt = getopt(argc, argv, "htcl")) != -1 ) {
		switch (opt) {
		case 'h':
			usage();
			exit(0);
		case 't':
			run_stest = 1;
			break;
		case 'l':
			hash_module = &lp_module;
			break;
		case 'c':
			hash_module = &cuckoo_module;
			break;
		default:
			usage();
			exit(1);
		}
	}

	if (optind >= argc)
		fatal("you have to specify a filename");

	if (!hash_module)
		fatal("you have to select a hash algorithm");

	path = argv[optind];
	setlinebuf(stdout);

	err = hash_module->mod_init();
	if (err) {
		perror("mod_init()");
		exit(1);
	}

	err = load_file(path);
	if (err) {
		perror("load_file()");
		err = 1;
		goto out;
	}

	hash_module->mod_stats();

	if (run_stest) {
		err = self_test(path);
		printf("Performing self-test: ");
		err ? printf("FAILED!\n") : printf("PASSED!\n");
		err = 0;
		goto out;
	}

	err = set_seed();
	if (err) {
		perror("set_seed()");
		goto out;
	}

	show_pid();

	signal(SIGUSR1, sig_handler);
	signal(SIGHUP, sig_handler);

	for (;;) {
		int found;

		/*
		 * The following checks will probably make the program
		 * run slower, but I've implemented them because they're
		 * cool features (specially the reload table one).
		 */

		if (show_status) {
			printf("-> Ran %lu (* %lu) times with %lu matches\n",
			       nr_runs, ulong_max, nr_found);
			show_status = 0;
		}

		if (reload_table) {
			/*
			 * XXX: Reading all the file again isn't a smart
			 * way to do this, we could start reading from
			 * the last line read.
			 */
			err = load_file(path);
			if (err) {
				perror("load_file()");
				fprintf(stderr, "Aborting...\n");
				goto out;
			}

			hash_module->mod_stats();
			reload_table = 0;
		}

		lottery(result, sizeof(result));

		found = hash_module->mod_lookup(result, &hash);
		if (found) {
			printf("-> HIT in run %lu: [%d] %s\n",
			       nr_runs, hash, result);
			nr_found++;
		}

		if (++nr_runs == ULONG_MAX) {
			if (++ulong_max == ULONG_MAX) {
				printf("-> ulong_max blew up [%lu]\n",
				       ulong_max);
				ulong_max = 0;
			}
			nr_runs = 0;
		}
	}

out:
	hash_module->mod_destroy();
	return err;
}
