"""Hash table classes.

Currently, two kinds of Open Addressed hash tables are available:

	o Double Hashing
	o Linear Probing

Please, consult each class documentation for more information
about them.
"""

class HashTableError(Exception):
	"""Base class for hash table exceptions."""
	pass

class TableFull(HashTableError):
	"""Raised when the hash table is full."""
	def __str__(self):
		return "<cannot insert, hash table is full>"

class _HashTable(dict):
	"""Base class for hash tables.

	Defines the methods and attributes used by any hash table
	implementation.
	"""
	def __init__(self, positions):
		"""Hash table constructor.

		Parameter 'positions' is the number of positions in the
		hash table, ie. its maximum size.
		"""
		self.positions = positions
		self.nr_collisions = 0
		self.gr_collision = -1
		self.no_collision = 0
		dict.__init__(self)

	def hash(self, name):
		"""Hash function from module-init-tools' depmod.

		Parameter 'name' is a string, whose hash coding will be
		returned.
		"""
		value = 0x238F13AF * len(name)

		for i in range(len(name)):
			value += (ord(name[i]) << (i*5 % 24))
		return (1103515243 * value + 12345)

	def size(self):
		"""Return hash table's current size."""
		return len(self)

	def show_stats(self):
		"""Print hash table's utilization info."""
		# XXX: Makes this more generic
		lfactor = self.size() / float(self.positions)
		probes = 1 / (1 - lfactor)
		print '-> Hash table statistics'
		print ' Table size:      %4d'   % self.positions
		print '  o free:         %4d'   % (self.positions -self.size())
		print '  o in use:       %4d'   % self.size()
		print ' Perfect hashing: %4d'   % self.no_collision
		print ' Collisions:      %4d'   % self.nr_collisions
		print '  o nr keys:      %4d'   % (self.size()-self.no_collision)
		print '  o greater:      %4d'   % (self.gr_collision + 1)
		print ' Load factor:     %4.2f' % lfactor
		print ' P. to probe:     %4.2f' % probes
		print ''

	def load_from_file(self, fname):
		"""Load data from file into the hash table.

		Parameter 'fname' is the file path.

		The file is assumed to have one data per line, a trailing
		new-line character is removed if present.

		The hash character (#) is considered a comment, therefore
		it's ignored.
		"""
		file = open(fname)
		for line in file:
			name = line.rstrip('\n')
			if name[0] == '#':
				continue
			self.insert(name)
		file.close()

class OpenAddressedDH(_HashTable):
	"""Open Addressed hash table (Double Hashing).

	Advantages:
		o Doesn't suffer of primary clustering

	Disadvantages:
		o Table's size must be prime
		o Bad cache performance is compared with Linear probing
	"""
	def hash1(self, key):
		return (key % self.positions)

	def hash2(self, key):
		hash = (key % (self.positions - 2)) + 1
		return hash

	def dhash(self, name):
		key = self.hash(name)
		hash1 = self.hash1(key)
		hash2 = self.hash2(key)
		return (hash1, hash2)

	def insert(self, name):
		"""Insert name into the hash table.

		If the table is full a TableFull exception will be raised.
		
		Complexity is O(1) if we approximate uniform hashing well.
		"""
		# Note: This function does some debug info gathering, for
		# full speed we could rename it to 'debug_insert()' and
		# create a new insert() method w/o that additional code

		hash1, hash2 = self.dhash(name)

		for i in range(self.positions):
			hash = (hash1 + i*hash2) % self.positions
			if not self.has_key(hash):
				self[hash] = name
				if i == 0:
					self.no_collision += 1
				elif i > self.gr_collision:
					self.gr_collision = i
				return
			self.nr_collisions += 1
		raise TableFull

	def lookup(self, name):
		"""Lookup name in the hash table.

		If name is in the hash table its hash coding is returned,
		otherwise this function returns -1.

		Complexity is O(1) if we approximate uniform hashing well.
		"""
		hash1, hash2 = self.dhash(name)

		for i in range(self.positions):
			hash = (hash1 + i*hash2) % self.positions
			if self.has_key(hash):
				if self[hash] == name:
					return hash
		return -1

class OpenAddressedLP(_HashTable):
	"""Open Addressed hash table (Linear Probing).

	Advantages:
		o Good cache performance
		o Implementation is simple
		o Doesn't require a prime number as the table's size

	Disadvantages:
		o Suffers of primary clustering (ie, create long sequences
		of filled slots)

	Use this when not suffering of primary clustering, otherwise use
	Double Hashing.
	"""
	def insert(self, name):
		"""Insert name into the hash table.

		If the table is full a TableFull exception will be raised.
		
		Complexity is O(1) if we approximate uniform hashing well.
		"""
		# Note: This function does some debug info gathering, for
		# full speed we could rename it to 'debug_insert()' and
		# create a new insert() method w/o that additional code

		hash = self.hash(name) % self.positions

		for i in range(self.positions):
			if not self.has_key(hash):
				self[hash] = name
				if i == 0:
					self.no_collision += 1
				elif i > self.gr_collision:
					self.gr_collision = i
				return
			self.nr_collisions += 1
			hash = (hash + 1) % self.positions
		raise TableFull

	def lookup(self, name):
		"""Lookup name in the hash table.

		If name is in the hash table its hash coding is returned,
		otherwise this function returns -1.

		Complexity is O(1) if we approximate uniform hashing well.
		"""

		hash = self.hash(name) % self.positions

		for i in range(self.positions):
			if self.has_key(hash):
				if self[hash] == name:
					return hash
			hash = (hash + 1) % self.positions
		return -1

class Chained(_HashTable):
	#
	# TODO:
	#	1. Review, was written in one shot
	#       2. Write the print_status() method
	#	3. Documentation
	def insert(self, name):

		hash = self.hash(name) % self.positions

		if not self.has_key(hash):
			self[hash] = []

		l = self.get(hash)
		l.append(name)

	def lookup(self, name):

		hash = self.hash(name) % self.positions

		if self.has_key(hash):
			l = self.get(hash)
			for i in l:
				if i == name:
					return (hash, i)
		return -1
