#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "module.h"
#include "mod_lp.h"

struct ohtbl {
	int positions;
	int size;
	void **table;

	/* debug stuff */
	int gr_collision;
	int nr_collisions;
	int no_collisions;
};

#define HASH_TABLE_SIZE 10240 	       /* 10k */
#define NAME_LENGTH        17	       /* bytes */

static struct ohtbl htable;

static inline unsigned int tdb_hash(char *name)
{
	unsigned value;	/* Used to compute the hash value.  */
	unsigned   i;	/* Used to cycle through random values. */

	/* Set the initial value from the key size. */
	for (value = 0x238F13AF * strlen(name), i=0; name[i]; i++)
		value = (value + (((unsigned char *)name)[i] << (i*5 % 24)));

	return (1103515243 * value + 12345);
}

static int ohtbl_init(void)
{
	int i;
	const int positions = HASH_TABLE_SIZE;

	htable.table = malloc(sizeof(void *) * positions);
	if (!htable.table)
		return -1;

	for (i = 0; i < positions; i++)
		htable.table[i] = NULL;

	htable.positions = positions;
	htable.gr_collision = -1;
	htable.size = htable.nr_collisions = htable.no_collisions = 0;

	return 0;
}

static void ohtbl_destroy(void)
{
	int i;

	for (i = 0; i < htable.positions; i++)
		if (htable.table[i])
			free(htable.table[i]);

	free(htable.table);
}

static int ohtbl_lookup(char *name, unsigned int *h)
{
	int i;
	unsigned int hash;

	hash = tdb_hash(name) % htable.positions;

	for (i = 0; i < htable.positions; i++) {
		if (htable.table[hash])
			if (!memcmp(htable.table[hash], name, NAME_LENGTH)) {
				/* found */
				if (h)
					*h = hash;
				return 1;
			}

		hash = (hash + 1) % htable.positions;
	}

	/* Not found */
	return 0;
}

static int ohtbl_insert(char *name)
{
	int ret, i;
	unsigned int hash;

	ret = ohtbl_lookup(name, NULL);
	if (ret) {
		/* key already exists */
		return 1;
	}

	hash = tdb_hash(name) % htable.positions;

	for (i = 0; i < htable.positions; i++) {
		if (!htable.table[hash]) {
			htable.table[hash] = (void *) name;
			htable.size++;

			if (!i)
				htable.no_collisions++;
			else if (i > htable.gr_collision)
				htable.gr_collision = i;

			return 0;
		}

		htable.nr_collisions++;
		hash = (hash + 1) % htable.positions;
	}

	/* Table is full */
	return -1;
}

static void ohtbl_stats(void)
{
	float lfactor, probes;
	struct ohtbl *p = &htable;

	lfactor = (float) p->size / p->positions;
	probes = 1 / (1 - lfactor);

	printf("\n-> Hash table statistics\n");
	printf("Table size:      %4d\n", p->positions);
	printf(" o free:         %4d\n", p->positions - p->size);
	printf(" o in use:       %4d\n", p->size);
	printf("Perfect hashing: %4d\n", p->no_collisions);
	printf("Collisions:      %4d\n", p->nr_collisions);
	printf(" o nr keys:      %4d\n", p->size - p->no_collisions);
	printf(" o greater:      %4d\n", p->gr_collision + 1);
	printf("Load factor:     %4.2f\n", lfactor);
	printf("P. to probe:     %4.2f\n\n", probes);
}

struct module lp_module = {
	.mod_name    = "Linear Probing hashing",
	.mod_init    = ohtbl_init,
	.mod_insert  = ohtbl_insert,
	.mod_lookup  = ohtbl_lookup,
	.mod_destroy = ohtbl_destroy,
	.mod_stats   = ohtbl_stats,
};
