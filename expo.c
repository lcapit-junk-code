/*
 * Exponentiation using recursion
 * 
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <stdlib.h>

double expr(int base, int n)
{
	if (!n)
		return 1;
	return (base * expr(base, n - 1));
}

int main(int argc, char *argv[])
{
	int base, expo;

	if (argc != 3) {
		printf("expo < base > < exponent>\n");
		exit(1);
	}

	base = atoi(argv[1]);
	expo = atoi(argv[2]);

	if (expo < 0) {
		fprintf(stderr, "error: the exponent must be positive\n");
		exit(1);
	}

	printf("%d exp %d = %.1f\n", base, expo, expr(base, expo));
	return 0;
}
