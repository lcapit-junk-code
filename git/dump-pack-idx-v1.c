/*
 * dump-pack-idx-v1: Dump the 'SHA1 offset' list from a .idx v1 file
 */
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <arpa/inet.h>

#define FAN_OUT_SIZE (4 * 256)
#define PACK_IDX_SIGNATURE 0xff744f63

struct pack_idx_header {
	uint32_t idx_signature;
	uint32_t idx_version;
};

static inline void hashcpy(unsigned char *sha_dst,
			   const unsigned char *sha_src)
{
	memcpy(sha_dst, sha_src, 20);
}

static char *sha1_to_hex(const unsigned char *sha1)
{
	static int bufno;
	static char hexbuffer[4][50];
	static const char hex[] = "0123456789abcdef";
	char *buffer = hexbuffer[3 & ++bufno], *buf = buffer;
	int i;

	for (i = 0; i < 20; i++) {
		unsigned int val = *sha1++;
		*buf++ = hex[val >> 4];
		*buf++ = hex[val & 0xf];
	}
	*buf = '\0';

	return buffer;
}

static void dump_index_v1(void *base, off_t size)
{
	unsigned char *list;
	unsigned long nr_sha1, i;

	list = base + FAN_OUT_SIZE;
	nr_sha1 = (size - (FAN_OUT_SIZE + 40)) / 24;
	for (i = 0; i < nr_sha1; i++) {
		unsigned char sha1[20];
		unsigned int offset;

		memcpy(&offset, list, sizeof(offset));
		hashcpy(sha1, list + 4);
		printf("%s %u\n", sha1_to_hex(sha1), ntohl(offset));
		list += 24;
	}
}

static void usage(void)
{
	printf("dump-pack-idx-v1 [ -f -v ] < pack-idx >\n");
}

int main(int argc, char *argv[])
{
	void *idx;
	char *idx_file;
	struct stat buf;
	struct pack_idx_header *hdr;
	int version, force, show_version, opt, err, fd;

	version = 1;
	show_version = force = 0;

	while ((opt = getopt(argc, argv, "vf")) != -1) {
		switch (opt) {
		case 'v':
			show_version = 1;
			break;
		case 'f':
			force = 1;
			break;
		default:
			usage();
			exit(1);
		}
	}

	if (optind >= argc) {
		usage();
		exit(1);
	}
	idx_file = argv[optind];

	if (!strstr(idx_file, ".idx") && !force) {
		fprintf(stderr,
			"ERROR: '%s' doesn't seem to be an .idx file, try '-f'"
			" if you know what you're doing\n", idx_file);
		exit(1);
	}

	fd = open(idx_file, O_RDONLY);
	if (fd < 0) {
		perror("open()");
		exit(1);
	}

	err = fstat(fd, &buf);
	if (err) {
		perror("fstat()");
		exit(1);
	}

	idx = mmap(NULL, buf.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	close(fd);
	if (idx == MAP_FAILED) {
		perror("mmap()");
		exit(1);
	}

	hdr = idx;
	if (hdr->idx_signature == htonl(PACK_IDX_SIGNATURE))
		version = ntohl(hdr->idx_version);

	if (show_version) {
		printf("version: %d\n", version);
		exit(0);
	}

	if (version != 1) {
		printf("Only dumps index v1, this is index v%d\n", version);
		exit(1);
	}

	dump_index_v1(idx, buf.st_size);

	munmap(idx, buf.st_size);
	return 0;
}
