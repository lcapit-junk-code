#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <cache.h>

#define PRINT_MAX 5

int main(int argc, char *argv[])
{
	void *buf;
	int err;
	unsigned long size;
	unsigned char sha1[20];

	if (argc != 3) {
		printf("commit SHA1 missing?");
		fprintf(stderr, "commit-dumper <repo> <commit>\n");
		exit(1);
	}

	err = chdir(argv[1]);
	if (err) {
		perror("chdir()");
		exit(1);
	}

	memset(sha1, 0, sizeof(sha1));
	get_sha1(argv[2], sha1);

	buf = read_object_with_reference(sha1, "commit", &size, NULL);
	if (!buf) {
		fprintf(stderr, "buf is NULL\n");
		exit(1);
	}

	write(1, buf, size);

	return 0;
}
