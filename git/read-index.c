#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <arpa/inet.h>

#ifndef FLEX_ARRAY
#if defined(__GNUC__) && (__GNUC__ < 3)
#define FLEX_ARRAY 0
#else
#define FLEX_ARRAY /* empty */
#endif
#endif

struct cache_header {
	unsigned int hdr_signature;
	unsigned int hdr_version;
	unsigned int hdr_entries;
};

struct cache_time {
	unsigned int sec;
	unsigned int nsec;
};

struct cache_entry {
	struct cache_time ce_ctime;
	struct cache_time ce_mtime;
	unsigned int ce_dev;
	unsigned int ce_ino;
	unsigned int ce_mode;
	unsigned int ce_uid;
	unsigned int ce_gid;
	unsigned int ce_size;
	unsigned char sha1[20];
	unsigned short ce_flags;
	char name[FLEX_ARRAY]; /* more */
};

#define CE_NAMEMASK  (0x0fff)

#define offsetof(type, member)  __builtin_offsetof (type, member)
#define cache_entry_size(len) ((offsetof(struct cache_entry,name) + (len) + 8) & ~7)
#define ce_namelen(ce) (CE_NAMEMASK & ntohs((ce)->ce_flags))
#define ce_size(ce) cache_entry_size(ce_namelen(ce))

int main(int argc, char *argv[])
{
	void *index;
	size_t offset;
	int i, err, fd;
	struct stat buf;
	unsigned int active_nr;
	struct cache_header *hdr;

	if (argc != 2) {
		printf("usage: %s: <index-file>\n", argv[0]);
		exit(1);
	}

	fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		perror("open()");
		exit(1);
	}

	err = fstat(fd, &buf);
	if (err) {
		perror("fstat()");
		exit(1);
	}

	index = mmap(NULL, buf.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (index == MAP_FAILED) {
		perror("mmap()");
		exit(1);
	}
	close(fd);

	hdr = index;
	active_nr = ntohl(hdr->hdr_entries);
	printf("Number of entries: %d\n\n", active_nr);
	printf("SHA1       file name\n");
	printf("----       ---------\n");
	offset = sizeof(*hdr);
	for (i = 0; i < active_nr; i++) {
		struct cache_entry *ce;

		ce = (struct cache_entry *) ((char *) index + offset);
		printf("%#x %s\n", ce->sha1, ce->name);

		offset = offset + ce_size(ce);
	}

	munmap(index, buf.st_size);
	return 0;
}
