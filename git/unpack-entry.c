#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <zlib.h>
#include <errno.h>
#include <sys/types.h>

static unsigned char stdin_next_byte(void)
{
	ssize_t ret;
	unsigned char b = 0;

	ret = read(0, &b, sizeof(b));
	if (ret != sizeof(b)) {
		fprintf(stderr, "read(): ");
		if (!ret)
			fprintf(stderr, "EOF\n");
		else if (ret < 0)
			fprintf(stderr, "%s\n", strerror(errno));
		else
			fprintf(stderr, "read %d bytes (1 expected)\b", ret);
		exit(1);
	}

	return b;
}

static int read_object_header(int *type, size_t *sizep)
{
	size_t size;
	unsigned shift;
	unsigned char c;

	c = stdin_next_byte();
	*type = (c >> 4) & 7;
	size = c & 15;
	shift = 4;
	while (c & 0x80) {
		if (sizeof(long) * 8 <= shift) {
			printf("don't know what happened\n");
			exit(1);
		}
		c = stdin_next_byte();
		size += (c & 0x7f) << shift;
		shift += 7;
	}

	*sizep = size;
	return 0;
}

static void show_object_header(int type, size_t size)
{
	printf("type: ");

	switch (type) {
	case 0:
		printf("None");
		break;
	case 1:
		printf("commit");
		break;
	case 2:
		printf("tree");
		break;
	case 3:
		printf("blob");
		break;
	case 4:
		printf("tag");
		break;
	case 6:
		printf("ofs delta");
		break;
	case 7:
		printf("ref delta");
		break;
	default:
		printf("unknown: %d", type);
		break;
	}
	printf(" {%d} size: %zd bytes\n", type, size);
}

static unsigned char *unpack_entry(size_t size)
{
	int st;
	z_stream stream;
	unsigned char *buf, c;

	buf = malloc(size + 1);
	if (!buf)
		return NULL;
	buf[size] = 0;

	memset(&stream, 0, sizeof(stream));
	stream.next_out = buf;
	stream.avail_out = size;

	inflateInit(&stream);
	do {
		c = stdin_next_byte();
		stream.next_in = &c;
		stream.avail_in = 1;
		st = inflate(&stream, Z_FINISH);
	} while (st == Z_OK || st == Z_BUF_ERROR);
	inflateEnd(&stream);

	if ((st != Z_STREAM_END) || stream.total_out != size) {
		free(buf);
		fprintf(stderr, "ERROR: zlib: %s\n", stream.msg);
		return NULL;
	}

	return buf;
}

int main(int argc, char *argv[])
{
	int type;
	size_t size;
	unsigned char *obj;
	unsigned long offset;

	if (argc != 2) {
		printf("unpack-pack-entry offset < packfile\n");
		exit(1);
	}

	/* go to where the object is stored */
	offset = strtoul(argv[1], (char **) NULL, 10);
	if (lseek(0, offset, SEEK_SET) < 0) {
		perror("lseek");
		exit(1);
	}

	/* header */
	read_object_header(&type, &size);
	show_object_header(type, size);

	if (type == 6 || type == 7) {
		fprintf(stderr, "(no support to unpack delta objects)\n");
		exit(1);
	}

	obj = unpack_entry(size);
	if (!obj)
		exit(1);

	/* show the object */
	fflush(stdout);
	write(1, obj, size);
	free(obj);

	return 0;
}
