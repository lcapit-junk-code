#include <cache.h>

int main(int argc, char *argv[])
{
	int err;
	char *hex;
	unsigned char sha1[20];

	if (argc != 2) {
		printf("usage: resolve-ref <ref>");
		exit(1);
	}

	err = get_sha1(argv[1], sha1);
	if (err) {
		perror("get_sha1()");
		exit(1);
	}

	hex = sha1_to_hex(sha1);
	if (!hex) {
		perror("sha1_to_hex()");
		exit(1);
	}

	printf("%s\n", hex);

	return 0;
}
