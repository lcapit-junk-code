#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <arpa/inet.h>

#define FAN_OUT_SIZE (4 * 256)
#define PACK_IDX_SIGNATURE 0xff744f63
#define PACK_SIGNATURE 0x5041434b	/* "PACK" */

struct pack_idx_header {
	uint32_t idx_signature;
	uint32_t idx_version;
};

struct pack_header {
	uint32_t hdr_signature;
	uint32_t hdr_version;
	uint32_t hdr_entries;
};

static char *sha1_to_hex(const unsigned char *sha1)
{
	static int bufno;
	static char hexbuffer[4][50];
	static const char hex[] = "0123456789abcdef";
	char *buffer = hexbuffer[3 & ++bufno], *buf = buffer;
	int i;

	for (i = 0; i < 20; i++) {
		unsigned int val = *sha1++;
		*buf++ = hex[val >> 4];
		*buf++ = hex[val & 0xf];
	}
	*buf = '\0';

	return buffer;
}

static int xread(int fd, void *buf, ssize_t size)
{
	ssize_t bytes;

	bytes = read(fd, buf, size);
	if (bytes != size)
		return -1;
	return 0;
}

static void idx_open(const char *file, int *fd)
{
	int version, err;
	struct pack_idx_header hdr;

	*fd = open(file, O_RDONLY);
	if (*fd < 0) {
		perror("open()");
		exit(1);
	}

	err = xread(*fd, &hdr, sizeof(hdr));
	if (err) {
		perror("read_stdin()");
		exit(1);
	}

	if (hdr.idx_signature != htonl(PACK_IDX_SIGNATURE)) {
		fprintf(stderr,
			"ERROR: '%s' is not an index file or is index v1\n",
			file);
		exit(1);
	}

	version = ntohl(hdr.idx_version);
	if (version != 3) {
		fprintf(stderr,
			"ERROR: must be index v3, but it's index v%d\n",
			version);
		exit(1);
	}
}

static void pck_open(const char *file, int *fd, size_t *nr_obj)
{
	int err;
	struct pack_header hdr;

	*fd = open(file, O_RDONLY);
	if (*fd < 0) {
		perror("open()");
		exit(1);
	}

	err = xread(*fd, &hdr, sizeof(hdr));
	if (err) {
		perror("read_stdin()");
		exit(1);
	}

	if (hdr.hdr_signature != htonl(PACK_SIGNATURE)) {
		fprintf(stderr, "file '%s' is not a GIT packfile\n", file);
		exit(1);
	}

	if (ntohl(hdr.hdr_version) != 4) {
		fprintf(stderr, "pack file is not v4 (it's %d)\n",
			htonl(hdr.hdr_version));
		exit(1);
	}

	*nr_obj = ntohl(hdr.hdr_entries);
}

static void usage(void)
{
	printf("dump-sha1-list < pack-idx > < pack-file >\n");
}

int main(int argc, char *argv[])
{
	size_t i, nr_obj;
	int idx_fd, pck_fd;
	char *idx_file, *pck_file;

	if (argc != 3) {
		usage();
		exit(1);
	}

	if (strstr(argv[1], ".idx")) {
		idx_file = argv[1];
		pck_file = argv[2];
	} else {
		pck_file = argv[1];
		idx_file = argv[2];
	}

	idx_open(idx_file, &idx_fd);
	pck_open(pck_file, &pck_fd, &nr_obj);

	/* skip the fan out and crc32 tables */
	if (lseek(idx_fd, FAN_OUT_SIZE + nr_obj * sizeof(uint32_t),
		  SEEK_CUR) < 0) {
		perror("lseek()");
		exit(1);
	}

	for (i = 0; i < nr_obj; i++) {
		unsigned char sha1[20];
		unsigned int offset;

		xread(pck_fd, sha1, 20);
		xread(idx_fd, &offset, sizeof(offset));
		printf("%s %u\n", sha1_to_hex(sha1), ntohl(offset));
	}

	return 0;
}
