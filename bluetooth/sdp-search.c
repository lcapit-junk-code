#include <stdio.h>
#include <stdlib.h>
#include <bluetooth/sdp.h>
#include <bluetooth/sdp_lib.h>
#include <bluetooth/bluetooth.h>

int main(void)
{
	int err;
	uuid_t svc_uuid;
	bdaddr_t target;
	sdp_session_t *session;
	uint32_t range;
	sdp_list_t *r, *response_list = NULL, *search_list, *attrid_list;

	/*
	 * First part, connect to the SDP server and get what we want
	 */

	str2ba("00:01:E3:59:A7:00", &target);

	/* Connect to the SDP server running on the remote machine */
	session = sdp_connect(BDADDR_ANY, &target, SDP_RETRY_IF_BUSY);
	if (!session) {
		perror("sdp_connect()");
		exit(1);
	}

	/* specify the UUID of the application we are searching for */
	sdp_uuid16_create(&svc_uuid, SERIAL_PORT_PROFILE_ID);
	search_list = sdp_list_append(NULL, &svc_uuid);

	/* specify that we want a list of all the matching applications attr */
	range = 0x0000ffff;
	attrid_list = sdp_list_append(NULL, &range);

	/* get a list of service records that have UUID */
	err = sdp_service_search_attr_req(session, search_list,
					  SDP_ATTR_REQ_RANGE,
					  attrid_list,
					  &response_list);
	if (err) {
		perror("sdp_service_search_attr_req()");
		exit(1);
	}

	/*
	 * Second part, parse the result
	 */

	/* go through each of the service records */
	for (r = response_list; r; r = r->next) {
		int ret;
		sdp_record_t *rec = (sdp_record_t *) r->data;
		sdp_list_t *proto_list;

		printf("found service record 0x%x\n", rec->handle);

		/* get a list of the protocol sequences */
		ret = sdp_get_access_protos(rec, &proto_list);
		if (!ret) {
			sdp_list_t *p = proto_list;

			/* go through each protocol sequence */
			for (; p; p = p->next) {
				sdp_list_t *pds = (sdp_list_t *)p->data;

				/* go through each protocol list of the protocol sequence */
				for (; pds; pds = pds->next) {
					sdp_data_t *d = (sdp_data_t *) pds->data;
					int proto = 0;

					/* check the protocol attributes */
					for (; d; d = d->next) {
						switch(d->dtd) {
						case SDP_UUID16:
						case SDP_UUID32:
						case SDP_UUID128:
							proto = sdp_uuid_to_proto(&d->val.uuid);
							break;
						case SDP_UINT8:
							if (proto == RFCOMM_UUID) {
								printf("rfcomm channel: ");
								printf("%d\n", d->val.int8);
							}
							break;
						}
					}
				}
				sdp_list_free((sdp_list_t *) p->data, NULL);
			}
			sdp_list_free(proto_list, NULL);
		}
		sdp_record_free(rec);
	}
	sdp_close(session);
	exit(0);
}
