#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>

int main(void)
{
	int err, sock;
	struct sockaddr_rc server;
	char data[] = "Hello, world!\n";

	sock = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
	if (sock < 0) {
		perror("Can't create socket");
		exit(1);
	}

	memset(&server, 0, sizeof(server));
	server.rc_family = AF_BLUETOOTH;
	str2ba("00:60:57:13:34:77", &server.rc_bdaddr);
	server.rc_channel = (uint8_t) 1;

	err = connect(sock, (struct sockaddr *) &server, sizeof(server));
	if (err < 0) {
		perror("Can't connect to the server");
		close(sock);
		exit(1);
	}

	printf("Connected! Sendind data!\n");
	write(sock, data, sizeof(data));

	close(sock);
	exit(0);
}
