#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>

int main(void)
{
	char addr[19];
	char name[248];
	inquiry_info *ii;
	int err, i, max_rsp, num_rsp;
	int dev_id, sock, len, flags;

	dev_id = hci_get_route(NULL);
	if (dev_id < 0) {
		perror("Error: Can't get route");
		exit(1);
	}

	sock = hci_open_dev(dev_id);
	if (sock < 0) {
		perror("Error: Can't open device");
		exit(1);
	}

	len = 5;
	max_rsp = 5;
	flags = IREQ_CACHE_FLUSH;

	ii = malloc(max_rsp * sizeof(inquiry_info));
	if (!ii) {
		perror("malloc()");
		exit(1);
	}

	num_rsp = hci_inquiry(dev_id, len, max_rsp, NULL, &ii, flags);
	if (num_rsp < 0) {
		perror("Can't inquiry devices");
		free(ii);
		exit(1);
	}

	for (i = 0; i < num_rsp; i++) {
		memset(name, '\0', sizeof(name));
		err = hci_read_remote_name(sock, &(ii + i)->bdaddr,
					   sizeof(name), name, 0);
		if (err < 0)
			strcpy(name, "[unknown]");

		ba2str(&(ii + i)->bdaddr, addr);
		printf("%s %s\n", addr, name);
	}

	free(ii);
	close(sock);

	exit(1);
}
