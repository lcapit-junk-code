#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>

int main(void)
{
	char buf[1024];
	struct sockaddr_rc server, client;
	int err, sock, csock, csize, bread;

	sock = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
	if (sock < 0) {
		perror("Can't create socket");
		exit(1);
	}

	memset(&server, 0, sizeof(server));
	server.rc_family = AF_BLUETOOTH;
	server.rc_bdaddr = *BDADDR_ANY;
	server.rc_channel = (uint8_t) 1;

	err = bind(sock, (struct sockaddr *) &server, sizeof(server));
	if (err) {
		perror("Can't bind socket");
		close(sock);
		exit(1);
	}

	listen(sock, 5);

	memset(&client, 0, sizeof(client));
	csize = sizeof(client);
	csock = accept(sock, (struct sockaddr *) &client, &csize);
	if (csock < 0) {
		perror("Can't accept connection");
		close(sock);
		exit(1);
	}

	ba2str(&client.rc_bdaddr, buf);
	printf("Accepted connection from %s\n", buf);

	bread = read(csock, buf, sizeof(buf));
	if (bread > 0)
		printf("Received [%s]\n", buf);

	close(csock);
	close(sock);

	exit(0);
}
