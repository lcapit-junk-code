#!/usr/bin/python
#
# Check if a number is prime (brute force)
#
# Luiz Fernando N. Capitulino
# <lcapitulino@gmail.com>

from sys import exit, stdout, argv

def is_prime(num):
	"Check if a number is prime (brute force)"
	if num < 2:
		return False

	for i in range(2, num):
		if not (num % i):
			return False
	return True

def main():
	if len(argv) != 2:
		stdout.write('Usage: prime < number >\n')
		exit(1)

	num = int(argv[1])
	if is_prime(num):
		print 'is prime'
		exit(1)
	else:
		print 'is not prime'
		exit(0)

if __name__ == "__main__":
	main()
