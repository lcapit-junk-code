#!/bin/bash
#
# Check if a number is prime (brute force)
#
# Luiz Fernando N. Capitulino
# <lcapitulino@gmail.com>

is_prime()
{
	local i
	local res
	local number=$1
	local max=$((${number} - 1))

	for ((i = 2; i < $max; i++)); do
		res=$(($number % $i))
		if [ $res -eq 0 ]; then
			return 0
		fi
	done

	return 1
}

if [ $# -ne 1 ]; then
	echo "usage: prime.sh < number >"
	exit 1
fi

is_prime $1
if [ $? -eq 1 ]; then
	echo is prime
	ret=1
else
	echo is not prime
	ret=0
fi

exit $ret
