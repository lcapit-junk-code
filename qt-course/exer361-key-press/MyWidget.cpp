#include <QLabel>
#include <QString>
#include <QKeyEvent>
#include <QPushButton>
#include <QVBoxLayout>

#include "MyWidget.h"

MyWidget::MyWidget(QWidget *parent)
: QWidget(parent)
{
	m_label = new QLabel("Hello, World!", this);
	m_button = new QPushButton("Press me", this);

	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->addWidget(m_label);
	layout->addWidget(m_button);

	saved = false;
}

MyWidget::~MyWidget()
{
}

void MyWidget::keyPressEvent(QKeyEvent *event)
{
	int y, x;
	const int incr_size = 4;

	y = this->size().width();
	x = this->size().height();

	if (!saved) {
		original_x = x;
		original_y = y;
		saved = true;
	}

	if (event->key() == Qt::Key_H)
		y += incr_size;
	if (event->key() == Qt::Key_V)
		x += incr_size;
	if (event->key() == Qt::Key_T) {
		x += incr_size;
		y += incr_size;
	}
	if (event->key() == Qt::Key_R) {
		x = original_x;
		y = original_y;
	}

	this->resize(y, x);
}
