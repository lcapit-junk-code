#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>

class QLabel;
class QPushButton;

class MyWidget : public QWidget
{
	Q_OBJECT;
public:
	MyWidget(QWidget *parent = 0);
	~MyWidget();
private:
	bool saved;
	int original_x, original_y;
	QLabel *m_label;
	QPushButton *m_button;
	void keyPressEvent(QKeyEvent *event);
};

#endif // MYWIDGET_H
