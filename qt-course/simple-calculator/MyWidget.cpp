#include <QString>
#include "MyWidget.h"

#define CONNECT_NUMBUTTON(button)                    \
	connect(button, SIGNAL(clicked(bool)), this, \
		SLOT(slot_numButtonClicked()))

#define CONNECT_OPERBUTTON(button)                     \
	connect(button, SIGNAL(clicked(bool)), this,   \
		SLOT(slot_operButtonClicked()))

#define DISABLE_BUTTON(button) button->setEnable(false)
#define ENABLE_BUTTON(button)  button->setEnable(true)

MyWidget::MyWidget(QWidget *parent)
: QWidget(parent)
{
	m_ui = new Ui::MyWidgetBase();
	m_ui->setupUi(this);

	CONNECT_NUMBUTTON(m_ui->m_button0);
	CONNECT_NUMBUTTON(m_ui->m_button1);
	CONNECT_NUMBUTTON(m_ui->m_button2);
	CONNECT_NUMBUTTON(m_ui->m_button3);
	CONNECT_NUMBUTTON(m_ui->m_button4);
	CONNECT_NUMBUTTON(m_ui->m_button5);
	CONNECT_NUMBUTTON(m_ui->m_button6);
	CONNECT_NUMBUTTON(m_ui->m_button7);
	CONNECT_NUMBUTTON(m_ui->m_button8);
	CONNECT_NUMBUTTON(m_ui->m_button9);

	CONNECT_OPERBUTTON(m_ui->m_buttonSum);
	CONNECT_OPERBUTTON(m_ui->m_buttonMult);
	CONNECT_OPERBUTTON(m_ui->m_buttonSub);
	CONNECT_OPERBUTTON(m_ui->m_buttonDiv);
	CONNECT_OPERBUTTON(m_ui->m_buttonRes);

	resetDisplay = true;
	buttonsEnabled(true);
}

MyWidget::~MyWidget()
{
}

void MyWidget::slot_numButtonClicked()
{
	QPushButton *btn = dynamic_cast<QPushButton *>(sender());
	if (!btn)
		return;

	QString text = m_ui->m_display->text();
	if (resetDisplay) {
		text = "";
		resetDisplay = false;
	}

	text += btn->text();
	m_ui->m_display->setText(text);
}

void MyWidget::buttonsEnabled(bool value)
{
	m_ui->m_buttonSum->setEnabled(value);
	m_ui->m_buttonMult->setEnabled(value);
	m_ui->m_buttonSub->setEnabled(value);
	m_ui->m_buttonDiv->setEnabled(value);
	m_ui->m_buttonRes->setEnabled(!value);
}

void MyWidget::slot_operButtonClicked()
{
	int result;
	QPushButton *btn = dynamic_cast<QPushButton *>(sender());
	if (!btn)
		return;

	resetDisplay = true;

	if (btn->text() == "=") {
		int lvalue = m_ui->m_display->text().toInt();
		if (m_oper == "+")
			result = m_number + lvalue;
		else if (m_oper == "-")
			result = m_number - lvalue;
		else if (m_oper == "*")
			result = m_number * lvalue;
		else // m_oper == "/"
			result = m_number / lvalue;
		m_ui->m_display->setText(QString::number(result));
		buttonsEnabled(true);
	} else {
		m_oper = btn->text();
		m_number = m_ui->m_display->text().toInt();
		buttonsEnabled(false);
	}
}
