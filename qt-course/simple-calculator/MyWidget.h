#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>
#include "ui_calculator.h"

class QString;

class MyWidget : public QWidget
{
	Q_OBJECT;
public:
	MyWidget(QWidget *parent = 0);
	~MyWidget();

private:
	void buttonsEnabled(bool);
	Ui::MyWidgetBase *m_ui;
	QString m_oper;
	int m_number;
	bool resetDisplay;

private slots:
	void slot_numButtonClicked();
	void slot_operButtonClicked();
};

#endif // MYWIDGET_H
