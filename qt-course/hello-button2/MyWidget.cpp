#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>

#include "MyWidget.h"

MyWidget::MyWidget(QWidget *parent)
: QWidget(parent)
{
	m_label = new QLabel("Hello, World!", this);
	m_button = new QPushButton("Press me", this);

	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->addWidget(m_label);
	layout->addWidget(m_button);

	connect(m_button, SIGNAL(clicked(bool)), this, SLOT(slotClicked()));
}

MyWidget::~MyWidget()
{
}

void MyWidget::slotClicked()
{
	if (m_label->text() == "Hello, World!")
		m_label->setText("Bye Bye World");
	else
		m_label->setText("Hello, World!");
}
