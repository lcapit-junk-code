#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>
#include "ui_untitled.h"

class QDate;

class MyWidget : public QWidget
{
	Q_OBJECT;
public:
	MyWidget(QWidget *parent = 0);
	~MyWidget();

private:
	Ui::MyWidgetBase *m_ui;

private slots:
	void slot_clicked(const QDate &date);
	void slot_changed(int value);
};

#endif // MYWIDGET_H
