#include <QDate>

#include "MyWidget.h"

MyWidget::MyWidget(QWidget *parent)
: QWidget(parent)
{
	m_ui = new Ui::MyWidgetBase();
	m_ui->setupUi(this);

	m_ui->m_hslider->setMinimum(1);
	m_ui->m_progress->setMinimum(1);

	connect(m_ui->m_calendar, SIGNAL(clicked(const QDate &)), this,
			SLOT(slot_clicked(const QDate &)));
	connect(m_ui->m_hslider, SIGNAL(valueChanged(int)), this,
			SLOT(slot_changed(int)));

	slot_clicked(QDate::currentDate());
}

MyWidget::~MyWidget()
{
}

void MyWidget::slot_clicked(const QDate &date)
{
	// slider
	m_ui->m_hslider->setMaximum(date.daysInMonth());
	m_ui->m_hslider->setValue(date.day());

	// progress bar
	m_ui->m_progress->setMaximum(date.daysInMonth());
	m_ui->m_progress->setValue(date.day());

	// label
	m_ui->m_label->setText(date.toString("dddd"));
}

void MyWidget::slot_changed(int value)
{
	QDate date;

	date = QDate::currentDate();

	date.setDate(date.year(), date.month(), value);
	m_ui->m_calendar->setSelectedDate(date);
	slot_clicked(date);
}
