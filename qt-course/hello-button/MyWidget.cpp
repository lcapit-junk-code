#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>

#include "MyWidget.h"

MyWidget::MyWidget(QWidget *parent)
: QWidget(parent)
{
	m_label = new QLabel("Hello, World!", this);
	m_button = new QPushButton("Press me", this);

	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->addWidget(m_label);
	layout->addWidget(m_button);
}

MyWidget::~MyWidget()
{
}
