#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>

class QLabel;
class QPushButton;

class MyWidget : public QWidget
{
	Q_OBJECT;
public:
	MyWidget(QWidget *parent = 0);
	~MyWidget();
private:
	QLabel *m_label;
	QPushButton *m_button;
	void mousePressEvent(QMouseEvent *event);
};

#endif // MYWIDGET_H
