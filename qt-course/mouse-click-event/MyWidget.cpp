#include <QLabel>
#include <QString>
#include <QMouseEvent>
#include <QPushButton>
#include <QVBoxLayout>

#include "MyWidget.h"

MyWidget::MyWidget(QWidget *parent)
: QWidget(parent)
{
	m_label = new QLabel("Hello, World!", this);
	m_button = new QPushButton("Press me", this);

	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->addWidget(m_label);
	layout->addWidget(m_button);
}

MyWidget::~MyWidget()
{
}

void MyWidget::mousePressEvent(QMouseEvent *event)
{
	QString text = "Clicked: " + QString::number(event->x()) +
		"," + QString::number(event->y());
	m_label->setText(text);
}
