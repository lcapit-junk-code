#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include "MyWidget.h"

MyWidget::MyWidget(QWidget *parent)
: QWidget(parent)
{
	QLabel *m_label;

	/* The labels */

	QVBoxLayout *labels = new QVBoxLayout();

	m_label = new QLabel("Binario", this);
	labels->addWidget(m_label);

	m_label = new QLabel("Octal", this);
	labels->addWidget(m_label);

	m_label = new QLabel("Decimal", this);
	labels->addWidget(m_label);

	m_label = new QLabel("Hexadecimal", this);
	labels->addWidget(m_label);

	/* QlineEdits */

	QVBoxLayout *lines = new QVBoxLayout();
	for (int i = 0; i < NR_LABELS; i++) {
		m_qlines[i] = new QLineEdit(this);
		lines->addWidget(m_qlines[i]);
		connect(m_qlines[i], SIGNAL(textEdited(const QString &)),
				this, SLOT(m_update_lines()));
	}

	/* Put them together */
	QHBoxLayout *main_layout = new QHBoxLayout(this);
	main_layout->addItem(labels);
	main_layout->addItem(lines);
}

MyWidget::~MyWidget()
{
}

void MyWidget::resetLines()
{
	for (int i = 0; i < NR_LABELS; i++)
		m_qlines[i]->setText("");
}

void MyWidget::m_update_lines()
{
	int base, i;

	for (i = 0; i < NR_LABELS; i++) {
		if (m_qlines[i]->isModified()) {
			if (m_qlines[i]->text() == "") {
				resetLines();
				return;
			}

			switch (i) {
			case 0:
				base = 2;
				break;
			case 1:
				base = 8;
				break;
			case 2:
				base = 10;
				break;
			case 3:
				base = 16;
				break;
			}
			break;
		}
	}

	int num = m_qlines[i]->text().toInt(0, base);
	m_qlines[0]->setText(QString::number(num, 2));
	m_qlines[1]->setText(QString::number(num, 8));
	m_qlines[2]->setText(QString::number(num, 10));
	m_qlines[3]->setText(QString::number(num, 16).toUpper());
}
