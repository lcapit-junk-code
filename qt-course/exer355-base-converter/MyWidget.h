#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>

#define NR_LABELS 4

class QLineEdit;
class QString;

class MyWidget : public QWidget
{
	Q_OBJECT;
public:
	MyWidget(QWidget *parent = 0);
	~MyWidget();
private:
	QLineEdit *m_qlines[NR_LABELS];
	QString *text;
	void resetLines();

private slots:
	void m_update_lines();
};

#endif // MYWIDGET_H
