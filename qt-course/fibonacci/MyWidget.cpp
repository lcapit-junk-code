#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>

#include "MyWidget.h"

MyWidget::MyWidget(QWidget *parent)
: QWidget(parent)
{
	m_label = new QLabel("-", this);
	m_button = new QPushButton("Calcular", this);

	QVBoxLayout *layout = new QVBoxLayout(this);
	layout->addWidget(m_label);
	layout->addWidget(m_button);

	m_label->setWordWrap(true);

	connect(m_button, SIGNAL(clicked(bool)), this, SLOT(slotClicked()));
}

MyWidget::~MyWidget()
{
}

void MyWidget::slotClicked()
{
	if (m_numbers.count() < 2) {
		m_numbers.append(1);
	} else {
		int last = m_numbers.count() - 1;
		m_numbers.append(m_numbers[last] + m_numbers[last - 1]);
	}

	QString texto;
	foreach(int num, m_numbers)
		texto += QString::number(num) + ", ";

	m_label->setText(texto);
}
