#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>
#include <QList>

class QLabel;
class QPushButton;

class MyWidget : public QWidget
{
	Q_OBJECT;
public:
	MyWidget(QWidget *parent = 0);
	~MyWidget();
private:
	QLabel *m_label;
	QPushButton *m_button;
	QList<int> m_numbers;

private slots:
	void slotClicked();
};

#endif // MYWIDGET_H
