#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include "MyWidget.h"

MyWidget::MyWidget(QWidget *parent)
: QWidget(parent)
{
	const int max = 10;

	QVBoxLayout *labels = new QVBoxLayout();
	for (int i = 0; i < max; i++) {
		m_label = new QLabel("TextLabel", this);
		labels->addWidget(m_label);
	}

	QVBoxLayout *buttons = new QVBoxLayout();
	for (int i = 0; i < max; i++) {
		m_button = new QPushButton("PushButton", this);
		buttons->addWidget(m_button);
	}

	QHBoxLayout *main_layout = new QHBoxLayout(this);
	main_layout->addItem(labels);
	main_layout->addItem(buttons);
}

MyWidget::~MyWidget()
{
}
