#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>
#include <QQueue>
#include "ui_containers.h"

class MyWidget : public QWidget
{
	Q_OBJECT;
public:
	MyWidget(QWidget *parent = 0);
	~MyWidget();

private:
	QQueue<int> *m_numbers;
	Ui::MyWidgetBase *m_ui;

private slots:
	void slot_addButtonClicked();
	void slot_invButtonClicked();
	void slot_clrButtonClicked();
};

#endif // MYWIDGET_H
