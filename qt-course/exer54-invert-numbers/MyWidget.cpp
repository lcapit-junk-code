#include <QString>
#include <QQueue>
#include <QStack>

#include "MyWidget.h"

MyWidget::MyWidget(QWidget *parent)
: QWidget(parent)
{
	m_ui = new Ui::MyWidgetBase();
	m_ui->setupUi(this);

	m_numbers = new QQueue<int>;

	connect(m_ui->m_addButton, SIGNAL(clicked(bool)), this,
			SLOT(slot_addButtonClicked()));

	connect(m_ui->m_invButton, SIGNAL(clicked(bool)), this,
			SLOT(slot_invButtonClicked()));

	connect(m_ui->m_clrButton, SIGNAL(clicked(bool)), this,
			SLOT(slot_clrButtonClicked()));
}

MyWidget::~MyWidget()
{
	delete m_numbers;
}

void MyWidget::slot_addButtonClicked()
{
	QString num = m_ui->m_lineEdit->text();
	if (num == "")
		return;

	m_numbers->append(num.toInt());

	QString text = m_ui->m_label->text();
	text += num + "  ";
	m_ui->m_label->setText(text);
	m_ui->m_lineEdit->setText("");
}

void MyWidget::slot_invButtonClicked()
{
	QStack<int> m_stack;

	if (m_numbers->isEmpty())
		return;

	int count = m_numbers->count();
	for (int i = 0; i < count; i++)
		m_stack.push(m_numbers->dequeue());

	QString text;
	for (int i = 0; i < count; i++)
		text += QString::number(m_stack.pop(), 10) + "  ";

	m_ui->m_label->setText(text);
}

void MyWidget::slot_clrButtonClicked()
{
	m_ui->m_label->setText("");
	m_ui->m_lineEdit->setText("");
	m_numbers->clear();
}
