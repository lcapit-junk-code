#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include "MyWidget.h"

MyWidget::MyWidget(QWidget *parent)
: QWidget(parent)
{
	// Lower part
	m_bclear = new QPushButton("Limpar", this);
	m_bcopy = new QPushButton("Copiar", this);

	QHBoxLayout *lower_part = new QHBoxLayout();
	lower_part->addWidget(m_bclear);
	lower_part->addWidget(m_bcopy);

	// Upper part
	m_label = new QLabel("Hello, World!", this);
	m_edit = new QLineEdit(this);

	QVBoxLayout *upper_part = new QVBoxLayout(this);
	upper_part->addWidget(m_label);
	upper_part->addWidget(m_edit);
	upper_part->addItem(lower_part);

	// Signals
	connect(m_bclear, SIGNAL(clicked(bool)), m_edit,
			SLOT(clear()));

	connect(m_bcopy, SIGNAL(clicked(bool)), this,
			SLOT(m_copy_text()));
}

MyWidget::~MyWidget()
{
}

void MyWidget::m_copy_text()
{
	m_label->setText(m_edit->displayText());
}
