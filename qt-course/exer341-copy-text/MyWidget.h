#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>

class QLabel;
class QPushButton;
class QLineEdit;

class MyWidget : public QWidget
{
	Q_OBJECT;
public:
	MyWidget(QWidget *parent = 0);
	~MyWidget();
private:
	QLabel *m_label;
	QLineEdit *m_edit;
	QPushButton *m_bclear;
	QPushButton *m_bcopy;

private slots:
	void m_copy_text();
};

#endif // MYWIDGET_H
