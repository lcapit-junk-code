#include <QPen>
#include <QBrush>
#include <QHBoxLayout>
#include <QGraphicsItem>

#include "MyWidget.h"

MyWidget::MyWidget(QWidget *parent)
: QWidget(parent)
{
	resize(640, 480);
	m_scene = new QGraphicsScene(this);
	m_view = new QGraphicsView(this);
	m_view2 = new QGraphicsView(this);

	QHBoxLayout *layout = new QHBoxLayout(this);
	layout->addWidget(m_view);
	layout->addWidget(m_view2);

	// add a red rectangle with black borders
	m_scene->addRect(10, 10, 100, 50, QPen(Qt::black), QBrush(Qt::red));

	// add a blue ellipse with black contour
	QGraphicsItem *item =
		m_scene->addEllipse(130, 80, 50, 20, QPen(Qt::black), QBrush(Qt::blue));
	item->setFlag(QGraphicsItem::ItemIsMovable, true);

	// add a green line to the scene
	m_scene->addLine(0, 0, 200, 200, QPen(Qt::green));

	m_view->setScene(m_scene);

	// applying transformations
	m_view2->rotate(90);
	m_view2->scale(0.5, 0.3);
	m_view2->setRenderHints(QPainter::Antialiasing);
	m_view2->setScene(m_scene);
}

MyWidget::~MyWidget()
{
}
