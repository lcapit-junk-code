#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>

class MyWidget : public QWidget
{
	Q_OBJECT;
public:
	MyWidget(QWidget *parent = 0);
	~MyWidget();
private:
	QGraphicsView *m_view;
	QGraphicsView *m_view2;
	QGraphicsScene *m_scene;
};

#endif // MYWIDGET_H
