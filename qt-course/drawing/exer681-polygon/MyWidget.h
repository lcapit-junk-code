#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QPoint>
#include <QWidget>
#include <QVector>
#include <QPaintEvent>
#include <QMouseEvent>

class MyWidget : public QWidget
{
	Q_OBJECT;
public:
	MyWidget(QWidget *parent = 0);
	~MyWidget();

private:
	QVector <QPoint> m_points;

protected:
	void paintEvent(QPaintEvent *event);
	void mousePressEvent(QMouseEvent *event);
};

#endif // MYWIDGET_H
