#include <QPainter>
#include <QPolygon>

#include "MyWidget.h"

MyWidget::MyWidget(QWidget *parent)
: QWidget(parent)
{
}

MyWidget::~MyWidget()
{
}

void MyWidget::paintEvent(QPaintEvent *event)
{
	QPainter p(this);

	if (m_points.size() == 1) {
		// draw only one Point
		p.drawPoint(m_points[0]);
		return;
	}

	// draw the Polygon
	QPolygon poly(m_points);
	p.setBrush(Qt::green);
	p.setRenderHints(QPainter::Antialiasing, true);
	p.drawPolygon(poly);
}

void MyWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
		m_points.append(event->pos());
	else if (event->button() == Qt::RightButton)
		m_points.clear();
	else
		return;

	update();
}
