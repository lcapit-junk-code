#include <QApplication>
#include <QLabel>

int main(int argc, char **argv)
{
	QApplication app(argc, argv);

	QLabel l("Hello, World!");
	l.show();

	return app.exec();
}
