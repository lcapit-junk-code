#include <iostream>

class Matematica
{
	public:
		int soma(int a, int b)
		{
			return a + b;
		}

		int soma(int a, int b, int c)
		{
			return a + b + c;
		}

		int subtrai(int a, int b)
		{
			return a - b;
		}

		int subtrai(int a, int b, int c)
		{
			return a - b - c;
		}

		int multiplica(int a, int b)
		{
			return a * b;
		}

		int multiplica(int a, int b, int c)
		{
			return a * b * c;
		}
};

int main(void)
{
	Matematica mat;

	return mat.soma(1, 2);
}
