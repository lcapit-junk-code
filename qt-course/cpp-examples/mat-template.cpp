#include <iostream>

template<class T>
class Matematica
{
	public:
		T soma (T a, T b)
		{
			return a + b;
		}

		T soma (T a, T b, T c)
		{
			return a + b + c;
		}

		T subtrai (T a, T b)
		{
			return a - b;
		}

		T subtrai (T a, T b, T c)
		{
			return a - b - c;
		}
};

int main(void)
{
	Matematica<int> mat;

	return mat.soma(10, 2);
}
