#include <iostream>

class Moradia
{
	private:
		int m_numero;
		int m_nr_apto;
		char *m_endereco;
		char *m_tipo;

	public:
		Moradia(char *end, int num, int nr, char *tipo)
		{
			m_endereco = end;
			m_numero = num;
			m_nr_apto = nr;
			m_tipo = tipo;
		}

		char *endereco(void)
		{
			return m_endereco;
		}

		char *tipo(void)
		{
			return m_tipo;
		}
};

class Casa : public Moradia
{
	private:
		int m_andares;
	public:
		Casa(char *end, int num, int nr, int andares)
		: Moradia(end, num, nr, "casa")
		{
			m_andares = andares;
		}
};

class Apartamento : public Moradia
{
	private:
		bool m_elevador;
	public:
		Apartamento(char *end, int num, int nr, bool elevador)
		: Moradia(end, num, nr, "apartamento")
		{
			m_elevador = elevador;
		}
};

int main(void)
{
	Casa c("rua foobar", 12, 2, 5);
	Apartamento d("rua blah", 500, 5, true);

	c.endereco();
	c.tipo();

	d.endereco();
	c.tipo();

	return 0;
}
