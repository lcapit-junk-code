/*
 * mshell.c: A modem shell program.
 *
 * (yet another small minicom-like program which will become bloat over the
 * time).
 *
 * TODO (to make bloat):
 *
 *         1- Would be cool to have support for internal commands
 *         (eg, to set new memory size);
 *
 *         2- read_response() sets the timeout using SIGALRM, would be
 *         simpler/better to use select() instead;
 *
 *         3- Would be good to catch some signals (eg, SIGINT).
 *
 *         4- The end of input at read_response() is too simple.
 *
 * Luiz Fernando N. Capitulino
 * <lcapitulino@gmail.com>
 */
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <readline/readline.h>
#include <readline/history.h>

#define MODEM_TIMEOUT     7 /* XXX: Random number */
#define DEFAULT_BUFFER 1024

static volatile sig_atomic_t timeout;

static void handler(int signum)
{
	signum = signum;
	timeout = 1;
	return;
}

/**
 * read_response - Reads a modem response
 *
 * @param fd      Modem's file-descriptor
 * @param buf     Buffer to store the response
 * @param buf_len 'buf' length
 *
 * @return 0 on success, -1 otherwise
 */
static int read_response(int fd, char *buf, size_t buf_len)
{
	char *p;
	size_t len;
	ssize_t rbytes;
	int err, ret = -1;
	struct sigaction new, old;

	if (fd < 0 || !buf || !buf_len) {
		errno = EINVAL;
		return -1;
	}

	new.sa_handler = handler;
	sigemptyset(&new.sa_mask);
	new.sa_flags = 0;

	timeout = 0;

	err = sigaction(SIGALRM, &new, &old);
	if (err)
		return -1;

	memset(buf, '\0', buf_len);
	p = buf;
	len = buf_len-1; /* Forces '\0' */

	for (;;) {
		alarm(MODEM_TIMEOUT);

		rbytes = read(fd, p, len);
		if (rbytes < 0) {
			if (timeout) {
				errno = ETIMEDOUT;
				timeout = 0;
			}
			goto out;
		}

		alarm(0);

		if (strstr(buf, "\r\nOK\r\n"))
			break;

		if (strstr(buf, "\r\nERROR\r\n"))
			break;

		p += rbytes;
		if (p >= (buf + buf_len -1)) {
			/* Out of bounds */
			errno = ENOMEM;
			goto out;
		}

		len -= rbytes;
	}

	ret = 0;

 out:
	sigaction(SIGALRM, &old, NULL);
	return ret;
}

/**
 * open_tty - Opens the modem port
 *
 * Note: All termios settings was taken from 'Advanced programming in the
 * Unix Environment' book p. 608.
 *
 * @param tty_port  modem's tty port absolute path
 *
 * @return file decriptor on success, -1 otherwise
 */
static int open_tty(const char *tty_port)
{
	struct termios term;
	int berrno, err, fd = -1;

	if (!tty_port) {
		errno = EINVAL;
		goto out;
	}

	fd = open(tty_port, O_RDWR | O_NOCTTY);
	if (fd < 0)
		goto out;

	err = tcgetattr(fd, &term);
	if (err)
		goto out;

	/*
	 * Input
	 *
	 * Ignore BREAK (XXX: why?).
	 * Enable XON/XOFF flow control.
	 * Strip Input to 7 bits (XXX: why?).
	 * Ignore input parity errors (XXX: why?).
	 * (Note: we are disabling the rest of the options)
	 */
	term.c_iflag = IGNBRK | IXOFF | IXOFF | ISTRIP | IGNPAR;

	/*
	 * Output
	 *
	 * turn off all output processing.
	 */
	term.c_oflag = 0;

	/*
	 * Control
	 *
	 * Resets baud rate
	 */
	term.c_cflag &= ~(CSIZE);

	/*
	 * Control
	 *
	 * Sets 8 bits character.
	 * Enable receiver.
	 * Hang up the modem when last processes closes it.
	 * Ignore modem control lines (it is attached).
	 */
	term.c_cflag |= (CS8 | CREAD | HUPCL | CLOCAL);

	/*
	 * Local
	 *
	 * Everything off: canonical mode, disables signal
	 * generation, disable ECHO.
	 */
	term.c_lflag = 0;

	/*
	 * Local
	 *
	 * 1 byte, no timer
	 */
	term.c_cc[VMIN] = 1;
	term.c_cc[VTIME] = 0;

	err = cfsetispeed(&term, B115200);
	if (err)
		goto out;

	err = cfsetospeed(&term, B115200);
	if (err)
		goto out;

	err = tcsetattr(fd, TCSANOW, &term);

 out:
	if (err && fd >= 0) {
		berrno = errno;
		close(fd);
		fd = -1;
		errno = berrno;
	}

	return fd;
}

/**
 * __send_command - Sends a command to the modem
 *
 * @param fd      Modem's file-descriptor
 * @param cmd     command to be sent
 * @param cmd_len 'cmd' length
 *
 * @return 0 on success, -1 otheriwse
 */
static int __send_command(int fd, char *cmd, size_t cmd_len)
{
	char *p;
	size_t len;
	ssize_t wbytes;

	if (fd < 0 || !cmd || !cmd_len) {
		errno = EINVAL;
		return -1;
	}

	p = cmd;
	len = cmd_len;

	while (len) {
		wbytes = write(fd, p, len);
		if (wbytes < 0)
			return -1;

		p += wbytes;
		len -= wbytes;
	}

	return 0;
}

/**
 * send_command - Sends a command to the modem
 *
 * @param fd  Modem's file-descriptor
 * @param cmd command to be sent
 *
 * @return 0 on success, -1 otheriwse
 */
static int send_command(int fd, char *cmd)
{
	int err;

	if (fd < 0 || !cmd) {
		errno = EINVAL;
		return -1;
	}

	err = __send_command(fd, cmd, strlen(cmd));
	if (err)
		return -1;

	err = __send_command(fd, "\r\n", 2);
	if (err)
		return -1;

	return 0;
}

static void usage(void)
{
	printf(
		"Usage: mshell < port_name > [ -s ] [ < file ]\n"
		);
}

int main(int argc, char *argv[])
{
	int err, fd;
	int silent = 0;
	char *cmd, buf[DEFAULT_BUFFER];

	if (argc < 2 || argc > 3) {
		fprintf(stderr, "Invalid number of parameters!\n");
		usage();
		exit(1);
	}

	if (argc == 3) {
		if (!strcmp(argv[1], "-s") || !strcmp(argv[2], "-s")) {
			silent = 1;
		} else {
			fprintf(stderr, "Error: option not recognized!\n");
			usage();
			exit(1);
		}
	}

	fd = open_tty(argv[1]);
	if (fd < 0) {
		perror("Error: cannot open modem's port");
		exit(1);
	}

	if (!silent) {
		printf("\nDefault settings:\n");
		printf("  response buffer %d bytes\n", DEFAULT_BUFFER);
		printf("  modem's timeout in %d seconds\n\n", MODEM_TIMEOUT);
	}

	for (;;) {
		cmd = readline("mshell> ");
		if (!cmd) {
			/* Probably EOF */
			break;
		}

		err = send_command(fd, cmd);
		if (err) {
			perror("Error: cannot send command");
			exit(1);
		}

		free(cmd);

		err = read_response(fd, buf, sizeof(buf));
		if (err)
			perror("Cannot read response");
		else
			printf("%s", buf);
	}

	printf("\n");
	exit(0);
}
